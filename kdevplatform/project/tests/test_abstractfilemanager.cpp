/*
    SPDX-FileCopyrightText: 2021 Jonathan L. Verner <jonathan.verner@matfyz.cz>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "test_abstractfilemanager.h"

#include <tests/autotestshell.h>
#include <tests/testcore.h>
#include <tests/testproject.h>

#include <project/projectmodel.h>
#include <project/abstractfilemanagerplugin.h>
#include <util/path.h>

#include <KJob>

#include <QCoreApplication>
#include <QDebug>
#include <QTemporaryDir>
#include <QTest>

using namespace KDevelop;

/**
 * A project filesystem hierarchy template
 *
 * (see @ref createHierarchy)
 */
QStringList BASE_HIERARCHY = {
    QStringLiteral("A"),
    QStringLiteral("B"),
    QStringLiteral("C"),
    QStringLiteral("1/"),
    QStringLiteral("1/1.A"),
    QStringLiteral("1/1.B"),
    QStringLiteral("1/1.C"),
    QStringLiteral("2/"),
    QStringLiteral("2/2.A"),
    QStringLiteral("2/2.B"),
    QStringLiteral("2/2.C"),
    QStringLiteral("2/1/"),
    QStringLiteral("2/1/2.1.A"),
    QStringLiteral("2/1/2.1.B"),
    QStringLiteral("2/1/2.1.C"),
};

/**
 * Returns all elements of the filesystem (template) hierarchy which
 * lie under @p subRoot (i.e. all paths which start with @p subRoot)
 */
QStringList subTree(const QStringList hierarchy, const QString& subRoot)
{
    QStringList ret;
    for (const auto& item: hierarchy) {
        if (item.startsWith(subRoot)) {
            ret << item;
        }
    }

    return ret;
}

/**
 * Converts the relative path @p subPath to an absolute path relative to @p root.
 * Moreover, it appends to each element in @p subPath the suffix @p nameSuffix.
 * E.g.  if @p nameSuffix is <tt>"projA"</tt>, @p root is the directory <tt>"/tmp/test"</tt>
 * and @p subPath is <tt>"2/sub/B"</tt> then the result will be
 * <tt>"/tmp/test/2-projA/sub-projA/B-projA"</tt>.
 */
QString absolutePath(QDir root, const QString subPath, const QString& nameSuffix)
{
    if (subPath.endsWith('/')) {
        return root.filePath(subPath.chopped(0).replace('/', nameSuffix + '/'));
    }
    return root.filePath(subPath.chopped(0).replace('/', nameSuffix + '/') + nameSuffix);
}

/**
 * Creates a FS hierarchy under @p root from the template @p fs. The hierarchy is defined
 * by a list of paths @p fs. A path ending with a '/' is considered to be a directory and
 * each must <b>must not</b> start with '/'. Files <b>must</b> come after their parent
 * directories and will be created so that they contain @p content.
 *
 * Each directory and file in the hierarchy will have an additional '-'+ @p nameSuffix added
 * to it. E.g., if @p nameSuffix is <tt>"projA"</tt>, then <tt>"2/sub/B"</tt> will be converted
 * to <tt>"2-projA/sub-projA/B-projA"</tt> first.
 *
 */
void createHierarchy(QDir root, const QStringList& fs, const QString& nameSuffix, const QString& content)
{
    QString suffix = '-' + nameSuffix;
    QByteArray cont = content.toUtf8();
    for (const auto& item: fs) {
        QString absPath = absolutePath(root, item, suffix);
        if (item.endsWith('/')) {
            QVERIFY(root.mkpath(absPath));
        } else {
            QFile file(absPath);
            QVERIFY(file.open(QIODevice::WriteOnly));
            QVERIFY(file.write(cont) == cont.length());
            file.close();
        }
    }
}

/**
 * Checks that @p subPath exists in the filesystem under the directory @p root.
 * Moreover, if @p subPath ends with '/', it checks that it is a directory; otherwise
 * it checks that it is a file @p subPath whose contents is @p content.
 *
 * @note: @p subPath is converted using @ref absolutePath with prefix "-" + @p nameSuffix
 */
bool checkPath(QDir root, const QString& subPath, const QString& nameSuffix, const QString& content = "")
{
    QString suffix = '-' + nameSuffix;
    QString absPath = absolutePath(root, subPath, suffix);
    if (subPath.endsWith('/')) {
        QDir dir(absPath);
        if (dir.exists())
            return true;
        return false;
    }
    QFile file(absPath);
    if (!file.exists())
        return false;
    file.open(QIODevice::ReadOnly);
    return (file.readAll() == content);
}

/**
 * Checks that @p subPath does <b>not</b> exists in the filesystem under the directory @p root.
 *
 * @note: @p subPath is converted using @ref absolutePath with prefix "-" + @p nameSuffix
 */
bool checkAbsent(const QDir& root, const QString& subPath, const QString& nameSuffix)
{
    QString suffix = '-' + nameSuffix;
    QFileInfo info(absolutePath(root, subPath, suffix));
    return !info.exists();
}

/**
 * A helper class for constructing an KDevelop::IProject project
 * out of a given filesystem hierarchy template and setting up an
 * AbstractFileManagerPlugin for it.
 *
 * The files will be created in a temporary directory and deleted
 * when the project is destroyed.
 */
class Project
{
public:
    Project(QString pName, TestCore* core, const QStringList& hierarchy) :
        name(pName)
        , dir(QTemporaryDir(name))
        , fileManager(new AbstractFileManagerPlugin({}, core)),
        project(new TestProject(Path(dir.path())))
    {
        // In the future, it might be nice to test with real projects
        // which will have their own file managers
        if (auto* testProject = dynamic_cast<TestProject*>(project)) {
            testProject->setFileManager(fileManager);
        }

        createHierarchy(dir.path(), hierarchy, name, name);
        project->reloadModel();
        auto root = fileManager->import(project);
        auto job = fileManager->createImportJob(root);
        QVERIFY(job->exec());
    };
    ~Project()
    {
        delete project;
        delete fileManager;
        QVERIFY(dir.remove());
    };

    QDir root() {return dir.path();}
    QString absPath(const QString& path_tpl) const
    {
        return absolutePath(dir.path(), path_tpl, "-" + name);
    }
    IndexedString absPathIS(const QString& path_tpl) const
    {
        return IndexedString {absPath(path_tpl)};
    }
    QList<ProjectBaseItem*> itemsFromPaths(const QStringList& paths) const
    {
        QList<ProjectBaseItem*> ret;
        for (const auto& path : paths) {
            ret += project->itemsForPath(absPathIS(path));
        }

        return ret;
    }

    QString name;
    QTemporaryDir dir;
    KDevelop::IProjectFileManager* fileManager;
    KDevelop::IProject* project;
};

void TestAbstractFileManager::initTestCase()
{
    AutoTestShell::init();
    core = TestCore::initialize(Core::NoUi);
}

void TestAbstractFileManager::cleanupTestCase()
{
    delete core;
    TestCore::shutdown();
}

void TestAbstractFileManager::testCrossProjectCopy()
{
    Project p1("p1", core, BASE_HIERARCHY);
    Project p2("p2", core, BASE_HIERARCHY);
    Project p3("p3", core, BASE_HIERARCHY);

    QStringList filesToCopy = {
        QStringLiteral("1/"),
        QStringLiteral("A"),
    };

    p2.fileManager->copyExternalFilesAndFolders(
        p1.itemsFromPaths(filesToCopy) + p3.itemsFromPaths(filesToCopy),
        p2.project->projectItem()
    );

    for (const auto& file: filesToCopy + subTree(BASE_HIERARCHY, "1/")) {
        QVERIFY(checkPath(p2.root(), file, p1.name, p1.name));
        QVERIFY(checkPath(p2.root(), file, p3.name, p3.name));
        QVERIFY(checkPath(p1.root(), file, p1.name, p1.name));
        QVERIFY(checkPath(p3.root(), file, p3.name, p3.name));
    }
}

void TestAbstractFileManager::testCrossProjectMove()
{
    Project p1("p1", core, BASE_HIERARCHY);
    Project p2("p2", core, BASE_HIERARCHY);
    Project p3("p3", core, BASE_HIERARCHY);

    QStringList filesToCopy = {
        QStringLiteral("1/"),
        QStringLiteral("A"),
    };

    p2.fileManager->moveExternalFilesAndFolders(
        p1.itemsFromPaths(filesToCopy) + p3.itemsFromPaths(filesToCopy),
        p2.project->projectItem()
    );

    for (const auto& file: filesToCopy + subTree(BASE_HIERARCHY, "1/")) {
        QVERIFY(checkPath(p2.root(), file, p1.name, p1.name));
        QVERIFY(checkPath(p2.root(), file, p3.name, p3.name));
        QVERIFY(checkAbsent(p1.root(), file, p1.name));
        QVERIFY(checkAbsent(p3.root(), file, p3.name));
    }
}

void TestAbstractFileManager::testCrossProjectMoveFailure()
{
    Project p1("p1", core, BASE_HIERARCHY);
    Project p2("p2", core, BASE_HIERARCHY);
    Project p3("p3", core, BASE_HIERARCHY);

    QStringList filesToCopy = {
        QStringLiteral("1/"),
        QStringLiteral("A"),
    };

    // Make file "A" in project p1 uncopyable, so that moving it should fail
    QFile::setPermissions(p1.absPath(QStringLiteral("A")), QFileDevice::Permissions());

    p2.fileManager->moveExternalFilesAndFolders(
        p1.itemsFromPaths(filesToCopy) + p3.itemsFromPaths(filesToCopy),
        p2.project->projectItem()
    );

    // Restore file "A" permissions, so that it can be checked
    QFile::setPermissions(p1.absPath(QStringLiteral("A")), QFileDevice::ReadOwner);

    // Check that the original files have been preserved
    for (const auto& file: filesToCopy + subTree(BASE_HIERARCHY, "1/")) {
        QVERIFY(checkPath(p1.root(), file, p1.name, p1.name));
        QVERIFY(checkPath(p3.root(), file, p3.name, p3.name));
    }
}

QTEST_MAIN(TestAbstractFileManager)
