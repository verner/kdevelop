/*
    SPDX-FileCopyrightText: 2021 Jonathan L. Verner <jonathan.verner@matfyz.cz>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef KDEVELOP_PROJECT_TEST_ABSTRACTFILEMANAGER
#define KDEVELOP_PROJECT_TEST_ABSTRACTFILEMANAGER

#include <QObject>

namespace KDevelop
{
class TestCore;
}

class TestAbstractFileManager : public QObject
{
    Q_OBJECT
private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void testCrossProjectCopy();
    void testCrossProjectMove();
    void testCrossProjectMoveFailure();
private:
    KDevelop::TestCore *core;
};


#endif // KDEVELOP_PROJECT_TEST_ABSTRACTFILEMANAGER

