/*
    SPDX-FileCopyrightText: 2010 Niko Sams <niko.sams@gmail.com>
    SPDX-FileCopyrightText: 2012 Milian Wolff <mail@milianw.de>
    SPDX-FileCopyrightText: 2021 Jonathan L. Verner <jonathan.verner@matfyz.cz>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef KDEVPLATFORM_TEST_PROJECT_H
#define KDEVPLATFORM_TEST_PROJECT_H

#include "testsexport.h"

#include <interfaces/iproject.h>
#include <serialization/indexedstring.h>
#include <shell/projectcontroller.h>
#include <util/path.h>

#include <QSet>

namespace KDevelop
{
/**
 * Dummy Project than can be used for Unit Tests.
 *
 * The following currently works
 *
 *  - FileSet methods are implemented.
 *  - the directory tree under project root is loaded  (and can be reloaded by calling @ref reloadModel)
 *    so that the methods files, itemsForPath, filesForPath and foldersForPath work
 *  - various plugins can be set so that the respective getter methods work (no other plugin integration
 *    is done)
 */
class KDEVPLATFORMTESTS_EXPORT TestProject
    : public IProject
{
    Q_OBJECT

public:
    /**
     * Constructs a TestProject and (recursively) loads all files/directories found
     * in the project directory @p url.
     */
    explicit TestProject ( const Path& url = Path(), QObject* parent = nullptr );
    ~TestProject() override;

    IProjectFileManager* projectFileManager() const override
    {
        return m_fileManager;
    }
    IBuildSystemManager* buildSystemManager() const override
    {
        return m_buildSystemManager;
    }
    IPlugin* managerPlugin() const override
    {
        return m_managerPlugin;
    }
    IPlugin* versionControlPlugin() const override
    {
        return m_versionControlPlugin;
    }

    /**
     * Use the methods below to set various plugins returned by the above
     * methods
     */
    void setFileManager ( IProjectFileManager* m )
    {
        m_fileManager = m;
    }
    void setBuildSystemManager ( IBuildSystemManager* m )
    {
        m_buildSystemManager = m;
    }
    void setManagerPlugin ( IPlugin* m )
    {
        m_managerPlugin = m;
    }
    void setVersionControlPlugin ( IPlugin* m )
    {
        m_versionControlPlugin = m;
    }



    ProjectFolderItem* projectItem() const override;
    QList<ProjectFileItem*> files() const;
    QList<ProjectBaseItem*> itemsForPath ( const IndexedString& absPath ) const override;
    QList<ProjectFileItem*> filesForPath ( const IndexedString& absPath ) const override;
    QList<ProjectFolderItem*> foldersForPath ( const IndexedString& absPath ) const override;

    Path projectFile() const override;
    void reloadModel() override
    {
        setPath ( m_path );
    }
    KSharedConfigPtr projectConfiguration() const override
    {
        return m_projectConfiguration;
    }

    /* FileSet methods */
    void addToFileSet ( ProjectFileItem* file ) override;
    void removeFromFileSet ( ProjectFileItem* file ) override;
    QSet<IndexedString> fileSet() const override
    {
        return m_fileSet;
    }


    Path path() const override;
    QString name() const override
    {
        return QStringLiteral ( "Test Project" );
    }
    bool inProject ( const IndexedString& path ) const override;

    /* Dummy methods */
    void setReloadJob ( KJob* ) override {}
    bool isReady() const override
    {
        return true;
    }
    void close() override {}

    /* Custom methods */
    void setPath ( const Path& path );
    void setProjectItem ( ProjectFolderItem* item );


private:
    IProjectFileManager* m_fileManager = nullptr;
    IBuildSystemManager* m_buildSystemManager = nullptr;
    IPlugin* m_managerPlugin = nullptr;
    IPlugin* m_versionControlPlugin = nullptr;


    QSet<IndexedString> m_fileSet;
    Path m_path;
    ProjectFolderItem* m_root = nullptr;
    KSharedConfigPtr m_projectConfiguration;
};

/**
 * ProjectController that can clear open projects. Useful in Unit Tests.
 */
class KDEVPLATFORMTESTS_EXPORT TestProjectController
    : public ProjectController
{
    Q_OBJECT

public:
    explicit TestProjectController ( Core* core ) : ProjectController ( core ) {}

public:
    using ProjectController::addProject;
    using ProjectController::takeProject;

    void initialize() override;
};
}
#endif // KDEVPLATFORM_TEST_PROJECT_H
