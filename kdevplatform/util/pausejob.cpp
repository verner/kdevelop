#include "pausejob.h"

using namespace KDevelop;

KDevelop::DelayedInitJob::DelayedInitJob(QObject* parent, KDevelop::DelayedInitJob::JobCreator creator)
    : KJob(parent)
    , m_jobCreator(creator)
{
    setCapabilities(KJob::Killable);
}

void connectJobSignals(KJob* source, KJob* target) {
    source->connect(source, &KJob::infoMessage, target, &KJob::infoMessage);
    source->connect(source, &KJob::warning, target, &KJob::warning);
    source->connect(source, &KJob::percentChanged, target, &KJob::percentChanged);
    source->connect(source, &KJob::processedAmountChanged, target, &KJob::processedAmountChanged);
    source->connect(source, &KJob::processedSize, target, &KJob::processedSize);
    source->connect(source, &KJob::speed, target, &KJob::speed);
    source->connect(source, &KJob::totalAmountChanged, target, &KJob::totalAmountChanged);
}

void KDevelop::DelayedInitJob::start()
{
    m_job = m_jobCreator();
    setCapabilities(m_job->capabilities());
    connect(m_job, &KJob::finished, this, &DelayedInitJob::handleFinished);
    connectJobSignals(m_job, this);
    m_job->start();
}

bool KDevelop::DelayedInitJob::doKill()
{
    if (m_job) {
        return m_job->kill();
    }
    return true;
}

KJob* KDevelop::DelayedInitJob::job()
{
    return m_job;
}

void KDevelop::DelayedInitJob::handleFinished()
{
    Q_ASSERT(m_job);
    if (m_job) {
        setError(m_job->error());
        setErrorText(m_job->errorText());
        emitResult();
    }
}

void PauseJob::start() {
    emit paused(this);
}

void PauseJob::finishPause()
{
    setError(0);
    emitResult();
}

void PauseJob::failPause(int error)
{
    setError(error);
    emitResult();
}

bool PauseJob::doKill()
{
    return true;
}

PauseOnErrorJob::PauseOnErrorJob(KJob* job)
: PauseJob(job->parent()),
m_job(job)
{
    setCapabilities(m_job->capabilities());
    connect(m_job, &KJob::finished, this, &PauseOnErrorJob::handleFinished);
    connectJobSignals(m_job, this);
}

bool PauseOnErrorJob::doKill()
{
    if (m_job) return m_job->kill();
    return true;
}

void PauseOnErrorJob::handleFinished()
{
    if (!m_job->error()) {
        setError(m_job->error());
        emitResult();
    } else {
        setError(m_job->error());
        setErrorText(m_job->errorText());
        emit paused(this);
    }
}

void PauseOnErrorJob::start()
{
    m_job->start();
}

KJob* KDevelop::PauseOnErrorJob::job()
{
    return m_job;
}
