/*
    SPDX-FileCopyrightText: 2021 Jonathan L. Verner <jonathan.verner@matfyz.cz>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef KDEVPLATFORM_UTIL_PAUSEJOB_H
#define KDEVPLATFORM_UTIL_PAUSEJOB_H

#include "utilexport.h"

#include <KJob>

#include <QDebug>

#include <functional>

namespace KDevelop {

/**
 * The DelayedInitJob class can be used to defer job creation to the time
 * when the job is actually started (e.g. when the information to construct
 * the job arguments is not available at the time the job is created)
 *
 * Currently, the following signals are forwarded from the wrapped job:
 *
 *     @ref KJob::infoMessage
 *     @ref KJob::warning
 *     @ref KJob::percentChanged
 *     @ref KJob::processedAmountChanged
 *     @ref KJob::processedSize
 *     @ref KJob::speed
 *     @ref KJob::totalAmountChangeds
 *
 */
class KDEVPLATFORMUTIL_EXPORT DelayedInitJob: public KJob
{
    Q_OBJECT
public:
    /** The signature of a function creating a job */
    typedef std::function<KJob*()> JobCreator;

    DelayedInitJob(QObject* parent, JobCreator creator);

    /**
     * Calls the jobCreator to create the actual job and calls
     * the job's start method
     */
    void start() override;

    /**
     * Returns the actual job, if its already created. Otherwise
     * returns a nullptr.
     */
    KJob* job();

protected:
    bool doKill() override;

private Q_SLOTS:
    void handleFinished();

private:
    JobCreator m_jobCreator;
    KJob* m_job = nullptr;
};

/**
 * A simple virtual job which can be used in a composite job to pause it.
 * When the job starts, instead of doing any processing, it emits
 * the @ref paused signal. When the @ref finishPause slot is called,
 * the job successfully finishes.
 */
class KDEVPLATFORMUTIL_EXPORT PauseJob: public KJob
{
    Q_OBJECT
public:
    PauseJob(QObject* parent): KJob(parent) {setCapabilities(KJob::Killable);}
    void start() override;

public Q_SLOTS:
    /**
     * Successfully finish the paused job.
     */
    void finishPause();

    /**
     * Unsuccessfully finish the paused job.
     */
    void failPause(int error);

Q_SIGNALS:
    void paused(KJob* job);


protected:
    bool doKill() override;
};

/**
 * The PauseOnErrorJob class can be used as a wrapper for another job which
 * emits a @ref paused signal, when the wrapped job fails, instead of failing.
 * The @ref finishPause slot can be used to successfully finish a failed job.
 *
 * Currently, the following signals are forwarded from the wrapped job:
 *
 *     @ref KJob::infoMessage
 *     @ref KJob::warning
 *     @ref KJob::percentChanged
 *     @ref KJob::processedAmountChanged
 *     @ref KJob::processedSize
 *     @ref KJob::speed
 *     @ref KJob::totalAmountChangeds
 *
 */
class KDEVPLATFORMUTIL_EXPORT PauseOnErrorJob: public PauseJob
{
    Q_OBJECT
public:
    PauseOnErrorJob(KJob* job);

    /**
     * Starts the wrapped job.
     */
    void start() override;

    /**
     * Returns the wrapped job.
     */
    KJob* job();

protected:
    bool doKill() override;

private Q_SLOTS:
    void handleFinished();

private:
    KJob* m_job = nullptr;
};

}



#endif // KDEVPLATFORM_UTIL_PAUSEJOB_H
