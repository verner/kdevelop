#include "cachedjob.h"

#include <interfaces/icore.h>
#include <interfaces/iruncontroller.h>


#include <QDebug>

#include <QHash>
#include <QMutex>
#include <QMutexLocker>

using namespace KDevelop;

static QMutex resultsCacheMutex;
static QHash<QString, QVariant> resultsCache;



CachedJob::CachedJob(VcsJob* job, const QString& cacheKey)
    : VcsJob(job->parent())
    , m_job(job)
    , m_key(cacheKey)
{
    setVerbosity(OutputJobVerbosity::NoOutput);
    setCapabilities(Killable);
    m_job->setParent(this);
}



void CachedJob::start()
{
    QMutexLocker lock(&resultsCacheMutex);
    if (!m_skipCache && resultsCache.contains(m_key)) {
        lock.unlock();
        emit resultsReady(this);
        emitResult();
    } else {
        lock.unlock();
        connect(m_job, &KJob::result, this, &CachedJob::cacheResult);
        setVerbosity(m_job->verbosity());
        m_job->start();
    }
}

QVariant CachedJob::fetchResults()
{
    QMutexLocker lock(&resultsCacheMutex);
    if (!m_skipCache && resultsCache.contains(m_key)) {
        return resultsCache[m_key];
    } else {
        lock.unlock();
        return m_job->fetchResults();
    }
}


void CachedJob::cacheResult(KJob* kjob)
{
    auto job = dynamic_cast<VcsJob*>(kjob);

    setError(job->error());
    setErrorText(job->errorText());

    if ( job->error() == 0 ) {
        if (!m_skipCache) {
            QMutexLocker lock(&resultsCacheMutex);
            resultsCache[m_key] = job->fetchResults();
        }
        emit resultsReady(this);
    }

    emitResult();
}


void KDevelop::CachedJob::setCacheKey(const QString& cacheKey)
{
    m_key = cacheKey;
}

VcsJob* CachedJob::job()
{
    return m_job;
}

IPlugin* CachedJob::vcsPlugin() const
{
    return m_job->vcsPlugin();
}

VcsJob::JobStatus CachedJob::status() const
{
    return m_job->status();
}

void KDevelop::CachedJob::setSkipCache(const bool skip)
{
    m_skipCache = skip;
}

bool CachedJob::doKill()
{
    if (m_skipCache) {
        return true;
    }
    return m_job->kill();
}
