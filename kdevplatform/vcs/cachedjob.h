/*
    SPDX-FileCopyrightText: 2022 Jonathan L. Verner <jonathan.verner@matfyz.cz>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef KDEVPLATFORMVCS_CACHEDJOB_H
#define KDEVPLATFORMVCSL_CACHEDJOB_H

#include "vcsexport.h"

#include "vcsjob.h"

#include <QVariant>

namespace KDevelop {

/**
 *  @class CachedJob can be used to cache the results of expensive jobs.
 *
 *  @note: Only "pure" jobs (i.e. jobs without side-effects which give
 *  the same results given the same inputs) can be safely cached.
 */
class KDEVPLATFORMVCS_EXPORT CachedJob: public VcsJob
{
    Q_OBJECT

public:

    /**
     * @p job ... the job to run to compute the results if they are not cached
     * @p cacheKey ... the key under which to store the results in the cache
     */
    CachedJob(VcsJob* job, const QString& cacheKey);

    /**
     * Sets the key under which results will be stored (or retrieved)
     *
     * @note: Only call this method <b>before</b> calling @ref start.
     */
    void setCacheKey(const QString& cacheKey);

    /**
     * This method sets the @p skipCache property. Setting it to true
     * will make the job bypass the cache and run the wrapped job to compute
     * the results.
     *
     * @note: Only call this method <b>before</b> calling @ref start.
     */
    void setSkipCache(const bool skip);

    /**
     * Starts the job.
     *
     * The implementation tests whether there is a cache entry. If so, it
     * immediately emits the resultsReady signal and calls emitResults.
     */
    virtual void start() override;

    /**
     * Returns the results (either from the cache, if available, or from
     * the job)
     *
     * @note: Only call this after the resultsReady signal is emitted.
     */
    QVariant fetchResults() override;

    /**
     * see @ref KJob::status
     */
    VcsJob::JobStatus status() const override;

    /**
     * see @ref VcsJob::vcsPlugin
     */
    IPlugin * vcsPlugin() const override;

    /**
     * Returns the wrapped job (i.e. the job that is used to compute the
     * results if they are not available in the cache).
     */
    VcsJob* job();

protected:

    virtual bool doKill() override;

private Q_SLOTS:

    /**
     * Fetches the results of @p job, stores them in the cache and
     * emits the resultsReady signal and calls emitResults.
     */
    void cacheResult(KJob* job);

private:
    VcsJob* m_job = nullptr; /**< The job to compute the results */
    QString m_key = {};      /**< The key under which to store the results in the cache */
    bool m_skipCache = false; /**< Whether to bypass the cache for this particular job instance */

};

}



#endif // KDEVPLATFORMVCSL_CACHEDJOB_H

