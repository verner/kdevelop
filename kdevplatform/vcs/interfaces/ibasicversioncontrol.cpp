/*
    SPDX-FileCopyrightText: 2011 Sergey Vidyuk <sir.vestnik@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "ibasicversioncontrol.h"

#include <interfaces/iplugin.h>
#include <vcs/vcsjob.h>

#include <KTextEdit>

/// Default empty implementation
void KDevelop::IBasicVersionControl::setupCommitMessageEditor(const QUrl&, KTextEdit* edit) const
{
    edit->setCheckSpellingEnabled(true);
}

KDevelop::VcsJob* KDevelop::IBasicVersionControl::ignore(const QUrl& repoLocation, const QString& pattern)
{
    const QUrl ignorePath = ignoreFile(repoLocation);
    if (ignorePath.isEmpty())
        return nullptr;
    return new KDevelop::FunctionVcsJob(dynamic_cast<KDevelop::IPlugin*>(this), [=](auto* job){
        QFile ignoreFile(ignorePath.toLocalFile());
        if (! ignoreFile.open(QIODevice::Append)) {
            job->setStatus(VcsJob::JobFailed);
            return QVariant();
        };
        QByteArray data = pattern.toUtf8()+"\n";
        if (ignoreFile.write(data) != data.length()) {
            job->setStatus(VcsJob::JobFailed);
        }
        return QVariant();
    });
}

KDevelop::VcsJob* KDevelop::IBasicVersionControl::diff(
    const QUrl& fileOrDirectory,
    const KDevelop::VcsRevision& rev,
    IBasicVersionControl::RecursionMode recursion)
{
    return diff(
        fileOrDirectory,
        KDevelop::VcsRevision::createSpecialRevision(KDevelop::VcsRevision::Previous),
        rev,
        recursion
    );
}

std::vector<KDevelop::IBasicVersionControl::ConflictPosition> KDevelop::IBasicVersionControl::findConflicts(const QUrl& fileURL)
{
    std::vector<ConflictPosition> ret;
    QFile file(fileURL.path());
    if (! file.open(QIODevice::ReadOnly)) {
        return ret;
    }
    auto content = file.readAll();
    ConflictPosition conflict;
    int lineNum = 0;
    for(const auto& ln: content.split('\n')) {
        if( ln.startsWith("<<<<<<<") ) {
            conflict.startMarker = lineNum;
        } else if (ln.startsWith("=======")) {
            if (conflict.startMarker > 0) {
                conflict.middleMarker = lineNum;
            }
        } else if (ln.startsWith(">>>>>>>")) {
            if (conflict.middleMarker > 0) {
                ret.push_back(conflict);
                conflict = {-1,-1,-1};
            }
        }
        lineNum++;
    }
    return ret;
}
