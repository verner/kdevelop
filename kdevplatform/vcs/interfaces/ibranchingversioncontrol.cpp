#include "ibranchingversioncontrol.h"

#include <vcs/vcsjob.h>

#include <QVariant>

using namespace KDevelop;

QString IBranchingVersionControl::currentBranchCached(const Path& repository)
{

    auto it = m_currentBranchCache.find(repository);
    if (it != m_currentBranchCache.end()) {
        return it.value();
    }

    auto job = currentBranch(repository.toUrl());
    job->setVerbosity(OutputJob::Silent);
    job->exec();
    QString branch;
    if(job->exec() && job->status()==VcsJob::JobSucceeded) {
        auto res = job->fetchResults();
        branch = res.value<QString>();
        updateCurrentBranchCache(repository, branch);
    }
    delete job;
    return branch;
}

void IBranchingVersionControl::updateCurrentBranchCache(const Path& repo, const QString& branch)
{
    m_currentBranchCache[repo] = branch;
}
