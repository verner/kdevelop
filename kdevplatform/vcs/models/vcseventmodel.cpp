/*
    SPDX-FileCopyrightText: 2007 Andreas Pakulat <apaku@gmx.de>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "vcseventmodel.h"

#include <QModelIndex>
#include <QVariant>
#include <QDateTime>
#include <QList>
#include <QLocale>
#include <QMimeDatabase>
#include <QMimeData>
#include <QDebug>
#include <QDir>

#include <KLocalizedString>

#include "../vcsevent.h"
#include "../vcsrevision.h"
#include <vcsjob.h>
#include <interfaces/ibasicversioncontrol.h>
#include <interfaces/ibranchingversioncontrol.h>
#include <interfaces/icore.h>
#include <interfaces/iruncontroller.h>
#include <interfaces/iplugin.h>
#include <interfaces/iprojectcontroller.h>
#include <util/path.h>

namespace KDevelop
{


class VcsBasicEventModelPrivate
{
public:
    QList<KDevelop::VcsEvent> m_events;
};

VcsBasicEventModel::VcsBasicEventModel(QObject* parent)
    : QAbstractTableModel(parent)
    , d_ptr(new VcsBasicEventModelPrivate)
{
}

VcsBasicEventModel::~VcsBasicEventModel() = default;

int VcsBasicEventModel::rowCount(const QModelIndex& parent) const
{
    Q_D(const VcsBasicEventModel);

    return parent.isValid() ? 0 : d->m_events.count();
}

int VcsBasicEventModel::columnCount(const QModelIndex& parent) const
{
    return parent.isValid() ? 0 : ColumnCount;
}

QVariant VcsBasicEventModel::data(const QModelIndex& idx, int role) const
{
    Q_D(const VcsBasicEventModel);

    if( !idx.isValid() || role != Qt::DisplayRole )
        return QVariant();

    if( idx.row() < 0 || idx.row() >= rowCount() || idx.column() < 0 || idx.column() >= columnCount() )
        return QVariant();

    KDevelop::VcsEvent ev = d->m_events.at( idx.row() );
    switch( idx.column() )
    {
        case RevisionColumn:
            return QVariant( ev.revision().revisionValue() );
        case SummaryColumn:
            // show the first line only
            return QVariant( ev.message().section(QLatin1Char('\n'), 0, 0) );
        case AuthorColumn:
            return QVariant( ev.author() );
        case DateColumn:
            return QVariant( QLocale().toString( ev.date() ) );
        default:
            break;
    }
    return QVariant();
}

QVariant VcsBasicEventModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if( section < 0 || section >= columnCount() || orientation != Qt::Horizontal || role != Qt::DisplayRole )
        return QVariant();
    switch( section )
    {
        case RevisionColumn:
            return QVariant( i18nc("@title:column", "Revision") );
        case SummaryColumn:
            return QVariant( i18nc("@title:column", "Message") );
        case AuthorColumn:
            return QVariant( i18nc("@title:column", "Author") );
        case DateColumn:
            return QVariant( i18nc("@title:column", "Date") );
        default:
            break;
    }
    return QVariant();
}

void VcsBasicEventModel::addEvents(const QList<KDevelop::VcsEvent>& list)
{
    Q_D(VcsBasicEventModel);

    if( list.isEmpty() )
        return;

    beginInsertRows( QModelIndex(), rowCount(), rowCount()+list.count()-1 );
    d->m_events += list;
    endInsertRows();
}

KDevelop::VcsEvent VcsBasicEventModel::eventForIndex(const QModelIndex& idx) const
{
    Q_D(const VcsBasicEventModel);

    if( !idx.isValid() || idx.row() < 0 || idx.row() >= rowCount() )
    {
        return KDevelop::VcsEvent();
    }
    return d->m_events.at( idx.row() );
}

class VcsEventLogModelPrivate
{
public:
    KDevelop::IBasicVersionControl* m_iface;
    VcsRevision m_rev;
    QUrl m_url;
    bool done;
    bool fetching;
};

VcsEventLogModel::VcsEventLogModel(KDevelop::IBasicVersionControl* iface, const VcsRevision& rev, const QUrl& url, QObject* parent)
    : KDevelop::VcsBasicEventModel(parent)
    , d_ptr(new VcsEventLogModelPrivate)
{
    Q_D(VcsEventLogModel);

    d->m_iface = iface;
    d->m_rev = rev;
    d->m_url = url;
    d->done = false;
    d->fetching = false;
}

VcsEventLogModel::~VcsEventLogModel() = default;

bool VcsEventLogModel::canFetchMore(const QModelIndex& parent) const
{
    Q_D(const VcsEventLogModel);

    return !d->done && !d->fetching && !parent.isValid();
}

void VcsEventLogModel::fetchMore(const QModelIndex& parent)
{
    Q_D(VcsEventLogModel);

    d->fetching = true;
    Q_ASSERT(!parent.isValid());
    Q_UNUSED(parent);
    VcsJob* job = d->m_iface->log(d->m_url, d->m_rev, qMax(rowCount(), 100));
    connect(this, &VcsEventLogModel::destroyed, job, [job] { job->kill(); });
    connect(job, &VcsJob::finished, this, &VcsEventLogModel::jobReceivedResults);
    ICore::self()->runController()->registerJob( job );
}

void VcsEventLogModel::jobReceivedResults(KJob* job)
{
    Q_D(VcsEventLogModel);

    const QList<QVariant> l = qobject_cast<KDevelop::VcsJob *>(job)->fetchResults().toList();
    if(l.isEmpty() || job->error()!=0) {
        d->done = true;
        return;
    }
    QList<KDevelop::VcsEvent> newevents;
    for (const QVariant& v : l) {
        if( v.canConvert<KDevelop::VcsEvent>() )
        {
            newevents << v.value<KDevelop::VcsEvent>();
        }
    }
    d->m_rev = newevents.last().revision();
    if (rowCount()) {
        newevents.removeFirst();
    }
    d->done = newevents.isEmpty();
    addEvents( newevents );
    d->fetching = false;
}

class VcsEventLogTreeModelPrivate
{
public:
    KDevelop::IBasicVersionControl* m_vcs;
    VcsRevision m_initialRev, m_lastLoadedRev;
    QUrl m_url;
    bool m_ignoreReloadRequests = false;
    bool done = false;
    bool fetching = false;
    VcsJob* fetchJob = nullptr;
    std::unordered_map<KDevelop::VcsRevision, QPersistentModelIndex> rev2index;
};

VcsEventLogTreeModel::VcsEventLogTreeModel(
        KDevelop::IBasicVersionControl* vcs,
        const QUrl& url,
        const KDevelop::VcsRevision& rev,
        QObject* parent
):
    QStandardItemModel(parent)
    , d_ptr(new VcsEventLogTreeModelPrivate)
{
    Q_D(VcsEventLogTreeModel);

    d->m_vcs = vcs;
    d->m_initialRev = d->m_lastLoadedRev = rev;
    d->m_url = url;

    if (auto* bvcs = dynamic_cast<KDevelop::IBranchingVersionControl*>(vcs)) {
        if (auto* plugin = dynamic_cast<KDevelop::IPlugin*>(bvcs)) {
            // can't use new signal slot syntax here, IBranchingVersionControl is not a QObject
            connect(
                plugin,
                SIGNAL(repositoryBranchChanged(QUrl)),
                this,
                SLOT(reloadRequest())
            );
        }
    }

    connect(this, &VcsEventLogTreeModel::receivedDiff, this, &VcsEventLogTreeModel::cacheDiff);
}

void VcsEventLogTreeModel::reloadRequest()
{
    Q_D(VcsEventLogTreeModel);

    if (d->m_ignoreReloadRequests) {
        return;
    }

    clear();
    d->rev2index.clear();

    // Reset the maximum number of events to load back to default.
    // The reasoning is that a user only wants to load more events
    // when he is investigating something (e.g. searching for a commit);
    // when he reloads the view, we assume he is not investigating
    // anymore so there is no reason to load a possibly huge number of
    // events.
    m_maxLoadEvents = DEFAULT_MAX_LOAD_EVENTS;

    if (d->fetching && d->fetchJob) {
        disconnect(d->fetchJob, nullptr, this, nullptr);
        d->fetchJob->kill();
    }
    d->done = false;
    d->fetching = false;
    d->m_lastLoadedRev = d->m_initialRev;
}

bool VcsEventLogTreeModel::ignoreReloadRequests() const
{
    Q_D(const VcsEventLogTreeModel);
    return d->m_ignoreReloadRequests;
}

void VcsEventLogTreeModel::setIgnoreReloadRequests(bool state)
{
    Q_D(VcsEventLogTreeModel);
    d->m_ignoreReloadRequests = state;
}

void VcsEventLogTreeModel::loadMoreEvents(const int number)
{
    Q_D(VcsEventLogTreeModel);
    if (number == 0) {
        m_maxLoadEvents += qMax(rowCount()/2, DEFAULT_MAX_LOAD_EVENTS);
    } else {
        m_maxLoadEvents += number;
    }
    d->done = false;
    fetchMore({});
}

bool VcsEventLogTreeModel::canFetchMore(const QModelIndex& parent) const
{
    Q_D(const VcsEventLogTreeModel);

    return !d->done && !d->fetching && !parent.isValid();
}

void VcsEventLogTreeModel::fetchMore(const QModelIndex& parent)
{
    Q_D(VcsEventLogTreeModel);

    d->fetching = true;
    Q_ASSERT(!parent.isValid());
    Q_UNUSED(parent);
    VcsJob* job = d->m_vcs->log(d->m_url, d->m_lastLoadedRev, qMax(rowCount(), 100));
    connect(this, &VcsEventLogModel::destroyed, job, [job] { job->kill(); });
    connect(job, &VcsJob::finished, this, &VcsEventLogTreeModel::jobReceivedResults);
    ICore::self()->runController()->registerJob( job );
}

bool VcsEventLogTreeModel::isTopEvent(const KDevelop::VcsEvent ev) const {
    if (rowCount()) {
        QModelIndex idx{index(0, 0)};
        auto data{idx.data(VcsDataRole)};
        return (data.canConvert<VcsEvent>() && data.value<VcsEvent>() == ev);
    }
    return false;
}

bool VcsEventLogTreeModel::isBottomEvent(const KDevelop::VcsEvent ev) const {
    if (rowCount()) {
        QModelIndex idx{index(rowCount()-1, 0)};
        auto data{idx.data(VcsDataRole)};
        return (data.canConvert<VcsEvent>() && data.value<VcsEvent>() == ev);
    }
    return false;
}

bool VcsEventLogTreeModel::contiguous(const QList<KDevelop::VcsEvent> events) const {
    QModelIndex latest = revisionIndex(events.front().revision());

    if (!latest.isValid())
        return false;

    int eventIdx = 1;
    for(int rowIdx = latest.row()-1; (rowIdx >= 0 && eventIdx < events.size()); --rowIdx) {
        auto rowItem = item(rowIdx, 0)->data(VcsDataRole);
        if (! rowItem.canConvert<VcsEvent>() ) {
            continue;
        }
        if (rowItem.value<VcsEvent>() == events[eventIdx]) {
            ++eventIdx;
        } else {
            return false;
        }
    }
    if (eventIdx < events.size()) {
        return false;
    }
    return true;
}

KDevelop::VcsEvent VcsEventLogTreeModel::parentVcsEvent(const KDevelop::VcsRevision rev) const {
    auto revIdx = revisionIndex(rev);
    auto parentIdx = index(revIdx.row()+1, 0);
    while (parentIdx.isValid() && !parentIdx.data(VcsDataRole).canConvert<VcsEvent>()) {
        parentIdx = index(parentIdx.row()+1, 0);
    }
    if (parentIdx.isValid()) {
        return parentIdx.data(VcsDataRole).value<VcsEvent>();
    }
    return {};
}

KDevelop::VcsEvent VcsEventLogTreeModel::parentVcsEvent(const KDevelop::VcsEvent event) const {
    return parentVcsEvent(event.revision());
}

KDevelop::VcsEvent VcsEventLogTreeModel::childVcsEvent(const KDevelop::VcsRevision rev) const {
    auto revIdx = revisionIndex(rev);
    auto childIdx = index(revIdx.row()-1, 0);
    while (childIdx.isValid() && !childIdx.data(VcsDataRole).canConvert<VcsEvent>()) {
        childIdx = index(childIdx.row()-1, 0);
    }
    if (childIdx.isValid()) {
        return childIdx.data(VcsDataRole).value<VcsEvent>();
    }
    return {};
}

KDevelop::VcsEvent VcsEventLogTreeModel::childVcsEvent(const KDevelop::VcsEvent event) const {
    return childVcsEvent(event.revision());
}

KDevelop::VcsEvent VcsEventLogTreeModel::vcsEvent(const KDevelop::VcsRevision rev) const {
    auto revIdx = revisionIndex(rev);
    if (revIdx.isValid()) {
        return revIdx.data(VcsDataRole).value<VcsEvent>();
    }
    return {};
}

void VcsEventLogTreeModel::jobReceivedResults(KJob* job)
{
    Q_D(VcsEventLogTreeModel);

    if (job->error() != 0) {
        qDebug() << "error: git log exit code:"<< job->error();
        d->done = true;
        return;
    }

    const QList<QVariant> l = qobject_cast<KDevelop::VcsJob *>(job)->fetchResults().toList();
    qDebug() << "Got results:" << l.length();
    if(l.isEmpty()) {
        d->done = true;
        emit finishedLoading();
        return;
    }
    QList<KDevelop::VcsEvent> newevents;
    for (const QVariant& v : l) {
        if( v.canConvert<KDevelop::VcsEvent>() )
        {
            newevents << v.value<KDevelop::VcsEvent>();
        }
    }
    d->m_lastLoadedRev = newevents.last().revision();

    // When rowCount is not 0, the newevents list starts with the last
    // loaded revision so we discard it
    if (rowCount()) {
        newevents.removeFirst();
    }

    addEvents( newevents );

    // If the returned list contained (at most) the lastLoadedRev, there is nothing
    // more to fetch
    d->done = (newevents.isEmpty() || rowCount() >= m_maxLoadEvents);
    d->fetching = false;

    emit finishedLoading();
}

void VcsEventLogTreeModel::addEvents(const QList<KDevelop::VcsEvent>& events) {
    Q_D(VcsEventLogTreeModel);
    KDevelop::Path repoPath(d->m_url);
    beginInsertRows( QModelIndex(), rowCount(), rowCount()+events.count()-1 );
    for(const auto& event: events) {
        auto messageLines = event.message().split(QLatin1Char('\n'));
        auto summary = messageLines[0];
        QString display{
            summary +
            QStringLiteral("<br/>")+
            event.revision().prettyValue()+
            QStringLiteral("&nbsp;&nbsp;&nbsp;<i>") + event.author()+ QStringLiteral("</i>")+
            QStringLiteral("&nbsp;&nbsp;(at ")+event.date().toString()+ QStringLiteral(")")

        };
        auto eventData = QVariant::fromValue<KDevelop::VcsEvent>(event);
        auto* item = new QStandardItem(display);
        item->setData(eventData, VcsDataRole);
        item->setData(eventData, ParentVcsDataRole);
        item->setToolTip(event.message());
        const auto vcsItems = event.items();
        for(const auto& vcsItem: vcsItems) {
            QMimeType mime = QMimeDatabase().mimeTypeForFile(
                vcsItem.repositoryLocation(),
                QMimeDatabase::MatchExtension
            );
            QIcon icon = QIcon::fromTheme(mime.iconName());
            auto* child = new QStandardItem(icon, vcsItem.repositoryLocation());
            child->setData(QVariant::fromValue<KDevelop::VcsItemEvent>(vcsItem), VcsDataRole);
            child->setData(eventData, ParentVcsDataRole);
            item->appendRow(child);
        }
        appendRow(item);
        d->rev2index[event.revision()] = item->index();
    }
    endInsertRows();
}

QModelIndex VcsEventLogTreeModel::revisionIndex(const KDevelop::VcsRevision rev) const {
    const auto d = d_func();
    auto it = d->rev2index.find(rev);
    if ( it != d->rev2index.end() ) {
        return it->second;
    }
    return {};
}

QList<KDevelop::VcsEvent> VcsEventLogTreeModel::eventRange(
    const std::variant<KDevelop::VcsRevision, QModelIndex> &start,
    const std::variant<KDevelop::VcsRevision, QModelIndex> &stop) const
{
    qDebug() << "Constructing event range";

    QList<KDevelop::VcsEvent> ret;
    bool collecting = false;

    bool searchingStart = true;
    KDevelop::VcsRevision startRev;
    int i = rowCount()-1;
    if (std::holds_alternative<QModelIndex>(start)) {
        auto startI = std::get<QModelIndex>(start);
        if (startI.isValid()) {
            i = startI.row();
        }
        collecting = true;
        searchingStart = false;
        qDebug() << "  -- start:" << startI;
    } else {
        startRev = std::get<KDevelop::VcsRevision>(start);
        collecting = (startRev.revisionType() == VcsRevision::Invalid);
        searchingStart = !collecting;
        qDebug() << "  -- start:" << startRev.prettyValue();
    }

    bool searchingStop = false;
    KDevelop::VcsRevision stopRev;
    int limit = 0;
    if (std::holds_alternative<QModelIndex>(stop)) {
        auto stopI = std::get<QModelIndex>(stop);
        if (stopI.isValid()) {
            limit = stopI.row();
        }
        searchingStop = false;
        qDebug() << "  -- stop:" << stopI;
    } else {
        stopRev = std::get<KDevelop::VcsRevision>(stop);
        if (stopRev.revisionType() == VcsRevision::Invalid) {
            limit = 0;
            searchingStop = false;
        }
        qDebug() << "  -- stop:" << stopRev.prettyValue();
    }

    qDebug() << "  -- starting at row:" << i;
    qDebug() << "  --   searching for start:" << searchingStart;
    qDebug() << "  -- stopping at row:" << limit;
    qDebug() << "  --   searching for stop:" << searchingStop;
    qDebug() << "  -- collecting:" << collecting;

    while(i>=limit) {
        auto rowData = data(index(i,0), VcsDataRole);
        if (rowData.canConvert<VcsEvent>()) {
            auto event = rowData.value<VcsEvent>();
            if (searchingStart && event.revision() == startRev) {
                collecting = true;
                searchingStart = false;
                qDebug()<< "      -- found start at:" << i;
            }

            if (collecting) {
                ret.push_back(event);
                qDebug()<< "      -- adding event:" << event.revision().prettyValue();
            }

            if (searchingStop && event.revision() == stopRev) {
                qDebug()<< "      -- found stop at:" << i;
                qDebug() << "returning range of size:" << ret.size();
                return ret;
            }
        }
        --i;
    }
    qDebug() << "returning range of size:" << ret.size();
    return ret;
}

VcsEventLogTreeModel::~VcsEventLogTreeModel() = default;

Qt::ItemFlags VcsEventLogTreeModel::flags(const QModelIndex& index) const {

    auto ret = QStandardItemModel::flags(index) | Qt::ItemIsDragEnabled;

    if (index.isValid() && index.data(VcsDataRole).canConvert<VcsEvent>()) {
        ret |= Qt::ItemIsDropEnabled;
    }
    return ret;
}

QStringList VcsEventLogTreeModel::mimeTypes() const {
    return QStringList{
        QLatin1String{"text/plain"},
        QLatin1String("application/x-kdevelop-eventlogtreemodel-index-rows"),
    };
}

QMimeData *VcsEventLogTreeModel::mimeData(const QModelIndexList &indexes) const
{
    QMimeData *mimeData = new QMimeData;

    QByteArray diffEncodedData;

    QByteArray indexEncodedData;
    QDataStream indexStream(&indexEncodedData, QIODevice::WriteOnly);


    for (const QModelIndex &index : indexes) {
        if (index.isValid()) {
            indexStream << index.row();
            VcsDiff diff;
            if (index.data(VcsDiffRole).canConvert<VcsDiff>()) {
                diff = index.data(VcsDiffRole).value<VcsDiff>();
            } else {
                VcsJob* fetchDiff = nullptr;
                VcsEvent commit;
                QUrl repoFile;
                if ( index.data(VcsDataRole).canConvert<VcsEvent>() ) {
                    commit = index.data(VcsDataRole).value<VcsEvent>();
                    repoFile = d_ptr->m_url;
                    fetchDiff = d_ptr->m_vcs->diff(repoFile, commit.revision());
                } else if ( index.data(VcsDataRole).canConvert<VcsItemEvent>() ) {
                    commit = index.data(ParentVcsDataRole).value<VcsEvent>();
                    QDir repoDir(d_ptr->m_url.toLocalFile());
                    repoFile = QUrl::fromLocalFile(repoDir.filePath(index.data(VcsDataRole).value<VcsItemEvent>().repositoryLocation()));
                    qDebug() << index.data(VcsDataRole) << index.data(VcsDataRole).value<VcsItemEvent>().repositoryLocation() << index << repoFile;
                    fetchDiff = d_ptr->m_vcs->diff(repoFile, commit.revision());
                } else {
                    // Each item's VcsDataRole must hold either a VcsEvent or a VcsItemEvent
                    Q_ASSERT(false);
                }

                if (fetchDiff && fetchDiff->exec()) {
                    auto result = fetchDiff->fetchResults();
                    if (result.canConvert<VcsDiff>()) {
                        diff = result.value<VcsDiff>();
                        QList<QString> headerLines {
                            i18n("commit %1", commit.revision().prettyValue()),
                            i18n("Author: %1", commit.author()),
                            i18n("Date: %1", commit.date().toString()),
                            {}
                        };
                        for(const auto& ln: commit.message().split(QLatin1Char('\n'))) {
                            headerLines += QStringLiteral("    ")+ln;
                        }
                        headerLines.push_back({});
                        headerLines.push_back(i18n("(Note: "
                            "The diff below shows changes between %1 and its parent commit on the current branch. \n"
                            "In certain cases this can be different from the changes introduced by %1.)",
                            commit.revision().prettyValue()
                        ));
                        diff.setDiffHeader(headerLines.join(QLatin1Char('\n'))+QStringLiteral("\n")+diff.diffHeader());
                        emit receivedDiff(index, diff);
                    }
                } else if (fetchDiff) {
                        qWarning() << "DiffJob Failed" << fetchDiff->errorText();
                }
            }
            if (! diff.isEmpty()) {
                diffEncodedData.append(diff.diff().toUtf8());
            }
        }
    }

    mimeData->setData(QLatin1String("text/plain"), diffEncodedData);
    mimeData->setData(QLatin1String("application/x-kdevelop-eventlogtreemodel-index-rows"), indexEncodedData);

    return mimeData;
}

void VcsEventLogTreeModel::cacheDiff(const QModelIndex& index, const KDevelop::VcsDiff& diff)
{
    setData(index, QVariant::fromValue(diff),  VcsDiffRole);
}


bool VcsEventLogTreeModel::canDropMimeData(const QMimeData *data,
    Qt::DropAction action, int row, int column, const QModelIndex &parent) const
{
    Q_UNUSED(action);
    Q_UNUSED(row);
    Q_UNUSED(parent);

    if (column > 0) return false;

    for(const auto& fmt: mimeTypes()) {
        if (data->hasFormat(fmt)) return true;
    }

    return false;
}

bool VcsEventLogTreeModel::dropMimeData(const QMimeData *data,
    Qt::DropAction action, int row, int column, const QModelIndex &parent)
{
    return false;
    if (!canDropMimeData(data, action, row, column, parent))
        return false;

    if (action == Qt::IgnoreAction)
        return true;
}

}

#include "moc_vcseventmodel.cpp"
