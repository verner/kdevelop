/*
    SPDX-FileCopyrightText: 2007 Andreas Pakulat <apaku@gmx.de>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef KDEVPLATFORM_VCSEVENTMODEL_H
#define KDEVPLATFORM_VCSEVENTMODEL_H

#include <QAbstractTableModel>
#include <QStandardItemModel>
#include <QScopedPointer>

#include <vcs/vcsexport.h>
#include <vcs/vcsevent.h>

#include <unordered_map>

class QUrl;
class KJob;

class QMimeData;

namespace KDevelop
{
class VcsRevision;
class IBasicVersionControl;
class VcsEventLogModelPrivate;
class VcsBasicEventModelPrivate;
class VcsDiff;

/**
 * This is a generic model to store a list of VcsEvents.
 *
 * To add events use @c addEvents
 */
class KDEVPLATFORMVCS_EXPORT VcsBasicEventModel : public QAbstractTableModel
{
Q_OBJECT
public:
    enum Column {
        RevisionColumn,
        SummaryColumn,
        AuthorColumn,
        DateColumn,
        ColumnCount,
    };

    explicit VcsBasicEventModel(QObject* parent);
    ~VcsBasicEventModel() override;
    int rowCount(const QModelIndex& = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex&, int role = Qt::DisplayRole) const override;
    QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const override;
    KDevelop::VcsEvent eventForIndex(const QModelIndex&) const;

protected:
    void addEvents(const QList<KDevelop::VcsEvent>&);

private:
    const QScopedPointer<class VcsBasicEventModelPrivate> d_ptr;
    Q_DECLARE_PRIVATE(VcsBasicEventModel)
};


/**
 * This model stores a list of VcsEvents corresponding to the log obtained
 * via IBasicVersionControl::log for a given revision. The model is populated
 * lazily via @c fetchMore.
 */
class KDEVPLATFORMVCS_EXPORT VcsEventLogModel : public VcsBasicEventModel
{
Q_OBJECT
public:

    VcsEventLogModel(KDevelop::IBasicVersionControl* iface, const KDevelop::VcsRevision& rev, const QUrl& url, QObject* parent);
    ~VcsEventLogModel() override;

    /// Adds events to the model via @sa IBasicVersionControl::log
    void fetchMore(const QModelIndex& parent) override;
    bool canFetchMore(const QModelIndex& parent) const override;

private Q_SLOTS:
    void jobReceivedResults( KJob* job );

private:
    const QScopedPointer<class VcsEventLogModelPrivate> d_ptr;
    Q_DECLARE_PRIVATE(VcsEventLogModel)
};


/**
 * This is a tree model to store a list of VcsEvents together with the corresponding
 * VcsEventItems as their children. The events are obtained via @ref IBasicVersionControl::log
 * for a given revision.
 *
 * @note: The model is populated lazily via @c fetchMore. Moreover, the model only
 * loads @ref m_maxLoadEvents events by default. More events can be fetched using the
 * @ref loadMoreEvents slot.
 * @note: The event list is linear. In case of branching, it represents only a single
 * branch (typically the current branch at the time log is called) so any event has at most
 * a single parent and a single child.
 * @warning: When changing branches, the model should be reloaded, otherwise it might give
 * inconsistent data.
 */
class KDEVPLATFORMVCS_EXPORT VcsEventLogTreeModel : public QStandardItemModel
{
Q_OBJECT
public:

    enum ItemRoles {
        VcsDataRole = Qt::UserRole+1, /**< @ref VcsEvent/VcsItemEvent */
        VcsDiffRole,                  /**< a (parsed, VcsDiff) diff */
        ParentVcsDataRole,            /**< @ref Parent VcsEvent */
    };

    explicit VcsEventLogTreeModel(
        KDevelop::IBasicVersionControl* iface,
        const QUrl& url,
        const KDevelop::VcsRevision& rev,
        QObject* parent
    );
    ~VcsEventLogTreeModel();

    /**
     * Adds events to the model via @ref IBasicVersionControl::log
     */
    void fetchMore(const QModelIndex& parent) override;
    bool canFetchMore(const QModelIndex& parent) const override;

    /**
     * Returns true if @p event is the latest event
     */
    bool isTopEvent(const KDevelop::VcsEvent event) const;

    /**
     * Returns true if @p event is the first event
     */
    bool isBottomEvent(const KDevelop::VcsEvent event) const;

    /**
     * Returns the event corresponding to the child revision of the
     * revision given by @p event.
     *
     * @note: If there is more than one child, the child on the
     * current branch is returned.
     */
    KDevelop::VcsEvent childVcsEvent(const KDevelop::VcsEvent event) const;

    /**
     * Returns the event corresponding to the child revision of @p revision.
     *
     * @note: If there is more than one child, the child on the
     * current branch is returned.
     */
    KDevelop::VcsEvent childVcsEvent(const KDevelop::VcsRevision revision) const;

    /**
     * Returns the event corresponding to the parent revision of the
     * revision given by @p event.
     *
     * @note: If there is more than one parent, the parent on the
     * current branch is returned.
     */
    KDevelop::VcsEvent parentVcsEvent(const KDevelop::VcsEvent event) const;

    /**
     * Returns the event corresponding to the parent revision @p revision
     *
     * @note: If there is more than one parent, the parent on the
     * current branch is returned.
     */
    KDevelop::VcsEvent parentVcsEvent(const KDevelop::VcsRevision revision) const;

    /**
     * Returns the event corresponding to revision @p revision
     */
    KDevelop::VcsEvent vcsEvent(const KDevelop::VcsRevision revision) const;

    /**
     * Returns true if @p events are contiguous, i.e. if they
     * are a full interval with no gaps.
     *
     * @note: The method assumes that the events are ordered
     * from the latest (first) to the oldest (last).
     */
    bool contiguous(const QList<KDevelop::VcsEvent> events) const;

    /**
     * Returns the model index of the event corresponding to revision @p rev
     * if it exists. Otherwise it returns an invalid index
     */
    QModelIndex revisionIndex(const KDevelop::VcsRevision rev) const;

    /**
     * Returns a list of events starting from the the event given by
     * revision/index @p start (included) and ending with the event
     * given by revision/index @p stop (included)
     */
    QList<KDevelop::VcsEvent> eventRange(const std::variant<KDevelop::VcsRevision, QModelIndex> &start,
                                         const std::variant<KDevelop::VcsRevision, QModelIndex> &stop) const;

    /** DND **/
    virtual Qt::ItemFlags flags(const QModelIndex & index) const override;
    virtual QStringList mimeTypes() const override;
    virtual QMimeData *mimeData(const QModelIndexList &indexes) const override;
    virtual bool canDropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex& parent) const override;
    virtual bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex& parent) override;

Q_SIGNALS:
    /**
     * Emitted when the model finished loading and is ready.
     */
    void finishedLoading();

public Q_SLOTS:
    /**
     * Request that the model reloads itself.
     */
    void reloadRequest();

    /**
     * Sets @ref ignoreReloadRequests to @p state.
     */
    void setIgnoreReloadRequests(bool state);

    /**
     * @returns whether the model is currently ignoring reload requests.
     */
    bool ignoreReloadRequests() const;

    /**
     * Increases the maximum number of events that the model can hold
     * by @p number and starts loading them. If @p number is 0, it is
     * replaced by a reasonable (possibly state dependent) positive number.
     */
    void loadMoreEvents(const int number = 0);

protected:
    void addEvents(const QList<KDevelop::VcsEvent>&);

private Q_SLOTS:
    void jobReceivedResults( KJob* job );

    void cacheDiff(const QModelIndex& index, const KDevelop::VcsDiff& diff);

Q_SIGNALS:
    void receivedDiff(const QModelIndex& index, const KDevelop::VcsDiff& diff) const;

private:
    const QScopedPointer<class VcsEventLogTreeModelPrivate> d_ptr;
    Q_DECLARE_PRIVATE(VcsEventLogTreeModel)

    /**
     * The default maximum number of events loaded by the history model.
     *
     * See also @ref mW_maxLoadEvents and @ref loadMoreEvents.
     */
    const int DEFAULT_MAX_LOAD_EVENTS = 300;

    /**
     * The maximum number of elements to load.
     *
     * This is used to limit the amount of memory taken-up by the model
     * and, more importantly, the amount of processing time.
     *
     * See also @ref DEFAULT_MAX_LOAD_EVENTS and @ref loadMoreEvents.
     */
    int m_maxLoadEvents = DEFAULT_MAX_LOAD_EVENTS;
};
}

// Q_DECLARE_METATYPE(KDevelop::VcsEventLogTreeModel::Item)

#endif
