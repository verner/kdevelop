/*
    SPDX-FileCopyrightText: 2017 Friedrich W. H. Kossebau <kossebau@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "test_vcsdiff.h"

#include <QTest>
#include <QStandardPaths>

#include <vcs/vcslocation.h>

using namespace KDevelop;

QString TestVcsDiff::getSample(const QString sampleName) const
{
    QStandardPaths::setTestModeEnabled(true);
    QString path = QFINDTESTDATA(QStringLiteral("data/")+sampleName);
    QFile in(path);
    in.open(QIODevice::ReadOnly);
    return QString::fromUtf8(in.readAll());
}

void TestVcsDiff::initTestCase()
{
    sampleDiff = getSample("sample1.diff");
    QCOMPARE(sampleDiff.length(), 6235);
}

void TestVcsDiff::setDiff(VcsDiff& diff,
                          const QString& diffString,
                          const QUrl& baseDiff,
                          uint depth)
{
    diff.setDiff(diffString);
    diff.setBaseDiff(baseDiff);
    diff.setDepth(depth);
}

void TestVcsDiff::compareDiff(const VcsDiff& diff,
                              const QString& diffString,
                              const QUrl& baseDiff,
                              uint depth)
{
    QCOMPARE(diff.diff(), diffString);
    QCOMPARE(diff.baseDiff(), baseDiff);
    QCOMPARE(diff.depth(), depth);
}

void TestVcsDiff::testHeader()
{
    VcsDiff diff;
    diff.setDiff(sampleDiff);
    QString expected = sampleDiff.split(QLatin1Char('\n')).mid(0,8).join(QLatin1Char('\n'));
    QCOMPARE(diff.diffHeader(), expected);
    QString expected2{QStringLiteral("Test header")};
    diff.setDiffHeader(expected2);
    QCOMPARE(diff.diffHeader(), expected2);
}

void TestVcsDiff::testSplitFiles()
{
    VcsDiff diff, split;
    const QString origSrc = getSample("sample2.diff");
    const QString splitSrc = getSample("sample2-splitFiles-vcsdiff-split.diff");
    const QString restSrc = getSample("sample2-splitFiles-vcsdiff-rest.diff");
    const QVector<VcsDiff::FilePair> splitFiles = {
        {"kdevplatform/vcs/vcsdiff.cpp", "kdevplatform/vcs/vcsdiff.cpp"},
        {"kdevplatform/vcs/vcsdiff.h", "kdevplatform/vcs/vcsdiff.h"}
    };
    const QVector<VcsDiff::FilePair> remainFiles = {
        {"kdevplatform/vcs/tests/test_vcsdiff.cpp", "kdevplatform/vcs/tests/test_vcsdiff.cpp"},
        {"plugins/git/diffviewsctrl.cpp", "plugins/git/diffviewsctrl.cpp"}
    };
    diff.setDiff(origSrc);
    split = diff.splitOff({splitFiles[0].source, splitFiles[1].source});
    diff.setDiffHeader({});
    split.setDiffHeader({});
    QCOMPARE(split.fileNames(), splitFiles);
    QCOMPARE(diff.fileNames(), remainFiles);
    QCOMPARE(split.diff(), splitSrc);
    QCOMPARE(diff.diff(), restSrc);
}

void TestVcsDiff::testSplitHunk()
{
    VcsDiff diff;
    QString splitSrc = getSample("sample1-splitHunk-2nd-split.diff");
    QString restSrc = getSample("sample1-splitHunk-2nd-rest.diff");
    diff.setDiff(sampleDiff);
    auto split = diff.splitOffHunk(29);
    split.setDiffHeader({});
    diff.setDiffHeader({});
    QCOMPARE(split.diff(), splitSrc);
    QCOMPARE(diff.diff(), restSrc);
}

void TestVcsDiff::testSplitLineRange()
{
    VcsDiff diff, split;
    QString origSrc = getSample("sample3.diff");
    QString splitSrc = getSample("sample3-splitLineRange-19_21-split.diff");
    QString restSrc = getSample("sample3-splitLineRange-19_21-rest.diff");
    diff.setDiff(origSrc);
    split = diff.splitOff(19-1, 21-1);
    diff.setDiffHeader({});
    split.setDiffHeader({});
    QCOMPARE(split.diff(), splitSrc);
    QCOMPARE(diff.diff(), restSrc);
}

void TestVcsDiff::testFileNames()
{
    VcsDiff diff;
    diff.setDiff(sampleDiff);
    const QVector<VcsDiff::FilePair> expected = {
        {"kdevplatform/vcs/vcsdiff.cpp", "kdevplatform/vcs/vcsdiff.cpp"},
        {"kdevplatform/vcs/vcsdiff.h", "kdevplatform/vcs/vcsdiff.h"}
    };
    QCOMPARE(diff.fileNames(), expected);
}

void TestVcsDiff::testSubDiff()
{
    /**************************
     * Test a real world diff *
     **************************/
    VcsDiff diff;
    diff.setDiff(sampleDiff);
    QString expected(R"diff(--- a/kdevplatform/vcs/vcsdiff.cpp
+++ b/kdevplatform/vcs/vcsdiff.cpp
@@ -39,7 +39,7 @@ public:
        , oldCount
        , newStart
        , newCount
-       , firstLineIdx
+       , firstLineIdx  /**< The 0-based line number (in the whole diff) of the hunk header line (the one starting with `@@`) */
        ;
     QString srcFile    /**< The source filename */
           , tgtFile    /**< The target filename */
)diff");
    const auto subHunkDiffs = {
        diff.subDiffHunk(15),
    };
    for(auto df: subHunkDiffs) {
        QVERIFY(df.fileNames().count()>0);
        QCOMPARE(df.fileNames().front().source, "kdevplatform/vcs/vcsdiff.cpp");
        QCOMPARE(df.diff(), expected);
    }

    VcsDiff subHunkDiff = diff.subDiff(15, 50);
    QVERIFY(subHunkDiff.fileNames().count()>0);
    QCOMPARE(subHunkDiff.fileNames().front().source, "kdevplatform/vcs/vcsdiff.cpp");

    /*********************************
     * Test start offset computation *
     *********************************/
    VcsDiff skipDiff;
    skipDiff.setDiff(R"diff(--- a/skip
+++ b/skip
@@ -1,2 +3,2 @@ heading
+ increase offset

+ dont increase offset
- deletion
- second deletion
)diff");
    expected = QStringLiteral(R"diff(--- a/skip
+++ b/skip
@@ -2,3 +4,2 @@ heading

- deletion
  second deletion
)diff");
    auto subDiff = skipDiff.subDiff(6,6);
    QCOMPARE(subDiff.diff(), expected);

}

void TestVcsDiff::testConflicts()
{
    VcsDiff conflictDiff;
    conflictDiff.setDiff(R"diff(--- a/skip
+++ b/skip
@@ -1,2 +3,2 @@ conflict
+ increase offset            3     2
                             4  0  3
+ dont increase offset       5     4
- deletion                   6  1
<<<<<<< HEAD                 7
blablabla                    8  2
=======                      9
yadayadayada                10     5
>>>>>>> commit              11
- second deletion           12  3
)diff");
    QCOMPARE(conflictDiff.diffLineToSource(8).line, 2);
    QCOMPARE(conflictDiff.diffLineToTarget(10).line, 5);
    QCOMPARE(conflictDiff.diffLineToSource(7).line, -1);
    QCOMPARE(conflictDiff.diffLineToSource(9).line, -1);
    QCOMPARE(conflictDiff.diffLineToSource(11).line, -1);
}


void TestVcsDiff::testLineMapping()
{
    VcsDiff diff;
    diff.setDiff(sampleDiff);

    auto src = QStringLiteral("kdevplatform/vcs/vcsdiff.cpp");
    auto tgt = QStringLiteral("kdevplatform/vcs/vcsdiff.h");
    QCOMPARE(diff.diffLineToSource(15-1).path, src);
    QCOMPARE(diff.diffLineToTarget(147-1).path, tgt);

    // We have no way to map the headings
    QCOMPARE(diff.diffLineToSource(169-1).line, -1);
    QCOMPARE(diff.diffLineToTarget(169-1).line, -1);

    QCOMPARE(diff.diffLineToSource(15-1).line, 42-1);
    QCOMPARE(diff.diffLineToTarget(15-1).line, -1);
    QCOMPARE(diff.sourceLocationToDiffLine({src, 42-1}), 15-1);

    QCOMPARE(diff.diffLineToTarget(147-1).line, 180-1);
    QCOMPARE(diff.diffLineToSource(147-1).line, 150-1);
    QCOMPARE(diff.sourceLocationToDiffLine({tgt, 150-1}), 147-1);
    QCOMPARE(diff.targetLocationToDiffLine({tgt, 180-1}), 147-1);

    QCOMPARE(diff.diffLineToSource(147-1).line, 150-1);

    VcsDiff diff4;
    diff4.setDiff(getSample("sample4.diff"));
    QCOMPARE(diff4.diffLineToTarget(30).line, 97);
}


 void TestVcsDiff::testCopyConstructor()
 {
    // test plain copy
    const QString diffString("diff");
    const QUrl baseDiff("git://1");
    const uint depth = 1;
    const VcsLocation location("server");

    {
        VcsDiff diffA;
        setDiff(diffA, 
                diffString, baseDiff, depth);
        diffA.setProperty(QStringLiteral("test_property"), QVariant::fromValue<VcsLocation>(location));

        VcsDiff diffB(diffA);
        compareDiff(diffA,
                    diffString, baseDiff, depth);
        compareDiff(diffB,
                    diffString, baseDiff, depth);
        QCOMPARE(
            diffA.property(QStringLiteral("test_property")),
            diffB.property(QStringLiteral("test_property"))
        );
    }

    const QString diffStringNew("diffNew");

    // test detach after changing A
    {
        VcsDiff diffA;
        setDiff(diffA, 
                diffString, baseDiff, depth);

        VcsDiff diffB(diffA);
        // change a property of A
        diffA.setDiff(diffStringNew);
        diffA.setProperty(QStringLiteral("test_property"), QVariant::fromValue<VcsLocation>(location));

        compareDiff(diffA,
                    diffStringNew, baseDiff, depth);
        compareDiff(diffB,
                    diffString, baseDiff, depth);
        QCOMPARE(
            diffB.property(QStringLiteral("test_property")),
            QVariant{}
        );
    }
}

void TestVcsDiff::testAssignOperator()
{
    // test plain copy
    const QString diffString("diff");
    const QUrl baseDiff("git://1");
    const uint depth = 1;
    const VcsLocation location("server");

    {
        VcsDiff diffA;
        setDiff(diffA, 
                diffString, baseDiff, depth);

        VcsDiff diffB;
        diffB = diffA;
        compareDiff(diffA,
                    diffString, baseDiff, depth);
        compareDiff(diffB,
                    diffString, baseDiff, depth);
    }

    const QString diffStringNew("diffNew");

    // test detach after changing A
    {
        VcsDiff diffA;
        setDiff(diffA, 
                diffString, baseDiff, depth);

        VcsDiff diffB;
        diffB = diffA;
        // change a property of A
        diffA.setDiff(diffStringNew);

        compareDiff(diffA,
                    diffStringNew, baseDiff, depth);
        compareDiff(diffB,
                    diffString,    baseDiff, depth);
    }
}

QTEST_GUILESS_MAIN(TestVcsDiff)

#include "moc_test_vcsdiff.cpp"
