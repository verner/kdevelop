/*
    SPDX-FileCopyrightText: 2007 Andreas Pakulat <apaku@gmx.de>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "vcsdiff.h"

#include "debug.h"

#include <QRegularExpression>
#include <QSharedData>
#include <QString>
#include <QtGlobal>
#include <QUrl>
#include <QVariant>

#include <vector>

namespace KDevelop
{


/**
 * A class representing a diff hunk (a collection of localized changes)
 */
class DiffHunk {
public:
    /* Metadata for the hunk */
    uint srcStart      /**< the 1-based (!) start line number of the range in the source file where the hunk applies */
       , srcCount      /**< the size of the range (in # of lines) in the source where the hunk applies  (i.e. ctx lines + deleted lines)*/
       , tgtStart      /**< the 1-based (!) start line number of the range in the target file where the hunk applies */
       , tgtCount      /**< the size of the range (in # of lines) in the target where the hunk applies  (i.e. ctx lines + deleted lines)*/
       , headingLineIdx  /**< The 0-based line number (in the whole diff) of the hunk header line (the one starting with `@@`) */
       ;
    QString srcFile    /**< The source filename (relative, dev/null indicates a new file) */
          , tgtFile    /**< The target filename (relative, dev/null indicates a deleted file) */
          , heading    /**< The heading of the hunk (the stuff in the header line after the position spec, i.e. after the second `@@`) */
          ;
    QStringList metadata; /**< The metadata lines for the hunk (e.g. file mode changes, command used to generate the hunk, ...) */
    QStringList lines; /**< The lines comprising the hunk (excluding the header) */

    /**
     * @returns the 0-based line number (in the whole diff) of the last line contained in the hunk.
     */
    uint lastLineIdx() const { return headingLineIdx+lines.size(); }

    /**
     * @param lineIdx the 0-based line number of the tested line in the whole diff
     * @returns true if the line is part of the diff and false otherwise
     * @note: Returns true also for the header line (the one starting with `@@`)
     */
    bool containsDiffLine( uint lineIdx ) const {return headingLineIdx <= lineIdx && lineIdx <= lastLineIdx();}

    /**
     * @param lineIdx the 0-based line number of the tested line in the source file
     * @returns true if the source location is in the hunk and false otherwise
     */
    bool containsSourceLocation(const VcsDiff::SourceLocation& loc ) const {
        if (!loc.isValid() || loc.path != srcFile || uint(loc.line-1) < srcStart || srcStart+srcCount < uint(loc.line-1) ) {
            return false;
        }
        return true;
    }

    /**
     * @returns the number of lines added - the number of lines deleted
     */
    int lineCountDelta() const {
        return tgtCount-srcCount;
    }

    /**
     * Returns the index of the line within the hunk
     *
     * @param diffLineIdx the 0-based index of the line in the diff
     *
     * @note assumes that the line is contained within the hunk
     * @note if the line is a header line, -1 is returned; otherwise the returned
     * number is the index of the line in the `lines` list
     */
    int diffLineToHunkLine(uint diffLineIdx) const {
        return diffLineIdx-(headingLineIdx+1);
    }

    /**
     * Returns the index of the line within the full diff
     *
     * @param hunkLineIdx the 0-based index of the line in the hunk
     */
    uint hunkLineToDiffLine(uint hunkLineIdx) const {
        return hunkLineIdx+headingLineIdx+1;
    }


    QString toString() const;

    /**
     * A helper method to construct a hunk header from the provided info
     *
     * A hunk header has the following form:
     *
     *      @@ -oldStart,oldCount +newStart,newCount @@ heading
     * e.g.
     *      @@ -36,14 +36,28 @@ public:
     *
     * @returns the hunk header
     *
     * @note: the line will not end with a newline
     */
    static QString formatHeader(uint oldStart, uint oldCount, uint newStart, uint newCount, QString head);

    /**
     * Constructs and returns the hunk header line (without a newline)
     *
     * @returns the hunk header
     */
    QString formatHeader() const {
        return formatHeader(srcStart, srcCount, tgtStart, tgtCount, heading);
    }

    enum class FileNameType {
        Source,
        Target
    };

    QString formatFileName(const enum FileNameType tp) const {
        if (tp == FileNameType::Source) {
            return formatFileName(srcFile, tp);
        } else {
            return formatFileName(tgtFile, tp);
        }
    }

    static QString formatFileName(const QString& name, const enum FileNameType tp) {
        if (name == QStringLiteral("dev/null")) {
            switch(tp) {
                case FileNameType::Source:
                    return QStringLiteral("--- /dev/null");
                case FileNameType::Target:
                    return QStringLiteral("+++ /dev/null");
            }
        }
        switch(tp) {
            case FileNameType::Source:
                return QStringLiteral("--- a/")+name;
            case FileNameType::Target:
                return QStringLiteral("+++ b/")+name;
        }
        Q_ASSERT(false);
        return {};
    }


    /**
     * The following operators define a PARTIAL order on the hunks list.
     * A hunk H is strictly below a hunk K iff the endline of H is strictly below
     * the start line of K. In particular, only non-overlapping hunks are
     * ordered.
     */
    bool operator<( const DiffHunk& b ) const {return lastLineIdx() < b.headingLineIdx;}
    bool operator<( uint line ) const  {return lastLineIdx() < line;}
    bool operator<( const VcsDiff::SourceLocation& loc ) const  {
        return loc.isValid() && (
              (loc.path == srcFile && srcStart+srcCount < uint(loc.line+1)) ||
              (loc.path == tgtFile && tgtStart+tgtCount < uint(loc.line+1))
        );
    }
    bool operator<=( const VcsDiff::SourceLocation& loc ) const  {
        return loc.isValid() && (
              (loc.path == srcFile && srcStart+srcCount <= uint(loc.line+1)) ||
              (loc.path == tgtFile && tgtStart+tgtCount <= uint(loc.line+1))
        );
    }
    bool operator<=( const KDevelop::DiffHunk& b ) const {return lastLineIdx() <= b.headingLineIdx;}
    bool operator<=( uint line ) const {return lastLineIdx() <= line;}
    bool operator>( const KDevelop::DiffHunk& b ) const {return headingLineIdx > b.lastLineIdx();}
    bool operator>( uint line ) const {return headingLineIdx > line;}
    bool operator>( const VcsDiff::SourceLocation& loc ) const  {
        return loc.isValid() && (
              (loc.path == srcFile && srcStart > uint(loc.line+1)) ||
              (loc.path == tgtFile && tgtStart > uint(loc.line+1))
        );
    }
    bool operator>= ( const KDevelop::DiffHunk& b ){return headingLineIdx >= b.lastLineIdx();}
    bool operator>=( uint line ) const {return headingLineIdx >= line;}
    bool operator>=( const VcsDiff::SourceLocation& loc ) const  {
        return loc.isValid() && (
              (loc.path == srcFile && srcStart >= uint(loc.line+1)) ||
              (loc.path == tgtFile && tgtStart >= uint(loc.line+1))
        );
    }

};

bool operator<(const uint line, const DiffHunk& b) {return (b > line);}
bool operator<(const VcsDiff::SourceLocation loc, const DiffHunk& b) {return (b > loc);}
bool operator<=(const uint line, const DiffHunk& b) {return (b >= line);}
bool operator<=(const VcsDiff::SourceLocation loc, const DiffHunk& b) {return (b >= loc);}
bool operator>(const uint line, const DiffHunk& b) {return (b < line);}
bool operator>(const VcsDiff::SourceLocation loc, const DiffHunk& b) {return (b < loc);}
bool operator>=(const uint line, const DiffHunk& b) {return (b <= line);}
bool operator>=(const VcsDiff::SourceLocation loc, const DiffHunk& b) {return (b <= loc);}


QString DiffHunk::toString() const {
    return metadata.join(QLatin1Char('\n')) + QLatin1Char('\n') +
           formatFileName(srcFile, FileNameType::Source) + QLatin1Char('\n') +
           formatFileName(tgtFile, FileNameType::Target) + QLatin1Char('\n') +
           formatHeader() + QLatin1Char('\n') +
           lines.join(QLatin1Char('\n'))+ QLatin1Char('\n');
}


/* RegExp matching a hunk header line */
Q_GLOBAL_STATIC_WITH_ARGS(const QRegularExpression, HUNK_HEADER_RE, (QLatin1String("^@@ -([0-9,]+) \\+([0-9,]+) @@(.*)")))

/* RegExp matching a meta line containing a source of target filename */
Q_GLOBAL_STATIC_WITH_ARGS(const QRegularExpression, DIFF_FILENAME_RE, (QLatin1String("^[-+]{3} [ab]?/(.*)")))

/* RegExp matching a meta line (hunk header, filename, other info) */
Q_GLOBAL_STATIC_WITH_ARGS(const QRegularExpression, META_LINE_RE, (QLatin1String("(^[-+]{3} )|^[^-+ ]")))

/* RegExps matching conflict delimiting lines */
Q_GLOBAL_STATIC_WITH_ARGS(const QRegularExpression, CONFLICT_START_RE, (QLatin1String("^<<<<<<<")))
Q_GLOBAL_STATIC_WITH_ARGS(const QRegularExpression, CONFLICT_MID_RE, (QLatin1String("^=======")))
Q_GLOBAL_STATIC_WITH_ARGS(const QRegularExpression, CONFLICT_END_RE, (QLatin1String("^>>>>>>>")))
Q_GLOBAL_STATIC_WITH_ARGS(const QRegularExpression, CONFLICT_RE, (QLatin1String("(^>>>>>>>)|(^=======)|(^<<<<<<<)")))

QString formatRange(uint start, uint count) {
    if (count == 1) return QString().setNum(start);
    return QString().setNum(start)+QLatin1Char(',')+QString().setNum(count);
}
std::pair<uint, uint> parseRange(const QString& range) {
    int commaPos = range.indexOf(QLatin1Char(','));
    if (commaPos > -1) {
        return {range.midRef(0,commaPos).toInt(), range.midRef(commaPos+1).toInt()};
    }
    return {range.toInt(), 1};
}

/* Creates a hunk header line (starting with @@)
 *
 * Note: The line will not end with a newline */
QString DiffHunk::formatHeader(uint oldStart, uint oldCount, uint newStart, uint newCount, QString head) {
    return QLatin1String("@@ -")
         + formatRange(oldStart, oldCount)
         + QLatin1String(" +")
         + formatRange(newStart, newCount)
         + QLatin1String(" @@")
         + head;
}

/**
 * Parses a unified diff into a list of "diff hunks" (each hunk starts with a
 * line starting with @@ and represents a collection of localized changes).
 *
 * @param diff a diff in git's unified diff format
 * @returns a list of hunk structures
 *
 * The diff is assumed to be a collection of hunks, where each hunk has the
 * following structure:
 *
 *   METADATA
 *   --- a/SOURCE_PATH
 *   +++ b/TARGET_PATH
 *   HUNK HEADER
 *   HUNK CONTENT
 *
 * All metadata lines match the @ref:META_LINE_RE regexp (starts with anything
 * except for a '+', '-' and ' ') and these are saved as is (except for the hunk
 * header and source/target path specifications, which are processed further).
 * The path specifications are assumed to apply to all following hunks until a
 * new path specification is found and are stored in the srcFileName and tgtFileName
 * attributes of the hunk structure.
 *
 *
 * Hunk Header
 * -----------
 *
 * The hunk header has the following form
 *
 *   @@ -SRC_OFFSET[, SRC_CHANGES_COUNT] +TGT_OFFSET[, TGT_CHANGES_COUNT] @@ Heading
 *
 * where the SRC_OFFSET is a 1-based line index pointing to the source file where
 * the hunk applies and TGT_OFFSET is a 1-based line index pointing to the target
 * file where the hunk applies. These are parsed into the srcStart and tgtStart
 * attributes of the hunk structure.
 *
 * The optional SRC_CHANGES_COUNTS (assumed to be 1 if not present) specifies the
 * number of context lines (starting with ' ') plus the number of deleted lines
 * (starting with '-'). Similarly, the optional TGT_CHANGES_COUNT specifies the
 * number of context lines plus the number of added lines (starting with '+').
 * These are parsed and stored in the srcCount and tgtCount attributes of the hunk
 * structure, but not checked (!). I.e. if the diff hunk has more/less changes then
 * specified, the returned hunk structure will have invalid src & tgt counts.
 *
 * Finally the Heading, used as a visual aid for users, is supposed to show the line
 * where the nearest enclosing function scope of the hunk starts. It is parsed and
 * stored in the heading attribute.
 *
 * Hunk Content
 * ------------
 *
 * The hunk content is a collection of lines which
 *
 *   1) start with '+' (additions); or
 *   2) start with '-' (deletions); or
 *   3) start with ' ' (context lines); or
 *   4) are empty (context lines); or
 *   5) are within conflict markers
 *
 * These lines are parsed and stored in the lines attribute of the hunk structure.
 * The parsing of the hunk stops when a METADATA line (outside of conflict markers)
 * is encountered or the end of the file is reached.
 *
 * Conflict Markers
 * ----------------
 *
 * Conflict markers are collections of lines of the form:
 *
 *   >>>>>>> our ref
 *   our content
 *   ...
 *   =======
 *   their content
 *   ...
 *   <<<<<<< their ref
 *
 * And show content which a merge was not able to merge automatically.
 * Strictly speaking, these should not appear in diffs, but git diff
 * generates them anyway for files with unresolved conflicts.
 */
std::vector<DiffHunk> parseHunks(VcsDiff& diff)
{
    std::vector<DiffHunk> ret;
    int lineNo = -1;
    QString curSrcFileName, curTgtFileName;
    QStringList curMetadata;
    QStringListIterator lines(diff.diff().split(QLatin1Char('\n')));
    while( lines.hasNext() ) {
        lineNo++;
        auto curln = lines.next();
        auto m = DIFF_FILENAME_RE->match(curln);
        if (m.hasMatch()) {
            if (curln.startsWith(QLatin1Char('-')))
                curSrcFileName = m.captured(1);
            else if (curln.startsWith(QLatin1Char('+')))
                curTgtFileName = m.captured(1);
            continue;
        }

        m = HUNK_HEADER_RE->match(curln);
        if (! m.hasMatch() ) {
            curMetadata.push_back(curln);
            continue;
        }

        auto [oldStart, oldCount] = parseRange(m.captured(1));
        auto [newStart, newCount] = parseRange(m.captured(2));
        auto heading = m.captured(3);
        uint firstLineIdx = lineNo;
        QStringList hunkLines;
        while (lines.hasNext()
                && (CONFLICT_START_RE->match(lines.peekNext()).hasMatch()
                    || !META_LINE_RE->match(lines.peekNext()).hasMatch())) {
            // Consume the conflict
            if (CONFLICT_START_RE->match(lines.peekNext()).hasMatch()) {
                lineNo++;
                hunkLines << lines.next();
                while (lines.hasNext() && !CONFLICT_END_RE->match(lines.peekNext()).hasMatch()) {
                    lineNo++;
                    hunkLines << lines.next();
                }
                if (!CONFLICT_END_RE->match(lines.peekNext()).hasMatch()) {
                    qCWarning(VCS) << "Invalid diff format, end of file reached before conflict finished";
                    qCDebug(VCS) << diff.diff();
                    break;
                }
            }
            lineNo++;
            hunkLines << lines.next();
        }

        // The number of filenames present in the diff should match the number
        // of hunks
        ret.push_back({ oldStart, oldCount, newStart, newCount, firstLineIdx, curSrcFileName, curTgtFileName,
                        heading, curMetadata, hunkLines });
        curMetadata.clear();
    }

    // If the diff ends with a newline, for the last hunk, when splitting into lines above
    // we will always get an empty string at the end, which we now remove
    if (diff.diff().endsWith(QLatin1Char('\n'))) {
        if (ret.size() > 0 && ret.back().lines.size() > 0) {
            ret.back().lines.pop_back();
        } else {
            qCWarning(VCS) << "Failed to parse a diff, produced no hunks";
            qCDebug(VCS) << "Failed diff:" << diff.diff();
        }
    }

    return ret;
}

class VcsDiffPrivate: public QSharedData
{
public:
    QUrl baseDiff{};
    QString diff{};
    uint depth = 0;
    std::vector<DiffHunk> hunks{};
    std::map<QString, QVariant> properties{};

    enum Dest {
        SRC = '-',
        TGT = '+',
    };

    /**
     * Maps a line position in the diff to a corresponding line position in the destination file.
     *
     * @param line a 0-based line position in the diff
     * @param dest specifies the destination file to map to:
     *             either SRC (the source file, '-') or TGT (the target file, '+')
     * @returns a @ref VcsDiff::SourceLocation whose path is the (relative to diff root)
     *          destination file path and line the 0-based line position in the
     *          destination file or {"", -1} if no such position exists.
     */
    VcsDiff::SourceLocation mapDiffLine ( const uint line, const Dest dest ) const
    {
        const QLatin1Char skipChar = (dest == SRC) ? QLatin1Char(TGT) : QLatin1Char(SRC);
        for (const auto& h : hunks) {
            if (h.containsDiffLine(line)) {
                int hunkPos = h.diffLineToHunkLine(line);

                // The line refers to the heading line
                if (hunkPos < 0)
                    return {};

                // Any lines in the diff hunk which come before line and come from the opposite
                // of dest should not be counted (they are not present in the dest)
                int skipCount = 0;
                for(int i=0; i<hunkPos; i++) {
                    if (h.lines.at(i).startsWith(skipChar))
                        skipCount++;
                }

                // Any lines in the diff hunk which come from the second part (src)/ first part (tgt)
                // of a conflict should not be counted either
                bool inConflict = false; // This is set so that a line inside a conflict is recognized as a valid line
                for(int i=0; i<hunkPos; i++) {
                    if (CONFLICT_START_RE->match(h.lines.at(i)).hasMatch()) {
                        skipCount++; // skip the conflict marker line
                        if (dest == TGT) {
                            while ((++i) < hunkPos && !CONFLICT_MID_RE->match(h.lines.at(i)).hasMatch()) {
                                skipCount++;
                            }
                        } else {
                            inConflict = true;
                        }
                    }
                    if (CONFLICT_MID_RE->match(h.lines.at(i)).hasMatch()) {
                        skipCount++; // skip the conflict marker line
                        if (dest == SRC) {
                            while ((++i) < hunkPos && !CONFLICT_END_RE->match(h.lines.at(i)).hasMatch())
                                skipCount++;
                        } else {
                            inConflict = true;
                        }
                    }
                    if (CONFLICT_END_RE->match(h.lines.at(i)).hasMatch()) {
                        skipCount++; // skip the conflict marker line
                        inConflict = false;
                    }
                }

                auto ln = h.lines[hunkPos];

                // This works around the fact that inConflict is set even if hunkPos
                // ends up hitting a conflict marker
                if (CONFLICT_RE->match(ln).hasMatch())
                    return {};

                if (ln.startsWith(dest) || ln.startsWith(QLatin1Char(' ')) || ln.isEmpty() || inConflict) {
                    if (dest == SRC)
                        // The -1 accounts for the fact that srcStart is 1-based
                        // but we need to return 0-based line numbers
                        return { h.srcFile, static_cast<int>(h.srcStart - 1 + hunkPos - skipCount) };
                    else
                        // The -1 accounts for the fact that srcStart is 1-based
                        // but we need to return 0-based line numbers
                        return { h.tgtFile, static_cast<int>(h.tgtStart - 1 + hunkPos - skipCount) };
                } else return {};
            }
        }
        return {};
    }
};

void VcsDiff::setDiffHeader(const QString& header)
{
    auto headerLines = header.split(QLatin1Char('\n'));
    auto oldHeader = diffHeader();
    QString newSrc = header + d->diff.mid(oldHeader.size());
    int adjustBy = headerLines.size()-oldHeader.split(QLatin1Char('\n')).size();
    for(auto& hunk : d->hunks) {
        hunk.headingLineIdx += adjustBy;
    }
    d->diff = newSrc;
}

QString VcsDiff::diffHeader() const
{
    if (d->hunks.size() == 0) {
        return d->diff;
    }
    auto diffLines = d->diff.split(QLatin1Char('\n')).mid(0, d->hunks[0].headingLineIdx-2);
    return diffLines.join(QLatin1Char('\n'));
}

enum LineType {
    ADD = '+',
    DEL = '-',
    CTX = ' ',
    NO_NEWLINE = '\\'
};

void VcsDiff::removeRange(const unsigned int startLine, const unsigned int endLine)
{
    QStringList resultLines;
    std::vector<DiffHunk> newHunks;
    for(const auto& hunk: d->hunks) {

        // We keep the hunks before and after the range as is
        if (hunk < startLine || endLine < hunk) {
            auto hunk_copy = hunk;
            resultLines << hunk.formatFileName(hunk.srcFile, DiffHunk::FileNameType::Source);
            resultLines << hunk.formatFileName(hunk.tgtFile, DiffHunk::FileNameType::Target);
            hunk_copy.headingLineIdx = resultLines.size();
            resultLines << hunk.formatHeader(hunk.srcStart, hunk.srcCount, hunk.tgtStart, hunk.tgtCount, hunk.heading);
            resultLines += hunk.lines;
            newHunks.push_back(hunk_copy);
            continue;
        }

        // We drop the hunks which are completely contained in the range
        if (startLine <= hunk && hunk <= endLine) {
            continue;
        }

        // If we are here, hunk must intersect the [startLine, endLine] range
        // We split the hunk into two hunks---preHunk and postHunk--so that
        //
        //   preHunk  = hunk \cap [0,startLine)
        //   postHunk = hunk cap (endLine, infty)
        //
        // i.e. preHunk is everything from hunk which comes before the removed range
        // and postHunk is everything from hunk which comes after the removed range


        // This will hold the counts for computing the pre/post hunk headers
        std::map<LineType, uint> counts = {
            {ADD, 0},
            {DEL, 0},
            {CTX, 0},
            {NO_NEWLINE, 0}
        };

        // Iterate over the lines in hunk that come before the removed range
        // and convert them to a new hunk
        int hunkLineIdx = 0;
        QStringList preLines; /**< lines in preHunk */
        for(; hunk.hunkLineToDiffLine(hunkLineIdx) < startLine; hunkLineIdx++) {
            const auto& line = hunk.lines[hunkLineIdx];
            LineType tp = line.length() > 0 ? (LineType) line[0].toLatin1() : (LineType) 0;
            counts[tp]++;
            preLines.push_back(line);
        }

        if (! preLines.empty()) {
            DiffHunk preHunk = {
                hunk.srcStart, counts[CTX] + counts[DEL],
                hunk.tgtStart, counts[CTX] + counts[ADD],
                (uint) resultLines.count() + 2,
                hunk.srcFile, hunk.tgtFile,
                hunk.heading, hunk.metadata,
                preLines
            };

            resultLines.push_back(preHunk.formatFileName(DiffHunk::FileNameType::Source));
            resultLines.push_back(preHunk.formatFileName(DiffHunk::FileNameType::Target));
            resultLines.push_back(preHunk.formatHeader());
            resultLines += preLines;
        }

        // Reset the counts
        counts[ADD] = counts[DEL] = counts[CTX] = counts[NO_NEWLINE];

        // Iterate over the lines in hunk that come after the removed range
        // and convert them to a new hunk
        hunkLineIdx = hunk.diffLineToHunkLine(endLine)+1;
        QStringList postRemovedLines;
        for(; hunkLineIdx < hunk.lines.size(); hunkLineIdx++) {
            const auto& line = hunk.lines[hunkLineIdx];
            LineType tp = line.length() > 0 ? (LineType) line[0].toLatin1() : (LineType) 0;
            counts[tp]++;
            postRemovedLines.push_back(line);
        }

        if (! postRemovedLines.empty()) {
            auto postSrcLoc = d->mapDiffLine(endLine+1, VcsDiffPrivate::Dest::SRC);
            auto postTgtLoc = d->mapDiffLine(endLine+1, VcsDiffPrivate::Dest::TGT);
            DiffHunk postHunk = {
                (uint) postSrcLoc.line, counts[CTX] + counts[DEL],
                (uint) postTgtLoc.line, counts[CTX] + counts[ADD],
                (uint) resultLines.count() + 2,
                hunk.srcFile, hunk.tgtFile,
                hunk.heading, hunk.metadata,
                postRemovedLines
            };

            resultLines.push_back(postHunk.formatFileName(DiffHunk::FileNameType::Source));
            resultLines.push_back(postHunk.formatFileName(DiffHunk::FileNameType::Target));
            resultLines.push_back(postHunk.formatHeader());
            resultLines += postRemovedLines;
        }
    }
    if (resultLines.size() > 2) {
        d->hunks = std::move(newHunks);
        d->diff = resultLines.join(QLatin1Char('\n'))+QLatin1Char('\n');
    } else {
        d->hunks.clear();
        d->diff = QString{};
    }
}

void VcsDiff::rebaseOnto(const KDevelop::VcsDiff& p)
{
    auto origDiffLines = d->diff.split(QLatin1Char('\n'));
    for(auto& hunk : d->hunks) {
        auto updatedHunkLoc = p.sourceToTarget({
            /* .path = */ hunk.srcFile,
            /* .line = */ static_cast<int>(hunk.srcStart)-1
        });
        hunk.srcFile = updatedHunkLoc.path;
        hunk.srcStart = updatedHunkLoc.line+1;

        // NOTE: Since we are dealing with "change" patches (i.e. changes to a single file,
        // as opposed to differences between two distinct files), we assume here that
        // srcStart == tgtStart;
        hunk.tgtStart = updatedHunkLoc.line+1;

        origDiffLines[hunk.headingLineIdx] = hunk.formatHeader();
    }
    d->diff = origDiffLines.join(QLatin1Char('\n'))+QLatin1Char('\n');
}



KDevelop::VcsDiff VcsDiff::subDiffHunk(const uint line, DiffDirection dir) const
{
    auto hunks = d->hunks;
    for (const auto& hunk : hunks) {
        if (hunk.containsDiffLine(line)) {
            return subDiff(hunk.headingLineIdx, hunk.lastLineIdx(), dir);
        }
    }

    VcsDiff emptyDiff;
    emptyDiff.setBaseDiff(d->baseDiff);
    emptyDiff.setDepth(d->depth);
    emptyDiff.setDiff(d->diff.mid(0,d->diff.indexOf(QStringLiteral("@@"))));
    return emptyDiff;
}

KDevelop::VcsDiff VcsDiff::subDiff(const uint startLine, const uint endLine, DiffDirection dir) const
{
    // Code adapted from cola/diffparse.py
    VcsDiff ret;
    ret.setBaseDiff(baseDiff());
    ret.setDepth(depth());


    auto hunks = d->hunks;
    QStringList lines;
    for (const auto& hunk : hunks) {
        // Skip hunks before the first line
        if (hunk < startLine)
            continue;

        // Skip hunks after the last line
        if (hunk > endLine)
            break;

        std::map<LineType, int> counts = {
            {ADD, 0},
            {DEL, 0},
            {CTX, 0},
            {NO_NEWLINE, 0}
        };
        QStringList filteredLines;

        // Will be set if the previous line in a hunk was
        // skipped because it was not in the selected range
        bool prevSkipped = false;

        uint lnIdx = hunk.headingLineIdx;

        // Store the number of skipped lines which start the hunk
        // (i.e. lines before a first deletion (addition in case of reverse)
        // so that we can adjust the start appropriately
        int startOffset = 0;
        const auto _lines = QStringList(hunk.lines.constBegin(), hunk.lines.constEnd());
        for(const auto& line: _lines) {
            lnIdx++;
            LineType tp = line.length() > 0 ? (LineType) line[0].toLatin1() : (LineType) 0;
            QString content = line.mid(1);

            if (dir == Reverse) {
                if (tp == ADD) tp = DEL;
                else if (tp == DEL) tp = ADD;
            }

            if (lnIdx < startLine || endLine < lnIdx) {
                // skip additions (or deletions if reverse) that are not in range
                if (tp == ADD) {
                    prevSkipped=true;
                    // If we are before the range and
                    // so far we only encountered ADD (or DEL, in case of reverse) lines
                    // these will not be included in the subdiff hunk so we increase the startOffset
                    if (lnIdx < startLine && counts[CTX] == 0) startOffset++;
                    continue;
                }
                // change deletions (or additions if reverse) that are not in range into context
                if (tp == DEL) tp=CTX;
            }

            // If the line immediately before a "No newline" line was
            // skipped (because it was an unselected addition) skip
            // the "No newline" line as well.
            if (tp == NO_NEWLINE && prevSkipped ) {
                if (lnIdx <= endLine ) startOffset++;
                continue;
            }

            // Empty lines are context lines and we
            // preserve them
            if ((int)tp == 0) {
                filteredLines << content;
                tp = CTX;
            } else {
                filteredLines << QLatin1Char(tp)+content;
            }
            counts[tp]++;
            prevSkipped = false;
        }

        // Skip hunks which have only context lines
        if (counts[ADD] + counts[DEL] == 0)
            continue;


        // Compute the start & counts of the hunks
        uint subSrcStart, subTgtStart;
        if (dir == Reverse) {
            subSrcStart = hunk.tgtStart + startOffset;
            subTgtStart = hunk.srcStart + startOffset;
        } else {
            subSrcStart = hunk.srcStart + startOffset;
            subTgtStart = hunk.tgtStart + startOffset;
        }
        uint subSrcCount = counts[CTX] + counts[DEL];
        uint subTgtCount = counts[CTX] + counts[ADD];

        // Prepend metadata
        lines << QStringLiteral("subdiff: ") + QString::number(startLine) + QStringLiteral(" ") + QString::number(endLine);
        for(const auto& m: hunk.metadata) {
            lines << QStringLiteral("orig: ")+m;
        }

        // Prepend lines identifying the source files
        lines << hunk.formatFileName(((dir == Reverse) ? hunk.tgtFile : hunk.srcFile), DiffHunk::FileNameType::Source);
        lines << hunk.formatFileName(((dir == Reverse) ? hunk.srcFile : hunk.tgtFile), DiffHunk::FileNameType::Target);

        lines << DiffHunk::formatHeader(subSrcStart, subSrcCount, subTgtStart, subTgtCount, hunk.heading);
        lines += filteredLines;
    }
    if (lines.size() > 3)
        ret.setDiff(lines.join(QLatin1Char('\n'))+QLatin1Char('\n'));
    return ret;
}

KDevelop::VcsDiff VcsDiff::splitOffHunk(const uint line)
{
    VcsDiff ret;
    ret.setBaseDiff(d->baseDiff);
    ret.setDepth(d->depth);

    auto origDiffLines = d->diff.split(QLatin1Char('\n'));

    std::vector<DiffHunk> remainingHunks;
    QString remainingSource;

    int newHunkStart = 2;
    for(auto& hunk: d->hunks) {
        if (hunk.containsDiffLine(line)) {
            ret.d->diff = hunk.toString();
            hunk.headingLineIdx=2;
            ret.d->hunks = {hunk};
            continue;
        }
        hunk.headingLineIdx = newHunkStart;
        remainingHunks.push_back(hunk);
        remainingSource += hunk.toString();
        newHunkStart+=hunk.lines.size()+2;
    }
    d->hunks = std::move(remainingHunks);
    d->diff = remainingSource;
    return ret;
}

KDevelop::VcsDiff VcsDiff::splitOff(const QList<QString> files)
{
    VcsDiff ret;
    ret.setBaseDiff(d->baseDiff);
    ret.setDepth(d->depth);

    std::vector<DiffHunk> remainingHunks, splitHunks;
    QString remainingSource, splitSource;

    int remainingHunkOffset = 2;
    int splitHunkOffset = 2;
    for(auto& hunk: d->hunks) {
        if (files.contains(hunk.srcFile) || files.contains(hunk.tgtFile)) {
            hunk.headingLineIdx = splitHunkOffset;
            splitHunks.push_back(hunk);
            splitSource += hunk.toString();
            splitHunkOffset += (hunk.lines.size()+2);
        } else {
            hunk.headingLineIdx = remainingHunkOffset;
            remainingHunks.push_back(hunk);
            remainingSource += hunk.toString();
            remainingHunkOffset += (hunk.lines.size()+2);
        }
    }
    d->hunks = std::move(remainingHunks);
    d->diff = remainingSource;
    ret.d->hunks = std::move(splitHunks);
    ret.d->diff = splitSource;
    return ret;
}

KDevelop::VcsDiff VcsDiff::splitOff(const uint startLine, const uint endLine)
{
    auto ret = subDiff(startLine, endLine, DiffDirection::Normal);
    removeRange(startLine, endLine);
    return ret;
}

QList<KDevelop::VcsDiff> VcsDiff::split(const QList<QString> files, const KDevelop::VcsDiff::SplitOptions opts) const
{
    auto remainDiff = *this;
    QList<KDevelop::VcsDiff> ret;
    auto push_split_op = (opts & SplitOption::SplitDown) ?
        &QList<KDevelop::VcsDiff>::push_front : &QList<KDevelop::VcsDiff>::push_back;
    auto push_remain_op = (opts & SplitOption::SplitDown) ?
        &QList<KDevelop::VcsDiff>::push_back : &QList<KDevelop::VcsDiff>::push_front;

    if ( opts & SplitOption::DiffPerFile) {
        for(const auto& file: files) {
            (ret.*push_split_op)(remainDiff.splitOff({file}));
        }
    } else {
        (ret.*push_split_op)(remainDiff.splitOff(files));
    }
    (ret.*push_remain_op)(remainDiff);

    return ret;
}

QList<KDevelop::VcsDiff> VcsDiff::split(const uint hunkLine, const KDevelop::VcsDiff::SplitOptions opts) const
{
    auto remainDiff = *this;
    QList<KDevelop::VcsDiff> ret;
    auto push_split_op = (opts & SplitOption::SplitDown) ?
        &QList<KDevelop::VcsDiff>::push_front : &QList<KDevelop::VcsDiff>::push_back;
    auto push_remain_op = (opts & SplitOption::SplitDown) ?
        &QList<KDevelop::VcsDiff>::push_back : &QList<KDevelop::VcsDiff>::push_front;
    (ret.*push_split_op)(remainDiff.splitOffHunk(hunkLine));
    (ret.*push_remain_op)(remainDiff);
    return ret;
}

QList<KDevelop::VcsDiff> VcsDiff::split(const uint lineRangeFirst, const uint lineRangeLast, const KDevelop::VcsDiff::SplitOptions opts) const
{
    auto remainDiff = *this;
    auto split = remainDiff.splitOff(lineRangeFirst, lineRangeLast);

    if (opts & SplitOption::SplitDown) {
        remainDiff.rebaseOnto(split);
        return {split, remainDiff};
    } else {
        split.rebaseOnto(remainDiff);
        return {remainDiff, split};
    }
}



const QVector<VcsDiff::FilePair> VcsDiff::fileNames() const
{
    QVector<VcsDiff::FilePair> ret;
    VcsDiff::FilePair current;
    for (const auto& h : d->hunks) {
        // List each pair only once
        if (h.srcFile == current.source && h.tgtFile == current.target)
            continue;
        current = { h.srcFile, h.tgtFile };
        ret.push_back(current);
    }
    return ret;
}

VcsDiff::SourceLocation VcsDiff::diffLineToSource ( const uint line ) const
{
    return d->mapDiffLine(line, VcsDiffPrivate::SRC);
}

VcsDiff::SourceLocation VcsDiff::diffLineToTarget ( const uint line ) const
{
    return d->mapDiffLine(line, VcsDiffPrivate::TGT);
}

KDevelop::VcsDiff::SourceLocation VcsDiff::sourceToTarget(const KDevelop::VcsDiff::SourceLocation& loc) const
{
    auto diffLine = sourceLocationToDiffLine(loc);
    if (diffLine > -1) {
        return diffLineToTarget(diffLine);
    }
    auto delta = 0;
    for(const auto& h: d->hunks) {
        if (h < loc) {
            delta += h.lineCountDelta();
        }
    }
    return {/*.path=*/loc.path, /*.line=*/loc.line+delta};
}


int VcsDiff::sourceLocationToDiffLine(const KDevelop::VcsDiff::SourceLocation& loc) const
{
    if (!loc.isValid())
        return -1;

    for(const auto& h: d->hunks) {

        // The hunk concerns a different file
        if (h.srcFile != loc.path)
            continue;

        // Hunk applies to a later part of the file. (we continue, since we do not want to
        // assume that the hunks are ordered; although, for sane diffs, they will be)
        if ((uint) (loc.line+1) < h.srcStart)
            continue;

        // The potential location in the hunk
        int delta = (loc.line+1)-h.srcStart;

        // If the hunk is small, we skip it
        if (delta > h.lines.size())
            continue;

        int posInHunk = 0;
        for(; posInHunk < h.lines.size() && delta > 0; ++posInHunk) {
            // skip lines added by diff
            if (h.lines[posInHunk].startsWith(QLatin1Char('+')))
                continue;
            delta--;
        }

        // We need to check this, since we may have skipped some hunk lines
        if (delta == 0) {
            return h.headingLineIdx+posInHunk+1;
        }
    }

    return -1;
}

int VcsDiff::targetLocationToDiffLine(const KDevelop::VcsDiff::SourceLocation& loc) const
{
    if (!loc.isValid())
        return -1;

    for(const auto& h: d->hunks) {

        // The hunk concerns a different file
        if (h.tgtFile != loc.path)
            continue;

        // Hunk applies to a later part of the file. (we continue, since we do not want to
        // assume that the hunks are ordered; although, for sane diffs, they will be)
        if ((uint) (loc.line+1) < h.tgtStart)
            continue;

        // The potential location in the hunk
        int delta = (loc.line+1)-h.tgtStart;

        // If the hunk is small, we skip it
        if (delta > h.lines.size())
            continue;

        int posInHunk = 0;
        for(; posInHunk < h.lines.size() && delta > 0; ++posInHunk) {
            // skip lines deleted by diff
            if (h.lines[posInHunk].startsWith(QLatin1Char('-')))
                continue;
            delta--;
        }

        // We need to check this, since we may have skipped some hunk lines
        if (delta == 0) {
            return h.headingLineIdx+posInHunk+1;
        }
    }

    return -1;
}


VcsDiff::VcsDiff()
    : d(new VcsDiffPrivate)
{
}

VcsDiff::~VcsDiff() = default;

VcsDiff::VcsDiff( const VcsDiff& rhs )
    : d(rhs.d)
{
}

VcsDiff::VcsDiff( const QString& diff )
    : VcsDiff()
{
    setDiff(diff);
}


bool VcsDiff::isEmpty() const
{
    return d->diff.isEmpty();
}

QString VcsDiff::diff() const
{
    return d->diff;
}


void VcsDiff::setDiff( const QString& s )
{
    d->diff = s;
    d->hunks = parseHunks(*this);
}

VcsDiff& VcsDiff::operator=( const VcsDiff& rhs)
{
    d = rhs.d;
    return *this;
}

QUrl VcsDiff::baseDiff() const
{
    return d->baseDiff;
}

uint VcsDiff::depth() const
{
    return d->depth;
}

void VcsDiff::setBaseDiff(const QUrl& url)
{
    d->baseDiff=url;
}

void VcsDiff::setDepth(const uint depth)
{
    d->depth = depth;
}

void VcsDiff::setProperty(const QString& name, const QVariant& value)
{
    d->properties[name] = value;
}

QVariant VcsDiff::property(const QString& name) const
{
    auto it = d->properties.find(name);
    if (it != d->properties.end()) {
        return it->second;
    }
    return {};
}

QList<QString> VcsDiff::properties() const
{
    QList<QString> ret;
    for(const auto& [key, val]: d->properties) {
        Q_UNUSED(val);
        ret.push_back(key);
    }
    return ret;
}

QDebug operator<<(QDebug debug, const KDevelop::VcsDiff& diff)
{
    debug << "====== DIFF START ======";
    debug.noquote() << "\n";
    int p = 0;
    for(const auto&ln: diff.diff().split(QLatin1Char('\n'))) {
        debug << p << ln;
        debug.noquote() << "\n";
        ++p;
    }
    debug << "====== DIFF END ======";
    debug.noquote() << "\n";
    return debug;
}

QDebug operator<<(QDebug debug, const DiffLine& diffLine)
{
    auto& [diff, line] = diffLine;
    debug << "====== DIFF START ======";
    debug.noquote() << "\n";
    int p = 0;
    for(const auto&ln: diff.diff().split(QLatin1Char('\n'))) {
        if (p == line) {
            debug << "->" << ln;
            debug.noquote() << "\n";
        } else {
            debug << p << ln;
            debug.noquote() << "\n";
        }
        ++p;
    }
    debug << "====== DIFF END ======";
    debug.noquote() << "\n";
    return debug;
}


}

