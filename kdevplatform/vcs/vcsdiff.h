/*
    SPDX-FileCopyrightText: 2007 Andreas Pakulat <apaku@gmx.de>
    SPDX-FileCopyrightText: 2007 Matthew Woehlke <mw_triad@users.sourceforge.net>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef KDEVPLATFORM_VCSDIFF_H
#define KDEVPLATFORM_VCSDIFF_H

#include <QDebug>
#include <QSharedDataPointer>
#include <QtGlobal>
#include <QMetaType>
#include <QVariant>
#include <QVector>


#include "vcsexport.h"

class QUrl;
class QString;

namespace KDevelop
{

/**
 * A class representing a unified diff, possibly with conflict markers.
 *
 * A diff is assumed to be a collection of hunks, where each hunk has the
 * following structure:
 *
 *   METADATA
 *   --- a/SOURCE_PATH
 *   +++ b/TARGET_PATH
 *   HUNK1 HEADER
 *   HUNK1 CONTENT
 *   [METADATA]
 *   HUNK2 HEADER
 *   HUNK2 CONTENT
 *   ...
 *
 * METADATA are lines which start with anything except for a '+', '-' and ' '.
 * The path specifications may optionally precede a hunk and are assumed to
 * apply to all following hunks until a new path specification
 * is found. These indicate the files for which the diff is generated.
 *
 * Content up to the first hunk is considered metadata and is ignored.
 *
 * Hunk Header
 * ------------
 *
 * Each hunk header has the following form
 *
 *   @@ -SRC_OFFSET[, SRC_CHANGES_COUNT] +TGT_OFFSET[, TGT_CHANGES_COUNT] @@ Heading
 *
 * where the SRC_OFFSET is a 1-based line index pointing to the source file where
 * the hunk applies and TGT_OFFSET is a 1-based line index pointing to the target
 * file where the hunk applies. The optional SRC_CHANGES_COUNTS (assumed to be 1
 * if not present) specifies the number of context lines plus the number of
 * deleted lines. Similarly, the optional TGT_CHANGES_COUNT specifies the
 * number of context lines plus the number of added lines. The Heading, used as a
 * visual aid for users, is supposed to show the line where the nearest enclosing
 * function scope of the hunk starts.
 *
 * Hunk Content
 * ------------
 *
 * The hunk content is a collection of lines which starting with '+' (additions),
 * '-' (deletions) and ' ' (context lines; empty lines are also take to be context
 * lines). Additionally, a hunk may contain conflict markers which are of the form
 *
 *   >>>>>>> our ref
 *   our content
 *   ...
 *   =======
 *   their content
 *   ...
 *   <<<<<<< their ref
 *
 * and indicate unresolved conflicts.
 *
 * NOTE: Metadata is currently not parsed.
 *
 */
class KDEVPLATFORMVCS_EXPORT VcsDiff
{
public:
    /* Used to represent a patch or its inverse */
    enum DiffDirection {
          Normal = 0        /**< the unchanged patch */
        , Foward = 0        /**< the unchanged patch */
        , Reverse = 1       /**< the inverse of the patch (i.e. a new patch which, when applied, undoes the old patch) */
    };

    VcsDiff();
    virtual ~VcsDiff();
    VcsDiff( const VcsDiff& );
    VcsDiff( const QString& diff );

    /**
     * @returns the source of the diff.
     */
    QString diff() const;
    
    /** @returns the base directory of the diff. */
    QUrl baseDiff() const;

	/**
	 * Depth - number of directories to left-strip from paths in the patch - see "patch -p"
     * Defaults to 0
	 */
    uint depth() const;

    /** Sets the base directory of the diff to the @p url */
    void setBaseDiff(const QUrl& url);

    /** Sets the depth of the diff to @p depth */
    void setDepth(const uint depth);

    /** Sets a property @p name on the diff (similar to @see QObject::setProperty) */
    void setProperty(const QString& name, const QVariant& value);

    /**
     * Retrieves property @p name set on the diff. If a property with the name wasn't set,
     * returns an empty QVariant
     */
    QVariant property(const QString& name) const;

    /**
     * Returns a list of property names that were set on the diff
     * (@see VcsDiff::setProperty)
     */
    QList<QString> properties() const;

    /**
     * Sets the diff source and parses it.
     *
     * @param diff the diff in unified diff format
     */
    void setDiff( const QString& diff);

    /**
     * Replaces all lines from the diff source up to the first hunk
     * with the provided header.
     *
     * @note: Hunk positions are updated accordingly.
     */
    void setDiffHeader(const QString& header);

    /**
     * Returns all lines from the diff source before the first hunk.
     */
    QString diffHeader() const;

    VcsDiff& operator=( const VcsDiff& rhs);
    
    /** @returns whether or not there are changes in the diff */
    bool isEmpty() const;

    /**
     * Changes the diff so that it applies to the file to which patch @p p
     * is applied first.
     */
    void rebaseOnto(const VcsDiff& p);

    /**
     * Creates a standalone diff containing the differences in a given range.
     *
     * @returns a diff containing only the changes from the current diff
     *  which are in the range startLine-endLine (in the diff text)
     *
     * @param startLine 0-based line number (in the diff) of the first line in the range
     * @param endLine 0-based line number (in the diff) of the last line in the range
     * @param dir if set to Reverse, the role of src and tgt are reversed, i.e. a diff is
     *            generated which can be applied to the target to get the source.
     *
     * TODO: rename to subDiffLines (or keep and rename subDiffHunk?)
     */
    VcsDiff subDiff(const uint startLine, const uint endLine, DiffDirection dir = Normal) const;


    /**
     * Removes the differences in the given range from the current diff and returns them
     * as a new diff.
     *
     * @returns a diff containing only the changes from the current diff
     *  which are in the range @p startLine - @p endLine (in the diff text)
     *
     * @param startLine 0-based line number (in the diff) of the first line in the range
     * @param endLine 0-based line number (in the diff) of the last line in the range
     *
     * @note: The returned diff might not have any (!) metadata (e.g. the command used to
     * generate the diff, any commit message, ...). Also, all metadata from the current
     * diff <b>might be discarded (!)</b>.
     *
     * TODO: Rename to splitOffLines/splitOffLineRange (or keep and rename splitOffHunk ?)
     */
    VcsDiff splitOff(const uint startLine, const uint endLine);

    /**
     * Removes the differences touching files in the given list and returns them
     * as a new diff.
     *
     * @returns a diff containing only the changes from the current diff
     * which touch some file from @p files
     *
     * @param files a list of relative file paths changes to which will be extracted
     *
     * @note: The returned diff might not have any (!) metadata (e.g. the command used to
     * generate the diff, any commit message, ...). Also, all metadata from the current
     * diff <b>might be discarded (!)</b>.
     *
     * TODO: Rename to splitOffFiles (or keep and rename splitOffHunk ?)
     */
    VcsDiff splitOff(const QList<QString> files);

    /**
     * Removes a single hunk, identified by a containing line, from the current diff
     * and returns it as a new diff.
     *
     * @returns a diff containing only the given hunk
     *
     * @param line 0-based line number (in the diff) identifying the hunk which contains it
     *
     * @note: The returned diff might not have any (!) metadata (e.g. the command used to
     * generate the diff, any commit message, ...). Also, all metadata from the current
     * diff <b>might be discarded (!)</b>.
     *
     * TODO: Keep name or rename to splitOff?
     */
    VcsDiff splitOffHunk(const uint line);


    /**
     * Option flags to specify the behaviour of the split methods.
     */
    enum class SplitOption {
        Default = 0x0,       /**< Default options (equivalent to SplitUp | GroupFiles) */

        /* Split ordering */
        SplitUp = 0x0,      /**< The indicated range is returned second (default) */
        SplitDown = 0x1,    /**< The indicated range is returned first */

        /* Split granularity */
        GroupFiles = 0x0,   /**< The split produces a single diff for all files (default) */
        DiffPerFile = 0x2,  /**< The split produces a separate diff for each file */
    };
    Q_DECLARE_FLAGS(SplitOptions, SplitOption)

    /**
     * Splits a diff into two diffs, one consisting of the hunk containing the line @p hunkline,
     * and the other consisting of the remaining hunks.
     *
     * Depending on @p opts, the diff with the remaining hunks is either returned first (default)
     * or second (if the @ref SplitOption::SplitDown flag is set).
     *
     * @note: The order in which the diffs are returned will also affect the actual content of the
     * diffs, in particular, the line numbers (!). This is a consequence of the fact that applying
     * diffs is not a commutative operation.
     *
     * @note: The @ref SplitOption::GroupFiles and @ref SplitOption::DiffPerFile flags do not make
     * sense here and are ignored.
     */
    QList<VcsDiff> split(const uint hunkLine, const SplitOptions opts = SplitOption::Default) const;

    /**
     * Splits a diff into two diffs, one consisting of the changes introduced by the specified
     * range of lines in the diff and the second containing the remaining changes.
     *
     * @param lineRangeFirst the first line in the range (0-based line number in the diff)
     * @param lineRangeLast the last line in the range (0-based line number in the diff)
     * @param opts determines the order in which the two diffs are returned: the remaining
     *             changes are returned first (by default) unless the
     *             @ref SplitOption::SplitDown flag is set.
     *
     *
     * @note: The order in which the diffs are returned will also affect the actual content of the
     * diffs, in particular, the line numbers (!). This is a consequence of the fact that applying
     * diffs is not a commutative operation.
     *
     * @note: The @ref SplitOption::GroupFiles and @ref SplitOption::DiffPerFile flags do not make
     * sense here and are ignored.
     */
    QList<VcsDiff> split(const uint lineRangeFirst, const uint lineRangeLast, const SplitOptions opts = SplitOption::Default) const;

    /**
     * Splits a diff into a series of diffs: diffs containing changes to the specified files, and a
     * diff containing the remaining changes.
     *
     * @param files a list of file names
     * @param opts determines how many diffs are returned and their order;
     *
     *             By default only two diffs are returned with the changes to the specified files
     *             all grouped into a single diff. If @ref SplitOption::DiffPerFile is set, the
     *             changes to the specified files are returned as a series of diffs, one per file.
     *
     *             By default the remaining changes are returned first, and while the changes to the
     *             specified files are returned second. This can be changed by setting the
     *             @ref SplitOption::SplitDown flag.
     *
     * @note: In this case, the order in which the diffs are returned does not affect the actual
     * content of the diffs, because applying diffs to different files **is** a commutative operation.
     *
     * @note: The @ref SplitOption::GroupFiles and @ref SplitOption::DiffPerFile flags do not make
     * sense here and are ignored.
     */
    QList<VcsDiff> split(const QList<QString> files, const SplitOptions opts = SplitOption::Default) const;

    /**
     * Creates a new standalone diff from a single hunk identified by a containing line.
     *
     * @returns a diff containing only the changes from hunk containing
     * the line the line
     *
     * @param line 0-based line number (in the diff) of the line in the hunk
     * @param dir if set to Reverse, the role of src and tgt are reversed, i.e. a diff is
     *            generated which can be applied to the target to get the source.
     *
     * @note: The returned diff might not have any (!) metadata (e.g. the command used
     * to generate the diff, any commit SHAs...).
     *
     * TODO: keep name or rename to subDiff?
     */
    VcsDiff subDiffHunk(const uint line, DiffDirection dir = Normal) const;

    /**
     * A struct representing a position in a source file.
     *
     * @note This should eventually be replaced with a @ref KDevelop::DocumentCursor,
     * which, however, currently cannot be used in this file.
     *
     * @note if line is negative, the location represents an invalid
     * location
     */
    struct SourceLocation {
        /** Path to the source file (may be relative) */
        QString path = {};

        /** 0-based line number in the source file */
        int line = -1;

        bool isValid() const {return 0 <= line;}
    };

    /**
     * Maps a line position in the diff to a corresponding line position in the source file.
     *
     * @param line a 0-based line position in the diff
     * @returns a @ref SourceLocation whose path is the target file path (relative to diff root)
     *          and line the 0-based line position in the target file or {"", -1} if no such
     *          position exists.
     */
    SourceLocation diffLineToSource (const uint line) const;

    /**
     * Maps a line position in the diff to a corresponding line position in the target file.
     *
     * @param line a 0-based line position in the diff
     * @returns a @ref SourceLocation whose path is the source file path (relative to diff root)
     *          and line the 0-based line position in the source file or {"", -1} if no such
     *          position exists.
     */
    SourceLocation diffLineToTarget (const uint line) const;

    /**
     * Maps a position in the source file to a corresponding position in the target file.
     *
     * @param loc a @ref SourceLocation to transform
     * @returns a @ref SourceLocation in the target or an invalid source location, if the
     *          line in @p loc was deleted by this diff.
     */
    SourceLocation sourceToTarget(const SourceLocation& loc) const;

    /**
     * Maps a line position in the source file to a corresponding line position in the diff.
     *
     * @param loc a @ref SourceLocation indicating the location in the source file.
     * @returns a 0-based line position in the diff, if the location corresponds to a line in the diff
     *          or -1 otherwise
     */
    int sourceLocationToDiffLine(const SourceLocation& loc) const;

    /**
     * Maps a line position in the target file to a corresponding line position in the diff.
     *
     * @param loc a @ref SourceLocation indicating the location in the target file.
     * @returns a 0-based line position in the diff, if the location corresponds to a line in the diff
     *          or -1 otherwise
     */
    int targetLocationToDiffLine(const SourceLocation& loc) const;

    /**
     * Represents a pair of files which are compared
     */
    struct FilePair {
        QString source;
        QString target;
        bool operator==(const FilePair p) const {return (source == p.source && target == p.target); }
    };

    /**
     * @returns a list of filename pairs that the patch applies to
     *
     * @note: Each file-pair is only listed once for each consecutive run
     * of hunks which apply to it.
     * @note: the filenames are relative to the base of the diff.
     */
    const QVector<FilePair> fileNames() const;

protected:
    /**
     * Removes a range from the diff.
     *
     * @param startLine 0-based line number (in the diff) of the first line in the range
     * @param endLine 0-based line number (in the diff) of the last line in the range
     */
    void removeRange(const unsigned int startLine, const unsigned int endLine);

private:
    QSharedDataPointer<class VcsDiffPrivate> d;
};

struct DiffLine {
    KDevelop::VcsDiff diff;
    int line;
};

KDEVPLATFORMVCS_EXPORT QDebug operator<<(QDebug debug, const KDevelop::VcsDiff&);
KDEVPLATFORMVCS_EXPORT QDebug operator<<(QDebug debug, const KDevelop::DiffLine&);

}


Q_DECLARE_METATYPE( KDevelop::VcsDiff )
Q_DECLARE_TYPEINFO( KDevelop::VcsDiff, Q_MOVABLE_TYPE );
Q_DECLARE_TYPEINFO( KDevelop::VcsDiff::FilePair, Q_MOVABLE_TYPE );

Q_DECLARE_OPERATORS_FOR_FLAGS(KDevelop::VcsDiff::SplitOptions)

#endif

