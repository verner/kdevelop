/*
    SPDX-FileCopyrightText: 2007 Andreas Pakulat <apaku@gmx.de>
    SPDX-FileCopyrightText: 2007 Matthew Woehlke <mw_triad@users.sourceforge.net>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef KDEVPLATFORM_VCSJOB_H
#define KDEVPLATFORM_VCSJOB_H

#include <QVariant>

#include <outputview/outputjob.h>

#include "vcsexport.h"

class QVariant;

namespace KDevelop
{

class IPlugin;
class VcsJobPrivate;

/**
 * This class provides an extension of KJob to get various VCS-specific
 * information about the job. This includes the type, the state
 * and the results provided by the job.
 *
 */
class KDEVPLATFORMVCS_EXPORT VcsJob : public OutputJob
{
    Q_OBJECT
public:
    explicit VcsJob( QObject* parent = nullptr, OutputJobVerbosity verbosity = OutputJob::Verbose);
    ~VcsJob() override;
    /**
     * To easily check which type of job this is.
     *
     * @todo Check how this can be extended via plugins, maybe use QFlag? (not
     * QFlags!)
     */
    enum JobType
    {
        Unknown = -1    /**< Unknown job type (default)*/,
        Add = 0         /**< An add job */,
        Remove = 1      /**< A remove job */,
        Copy = 2        /**< A copy job */,
        Move = 3        /**< A move job */,
        Diff = 4        /**< A diff job */,
        Commit = 5      /**< A commit job */,
        Update = 6      /**< An update job */,
        Merge = 7       /**< A merge job */,
        Resolve = 8     /**< A resolve job */,
        Import = 9      /**< An import job */,
        Checkout = 10   /**< A checkout job */,
        Log = 11        /**< A log job */,
        Push = 12       /**< A push job */,
        Pull = 13       /**< A pull job */,
        Annotate = 14   /**< An annotate job */,
        Clone = 15      /**< A clone job */,
        Status = 16     /**< A status job */,
        Revert = 17     /**< A revert job */,
        Cat = 18        /**< A cat job */,
        Reset = 19      /**< A reset job */,
        Apply = 20      /**< An apply job */,
        Ignore = 21     /**< An ignore job */,
        UserType = 1000 /**< A custom job */
    };

    /**
     * Simple enum to define how the job finished.
     */
    enum JobStatus
    {
        JobRunning = 0    /**< The job is running */,
        JobSucceeded = 1  /**< The job succeeded */,
        JobCanceled = 2   /**< The job was cancelled */,
        JobFailed = 3     /**< The job failed */,
        JobNotStarted = 4 /**< The job is not yet started */
    };

    /**
     * This method will return all new results of the job. The actual data
     * type that is wrapped in the QVariant depends on the type of job.
     *
     * @note Results returned by a previous call to fetchResults are not
     * returned.
     */
    virtual QVariant fetchResults() = 0;

    /**
     * Find out in which state the job is. It can be running, canceled,
     * failed or finished
     *
     * @return the status of the job
     * @see JobStatus
     */
    virtual JobStatus status() const = 0;

    /**
     * Used to find out about the type of job.
     *
     * @return the type of job
     */
    JobType type() const;

    /**
     * Used to get at the version control plugin. The plugin
     * can be used to get one of the interfaces to execute
     * more vcs actions, depending on this job's results
     * (like getting a diff for an entry in a log)
     */
    virtual KDevelop::IPlugin* vcsPlugin() const = 0;

    /**
     * This can be used to set the type of the vcs job in subclasses.
     */
    void setType( JobType );

Q_SIGNALS:
    /**
     * This signal is emitted when new results are available. Depending on
     * the plugin and the operation, it may be emitted only once when all
     * results are ready, or several times.
     */
    void resultsReady( KDevelop::VcsJob* );

private Q_SLOTS:
    void delayedModelInitialize();

private:
    const QScopedPointer<class VcsJobPrivate> d_ptr;
    Q_DECLARE_PRIVATE(VcsJob)
};

/**
 * A VcsJob interface to run a function like a job.
 *
 * This can be used when one doesn't want to define a class in order to run
 * a simple function. The vcs interface models operations as jobs. Mostly,
 * these jobs run an external program (e.g. git), to perform the operation.
 * Typically a plugin will define a job class to run this program. However,
 * in some situations, its easier to implement the operation as a simple
 * function (e.g. adding a line to a .gitignore file).
 *
 * The @ref FunctionVcsJob is designed to be used in these situations. Just
 * pass the function (can also be a lambda) to the constructor and return
 * the job. The class will take care of running the function, when the start
 * method runs and storing its result (a @ref QVariant) for later retrieval.
 *
 * The following example illustrates how one would use it:
 *
 * <code>
 *      MyPlugin::doSimpleStuff(param) {
 *
 *          ... do_some_setup, prepare arguments, etc ...
 *
 *          auto* job = new FunctionVcsJob(this, [=](auto* the_job){
 *
 *                  ... do_simple_stuff, compute result ...
 *
 *                  if (error) {
 *                       the_job->setStatus(VcsJob::Failed);
 *                       return QVariant();
 *                   }
 *
 *                   return QVariant(result);
 *           }
 *
 *           // Return the job for later execution
 *           // e.g. via ICore::self()->runController()->registerJob(job);
 *           return job;
 *      }
 *
 * </code>
 *
 */
class KDEVPLATFORMVCS_EXPORT FunctionVcsJob: public KDevelop::VcsJob
{
    Q_OBJECT
public:
    /**
     * Constructs a new job which runs @p job when instructed. The @p parent
     * argument should be the plugin which creates the job (this is returned
     * when the @ref vcsPlugin method is called)
     */
    FunctionVcsJob(KDevelop::IPlugin* parent, std::function<QVariant (FunctionVcsJob*)> job);

    /**
     * Returns the QVariant returned by the called function (after the job has finished).
     */
    QVariant fetchResults() override {return m_result;}

    /**
     * This actually runs the function provided to the constructor.
     */
    void start() override;

    /**
     * This can be used by the function to report its status (e.g. error/success)
     * upon termination
     */
    void setStatus(JobStatus status){m_status=status;};

    /**
     * Returns the status of the job.
     *
     * @note: This will be either @ref JobNotStarted (prior to @ref start being called),
     * @ref JobSucceeded after the start method was called if the function did not
     * explicitly call @ref setStatus, or whatever the function provided to the
     * @ref setStatus call.
     */
    JobStatus status() const override { return m_status; }

    /**
     * Returns the plugin which created this job.
     *
     * @note: This just returns the argument provided to the constructor
     */
    KDevelop::IPlugin* vcsPlugin() const override { return m_plugin; }


protected:
    KDevelop::IPlugin* m_plugin;
    JobStatus m_status;
    std::function<QVariant (FunctionVcsJob*)> m_functionJob;
    QVariant m_result;
};


}

#endif

