// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2021 Jonathan Verner <jonathan@temno.eu>
// SPDX-License-Identifier: LGPL-2.1-or-later

#include "vcseventitemdelegate.h"

#include "../models/vcseventmodel.h"

#include <QTextDocument>
#include <QPainter>

using namespace KDevelop;

void VcsEventItemDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if (index.data(VcsEventLogTreeModel::VcsDataRole).canConvert<KDevelop::VcsEvent>()) {
        QTextDocument ed;
        ed.setHtml(index.data(Qt::DisplayRole).toString());
        if (option.state & QStyle::State_Selected)
            painter->fillRect(option.rect, option.palette.highlight());
        painter->save();
        painter->translate(option.rect.x(), option.rect.y());
        ed.drawContents(painter);
        painter->restore();
        return;
    }
    QStyledItemDelegate::paint(painter, option, index);
}

QSize VcsEventItemDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if (index.data(VcsEventLogTreeModel::VcsDataRole).canConvert<KDevelop::VcsEvent>()) {
        QTextDocument ed;
        ed.setHtml(index.data(Qt::DisplayRole).toString());
        return ed.size().toSize();

//             QTextEdit edd(nullptr);
//             edd.setHtml(index.data(Qt::DisplayRole).toString());
//             return edd.sizeHint();
    }
    return QStyledItemDelegate::sizeHint(option, index);
}

// QWidget* VcsEventItemDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const
// {
// }
//
// void VcsEventItemDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
// {
// }
//
// void VcsEventItemDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
// {
// }
