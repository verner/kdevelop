/*
    SPDX-FileCopyrightText: 2021 Jonathan L. Verner <jonathan.verner@matfyz.cz>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef KDEVELOP_VCSEVENTITEMDELEGATE_H
#define KDEVELOP_VCSEVENTITEMDELEGATE_H

#include <vcs/vcsexport.h>

#include <QStyledItemDelegate>

namespace KDevelop
{

/**
 * @todo write docs
 */
class KDEVPLATFORMVCS_EXPORT VcsEventItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:

    void paint ( QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index ) const override;

    QSize sizeHint ( const QStyleOptionViewItem& option, const QModelIndex& index ) const override;

//     /**
//      * @todo write docs
//      *
//      * @param parent TODO
//      * @param option TODO
//      * @param index TODO
//      * @return TODO
//      */
//     QWidget* createEditor ( QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index ) const override;
//
//     /**
//      * @todo write docs
//      *
//      * @param editor TODO
//      * @param index TODO
//      * @return TODO
//      */
//     void setEditorData ( QWidget* editor, const QModelIndex& index ) const override;
//
//     /**
//      * @todo write docs
//      *
//      * @param editor TODO
//      * @param model TODO
//      * @param index TODO
//      * @return TODO
//      */
//     void setModelData ( QWidget* editor, QAbstractItemModel* model, const QModelIndex& index ) const override;

};

}

#endif // KDEVELOP_VCSEVENTITEMDELEGATE_H
