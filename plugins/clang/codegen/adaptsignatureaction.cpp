/*
    SPDX-FileCopyrightText: 2009 David Nolden <david.nolden.kdevelop@art-master.de>
    SPDX-FileCopyrightText: 2014 Kevin Funk <kfunk@kde.org>

    SPDX-License-Identifier: LGPL-2.0-only
*/

#include "adaptsignatureaction.h"
#include "codegenhelper.h"

#include "../duchain/duchainutils.h"
#include "../util/clangdebug.h"

#include <language/assistant/renameaction.h>
#include <language/codegen/documentchangeset.h>
#include <language/duchain/duchain.h>
#include <language/duchain/duchainlock.h>
#include <language/duchain/duchainutils.h>
#include <language/duchain/functiondefinition.h>

#include <interfaces/icore.h>
#include <interfaces/iuicontroller.h>
#include <sublime/message.h>
// KF
#include <KLocalizedString>

using namespace KDevelop;

AdaptSignatureAction::AdaptSignatureAction(const DeclarationId& definitionId,
                                           const ReferencedTopDUContext& definitionContext,
                                           const Signature& oldSignature,
                                           const Signature& newSignature,
                                           bool editingDefinition,
                                           const QList<RenameAction*>& renameActions)
    : m_otherSideId(definitionId)
    , m_otherSideTopContext(definitionContext)
    , m_oldSignature(oldSignature)
    , m_newSignature(newSignature)
    , m_editingDefinition(editingDefinition)
    , m_renameActions(renameActions)
{
}

AdaptSignatureAction::~AdaptSignatureAction()
{
    qDeleteAll(m_renameActions);
}

QString AdaptSignatureAction::description() const
{
    return m_editingDefinition ? i18n("Update declaration signature") : i18n("Update definition signature");
}

QString AdaptSignatureAction::toolTip() const
{
    DUChainReadLocker lock;
    auto declaration = m_otherSideId.declaration(m_otherSideTopContext.data());
    if (!declaration) {
        return {};
    }
    KLocalizedString msg = m_editingDefinition
                         ? ki18n("Update declaration signature\nfrom: %1\nto: %2")
                         : ki18n("Update definition signature\nfrom: %1\nto: %2");
    msg = msg.subs(CodegenHelper::makeSignatureString(declaration, m_oldSignature, m_editingDefinition));
    msg = msg.subs(CodegenHelper::makeSignatureString(declaration, m_newSignature, !m_editingDefinition));
    return msg.toString();
}

void AdaptSignatureAction::execute()
{
    // CRASH BACKTRACE:
// #8  0x00007fefeedf4fe5 in qt_assert(char const*, char const*, int) (assertion=<optimized out>, file=<optimized out>, line=<optimized out>) at ../../include/QtCore/../../src/corelib/global/qlogging.h:90
// #9  0x00007fef411cb9b0 in AdaptSignatureAction::execute() (this=0x560c7d467fe0) at /home/jonathan/zdroj/clones/kdev/plugins/clang/codegen/adaptsignatureaction.cpp:71
// #10 0x00007fefed994ea0 in KDevelop::ProblemNavigationContext::executeAction(int) (this=0x560ca18bb8e0, index=0) at /home/jonathan/zdroj/clones/kdev/kdevplatform/language/duchain/navigation/problemnavigationcontext.cpp:246
// #11 0x00007fefed994daa in KDevelop::ProblemNavigationContext::executeKeyAction(QString const&) (this=0x560ca18bb8e0, key=...) at /home/jonathan/zdroj/clones/kdev/kdevplatform/language/duchain/navigation/problemnavigationcontext.cpp:231
// #12 0x00007fefed99ca8c in KDevelop::AbstractNavigationContext::execute(KDevelop::NavigationAction const&) (this=0x560ca18bb8e0, action=...) at /home/jonathan/zdroj/clones/kdev/kdevplatform/language/duchain/navigation/abstractnavigationcontext.cpp:171
// #13 0x00007fefed99e00f in KDevelop::AbstractNavigationContext::acceptLink(QString const&) (this=0x560ca18bb8e0, link=...) at /home/jonathan/zdroj/clones/kdev/kdevplatform/language/duchain/navigation/abstractnavigationcontext.cpp:475
// #14 0x00007fefed99a10f in KDevelop::AbstractNavigationWidgetPrivate::anchorClicked(QUrl const&) (this=0x560c8c104720, url=...) at /home/jonathan/zdroj/clones/kdev/kdevplatform/language/duchain/navigation/abstractnavigationwidget.cpp:273
// #15 0x00007fefed998fd0 in KDevelop::AbstractNavigationWidget::<lambda(const QUrl&)>::operator()(const QUrl &) const (__closure=0x560c71e059e0, url=...) at /home/jonathan/zdroj/clones/kdev/kdevplatform/language/duchain/navigation/abstractnavigationwidget.cpp:109
// #16 0x00007fefed99b1b0 in QtPrivate::FunctorCall<QtPrivate::IndexesList<0>, QtPrivate::List<const QUrl&>, void, KDevelop::AbstractNavigationWidget::initBrowser(int)::<lambda(const QUrl&)> >::call(KDevelop::AbstractNavigationWidget::<lambda(const QUrl&)> &, void **) (f=..., arg=0x7ffc6f9a8e90) at /usr/include/x86_64-linux-gnu/qt5/QtCore/qobjectdefs_impl.h:146
// #17 0x00007fefed99b11c in QtPrivate::Functor<KDevelop::AbstractNavigationWidget::initBrowser(int)::<lambda(const QUrl&)>, 1>::call<QtPrivate::List<QUrl const&>, void>(KDevelop::AbstractNavigationWidget::<lambda(const QUrl&)> &, void *, void **) (f=..., arg=0x7ffc6f9a8e90) at /usr/include/x86_64-linux-gnu/qt5/QtCore/qobjectdefs_impl.h:256
// #18 0x00007fefed99b0c0 in QtPrivate::QFunctorSlotObject<KDevelop::AbstractNavigationWidget::initBrowser(int)::<lambda(const QUrl&)>, 1, QtPrivate::List<const QUrl&>, void>::impl(int, QtPrivate::QSlotObjectBase *, QObject *, void **, bool *) (which=1, this_=0x560c71e059d0, r=0x560c993bf620, a=0x7ffc6f9a8e90, ret=0x0) at /usr/include/x86_64-linux-gnu/qt5/QtCore/qobjectdefs_impl.h:443
    ENSURE_CHAIN_NOT_LOCKED             // FIXME: This seems to fail in some cases
    DUChainReadLocker lock;
    IndexedString url = m_otherSideTopContext->url();
    lock.unlock();
    m_otherSideTopContext = DUChain::self()->waitForUpdate(url, TopDUContext::AllDeclarationsContextsAndUses);
    if (!m_otherSideTopContext) {
        clangDebug() << "failed to update" << url.str();
        return;
    }

    lock.lock();

    Declaration* otherSide = m_otherSideId.declaration(m_otherSideTopContext.data());
    if (!otherSide) {
        clangDebug() << "could not find definition";
        return;
    }
    DUContext* functionContext = DUChainUtils::functionContext(otherSide);
    if (!functionContext) {
        clangDebug() << "no function context";
        return;
    }
    if (!functionContext || functionContext->type() != DUContext::Function) {
        clangDebug() << "no correct function context";
        return;
    }

    DocumentChangeSet changes;
    KTextEditor::Range parameterRange = ClangIntegration::DUChainUtils::functionSignatureRange(otherSide);
    QString newText = CodegenHelper::makeSignatureString(otherSide, m_newSignature, !m_editingDefinition);
    if (!m_editingDefinition) {
        // append a newline after the method signature in case the method definition follows
        newText += QLatin1Char('\n');
    }

    DocumentChange changeParameters(functionContext->url(), parameterRange, QString(), newText);
    lock.unlock();
    changeParameters.m_ignoreOldText = true;
    changes.addChange(changeParameters);
    changes.setReplacementPolicy(DocumentChangeSet::WarnOnFailedChange);
    DocumentChangeSet::ChangeResult result = changes.applyAllChanges();
    if (!result) {
        const QString messageText = i18n("Failed to apply changes: %1", result.m_failureReason);
        auto* message = new Sublime::Message(messageText, Sublime::Message::Error);
        ICore::self()->uiController()->postMessage(message);
    }
    emit executed(this);

    for (RenameAction* renAct : m_renameActions) {
        renAct->execute();
    }
}

#include "moc_adaptsignatureaction.cpp"
