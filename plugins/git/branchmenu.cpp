/*
 *  SPDX-FileCopyrightText: 2023 Jonathan L. Verner <jonathan.verner@matfyz.cz>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */

#include "branchmenu.h"

#include <KLocalizedString> // for i18n

#include <QMenu>

#include <algorithm>        // for std::sort

void BranchHierarchyNode::addBranch(BranchName& name) {
    if (name.m_current == name.m_components.end()) {
        return;
    }
    auto child = m_children.find(*name.m_current);
    if (child == m_children.end()) {
        child = m_children.insert(*name.m_current, BranchHierarchyNode{name.pathToCurrent(), *name.m_current});
    }
    name.m_current++;
    child->addBranch(name);
}

void BranchHierarchyNode::constructMenu(QMenu* parent, const BranchAction& act) const
{
    size_t count = 0;
    auto keys = m_children.keys();
    std::sort(keys.begin(), keys.end(), [](auto a, auto b) {
        return a.toString() < b.toString();
    });
    for(const auto& name: keys) {
        count++;
        if (count > MAX_BRANCHES_TO_SHOW ) {
            parent = parent->addMenu(i18n("More..."));
            count = 0;
        }
        const auto ch = m_children.find(name);
        const auto bn = ch->pathToLeaf();
        if( ! bn.isEmpty() ) {
            const QString branchName = m_fullName + QLatin1Char('/') + bn;
            parent->addAction(bn, parent, [branchName, act] {
                act(branchName);
            });
        } else {
            auto subMenu = parent->addMenu(name.toString());
            ch->constructMenu(subMenu, act);
        }
    }

}

