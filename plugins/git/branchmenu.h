/*
 *  SPDX-FileCopyrightText: 2023 Jonathan L. Verner <jonathan.verner@matfyz.cz>
 *
 *  SPDX-License-Identifier: LGPL-2.0-or-later
 */
#pragma once

#include <QChar>        // for QLatin1Char
#include <QString>
#include <QStringRef>
#include <QHash>
#include <QVector>

#include <functional>   // for std::function

class QMenu;

/**
 * A helper struct for simulating some of C++20's ranges features.
 * When processing branches, each branch name is split into its
 * '/'-separated components and the resulting QVector of references
 * is stored in the struct together with a position in this vector
 * (which acts as a poor-man's substitute for a subrange
 *
 */
struct BranchName {
    const QStringRef m_name;             /**< the full name of the branch, including the '/' component separators */
    QVector<QStringRef> m_components; /**< the '/'-separated components of the branch name */

    /** a "pointer" into the component vector, used as a
     *  subrange, i.e. as [current, components::end)
     */
    QVector<QStringRef>::const_iterator m_current;

    BranchName(const QStringRef& name)
    : m_name(name)
    , m_components(m_name.split(QLatin1Char('/')))
    , m_current(m_components.cbegin())
    {}

    /**
     * Returns the initial segment of the path up to and including the current
     * component, separated by '/'.
     */
    QString pathToCurrent() {
        QString ret;
        for(auto c = m_components.cbegin(); c != m_current; ++c) {
            ret += c->toString() + QLatin1Char('/');
        }
        ret += m_current->toString();
        return ret;
    }
};


/**
 * We consider each branch name to be a "virtual" path, where path
 * components are separated by '/' as in normal paths. In this interpretation,
 * a list of branches corresponds to a file-system like hierarchical tree
 * structur, e.g., given branches
 *
 *    work/test/a
 *    work/test/b
 *    work/WIP/git-plugin-branching
 *    review/MR-1
 *
 * would correspond to the following tree
 *                             + a
 *           + work   +  test  + b
 *  +        + WIP    +  git-plugin-branching
 *           + review +  MR-1
 *
 * The @ref BranchHierarchyNode structure corresponds to a node in the
 * tree.
 */
class BranchHierarchyNode {
private:
    QString m_fullName = {};     /**< The full path up to and including the node name */
    QStringRef m_nodeName = {};  /**< The last component of the full path of the node */

    /**
     * An optional list of child nodes, keyed by the first
     * component of their name differing from (extending) the
     * current node name
     */
    QHash<QStringRef, BranchHierarchyNode> m_children = {};

public:
    BranchHierarchyNode() = default;

    BranchHierarchyNode(const QString fullName, const QStringRef nodeName)
    : m_fullName(fullName)
    , m_nodeName(nodeName)
    {}

    /**
     * Adds a single branch @p name into the hierarchy, assuming that
     * all of its components up to and including @p name.current
     * are already added.
     *
     * Works by recursively calling itself on shorter and shorter
     * tail sequences of the branch name (i.e. with increasing @p name.current)
     */
    void addBranch(BranchName& name);

    /**
     * Returns true if this node is a leaf node, i.e. does not have any children.
     */
    bool isLeaf() const { return m_children.empty(); };

    /**
     * Returns true, if the tree splits below this node.
     */
    bool hasSplitBelow() const {
        if (isLeaf()) return false;
        if (m_children.size() > 1) return true;
        return m_children.cbegin()->hasSplitBelow();
    }

    /**
     * Returns the path from this node up to and including the unique leaf below this
     * node, if there is no split, otherwise returns an empty string.
     */
    QString pathToLeaf() const {
        if (isLeaf()) return m_nodeName.toString();
        if (m_children.size() == 1) {
            return m_nodeName.toString() + QLatin1Char('/') + m_children.cbegin()->pathToLeaf();
        }
        return {};
    }

    /**
     * The maximum number of branches to show, before adding a "More..." submenu
     */
    static constexpr size_t MAX_BRANCHES_TO_SHOW = 30;

    typedef std::function<void(const QString&)> BranchAction;

    /**
     * Adds the current branch hierarchy to menu @p parent with internal nodes
     * corresponding to submenus and leaf nodes corresponding to action items
     * which will call @p act with the full leaf node name as argument.
     *
     */
    void constructMenu(QMenu* parent, const BranchAction& act) const;
};
