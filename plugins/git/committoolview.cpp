/*
    SPDX-FileCopyrightText: 2020 Jonathan L. Verner <jonathan.verner@matfyz.cz>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "committoolview.h"

#include "branchmenu.h"
#include "diffviewsctrl.h"
#include "historydiffctrl.h"
#include "gitplugin.h"
#include "historyview.h"
#include "repostatusmodel.h"
#include "simplecommitform.h"
#include "rebasecontroller.h"
#include "stashmanagerdialog.h"

#include "debug.h"

#include <interfaces/icore.h>
#include <interfaces/idocument.h>
#include <interfaces/idocumentcontroller.h>
#include <interfaces/iplugin.h>
#include <interfaces/iproject.h>
#include <interfaces/iprojectcontroller.h>
#include <interfaces/iruncontroller.h>
#include <interfaces/iuicontroller.h>
#include <project/projectmodel.h>
#include <util/path.h>
#include <util/scopeddialog.h>
#include <vcs/vcsjob.h>

#include <KActionCollection>
#include <KColorScheme>
#include <KLocalizedString>
#include <KMessageBox>
#include <KMessageWidget>
#include <KTextEditor/Document>
#include <KTextEditor/MovingInterface>
#include <KTextEditor/MovingRange>
#include <KTextEditor/View>


#include <QAbstractItemView>
#include <QAction>
#include <QBoxLayout>
#include <QDockWidget>
#include <QInputDialog>
#include <QLineEdit>
#include <QList>
#include <QMenu>
#include <QSortFilterProxyModel>
#include <QSplitter>
#include <QStandardItemModel>
#include <QStyledItemDelegate>
#include <QTreeView>
#include <QUrl>
#include <QWidgetAction>

#include <algorithm>
#include <functional>

using namespace KDevelop;

CommitToolViewFactory::CommitToolViewFactory(
    GitPlugin* git
)
    : m_git(git)
{}

QWidget* CommitToolViewFactory::create(QWidget* parent)
{
    return new CommitToolView(parent, m_git);
}

Qt::DockWidgetArea CommitToolViewFactory::defaultPosition() const
{
    return Qt::RightDockWidgetArea;
}

QString CommitToolViewFactory::id() const
{
    return QStringLiteral("org.kdevelop.CommitToolView");
}

/**
 * A filter to be used on RepoStatusModel to hide the areas
 * (index, worktree, conflicts, untracked) which are empty
 */
class FilterEmptyItemsProxyModel : public QSortFilterProxyModel
{
public:
    using QSortFilterProxyModel::QSortFilterProxyModel;
    bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const override
    {
        const QModelIndex rowIndex = sourceModel()->index(sourceRow, 0, sourceParent);

        if (!QSortFilterProxyModel::filterAcceptsRow(sourceRow, sourceParent))
            return false;

        if (sourceModel()->hasChildren(rowIndex))
            return true;

        const auto area = rowIndex.data(RepoStatusModel::AreaRole);
        return area == RepoStatusModel::Index || area == RepoStatusModel::WorkTree || area == RepoStatusModel::Conflicts
            || area == RepoStatusModel::Untracked;
    }
};

/**
 * A style delegate to show active project in bold
 */
class ActiveStyledDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    void initStyleOption(QStyleOptionViewItem* option, const QModelIndex& idx) const override
    {
        QStyledItemDelegate::initStyleOption(option, idx);
        if (idx == m_activeProject)
            option->font.setBold(true);
    }
    /**
     * Sets the active project which will be styled in bold.
     */
    void setActive(const QModelIndex& idx) { m_activeProject = idx; }

private:
    QPersistentModelIndex m_activeProject;
};


void CommitToolView::doLayOut(const Qt::DockWidgetArea area)
{
    if (layout()) {
        delete layout();
    }

    QSplitter* _splitter;
    QBoxLayout* _layout;
    if (area == Qt::LeftDockWidgetArea || area == Qt::RightDockWidgetArea || area == Qt::NoDockWidgetArea) {
        _layout = new QHBoxLayout(this);
        _splitter = new QSplitter(Qt::Vertical, this);
        _splitter->addWidget(m_commitForm);
        _splitter->addWidget(m_inlineError);
        _splitter->addWidget(m_filter);
        _splitter->addWidget(m_view);
        _splitter->addWidget(m_historyView);
        _splitter->setStretchFactor(0, 2);
        _splitter->setStretchFactor(2, 5);
        _splitter->setStretchFactor(3, 5);
    } else {
        _layout = new QVBoxLayout(this);
        _splitter = new QSplitter(Qt::Horizontal, this);
        auto _filter_plus_view = new QSplitter(Qt::Vertical, this);
        _filter_plus_view->addWidget(m_inlineError);
        _filter_plus_view->addWidget(m_filter);
        _filter_plus_view->addWidget(m_view);
        _splitter->addWidget(m_commitForm);
        _splitter->addWidget(_filter_plus_view);
        _splitter->addWidget(m_historyView);
    }
    _layout->addWidget(_splitter);
    m_filter->setMaximumHeight(35);
    setLayout(_layout);
}


QMenu* CommitToolView::buildMenu(const QList<CommitToolView::MenuEntry>& entries) const
{
    auto* ret = new QMenu;
    for(const auto& entry: entries) {
        switch(entry.type) {
            case MenuEntry::Action:
                ret->addAction(entry.elt.action);
                break;
            case MenuEntry::Menu:
                ret->addMenu(entry.elt.menu);
                break;
            case MenuEntry::Separator:
                ret->addSeparator();
                break;
        }
    }
    return ret;
}

CommitToolView::CommitToolView ( QWidget* parent, GitPlugin* git)
    : QWidget(parent)
    , m_git(git)
    , m_statusModel(m_git->repoStatusModel())
    , m_proxyModel(new FilterEmptyItemsProxyModel(this))
    , m_commitForm(new SimpleCommitForm(this))
    , m_inlineError(new KMessageWidget)
    , m_view(new QTreeView(this))
    , m_historyView(new HistoryView(this, m_git))
    , m_filter(new QLineEdit(this))
    , m_styleDelegate(new ActiveStyledDelegate)


    /* Construct Context Menu Actions */
    , m_refreshModelAct(new QAction(QIcon::fromTheme(QStringLiteral("view-refresh")), i18n("Refresh"), this))

    , m_stageFilesAct(new QAction(QIcon::fromTheme(QStringLiteral("list-add")), i18n("Stage selected files"), this))
    , m_stageAllAct(new QAction(QIcon::fromTheme(QStringLiteral("list-add")), i18n("Stage all changed files"), this))

    , m_unstageFilesAct(new QAction(QIcon::fromTheme(QStringLiteral("list-remove")), i18n("Unstage selected files"), this))
    , m_unstageAllAct(new QAction(QIcon::fromTheme(QStringLiteral("list-remove")), i18n("Unstage all files"), this))

    , m_revertFilesAct(new QAction(QIcon::fromTheme(QStringLiteral("edit-undo")), i18n("Discard unstaged changes"), this))
    , m_revertAllAct(new QAction(QIcon::fromTheme(QStringLiteral("edit-undo")), i18n("Discard all unstaged changes"), this))

    , m_resolveOursFilesAct(new QAction(QIcon::fromTheme(QStringLiteral("vcs-stash")), i18nc("@action:inmenu", "Resolve (Ours)"), this))
    , m_resolveOursAllAct(new QAction(QIcon::fromTheme(QStringLiteral("vcs-stash")), i18nc("@action:inmenu", "Resolve All (Ours)"), this))
    , m_resolveTheirsFilesAct(new QAction(QIcon::fromTheme(QStringLiteral("vcs-stash")), i18nc("@action:inmenu", "Resolve (Theirs)"), this))
    , m_resolveTheirsAllAct(new QAction(QIcon::fromTheme(QStringLiteral("vcs-stash")), i18nc("@action:inmenu", "Resolve All (Theirs)"), this))

    , m_deleteFilesAct(new QAction(QIcon::fromTheme(QStringLiteral("user-trash")), i18nc("@action:inmenu", "Delete Selected Files"), this))
    , m_deleteAllAct(new QAction(QIcon::fromTheme(QStringLiteral("user-trash")), i18nc("@action:inmenu", "Delete All Untracked Files"), this))

    , m_ignoreFilesAct(new QAction(QIcon::fromTheme(QStringLiteral("vcs-stash")), i18nc("@action:inmenu", "Ignore Selected Files"), this))
    , m_ignoreAllAct(new QAction(QIcon::fromTheme(QStringLiteral("vcs-stash")), i18nc("@action:inmenu", "Ignore All Untracked Files"), this))

    , m_stashPushFilesAct(new QAction(QIcon::fromTheme(QStringLiteral("vcs-stash")), i18nc("@action:inmenu", "Stash Selected Files"), this))
    , m_stashPushAllAct(new QAction(QIcon::fromTheme(QStringLiteral("vcs-stash")), i18nc("@action:inmenu", "Push Stash"), this))
    , m_stashPopAct(new QAction(QIcon::fromTheme(QStringLiteral("vcs-stash-pop")), i18nc("@action:inmenu", "Pop Stash"), this))
    , m_stashManagerAct(new QAction(i18nc("@action:inmenu", "Stash Manager…"), this))

    , m_switchToNewBranchAct(new QAction(i18nc("@action:inmenu", "New Branch…"), this))
    , m_branchManagerAct(new QAction(i18nc("@action:inmenu", "Branch Manager…"), this))

    /* Assing actions to their handlers */
    , m_actionHandlers {
        {m_refreshModelAct, std::bind(&CommitToolView::refresh, this)},

        {m_stageFilesAct, std::bind(&CommitToolView::stageSelectedFiles, this, std::placeholders::_1)},
        {m_stageAllAct, std::bind(&CommitToolView::stageSelectedFiles, this, std::placeholders::_1)},

        {m_unstageFilesAct, std::bind(&CommitToolView::unstageSelectedFiles, this, std::placeholders::_1)},
        {m_unstageAllAct, std::bind(&CommitToolView::unstageSelectedFiles, this, std::placeholders::_1)},

        {m_revertFilesAct, std::bind(&CommitToolView::revertSelectedFiles, this, std::placeholders::_1)},
        {m_revertAllAct, std::bind(&CommitToolView::revertSelectedFiles, this, std::placeholders::_1)},

        {m_resolveOursFilesAct, std::bind(&CommitToolView::resolveSelectedFiles, this, Ours, std::placeholders::_1)},
        {m_resolveOursAllAct, std::bind(&CommitToolView::resolveSelectedFiles, this, Ours, std::placeholders::_1)},
        {m_resolveTheirsFilesAct, std::bind(&CommitToolView::resolveSelectedFiles, this, Theirs, std::placeholders::_1)},
        {m_resolveTheirsAllAct, std::bind(&CommitToolView::resolveSelectedFiles, this, Theirs, std::placeholders::_1)},

        {m_deleteFilesAct, std::bind(&CommitToolView::deleteSelectedFiles, this, std::placeholders::_1)},
        {m_deleteAllAct, std::bind(&CommitToolView::deleteSelectedFiles, this, std::placeholders::_1)},

        {m_ignoreFilesAct, std::bind(&CommitToolView::ignoreSelectedFiles, this, std::placeholders::_1)},
        {m_ignoreAllAct, std::bind(&CommitToolView::ignoreSelectedFiles, this, std::placeholders::_1)},

        {m_stashPushFilesAct, std::bind(&CommitToolView::stashSelectedFiles, this, std::placeholders::_1)},
        {m_stashPushAllAct, std::bind(&CommitToolView::stashSelectedFiles, this, std::placeholders::_1)},
        {m_stashPopAct, std::bind(&CommitToolView::stashPopActiveProject, this)},
        {m_stashManagerAct, std::bind(&CommitToolView::showStashManager, this)},

        {m_branchManagerAct, std::bind(&CommitToolView::showBranchManager, this)},
        {m_switchToNewBranchAct, std::bind(&CommitToolView::createNewBranch, this)},
    }

    /* Construct the "Switch to Branch" menu */
    , m_branchesMenu(new QMenu(i18n("Switch to branch"), this))

    /* The structure of the project menu */
    , m_projectRootEntries({
        { MenuEntry::Separator },
        { m_branchesMenu },
        { MenuEntry::Separator },
        { m_stashPushAllAct },
        { m_stashPopAct },
        { m_stashManagerAct },
        { MenuEntry::Separator },
        { m_refreshModelAct },
    })

    /* Construct the treeview context menus */
    , m_contextMenus({
        {RepoStatusModel::ProjectRoot, buildMenu(m_projectRootEntries)},
        {RepoStatusModel::IndexRoot, buildMenu(QList<MenuEntry>({
            {m_unstageAllAct},
        }) + m_projectRootEntries)},
        {RepoStatusModel::Index, buildMenu(QList<MenuEntry>({
            {m_unstageFilesAct},
        }) + m_projectRootEntries)},
        {RepoStatusModel::ConflictsRoot, buildMenu(QList<MenuEntry>({
            {m_resolveOursAllAct},
            {m_resolveTheirsAllAct},
            {m_stageAllAct},
        }) + m_projectRootEntries)},
        {RepoStatusModel::Conflicts, buildMenu(QList<MenuEntry>({
            {m_resolveOursFilesAct},
            {m_resolveTheirsFilesAct},
            {m_stageFilesAct},
        }) + m_projectRootEntries)},
        {RepoStatusModel::WorkTreeRoot, buildMenu(QList<MenuEntry>({
            {m_stageAllAct},
            {m_revertAllAct},
        }) + m_projectRootEntries)},
        {RepoStatusModel::WorkTree, buildMenu(QList<MenuEntry>({
            {m_stageFilesAct},
            {m_revertFilesAct},
            {m_stashPushFilesAct},
        }) + m_projectRootEntries)},
        {RepoStatusModel::UntrackedRoot, buildMenu(QList<MenuEntry>({
            {m_stageAllAct},
            {m_ignoreAllAct},
            {m_deleteAllAct},
        }) + m_projectRootEntries)},
        {RepoStatusModel::Untracked, buildMenu(QList<MenuEntry>({
            {m_stageFilesAct},
            {m_ignoreFilesAct},
            {m_deleteFilesAct},
        }) + m_projectRootEntries)},
    })


{
    setWindowIcon(QIcon::fromTheme(QStringLiteral("git")));


    // FIXME: We should get the current area dock area from somewhere (the sublime area?)
    //        Right now we initially assume it is in the Qt::RightWidgetArea and layout
    //        accordingly (this may be wrong if the user previously moved it to, e.g. the
    //        bottom so its restored in the bottom area); when the user moves the dock
    //        widget we re-layout it and then the layout is always correct, since the signal
    doLayOut(Qt::RightDockWidgetArea);
    connect(dynamic_cast<QDockWidget*>(parent), &QDockWidget::dockLocationChanged, this, &CommitToolView::doLayOut);

    // Creates a proxy model so that we can filter the
    // items by the text entered into the filter lineedit
    m_proxyModel->setSourceModel(m_statusModel);
    m_proxyModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    m_proxyModel->setSortCaseSensitivity(Qt::CaseInsensitive);
    m_proxyModel->setSortRole(Qt::DisplayRole);
    m_proxyModel->setRecursiveFilteringEnabled(true);

    m_filter->setToolTip(i18n("Filter by filename/project name"));
    m_filter->setPlaceholderText(i18n("Filter by filename/project name"));
    m_filter->setClearButtonEnabled(true);

    connect(m_filter, &QLineEdit::textEdited, m_proxyModel, &QSortFilterProxyModel::setFilterWildcard);


    // Set up the error widget
    m_inlineError->setHidden(true);
    m_inlineError->setMessageType(KMessageWidget::Error);
    m_inlineError->setCloseButtonVisible(true);
    m_inlineError->setWordWrap(true);

    // Sets up the view
    m_view->setModel(m_proxyModel);
    m_view->setHeaderHidden(true);
    m_view->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_view->setSelectionMode(QAbstractItemView::SelectionMode::ContiguousSelection);
    m_view->setContextMenuPolicy(Qt::CustomContextMenu);
    m_view->setAnimated(true);
    m_view->setItemDelegate(m_styleDelegate);

    connect(m_view, &QTreeView::customContextMenuRequested, this, &CommitToolView::popupContextMenu);
    connect(m_view, &QTreeView::doubleClicked, this, &CommitToolView::dblClicked);
    connect(m_view, &QTreeView::clicked, this, &CommitToolView::clicked);
    connect(m_view, &QTreeView::expanded, this, &CommitToolView::activateProject);


    // Disable the "Switch to Branch" menu (will be enabled by the refresh method)
    // and connect the branch actions
    m_branchesMenu->setDisabled(true);

    // Disable stash actions (will be enabled by the refresh method if appropriate)
    for(auto* act: {m_stashPushAllAct, m_stashPopAct, m_stashManagerAct}) act->setDisabled(true);


    // Refresh diffs when documents are saved
    connect(ICore::self()->documentController(), &IDocumentController::documentSaved, this,
            [=](KDevelop::IDocument* doc) {
                emit updateUrlDiffs(doc->url());
    });


    // Connect the commit form
    // TODO: Create a commit controller?
    connect(m_commitForm, &SimpleCommitForm::committed, this, &CommitToolView::commitActiveProject);

    connect(m_commitForm, &SimpleCommitForm::optionChanged, this, [=](const auto opt, const bool value) {
        if (opt == GitPlugin::CommitOption::AmendLastCommit) {
            if (auto* proj = activeProject()) {
                if (value) {
                    if (auto history = m_git->historyModelFor(proj)) {
                        auto event = history->data(history->index(0,0), VcsEventLogTreeModel::VcsDataRole).value<VcsEvent>();
                        m_commitForm->setSummary(event.messageSummary());
                        m_commitForm->setExtendedDescription(event.messageBody());
                    }
                } else {
                    m_commitForm->setSummary(QString{});
                    m_commitForm->setExtendedDescription(QString{});
                }
            }
        }
    });

    // Disable the commit button if the active project has no staged changes
    // TODO: Move this to SimpleCommitForm
    connect(m_statusModel, &QAbstractItemModel::rowsRemoved, this, [=](const QModelIndex& parent) {
        if (parent.data(RepoStatusModel::AreaRole) == RepoStatusModel::IndexRoot
            && m_statusModel->itemFromIndex(parent)->rowCount() == 0 && isActiveProject(parent.parent()))
            m_commitForm->disableCommitButton();
    });
    connect(m_statusModel, &QAbstractItemModel::rowsInserted, this, [=](const QModelIndex& parent) {
        if (parent.data(RepoStatusModel::AreaRole) == RepoStatusModel::IndexRoot
            && m_statusModel->itemFromIndex(parent)->rowCount() > 0 && isActiveProject(parent.parent()))
            m_commitForm->enableCommitButton();
    });

    // Connect the show source signal
    // TODO: Move this into a private function and remove the signal?
    connect(this, &CommitToolView::showSource, this, [=](const QUrl& url, const KTextEditor::Range& range) {
        if (url.fileName().isEmpty()) return;
        auto* docCtrl = ICore::self()->documentController();
        if (auto* srcDoc = docCtrl->openDocument(url, range)) {
            docCtrl->activateDocument(srcDoc);
        }
    });

    // Connect the DiffViewsCtrl
    connect(this, &CommitToolView::updateUrlDiffs, m_git->areaDiffViewController(), &DiffViewsCtrl::updateUrlDiffs);
    connect(this, &CommitToolView::updateProjectDiffs, m_git->areaDiffViewController(), &DiffViewsCtrl::updateProjectDiffs);
    connect(this, &CommitToolView::updateDiff, m_git->areaDiffViewController(), [this](auto url, auto area){
        m_git->areaDiffViewController()->updateDiff(url, area, DiffViewsCtrl::NoActivate);
    });
    connect(this, &CommitToolView::showDiff, m_git->areaDiffViewController(), [this](auto url, auto area){
            m_git->areaDiffViewController()->updateDiff(url, area, DiffViewsCtrl::Activate);
    });

    connect(m_historyView, &HistoryView::populateCommitForm, this, &CommitToolView::populateCommitForm);
    refresh();
}



KDevelop::IProject* CommitToolView::activeProject() const
{
    auto* proj_item = activeProjectItem();
    if (proj_item && isActiveProject(proj_item->index())) {
        return ICore::self()->projectController()->findProjectByName(
            proj_item->data(RepoStatusModel::NameRole).toString());
    }
    return nullptr;
}

QStandardItem* CommitToolView::activeProjectItem() const
{
    for (auto* pr : m_statusModel->projectRoots()) {
        if (isActiveProject(pr->index()))
            return pr;
    }
    return nullptr;
}

bool CommitToolView::isActiveProject(const QModelIndex& idx) const
{
    return (m_view->isExpanded(m_proxyModel->mapFromSource(idx)));
}

void CommitToolView::activateProject(const QModelIndex& idx)
{
    if (idx.data(RepoStatusModel::AreaRole).value<RepoStatusModel::Areas>() == RepoStatusModel::ProjectRoot) {
        if (m_historyView->rebaseInProgress()) {
            m_historyView->showError(i18n("Please finish the current rebase before switching projects"));
            return;
        }
        m_styleDelegate->setActive(idx);
        auto repoIdx = m_proxyModel->mapToSource(idx);
        for (const auto* pr : m_statusModel->projectRoots()) {
            if (pr->index() != repoIdx)
                m_view->collapse(m_proxyModel->mapFromSource(pr->index()));
        }
        m_commitForm->setProjectName(idx.data(RepoStatusModel::NameRole).toString());
        m_commitForm->setBranchName(idx.data(RepoStatusModel::BranchNameRole).toString());
        m_commitForm->clearError();
        m_commitForm->enable();
        if (m_statusModel->projectItem(m_statusModel->itemFromIndex(repoIdx)).index->rowCount() == 0)
            m_commitForm->disableCommitButton();
        else
            m_commitForm->enableCommitButton();

        auto* project = ICore::self()->projectController()->findProjectByName(idx.data(RepoStatusModel::NameRole).toString());

        if (! m_git->readConfigOption(project->path().toUrl(), QLatin1String("user.signingkey")).isEmpty()) {
            m_commitForm->setDefaultOptions(GitPlugin::CommitOption::SignCommit);
        }
        m_commitForm->resetOptions();

        m_historyView->setProject(project);
        refresh();
    }
}

void CommitToolView::populateCommitForm(const QString& message)
{
    m_commitForm->setSummary(VcsEvent::messageSummary(message));
    m_commitForm->setExtendedDescription(VcsEvent::messageBody(message));
    m_commitForm->clearError();
    m_commitForm->enable();
}

void CommitToolView::popupContextMenu(const QPoint& pos)
{
    QList<QUrl> urls;
    const QModelIndexList selectionIdxs = m_view->selectionModel()->selectedIndexes();

    // Get the active area
    QModelIndex idx = selectionIdxs.isEmpty() ? m_view->indexAt(pos) : selectionIdxs[0];

    // Get the context menu appropriate for the area
    RepoStatusModel::Areas area = idx.data(RepoStatusModel::AreaRole).value<RepoStatusModel::Areas>();
    QMenu* menu = m_contextMenus[area];
    if (! menu)  {
        menu = m_contextMenus[RepoStatusModel::ProjectRoot];
    }

    // Extract urls for selected files (if any)
    for (const QModelIndex& idx : selectionIdxs) {
        if (idx.column() == 0) {
            // Check that idx is a file & is located in the appropriate area
            if (idx.parent().isValid() && idx.data(RepoStatusModel::AreaRole).value<RepoStatusModel::Areas>() == area)
                urls += idx.data(RepoStatusModel::UrlRole).value<QUrl>();
        }
    }

    if (urls.isEmpty()) urls = getAreaFilesForActiveProject(area);

    // No files are selected and the current area is empty,
    // so we only show the project root menu
    if (urls.isEmpty()) {
        menu = m_contextMenus[RepoStatusModel::ProjectRoot];
    }

    QAction* res = menu->exec(m_view->viewport()->mapToGlobal(pos));
    if (auto handler = m_actionHandlers[res]) {
        handler(urls);
    }
}

void CommitToolView::showError(const QString& error)
{
    m_inlineError->setText(error);
    m_inlineError->animatedShow();
}

void CommitToolView::clearError()
{
    if (!m_inlineError->isHidden() && !m_inlineError->isHideAnimationRunning()) {
        m_inlineError->animatedHide();
    }
}

void CommitToolView::showBranchManager()
{
    if (auto* proj = activeProject()) {
        if (auto* dvcs = proj->versionControlPlugin()->extension<KDevelop::DistributedVersionControlPlugin>()) {
            dvcs->showBranchManager(proj->path().pathOrUrl());
        }
    }
}

void CommitToolView::showStashManager()
{
    if (auto* proj = activeProject()) {
        if (auto* git = proj->versionControlPlugin()->extension<GitPlugin>()) {
            ScopedDialog<StashManagerDialog> stashManager(
                QDir(proj->path().toLocalFile()),
                git,
                this
            );
            stashManager->exec();
        }
    }
}

void CommitToolView::dblClicked ( const QModelIndex& idx )
{
    // A different action is performed based on where the
    // file that was double-clicked on is.
    switch (idx.data(RepoStatusModel::AreaRole).value<RepoStatusModel::Areas>()) {

    // Files in the staging area are unstaged
    case RepoStatusModel::Index:
        unstageSelectedFiles({ idx.data(RepoStatusModel::UrlRole).toUrl() });
        break;

    // Files in the other areas are staged for commit
    // (including marking conflicts as resolved and adding the
    //  untracked files into the repo)
    case RepoStatusModel::WorkTree:
    case RepoStatusModel::Conflicts:
    case RepoStatusModel::Untracked:
        idx.data(RepoStatusModel::UrlRole).toUrl();
        stageSelectedFiles({ idx.data(RepoStatusModel::UrlRole).toUrl() });
        break;

    // Do nothing special for roots
    case RepoStatusModel::ProjectRoot:
    case RepoStatusModel::IndexRoot:
    case RepoStatusModel::ConflictsRoot:
    case RepoStatusModel::WorkTreeRoot:
    case RepoStatusModel::UntrackedRoot:
    case RepoStatusModel::None:
        break;
    }
}

void CommitToolView::clicked ( const QModelIndex& idx )
{
    auto url = idx.data(RepoStatusModel::UrlRole).toUrl();
    auto projectUrl = idx.data(RepoStatusModel::ProjectUrlRole).toUrl();

    switch (idx.data(RepoStatusModel::AreaRole).value<RepoStatusModel::Areas>()) {
    case RepoStatusModel::ProjectRoot:
        emit activateProject(idx);
        break;
    case RepoStatusModel::IndexRoot:
        emit showDiff(projectUrl, RepoStatusModel::IndexRoot);
        break;
    case RepoStatusModel::Index:
        emit showDiff(url, RepoStatusModel::Index);
        break;
    case RepoStatusModel::WorkTreeRoot:
        emit showDiff(projectUrl, RepoStatusModel::WorkTreeRoot);
        break;
    case RepoStatusModel::WorkTree:
        emit showDiff(url, RepoStatusModel::WorkTree);
        break;
    case RepoStatusModel::Conflicts: {
        auto vcsplugin = vcsPluginForUrl(url);
        auto conflicts = vcsplugin->findConflicts(url);
        if (! conflicts.empty()) {
            KTextEditor::Cursor mid = {conflicts[0].middleMarker, 0};
            emit showSource(url, {mid, mid});
            break;
        }
        emit showSource(url, KTextEditor::Range::invalid());
        break;
    }
    case RepoStatusModel::Untracked:
        emit showSource(url);
        break;
    case RepoStatusModel::ConflictsRoot:
    case RepoStatusModel::UntrackedRoot:
    case RepoStatusModel::None:
        break;
    }
}

IBasicVersionControl* CommitToolView::vcsPluginForUrl ( const QUrl& url ) const
{
    IProject* project = ICore::self()->projectController()->findProjectForUrl(url);
    IPlugin* vcsplugin = project ? project->versionControlPlugin() : nullptr;
    return vcsplugin ? vcsplugin->extension<IBasicVersionControl>() : nullptr;
}

QList<QUrl> CommitToolView::getAreaFilesForActiveProject(RepoStatusModel::Areas area)
{
    QList<QUrl> ret;
    if (auto* proj = activeProject()) {
        auto projItem = m_statusModel->projectItem(proj);
        for(const auto* item: m_statusModel->items(projItem.project, RepoStatusModel::childArea(area))) {
            auto url = item->data(RepoStatusModel::UrlRole).toUrl();
            if (url.isValid()) {
                ret << url;
            }
        }
    }
    return ret;
}


void CommitToolView::refresh()
{
    if (auto *proj = activeProject()) {
        m_statusModel->reload({proj});
        reloadBranchList(proj);
        if (auto* git = proj->versionControlPlugin()->extension<GitPlugin>()) {
            bool enablePop = git->hasStashes(proj->path().path());
            m_stashManagerAct->setEnabled(enablePop);
            m_stashPopAct->setEnabled(enablePop);

            auto projItem = m_statusModel->projectItem(proj);
            bool enablePush = (projItem.index->hasChildren() || projItem.worktree->hasChildren() || projItem.conflicts->hasChildren());
            m_stashPushAllAct->setEnabled(enablePush);
            m_stashPushFilesAct->setEnabled(enablePush);
        }
    } else {
        for(auto* act: {m_stashManagerAct, m_stashPopAct, m_stashPushAllAct, m_stashPushFilesAct})
            act->setEnabled(false);
        m_branchesMenu->setEnabled(false);
        m_statusModel->reloadAll();
    }
}

// Note that the this is a dangerous operation;
// we rely on the vcs job to show a confirmation dialog
void CommitToolView::revertSelectedFiles ( const QList<QUrl>& urls )
{
    if (urls.isEmpty()) return;

    IProject* project = ICore::self()->projectController()->findProjectForUrl(urls.front());
    IBasicVersionControl* vcs = vcsPluginForUrl(urls.front());

    if (vcs) {
        VcsJob* job = vcs->revert(urls, IBasicVersionControl::NonRecursive);
        job->setProperty("urls", QVariant::fromValue<QList<QUrl>>(urls));
        job->setProperty("project", QVariant::fromValue(project));
        ICore::self()->runController()->registerJob(job);
        connect(job, &VcsJob::resultsReady, this, [=]() {
            if (job->status() != VcsJob::JobSucceeded) {
                showError(i18n("Failed reverting files."));
            }
            // Close the document tabs showing diffs for the urls
            for (const auto& url : urls) {
                emit updateUrlDiffs(url);
            }
        });
    }
}

void CommitToolView::stageSelectedFiles ( const QList<QUrl>& urls )
{
    if (urls.isEmpty()) return;

    IProject* project = ICore::self()->projectController()->findProjectForUrl(urls.front());

    // This can happen for files which are filtered out by the project.
    // If the user wants to stage them anyway, we should let them
    if (!project) project = activeProject();

    IBasicVersionControl* vcs = vcsPluginForUrl(urls.front());
    if (vcs) {
        VcsJob* job = vcs->add(urls, IBasicVersionControl::NonRecursive);
        job->setProperty("urls", QVariant::fromValue<QList<QUrl>>(urls));
        job->setProperty("project", QVariant::fromValue(project));
        connect(job, &VcsJob::resultsReady, this, [=]() {
            if (job->status() != VcsJob::JobSucceeded) {
                showError(i18n("Failed staging files."));
            }
            // Close the document tabs showing diffs for the urls
            for (const auto& url : urls) {
                emit updateUrlDiffs(url);
            }
        });
        ICore::self()->runController()->registerJob(job);
    }
}

void CommitToolView::unstageSelectedFiles(const QList<QUrl>& urls)
{
    if (urls.isEmpty()) return;

    if (GitPlugin* git = dynamic_cast<GitPlugin*>(vcsPluginForUrl(urls.front()))) {
        IProject* project = ICore::self()->projectController()->findProjectForUrl(urls.front());
        VcsJob* job = git->reset(urls, IBasicVersionControl::NonRecursive);
        job->setProperty("urls", QVariant::fromValue<QList<QUrl>>(urls));
        job->setProperty("project", QVariant::fromValue(project));
        connect(job, &VcsJob::resultsReady, this, [=]() {
            if (job->status() != VcsJob::JobSucceeded) {
                showError(i18n("Failed unstaging files."));
            }
            for (const auto& url : urls) {
                emit updateUrlDiffs(url);
            }
        });
        ICore::self()->runController()->registerJob(job);
    }
}

void CommitToolView::resolveSelectedFiles(const CommitToolView::ConflictResolveStrategy strategy,
                                          const QList<QUrl>& urls)
{
    if (urls.isEmpty()) return;
    showError(QStringLiteral("Resolving conflicts is not yet implemented"));
}

void CommitToolView::deleteSelectedFiles(const QList<QUrl>& urls)
{
    if (urls.isEmpty()) return;
    if (auto* proj = activeProject()) {
        QStringList display_urls;
        for (const auto& file: urls) {
            display_urls << file.toDisplayString(QUrl::PreferLocalFile);
        }
        auto res = KMessageBox::questionYesNo(nullptr,
                                              i18n("Really delete The following files?") +
                                              QStringLiteral("<br/><br/>") +
                                              display_urls.join(QStringLiteral("<br/>"))
        );
        if (res != KMessageBox::Yes) {
            return;
        }
        proj->versionControlPlugin()->extension<IBasicVersionControl>()->remove(urls);
    }
}

void CommitToolView::ignoreSelectedFiles(const QList<QUrl>& urls)
{
    if (urls.isEmpty()) return;
    if (auto* proj = activeProject()) {
        auto* vcs = proj->versionControlPlugin()->extension<IBasicVersionControl>();
        auto projectRoot = proj->path();
        QStringList patterns;
        for(const auto& url: urls) {
            patterns << projectRoot.relativePath(Path(url));
        }
        runJobSequence({{
            vcs->ignore(projectRoot.toUrl(), patterns.join(QLatin1Char('\n'))),
            i18n("Adding pattern to .gitignore failed")
        }}, std::bind(&CommitToolView::refresh, this));
    }
}

void CommitToolView::stashPopActiveProject()
{
    if (auto* proj = activeProject()) {
        if (auto* git = proj->versionControlPlugin()->extension<GitPlugin>()) {
            VcsJob* job = git->gitStash(QDir{proj->path().path()}, {QStringLiteral("pop")}, OutputJob::Verbose);
            runJobSequence({{job, i18n("Git stash pop failed.")}}, [=]{
                emit updateProjectDiffs(proj);
                refresh();
            });
        }
    }
}

void CommitToolView::stashSelectedFiles(const QList<QUrl>& urls)
{
    QStringList args {};
    if (!urls.empty()) {
        args << QStringLiteral("--");
        for(const auto& u: urls) {
            args << u.toLocalFile();
        }
    }
    if (auto* proj = activeProject()) {
        if (auto* git = proj->versionControlPlugin()->extension<GitPlugin>()) {
            VcsJob* job = git->gitStash(QDir{proj->path().path()}, args, OutputJob::Verbose);
            runJobSequence({{job, i18n("Git stash failed.")}}, [=]{
                emit updateProjectDiffs(proj);
                refresh();
            });
        }
    }
}

void CommitToolView::createNewBranch()
{
    if (auto* proj = activeProject()) {
        if (auto* vcs = proj->versionControlPlugin()->extension<KDevelop::IBranchingVersionControl>()) {
            bool branchNameEntered = false;
            QString branchName = QInputDialog::getText(
                this,
                i18nc("@title:window", "New Branch"),
                i18nc("@label:textbox", "Name of the new branch:"),
                QLineEdit::Normal, QString(), &branchNameEntered
            );
            if (!branchNameEntered)
                return;

            if (branchName.isEmpty()) {
                showError(i18n("Cannot create a branch with empty name"));
                return;
            }

            VcsJob* createJob = vcs->branch(
                proj->path().toUrl(),
                VcsRevision::createSpecialRevision(VcsRevision::Head),
                branchName
            );
            VcsJob* switchJob = vcs->switchBranch(proj->path().toUrl(), branchName);

            runJobSequence({
                {createJob, i18n("Failed to create branch %1", branchName)},
                {switchJob, i18n("Failed to switch to the new branch %1", branchName)}
            }, std::bind(&CommitToolView::refresh, this));

        }
    }

}

void CommitToolView::commitActiveProject(const GitPlugin::CommitOptions options)
{
    if (auto* proj = activeProject()) {
        if (auto* vcs = proj->versionControlPlugin()->extension<GitPlugin>()) {
            QString msg = m_commitForm->summary();
            QString extended = m_commitForm->extendedDescription(70);
            if (extended.length() > 0)
                msg += QStringLiteral("\n\n") + extended;
            VcsJob* job = vcs->commitStaged(msg, proj->projectItem()->path().toUrl(), options | GitPlugin::CommitOption::AddNegativeOptions);
            m_commitForm->clearError();
            m_commitForm->disable();
            connect(job, &VcsJob::finished, m_commitForm, [=]{
                if (job->status() == VcsJob::JobSucceeded){
                    m_commitForm->clear();
                    emit updateProjectDiffs(proj);
                } else {
                    m_commitForm->showError(i18n("Committing failed. See Version Control tool view."));
                }
                m_commitForm->enable();
                m_historyView->reloadModel();
            });
            ICore::self()->runController()->registerJob(job);
        }
    }
}


void CommitToolView::runJobSequence(QList<CommitToolView::SubJob> jobs, std::optional<std::function<void ()>> onSuccess)
{
    if (jobs.isEmpty()) {
        if (onSuccess.has_value())
            onSuccess.value()();
        return;
    }
    auto nextJob = jobs.takeFirst();
    if (!nextJob.job) {
        showError(nextJob.errorMessage+i18n("No job to run"));
        return;
    }
    connect(nextJob.job, &VcsJob::finished, this, [=] {
        if (nextJob.job->status() == VcsJob::JobSucceeded)  {
            runJobSequence(jobs, onSuccess);
        } else {
            showError(nextJob.errorMessage+nextJob.job->errorString());
        }
    });
    ICore::self()->runController()->registerJob(nextJob.job);
}


void CommitToolView::reloadBranchList(KDevelop::IProject* proj)
{
    Q_ASSERT(proj);

    if (auto* vcs = proj->versionControlPlugin()->extension<KDevelop::IBranchingVersionControl>()) {
        VcsJob* job = vcs->branches(proj->path().toUrl());
        job->setVerbosity(OutputJob::Silent);
        connect(job, &VcsJob::finished, this, [=]{
            if (job->status() == VcsJob::JobSucceeded) {

                m_branchesMenu->setDisabled(false);
                m_branchesMenu->clear();
                m_branchesMenu->addAction(m_branchManagerAct);
                m_branchesMenu->addAction(m_switchToNewBranchAct);
                m_branchesMenu->addSeparator();

                // Construct the branch hierarchy, splitting it into
                // the local & remote branches
                BranchHierarchyNode local;
                BranchHierarchyNode remote;
                auto branches = job->fetchResults().toStringList();
                for(const auto& branch: branches) {
                    if (branch.startsWith(QStringLiteral("remotes/"))) {
                        BranchName b(branch.midRef(8));
                        remote.addBranch(b);
                    } else if (branch.startsWith(QStringLiteral("+ "))) {
                        BranchName b(branch.midRef(2));
                        local.addBranch(b);
                    } else {
                        BranchName b(branch.midRef(0));
                        local.addBranch(b);
                    }
                }

                auto switchAction = [vcs, proj, this](const QString& branch) {
                    runJobSequence({{vcs->switchBranch(proj->path().toUrl(), branch), i18n("Failed to switch branch")}});
                };

                // First add local branches
                m_branchesMenu->addSection(i18n("Local"));
                local.constructMenu(m_branchesMenu, switchAction);

                // Now add remote branches
                m_branchesMenu->addSection(i18n("Remotes"));
                remote.constructMenu(m_branchesMenu, switchAction);

            }
        });
        ICore::self()->runController()->registerJob(job);
    }
}


#include "committoolview.moc"
#include "moc_committoolview.cpp"
