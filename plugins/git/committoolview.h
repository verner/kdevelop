/*
    SPDX-FileCopyrightText: 2020 Jonathan L. Verner <jonathan.verner@matfyz.cz>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef KDEVPLATFORM_PLUGIN_COMMIT_TOOLVIEW_H
#define KDEVPLATFORM_PLUGIN_COMMIT_TOOLVIEW_H

#include "repostatusmodel.h"
#include "diffviewsctrl.h"

// kdevplatform headers
#include <interfaces/iuicontroller.h>

// KDE Frameworks headers
#include <KTextEditor/Attribute>
#include <KTextEditor/Range>

// Qt Headers
#include <QWidget>

class ActiveStyledDelegate;
class DiffViewsCtrl;
class RevisionDiffCtrl;
class RebaseController;
class FilterEmptyItemsProxyModel;
class HistoryView;
class RepoStatusModel;
class SimpleCommitForm;

class KMessageWidget;

class QAction;
class QMenu;
class QModelIndex;
class QLineEdit;
class QTreeView;
class QAbstractProxyModel;
class QUrl;

namespace KDevelop {
    class IBasicVersionControl;
    class IDocument;
    class IProject;
    class VcsJob;
    class VcsEventLogTreeModel;
}

namespace KTextEditor {
    class View;
    class Document;
}

/**
 * This implements the git-cola like toolview for preparing commits.
 *
 * The view contains a list of projects. Each project contains four
 * lists:
 *
 *   - the staged changes list lists all files in the project which
 *     have changes staged for commit;
 *   - the unstaged changes list lists all files in the project which
 *     have changes which are not currently staged for commit;
 *   - the conflicts list lists all files which have unresolved (merge)
 *     conflicts; and
 *   - the untracked list which lists all files not tracked in the VCS
 *
 * Clicking on a file in one of the staged/unstaged lists opens a document
 * tab with the diff showing the changes. The user can then select lines/hunks
 * from the diff and remove/add them from the staged changes using the context menu.
 *
 * Double clicking on a file will, instead, stage/unstage all changes in the file
 * or mark the conflicts as resolved or add the file to be tracked in VCS.
 *
 * Above these lists a lineedit and a textedit may be used to prepare a
 * commit message. The commit button will commit the staged changes to the
 * repo. If several projects are listed, the one which is expanded will be
 * used (only one project is allowed to be expaned to show the lists at a time,
 * an expaned project is automatically collapsed when a different one is expanded).
 *
 * TODO: Show (and allow activating) a project even when it has no changes.
 *
 * @author Jonathan L. Verner <jonathan.verner@matfyz.cz>
 */

class CommitToolView : public QWidget
{
    Q_OBJECT

public:

    /**
     * @note: @p statusModel remains the property of the caller whose
     * responsibility is to delete it (and care must be taken not
     * to delete it before the CommitToolView is deleted)
     */
    CommitToolView(QWidget* parent, GitPlugin* git);

    /**
     * @returns the currently active project (i.e. the one that
     *          is expanded in the treeview)
     */
    KDevelop::IProject* activeProject() const;

    /**
     * @returns the index of the currently active project (i.e. the one that
     *          is expanded in the treeview)
     */
    QStandardItem* activeProjectItem() const;

    /**
     * @returns true if the item pointed to by the repostatusmodel index
     *          idx is the root item of the currently active project.
     */
    bool isActiveProject(const QModelIndex& idx) const;

Q_SIGNALS:

    /**
     * This signal is emitted when the view wants to show a diff
     *
     * @param url the url to display the changes for
     * @param area the type of changes to display
     */
    void showDiff(const QUrl& url, const RepoStatusModel::Areas area);

    /**
     * This signal is emitted when the view wants to show a file
     *
     * @param url the url of the file to show
     * @param position if valid, indicates where in the file the cursor should
     *                 be placed (and, if range is nonempty, what text should be selected)
     */
    void showSource(const QUrl& url, const KTextEditor::Range& range = KTextEditor::Range::invalid());

    /**
     * This signal is emitted when the diff showing changes of type @param area
     * to the file @param url needs to be updated.
     */
    void updateDiff(const QUrl& url, const RepoStatusModel::Areas area);

    /**
     * This signal is emitted when all diffs showing changes to files in
     * project @param project need to be updated.
     */
    void updateProjectDiffs(KDevelop::IProject* project);

    /**
     * This signal is emitted when all diffs showing changes to the file
     * @param url need to be updated.
     *
     * @note: In contrast to the updateDiff signal, this also includes diffs
     * showing all changes to the owning project (staged/unstaged)
     */
    void updateUrlDiffs(const QUrl& url);

public Q_SLOTS:
    /**
     * Shows the toolview context menu
     */
    void popupContextMenu(const QPoint& pos);

    /**
     * A handler called when the user double clicks
     * an item in the treeview.
     */
    void dblClicked(const QModelIndex& idx);

    /**
     * A handler called when the user clicks an item
     * in the treeview.
     */
    void clicked(const QModelIndex& idx);

    /**
     * A handler called when a user expands an item
     * in the treeview.
     */
    void activateProject(const QModelIndex& idx);

    /**
     * Reloads the active project model, reloads the branches menu
     * and enables/disables the appropriate stash actions based on
     * whether there are changes to stash/stashes to pop
     */
    void refresh();

    /**
     * Stages the staged changes in the given files.
     *
     * @param urls the list of files whose changes to stage
     */
    void stageSelectedFiles(const QList<QUrl>& urls);

    /**
     * Unstages the staged changes in the given files.
     *
     * @param urls the list of files whose changes to unstage
     */
    void unstageSelectedFiles(const QList<QUrl>& urls);

    /**
     * Reverts the uncommited changes in the given files.
     *
     * @param urls the list of files whose changes to revert
     *
     * @note: This is an irreversible and dangerous action,
     * a confirmation dialog is shown before it is applied
     */
    void revertSelectedFiles(const QList<QUrl>& urls);

public:
    /**
     * A conflict resulution strategy
     */
    enum ConflictResolveStrategy {
        Ours,   /**< Resolve all conflicts by discarding "their" changes */
        Theirs, /**< Resolve all conflicts by discarding "our" changes */
    };

public Q_SLOTS:

    /**
     * Resolves all conflicts in the files @p urls with the
     * conflict resolution strategy @p strategy
     *
     * @param urls the list of files to resolve conflicts in
     *
     */
    void resolveSelectedFiles(const ConflictResolveStrategy strategy, const QList<QUrl>& urls);

    /**
     * Deletes the files @p urls
     *
     * @param urls the list of files to delete
     *
     */
    void deleteSelectedFiles(const QList<QUrl>& urls);

    /**
     * Adds the files @p urls to the projects ".gitignore" file
     *
     * @param urls the list of files to ignore
     *
     */
    void ignoreSelectedFiles(const QList<QUrl>& urls);

    /**
     * Saves the changes to the files @p urls onto the stash stack
     *
     * @param urls the list of files whose changes to save
     *
     */
    void stashSelectedFiles(const QList<QUrl>& urls);

    /**
     * Applies changes from the top of the stash to the current project
     * work directory and pops the stash
     */
    void stashPopActiveProject();

    /**
     * Shows the stash manager dialog for the currently active project
     */
    void showStashManager();

    /**
     * Shows the branch manager dialog for the currently active project
     */
    void showBranchManager();

    /**
     * Runs git commit on the staged changes. The commit message
     * is constructed from the data in the commit form.
     *
     * @note This function assumes that there are some staged changes.
     * @note The extended description of the commit is wrapped at 70 columns
     */
    void commitActiveProject(const GitPlugin::CommitOptions options);

    /**
     * Populates the commit form with the given @p message.
     */
    void populateCommitForm(const QString& message);

    /**
     * Creates a new branch off of the current branch in the currently
     * active project and switches to it.
     */
    void createNewBranch();

    /**
     * Shows an error message using the inline KMessageWidget
     *
     * (Use e.g. when a git action fails)
     */
    void showError(const QString& error);

    /**
     * Hides the inline error message if shown.
     */
    void clearError();

Q_SIGNALS:
    void showRebaseError(KDevelop::IProject* project, const QString& error);
    void showRebaseInfo(KDevelop::IProject* project, const QString& info);
    void clearRebaseError(KDevelop::IProject* project);
    void updateRebaseProgress(KDevelop::IProject* project, const int step, const int totalSteps);
    void rebaseFinished(KDevelop::IProject* project);
    void currentRebasePaused();
    void continueCurrentRebase();
    void abortCurrentRebase();

private:


    /**
     * Updates the toolview layout based on the dock area position:
     *
     * When the toolview is placed on the left/right, all the widgets
     * sit on top of each other; when it is placed on the top/bottom,
     * the commit area (commit header, button, description textedit)
     * will sit to the left of the changes view with the search filter.
     */

    void doLayOut(const Qt::DockWidgetArea area);

    /**
     * A helper function which return the VCS plugin which
     * handles `url`.
     *
     * @param url the url for which the plugin is returned
     *
     * @note: Returns nullptr if no project/VCS plugin for the
     * given url exists.
     */
    KDevelop::IBasicVersionControl* vcsPluginForUrl(const QUrl& url) const;

    /**
     * A structure describing a job in a job sequence
     */
    struct SubJob {
        KDevelop::VcsJob* job; /** The job to run */
        QString errorMessage;  /** The error to show, if the job fails */
    };

    /**
     * Runs the @p jobs in sequence. If a job fails, an error message is shown
     * and the remaining jobs are not run. If all jobs succeed, the optional
     * callback @p onSuccess is called;
     */
    void runJobSequence(QList<SubJob> jobs, std::optional<std::function<void ()>> onSuccess = std::nullopt);

private Q_SLOTS:

    /**
     * Reloads the branch list of @p project. Also it builds
     * up the "Switch to branch..." submenu.
     *
     * @warning @p project must not be null
     */
    void reloadBranchList(KDevelop::IProject* project);



private:

    /**
     * The git plugin.
     */
    GitPlugin* m_git;

    /**
     * The model which lists the projects and staged/modified/... files
     * which are shown in the treeview.
     */
    RepoStatusModel* m_statusModel;

    /**
     * The filtered repostatus model
     */
    FilterEmptyItemsProxyModel* m_proxyModel;

    /** The form for composing the commit message and doing the commit. */
    SimpleCommitForm* m_commitForm = nullptr;

    /**
     * A KMessage inline widget for showing errors
     */
    KMessageWidget* m_inlineError;

    /** The treeview listing the projects & their staged/modified/... files */
    QTreeView* m_view = nullptr;

    HistoryView* m_historyView = nullptr;

    /** The lineedit for filtering the treeview */
    QLineEdit* m_filter = nullptr;

    /** A style delegate for showing the currently selected project in bold */
    ActiveStyledDelegate* m_styleDelegate;

    /**
     ****************************************
     * Various context menus & their actions *
     ****************************************
     *
     * Each area (i.e. Staged Changes, ..., @see @ref RepoStatusModel::Areas )
     * in the treeview allows different actions (e.g. the Index area allows
     * only unstage actions, since other actions don't make sense). Moreover,
     * there are actions, which act on selected files and actions which
     * act on all area (this is reflected in the name which ends either
     * with FilesAct or AllAct). Each of these actions is defined below
     * as
     *
     *      m_action_name{Files|All}Act
     *
     * They are initialized in the constructor initializer list together with
     * appropriate icons and labels.
     *
     * Each action is mapped to a handler, which does the actual work.
     * This is done through the @ref m_actionHandlers map. A handler is
     * a function which receives a list of urls on which to act:
     *
     *      void handler(const QList<QUrl>&)
     *
     * This list is supplied by the function @ref popupContextMenu, which
     * handles showing context menus and acting on the user choices. In case
     * an action is triggered which should act on all files in a given area,
     * it constructs an appropriate file list and calls the handler with this
     * list.
     *
     * The actual context menus for a given treeview area are stored in
     * @ref m_contextMenus. They are built from a relatively declarative
     * description by a list of MenuEntry elements using the helper function
     * @ref buildMenu in the CommitToolView initializer list.
     *
     * All context menus also have a common set of actions for the whole
     * repository. These are described by the @ref m_projectRootEntries list,
     * which (at the time this comment was originally written) was as follows:
     *
     * <code>
     *  {
     *      { MenuEntry::Separator },
     *      { m_branchesMenu },
     *      { MenuEntry::Separator },
     *      { m_stashPushAllAct },
     *      { m_stashPopAct },
     *      { m_stashManagerAct },
     *      { MenuEntry::Separator },
     *      { m_refreshModelAct },
     *  }
     * </code>
     *
     */

    /** Actions available in the tree view */
    QAction *m_refreshModelAct          /** refresh git status, reload all branches */
          , *m_stageFilesAct            /** stage selected files */
          , *m_stageAllAct              /** stage all changed files */
          , *m_unstageFilesAct          /** unstage selected files */
          , *m_unstageAllAct            /** unstage all staged changes */
          , *m_revertFilesAct           /** revert changes to selected (unstaged) files */
          , *m_revertAllAct             /** revert all unstaged changes  */
          , *m_resolveOursFilesAct      /** resolve conflits in selected files, use the "ours" strategy */
          , *m_resolveOursAllAct        /** resolve all conflits, use the "ours" strategy */
          , *m_resolveTheirsFilesAct    /** resolve conflits in selected files, use the "theirs" strategy */
          , *m_resolveTheirsAllAct      /** resolve all conflits, use the "theirs" strategy */
          , *m_deleteFilesAct           /** delete selected files */
          , *m_deleteAllAct             /** delete all untracked files */
          , *m_ignoreFilesAct           /** ignore selected untracked files */
          , *m_ignoreAllAct             /** ignore all untracked files */
          , *m_stashPushFilesAct        /** push the changes to selected files onto the stash stack */
          , *m_stashPushAllAct          /** stash all changes */
          , *m_stashPopAct              /** apply the top patch on the stash stack and drop it */
          , *m_stashManagerAct          /** show the stash manager dialog */
          , *m_switchToNewBranchAct     /** switch to a new branch */
          , *m_branchManagerAct         /** show the branch manager dialog */
          ;

    std::unordered_map<QAction*, std::function<void (const QList<QUrl>&)>> m_actionHandlers;


    /**
     * A helper structure for describing menu items.
     *
     */
    struct MenuEntry {
        enum EntryType {
            Action,     /**< A menuitem which is an action */
            Menu,       /**< A menuitem which is a submenu */
            Separator,  /**< A separator menuitem */
        } type; /* The type of the menuitem */
        MenuEntry(QAction* action): type(Action), elt(action) {};
        MenuEntry(QMenu* menu): type(Menu), elt(menu) {};
        MenuEntry(EntryType tp): type(Separator) {Q_ASSERT(tp == Separator);};
        union _ { /* The menuitem (which can be directly added to a QMenu)*/
            QAction* action;
            QMenu* menu;
            _() {};
            _(QAction* a): action(a) {};
            _(QMenu* m): menu(m) {};
        } elt;
    };

    /**
     * A helper function to build a QMenu described by a list of MenuEntry elements @p entries.
     *
     * Example Usage:
     * <code>
     * buildMenu({
            { MenuEntry::Separator },
            { m_branchesMenu },
            { MenuEntry::Separator },
            { m_stashPushAct },
            { m_stashPopAct },
            { m_stashManagerAct },
            { MenuEntry::Separator },
            { m_refreshModelAct },
       });
       </code>
     */
    QMenu* buildMenu(const QList<MenuEntry>& entries) const;

    const int MAX_BRANCHES_TO_SHOW = 30; /**< The maximum number of branches to show in the Switch to branch ctx menu */

    /** Menu with a list of a few local branches to switch to (shown for projects in the toolview) */
    QMenu  *m_branchesMenu;

    /**
     * A list of menu entries describing the context menu for the project root
     * (which is also displayed in other context menus)
     */
    QList<MenuEntry> m_projectRootEntries;

    /**
     * Context Menus to show for various subtrees of the treeview
     */
    std::unordered_map<RepoStatusModel::Areas, QMenu*> m_contextMenus;


    /**
     * Returns a list of urls of files in the given treeview area
     * @p area
     */
    QList<QUrl> getAreaFilesForActiveProject(RepoStatusModel::Areas area);

};

/**
 * A factory for creating CommitToolViews.
 */
class CommitToolViewFactory : public KDevelop::IToolViewFactory
{
public:
    explicit CommitToolViewFactory(GitPlugin* git);
    QWidget* create(QWidget* parent = nullptr) override;
    Qt::DockWidgetArea defaultPosition() const override;
    QString id() const override;

private:
    GitPlugin* m_git;
};

#endif
