/*
    SPDX-FileCopyrightText: 2021 Jonathan Verner <jonathan@temno.eu>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/
#include "diffview.h"

#include "gitplugin.h"

#include <interfaces/ibasicversioncontrol.h>
#include <interfaces/icore.h>
#include <interfaces/idocument.h>
#include <interfaces/idocumentcontroller.h>
#include <interfaces/iplugin.h>
#include <interfaces/iproject.h>
#include <interfaces/iuicontroller.h>
#include <shell/partdocument.h>

#include <KActionCollection>
#include <KLocalizedString>
#include <KTextEditor/Document>
#include <KTextEditor/View>

#include <QAction>
#include <QClipboard>
#include <QDateTime>
#include <QGuiApplication>
#include <QIcon>
#include <QMenu>

using namespace KDevelop;



DiffView::DiffView(QObject* parent, KDevelop::IProject* project, const QString& title, const KTextEditor::Cursor initialPos):
 QObject(parent)
 , m_project(project)
 , m_title(title)
 , m_initialPos(initialPos)
 , m_baseDiffPath(m_project->path())
 , m_gotoSourceAct(new QAction(QIcon::fromTheme(QStringLiteral("go-parent-folder")), i18n("Go to line in (current) source")))
 , m_copyTrimmedAct(new QAction(QIcon::fromTheme(QStringLiteral("edit-copy")), i18n("Copy without diff prefixes (+/-)")))
 , m_actionCollection({
     {m_gotoSourceAct, i18n("G")},
     {m_copyTrimmedAct, {}},
   })
{
    connect(m_gotoSourceAct, &QAction::triggered, this, &DiffView::gotoSource);
    m_gotoSourceAct->setDisabled(true);

    connect(m_copyTrimmedAct, &QAction::triggered, this, &DiffView::copyTrimmed);

}


void DiffView::cleanup()
{
    if (m_ktDoc) {
        for(auto view: m_ktDoc->views()) {
            delete view->contextMenu();
        }
    }

    if (m_doc)
        m_doc->close();

    emit cleanedUp();
}

void DiffView::copyTrimmed()
{
    if (auto* view = activeView()) {
        auto range = view->selectionRange();
        if (range.isEmpty())
            return;

        // We want all lines hitting the selection, we do this
        // by adding a line at the end and setting columns to 0
        auto end = range.end();
        end.setLine(end.line()+1);
        range.setEnd(end);
        range.setBothColumns(0);

        auto docLines = m_ktDoc->textLines(range);
        QStringList trimmedLines;
        for(const auto& ln : docLines) {
            trimmedLines << ln.mid(1);
        }
        QClipboard *clipboard = QGuiApplication::clipboard();
        clipboard->setText(trimmedLines.join(QLatin1Char('\n')));
    }
}

void DiffView::diffReady(KDevelop::VcsDiff diff)
{
    auto* docCtrl = ICore::self()->documentController();
    m_diff = diff;
    m_doc = docCtrl->openDocumentFromText(QString());
    m_doc->setPrettyName(m_title);

    m_ktDoc = m_doc->textDocument();
    m_ktDoc->setReadWrite(true);
    m_ktDoc->setText(m_diff.diff());
    m_ktDoc->setReadWrite(false);
    m_ktDoc->setModified(false);
    m_ktDoc->setMode(QStringLiteral("diff"));
    m_ktDoc->setHighlightingMode(QStringLiteral("diff"));
    connect(m_ktDoc, &KTextEditor::Document::aboutToClose, this, &DiffView::cleanup);

    if (auto doc = dynamic_cast<KDevelop::PartDocument*>(m_doc)) {
        // FIXME: Figure out how to indicate to problem reporter (and, possibly, other plugins/places)
        // the the document is a generated document not stored on disk; the following hits an assert
        // in documentcontroller.cpp: 329
        // doc->setUrl({});
    }

    activate();

    for (const auto view : m_ktDoc->views()) {
        QMenu* menu = new QMenu;
//         menu->addAction(m_gotoSourceAct);
        for(const auto& [action, shortcut]: m_actionCollection) {
            menu->addAction(action);
        }
//         menu->addAction(m_copyTrimmedAct);
        view->setContextMenu(menu);
        if (m_initialPos.isValid()) {
            view->setCursorPosition(m_initialPos);
        }

        // Add the actions to the view action collection, so that shortcuts work and can be edited
        auto actCollection = view->actionCollection();
        for(const auto& [action, shortcut]: m_actionCollection) {
            actCollection->addAction(
                QStringLiteral("git_diff_")+action->text().replace(QLatin1Char(' '), QLatin1Char('_')).toLower(),
                action
            );
            if (!shortcut.isEmpty())
                actCollection->setDefaultShortcut(action, shortcut);
        }
        connect(view, &KTextEditor::View::contextMenuAboutToShow, this, &DiffView::contextMenuAboutToShow);
    }
}

void DiffView::contextMenuAboutToShow(KTextEditor::View* view, QMenu* menu)
{
    Q_UNUSED(menu);
    if (!view->selectionRange().isEmpty()) {
        m_copyTrimmedAct->setEnabled(true);
    } else {
        m_copyTrimmedAct->setEnabled(false);
    }
}

KTextEditor::View* DiffView::activeView() const
{
    auto view = ICore::self()->documentController()->activeTextDocumentView();
    if (m_ktDoc) {
        for(auto* v: m_ktDoc->views()) {
            if (view == v) {
                return v;
            }
        }
    }
    return nullptr;
}

void DiffView::activate() const
{
    if (activeView())
        return;
    ICore::self()->documentController()->activateDocument(m_doc);
}



