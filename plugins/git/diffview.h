/*
    SPDX-FileCopyrightText: 2021 Jonathan Verner <jonathan@temno.eu>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#ifndef DIFFVIEW__H
#define DIFFVIEW__H

// kdevplatform headers
#include <vcs/vcsdiff.h>
#include <util/path.h>

// KDE Frameworks headers
#include <KTextEditor/Cursor>

// System Headers
#include <QList>
#include <QObject>

namespace KDevelop {
    class IDocument;
    class IProject;
}

namespace KTextEditor {
    class View;
    class Document;
}

class QAction;
class QMenu;

class DiffView : public QObject {
    Q_OBJECT
public:
    DiffView(
        QObject* parent,
        KDevelop::IProject* project,
        const QString& title,
        const KTextEditor::Cursor initialPos = {}
    );

    /**
     * Returns the active KTextEditor view showing this diff
     */
    KTextEditor::View* activeView() const;

    /**
     * Activates a view for this diff
     */
    void activate() const;

    /**
     * Closes associated views, cancels all jobs and cleans up.
     */
    void cleanup();

    KDevelop::IProject* project() const { return m_project;}

protected:
    struct ActionCollectionItem {
        QAction* action;
        QString  shortCut;
    };

    KDevelop::IProject* m_project = nullptr;    /**< The associated project */
    KDevelop::IDocument* m_doc = nullptr;       /**< The associated IDocument */
    KTextEditor::Document* m_ktDoc = nullptr;   /**< The associated KTextEditor::Document */
    QString m_title = {};                       /**< The title of the diff view */
    const KTextEditor::Cursor m_initialPos = {};/**< The initial position to place the cursor at */
    KDevelop::Path m_baseDiffPath = {};         /**< The path to the base diff directory */
    KDevelop::VcsDiff m_diff;                   /**< The actual diff to show */
    QAction* m_gotoSourceAct;                   /**< An action to go to the current source file location
                                                     corresponding to a diff location */
    QAction* m_copyTrimmedAct;                  /**< An action to copy a diff selection without the leading +/- characters */

    QList<ActionCollectionItem> m_actionCollection;

Q_SIGNALS:
    void cleanedUp();
    void error(const QString& error);

protected Q_SLOTS:
    virtual void contextMenuAboutToShow(KTextEditor::View* view, QMenu* menu);
    virtual void gotoSource() {};
    void copyTrimmed();
    void diffReady(KDevelop::VcsDiff m_diff);
};



#endif // DIFFVIEW__H
