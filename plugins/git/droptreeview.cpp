/*
    SPDX-FileCopyrightText: 2022 Jonathan L. Verner <jonathan.verner@matfyz.cz>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "droptreeview.h"

#include <QDropEvent>
#include <QModelIndex>


DropTreeView::DropTarget DropTreeView::eventTarget(QDropEvent* event) {
    if (! viewport()->rect().contains(event->pos())) {
        return {};
    }

    DropTarget tgt;

    tgt.index = indexAt(event->pos());

    switch(dropIndicatorPosition()) {
        case QAbstractItemView::OnItem:
            tgt.pos = Position::On;
            break;
        case QAbstractItemView::AboveItem:
            tgt.pos = Position::Above;
            break;
        case QAbstractItemView::BelowItem:
            tgt.pos = Position::Below;
            break;
        default:
            tgt.pos = Position::Other;
            break;
    }

    if (!tgt.index.isValid() || !visualRect(tgt.index).contains(event->pos())) {
        tgt.index = QModelIndex{};
        return tgt;
    }

    return tgt;
}

void DropTreeView::dropEvent(QDropEvent* event)
{
    if ((!event->isAccepted()) && (model()->supportedDropActions() & event->dropAction())) {
        DropTreeView::Source src = event->source() == this ? Source::Self : Source::Other;
        DropTarget tgt = eventTarget(event);
        emit dropped(event->mimeData(), src, event->dropAction(), tgt);
        event->acceptProposedAction();
    }
    stopAutoScroll();
    setState(NoState);
    viewport()->update();
}


