/*
    SPDX-FileCopyrightText: 2022 Jonathan L. Verner <jonathan.verner@matfyz.cz>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef KDEVPLATFORM_PLUGIN_DROP_TREEVIEW_H
#define KDEVPLATFORM_PLUGIN_DROP_TREEVIEW_H
#include <QTreeView>

class QDropEvent;
class QMimeData;
class QModelIndex;

class DropTreeView : public QTreeView
{
      Q_OBJECT
public:

    enum class Source {
        Self = 0,
        Other = 1,
    };

    enum class Position {
        Above,
        On,
        Below,
        Other
    };

    /**
     * Represents a place to drop, i.e. either an existing row in the model,
     * or above or below an existing row in the model.
     */
    struct DropTarget {
        QModelIndex index; /**< The index relative to which to drop */

        Position pos;      /**< Where to drop, relative to the index */

        bool isValid() const { return index.isValid(); } /** Returns @c true if the target is valid */
    };

Q_SIGNALS:

    void dropped(const QMimeData* mimeData, const Source src, Qt::DropAction action, const DropTarget& tgt);

protected:
    virtual void dropEvent(QDropEvent* event) override;



    /**
     * Determines the place in the model the event targets.
     */
    DropTarget eventTarget(QDropEvent* event);

};

#endif // KDEVPLATFORM_PLUGIN_DROP_TREEVIEW_H
