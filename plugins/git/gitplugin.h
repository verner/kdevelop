/*
    SPDX-FileCopyrightText: 2008 Evgeniy Ivanov <powerfox@kde.ru>
    SPDX-FileCopyrightText: 2009 Hugo Parente Lima <hugo.pl@gmail.com>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#ifndef KDEVPLATFORM_PLUGIN_GIT_PLUGIN_H
#define KDEVPLATFORM_PLUGIN_GIT_PLUGIN_H

#include <util/path.h>
#include <vcs/interfaces/idistributedversioncontrol.h>
#include <vcs/interfaces/icontentawareversioncontrol.h>
#include <vcs/dvcs/dvcsplugin.h>
#include <vcs/vcsstatusinfo.h>
#include <outputview/outputjob.h>
#include <vcs/vcsjob.h>
#include <vcs/vcsevent.h>

#include <QDebug>
#include <QDateTime>

#include <memory>
#include <unordered_map>                                 // for std::unordered_map
#include <variant>                                       // for std::variant

#include <QDateTime>

class KDirWatch;
class QDir;

class CommitToolViewFactory;
class DiffViewsCtrl;
class GitRebaseJob;
class RebaseController;
class RepoStatusModel;
class RevisionDiffCtrl;

namespace KDevelop
{
    class IProject;
    class Path;
    class VcsJob;
    class VcsRevision;
    class VcsEventLogTreeModel;
}

class StandardJob : public KDevelop::VcsJob
{
    Q_OBJECT
    public:
        StandardJob(KDevelop::IPlugin* parent, KJob* job, OutputJobVerbosity verbosity);

        QVariant fetchResults() override { return QVariant(); }
        void start() override;
        JobStatus status() const override { return m_status; }
        KDevelop::IPlugin* vcsPlugin() const override { return m_plugin; }

    public Q_SLOTS:
        void result(KJob*);

    private:
        KJob* m_job;
        KDevelop::IPlugin* m_plugin;
        JobStatus m_status;
};

/**
 * This is the main class of KDevelop's Git plugin.
 *
 * It implements the DVCS dependent things not implemented in KDevelop::DistributedVersionControlPlugin
 * @author Evgeniy Ivanov <powerfox@kde.ru>
 */
class GitPlugin: public KDevelop::DistributedVersionControlPlugin, public KDevelop::IContentAwareVersionControl
{
    Q_OBJECT
    Q_INTERFACES(KDevelop::IBasicVersionControl KDevelop::IDistributedVersionControl KDevelop::IContentAwareVersionControl)
    friend class GitInitTest;
public:

    enum ExtendedState {
        /* Unchanged in index (no staged changes) */
        GitXX = KDevelop::VcsStatusInfo::ItemUserState, // No changes in worktree

               // Changed in worktree, not staged for commit
        GitXM, // Modified in worktree
        GitXD, // Deleted in worktree
        GitXR, // Renamed in worktree
        GitXC, // Copied in worktree

        /* Changes in index (staged changes) */
        GitMX, // No changes in worktree
                  // Changed in worktree, not staged for commit
        GitMM, // Modified in worktree
        GitMD, // Deleted in worktree

        /* Added to index (new item) */
        GitAX, // No changes in worktree
                  // Changes in worktree, not staged for commit
        GitAM, // Modified in worktree
        GitAD, // Deleted in worktree

        /* Deleted from index */
        GitDX, // No changes in worktree (deleted in wt)
        GitDR, // Renamed in worktree
        GitDC, // Copied in worktree

        /* Renamed in index */
        GitRX, // No changes in worktree
        GitRM, // Modified in worktree
        GitRD, // Deleted in worktree

        /* Copied in index */
        GitCX, // No changes in worktree
        GitCM, // Modified in worktree
        GitCD, // Deleted in worktree

        /* Special states */
        GitUntracked, // ? ? --- untracked files
        GitConflicts, // U, AA, DD --- conflicts
        GitInvalid = -1, // not really a state
    };

    /**
     * Enums with values which are used as function arguments
     * instead of bools for better readability.
     *
     * The enums are named ${function_name}Params.
     */

    /**
     * Parameters for the apply method
     */
    enum ApplyParams {
        Index = 0,    /**< Apply the patch to the index only */
        WorkTree = 2, /**< Apply the patch to the worktree only */
        Both = 4      /**< Apply the patch both to the index & the worktree */
    };

    struct ExtendedRevisionInfo {
        QString fullHash;
        QString authorEmail;
        QStringList tags;
        QStringList refs;
        QString sigName;
        QString sigKeyID;
        QString sigKeyFP;
        /**
         * Commit Signature status
         *
         * See also git log manpage under %G?.
         */
        enum SignatureStatus {
            GOOD = 'G',                   /**< a good (valid) signature */
            BAD = 'B',                    /**< a bad signature */
            GOOD_UNKNOWN_VALIDITY = 'U',  /**< a good signature with unknown validity */
            GOOD_KEY_EXPIRED = 'Y',       /**< a good signature made by an expired key */
            GOOD_KEY_REVOKED = 'R',       /**< a good signature made by a revoked key */
            GOOD_SIGNATURE_EXPIRED = 'X', /**< a good signature that has expired */
            CANNOT_CHECK = 'E',           /**< signature cannot be checked (e.g. missing key) */
            NO_SIGNATURE = 'N',           /**< no signature */
        } sigStatus;
    };

    explicit GitPlugin(QObject *parent, const QVariantList & args = QVariantList() );
    ~GitPlugin() override;

    QString name() const override;

    bool isValidRemoteRepositoryUrl(const QUrl& remoteLocation) override;
    bool isVersionControlled(const QUrl &path) override;

    /**
     * Returns the path to the .git directory for repository/worktree @p repoPath.
     *
     * @note: This takes into account worktrees, where the root of
     * a worktree contains a <b>file</b> (!) ".git" which points to
     * the real gitdir (in the .git/worktrees subdirectory of the original
     * repository root).
     */
    KDevelop::Path gitDirPath(const KDevelop::Path repoPath) const;
    KDevelop::VcsJob* copy(const QUrl& localLocationSrc, const QUrl& localLocationDstn) override;
    KDevelop::VcsJob* move(const QUrl& localLocationSrc, const QUrl& localLocationDst) override;

    //TODO
    KDevelop::VcsJob* pull(const KDevelop::VcsLocation& localOrRepoLocationSrc, const QUrl& localRepositoryLocation) override;
    KDevelop::VcsJob* push(const QUrl& localRepositoryLocation, const KDevelop::VcsLocation& localOrRepoLocationDst) override;
    KDevelop::VcsJob* repositoryLocation(const QUrl& localLocation) override;
    KDevelop::VcsJob* resolve(const QList<QUrl>& localLocations, RecursionMode recursion) override;
    KDevelop::VcsJob* update(const QList<QUrl>& localLocations, const KDevelop::VcsRevision& rev, RecursionMode recursion) override;
    KDevelop::VcsLocationWidget* vcsLocation(QWidget* parent) const override;
    void setupCommitMessageEditor(const QUrl& localLocation, KTextEdit* editor) const override;
    //End of

    KDevelop::VcsJob* add(const QList<QUrl>& localLocations,
                          KDevelop::IBasicVersionControl::RecursionMode recursion = KDevelop::IBasicVersionControl::Recursive) override;
    KDevelop::VcsJob* createWorkingCopy(const KDevelop::VcsLocation & localOrRepoLocationSrc,
                            const QUrl& localRepositoryRoot, KDevelop::IBasicVersionControl::RecursionMode) override;

    KDevelop::VcsJob* remove(const QList<QUrl>& files) override;
    KDevelop::VcsJob* status(const QList<QUrl>& localLocations,
                             KDevelop::IBasicVersionControl::RecursionMode recursion = KDevelop::IBasicVersionControl::Recursive) override;
    KDevelop::VcsJob* commit(const QString& message,
                             const QList<QUrl>& localLocations,
                             KDevelop::IBasicVersionControl::RecursionMode recursion = KDevelop::IBasicVersionControl::Recursive) override;


    /**
     * git commit flags
     */
    enum class CommitOption {
        None                = 0,
        AddNegativeOptions  = 1 << 0, /**< an unset option will add the `--no-*` version of the option, if any. */
        AmendLastCommit     = 1 << 1, /**< amend the last commit instead of creating a new commit (--amend) */
        SignCommit          = 1 << 2, /**< cryptographically sign the commit (--gpg-sign/--no-gpg-sign) */
        BypassCommitHooks   = 1 << 3, /**< do not run the pre-commit and commit-msg hooks (--no-verify) */
        AddSignOffLine      = 1 << 4, /**< add a Signed-off-by trailer by the committer at the end of the commit log message. (--signoff/--no-signoff) */
    };
    Q_DECLARE_FLAGS(CommitOptions, CommitOption)
    Q_FLAGS(CommitOptions)

    struct ExtendedCommitOptions {
        QString author;             /**< name & email of the person who authored the changes*/
        QDateTime authorDate;       /**< date & time when the commit was authored */
        QString committer;          /**< name & email of the person committing the changes */
        QDateTime committerDate;    /**< date & time the changes were committed */
    };

    /**
     * Commits staged changes to the repo located at repoUrl.
     *
     * @param message the commit message
     * @param repoUrl the url pointing to the repo directory (or a file in the repo)
     * @param options additional options to pass to the git commit command;
     *
     * Note: When @ref CommitOption::AddNegativeOptions is set, when an option has a positive and negative version (e.g. `--gpg-sign` and `--no-gpg-sign`)
     * and it is *unset*, the  negative version is added. When @ref CommitOption::AddNegativeOptions is not set, unset options
     * do not change the git command line.
     */
    KDevelop::VcsJob* commitStaged(const QString& message, const QUrl& repoUrl, const CommitOptions options = CommitOption::None, const ExtendedCommitOptions& extendedOpts = {});

    KDevelop::VcsJob* diff(const QUrl& fileOrDirectory, const KDevelop::VcsRevision& srcRevision, const KDevelop::VcsRevision& dstRevision,
                                   RecursionMode recursion) override;

    using IBasicVersionControl::diff;

    /**
     * Computes a diff of changes between @p srcRevision and @p dstRevision which touch @p files.
     *
     * @param repoPath the repository (workdir) path
     * @param files the paths to the files (can be relative)
     * @param srcRevision the source revision
     * @param dstRevision the destination revision
     *
     * @note: This differs from the @ref:diff method in @ref:IBasicVersionControl in that it
     * allows providing a **list** of files.
     */
    KDevelop::VcsJob* diff(const KDevelop::Path& repoPath, const QList<QString> files, const KDevelop::VcsRevision& srcRevision, const KDevelop::VcsRevision& dstRevision);

    /**
     * Computes a diff of changes between srcRevision and dstRevision.
     *
     * @param repoPath a path pointing somewhere inside the repo
     * @param srcRevision the source revision
     * @param dstRevision the destination revision
     *
     * @note: This differs from the @ref:diff method in @ref:IBasicVersionControl in that it does not require
     * a file but automatically shows all changed files
     */
    KDevelop::VcsJob* diff(const QUrl& repoPath, const KDevelop::VcsRevision& srcRevision, const KDevelop::VcsRevision& dstRevision);
    KDevelop::VcsJob* diff(const KDevelop::Path& repoPath, const KDevelop::VcsRevision& srcRevision, const KDevelop::VcsRevision& dstRevision);

    KDevelop::VcsJob* log( const QUrl& localLocation, const KDevelop::VcsRevision& rev, unsigned long limit) override;
    KDevelop::VcsJob* log(const QUrl& localLocation, const KDevelop::VcsRevision& rev, const KDevelop::VcsRevision& limit) override;
    KDevelop::VcsJob* annotate(const QUrl &localLocation, const KDevelop::VcsRevision &rev) override;
    KDevelop::VcsJob* revert(const QList<QUrl>& localLocations, RecursionMode recursion) override;

    /**
     * Resets all changes in the specified files which were "staged for commit".
     *
     * @param localLocations the local files/dirs changes to which should be reset
     * @param recursion defines whether changes should be reset recursively in all files
     * in a directory, if localLocations contain a directory
     */
    KDevelop::VcsJob* reset(const QList<QUrl>& localLocations, RecursionMode recursion);

    /**
     * Applies the patch given by a diff to the repo
     *
     * @param diff the patch
     * @param applyTo where to apply the patch (index or worktree)
     */
    KDevelop::VcsJob* apply(const KDevelop::VcsDiff& diff, ApplyParams applyTo = WorkTree);

    // Begin:  KDevelop::IDistributedVersionControl
    KDevelop::VcsJob* init(const QUrl & directory) override;

    // Branch management
    KDevelop::VcsJob* tag(const QUrl& repository, const QString& commitMessage, const KDevelop::VcsRevision& rev, const QString& tagName) override;
    KDevelop::VcsJob* branch(const QUrl& repository, const KDevelop::VcsRevision& rev, const QString& branchName) override;
    KDevelop::VcsJob* branches(const QUrl& repository) override;
    KDevelop::VcsJob* currentBranch(const QUrl& repository) override;
    KDevelop::VcsJob* deleteBranch(const QUrl& repository, const QString& branchName) override;
    enum NoUIOptions {
        FailOnUncleanRepo=0,  /**< The job fails if the repo is unclean */
        ProceedOnUncleanRepo, /**< The job proceeds even if the repo is unclean */
    };

    /**
     * Like @ref switchBranch, but does does not ask the user to stash if the repository is not
     * clean. Instead it either returns a nullptr or proceeds with the switch anyway based on
     * @p options.
     */
    KDevelop::VcsJob* switchBranchNoUI(const QUrl& repository, const QString& branchName, const NoUIOptions options);

    /**
     * Switches the current branch to @p branchName.
     *
     * If the repository is not in a clean state, it first asks the user whether to stash changes.
     * If the user answers yes, changes are stashed and then the branch is switched. If the user
     * ansers no, changes are not stashed and the branch is switched. If the user answers
     * cancel, nothing is done and a nullptr returned.
     *
     * @note The clean check is run at the time the method is called, not at the time the switch
     * job is actually started, which may be later. The switch job does not check that the repo
     * is still clean when it is started.
     */
    KDevelop::VcsJob* switchBranch(const QUrl& repository, const QString& branchName) override;
    KDevelop::VcsJob* renameBranch(const QUrl& repository, const QString& oldBranchName, const QString& newBranchName) override;
    KDevelop::VcsJob* mergeBranch(const QUrl& repository, const QString& branchName) override;
    KDevelop::VcsJob* rebase(const QUrl& repository, const QString& branchName);

    enum class ResetType {
        Hard,
        Soft
    };
    KDevelop::VcsJob* resetHEADTo(const KDevelop::Path& repository, const KDevelop::VcsRevision& targetRev, const ResetType type);


    KDevelop::VcsJob* checkOut(const KDevelop::Path& repository, const KDevelop::VcsRevision& rev);

    enum class CherryPickParams {
        Commit,
        NoCommit,
    };
    KDevelop::VcsJob* cherryPick(const KDevelop::Path& repository, const KDevelop::VcsRevision& rev, CherryPickParams params=CherryPickParams::Commit);


    /**
     * A struct holding a sequence of commits to rebase and a base commit to start rebasing.
     */
    struct RebaseRange {
        KDevelop::VcsRevision baseRev;          /**< The commit to start rebasing at */
        QList<KDevelop::VcsEvent> eventRange;   /**< The commits starting from baseRev (not included) to HEAD */
        bool isValid() const {return baseRev.isValid() && !eventRange.isEmpty();};
    };

    /**
     * Constructs a @ref RebaseRange structure so that its @ref RebaseRange::eventRange contains
     * revision @p ref and its @ref RebaseRange::baseRev is @p distToBase commits before @p ref.
     *
     * @note: If the revision @p ref is not found in the history model of the project given by @p repository,
     * returns an invalid range.
     */
    RebaseRange rebaseRange(const KDevelop::Path& repository, const KDevelop::VcsRevision ref, int distToBase = 1);

    /**
     * Sorts the list of revisions @p list from the oldest revision to newest.
     */
    void sortRevisions(const KDevelop::Path& repository, QList<KDevelop::VcsRevision>& list);

    /**
     * Returns a revision corresponding to (a) child of @p rev on the current branch, if it exists.
     *
     * @note: If the @p rev has multiple children, the one in the branch represented by the current history-model
     * is returned.
     */
    KDevelop::VcsRevision childRev(const KDevelop::Path& repository, const KDevelop::VcsRevision rev);

    /**
     * Returns the revision corresponding to the parent of @p rev, if it exists.
     *
     */
    KDevelop::VcsRevision parentRev(const KDevelop::Path& repository, const KDevelop::VcsRevision rev);


    /**
     * Options for how to create the commit message when squashing
     */
    enum class RebaseStepOption {
        Default =               0x00,   /**< Commit & Don't pause */
        NoPause =               Default,/**< Don't pause before continuing with next step */
        Commit =                Default,/**< Commit after applying and before continuing with next step */
        NoCommit =              0x01,   /**< Do not commit before continuing with next step */
        PauseBeforeNextStep  =  0x02,   /**< Pause after applying and before continuing with next step
                                            (the pause comes before the optional commit) */
    };
    Q_DECLARE_FLAGS(RebaseStepOptions, RebaseStepOption)
    Q_FLAGS(RebaseStepOptions)

    /**
     * Describes a single step of a rebase operation.
     * The step consists of
     *
     *   1. applying the patch @p patch (the patch will be either directly applied
     *      when it holds a diff, or cherry-picked and "uncommitted" when it holds a revision)
     *   2. pausing, if the @ref RebaseStepOption::PauseBeforeNextStep flag is set, for the user to make any
     *      further desired changes. These can include creating commits, modifying the index,
     *      modifying the worktree, etc. The changes will persist to the following steps
     *      (with the exception of the index, which, if the commit flag is set, will be committed)
     *   3. committing, if the @ref RebaseStepOption::Commit flag is set, with the message @p message
     *
     * If @p patch holds a revision, then @p message may optionally be empty. In
     * this case the message from the revision will be reused.
     */
    struct RebaseStep {
        typedef std::variant<KDevelop::VcsRevision, KDevelop::VcsDiff> Patch;
        Patch patch;                /**< The change to apply, either a revision to cherry-pick or a patch */
        QString message;            /**< The message to use when @p commit is true and it is not provided by other means */
        RebaseStepOptions options;  /**< Flags specifying step options (e.g. whether to commit, pause, ...) */

        // Git Options to apply when (if) committing the step
        CommitOptions commitOpts = CommitOption::None;
        ExtendedCommitOptions extendedCommitOpts;

        QVariant userData;          /**< Custom user specified data, to be passed to signals when pausing */

        /** Returns true if the @ref RebaseStepOption::Commit flag is set */
        bool isCommitSet() const { return ! (options & RebaseStepOption::NoCommit); }
        /** Returns true if the @ref RebaseStepOption::PauseBeforeNextStep flag is set */
        bool isPauseSet() const { return options & RebaseStepOption::PauseBeforeNextStep; }
    };

    struct RebasePlan {
        std::vector<RebaseStep> steps; /**< a series of rebase steps */
        KDevelop::VcsRevision base;    /**< the commit to start the rebase at */
        bool isValid() const { return (steps.size() > 0) && base.isValid(); }
    };

    /**
     * Creates a rebase job in the repository @p repository, which rebases
     * the branch @p branchName (replaces the commits base..branch_head with
     * the steps in @p plan) starting at commit @p plan.base, performing the
     * steps specified in @p plan.steps and, optionally, creating a backup branch
     * @p backupBranchName whose HEAD points to the old (pre-rebase) HEAD
     * of the branch @p branchName.
     *
     * @note The job will abort if another rebase job is in progress in the repository.
     * @note The rebased branch will be created as a temporary branch and
     * only moved to @p branchName as the last step. This should make recovery
     * easy in case of an unfinished rebase: its enough to just remove the
     * temporary branch.
     */

    GitRebaseJob* rebase(
        const KDevelop::Path& repository,
        const QString& branchName,
        const RebasePlan& plan,
        const QString& backupBranchName
    );


    //graph helpers
    QVector<KDevelop::DVcsEvent> allCommits(const QString& repo) override;

    //used in log
    void parseLogOutput(const KDevelop::DVcsJob* job,
                        QVector<KDevelop::DVcsEvent>& commits) const override;




    void additionalMenuEntries(QMenu* menu, const QList<QUrl>& urls) override;

    // Stash Management

    /**
     * Structure to hold information about an item on the stash stack
     */
    struct StashItem {
        int stackDepth = -1;        /* Position on the stack */
        QString shortRef;           /* The reflog selector (e.g. stash@{0}) */
        QString parentSHA;          /* The short SHA of the commit on which the stash was made */
        QString parentDescription;  /* A short description of the commit on which the stash was made */
        QString branch;             /* The branch on which the stash was made */
        QString message;            /* The message with which the stash was made */
        QDateTime creationTime;     /* The date-time the stash item was committed */
    };

    /**
     * Returns a job to run `git stash` in the repository @p repository with
     * additional arguments @p args.
     *
     * The @p verbosity parameter will determine whether the job output will
     * be shown in the VCS Output ToolView.
     *
     * For example, a job to silently apply the top-most stashed item to the current
     * tree would be created as follows:
     *
     *      gitStash(repoDir, {QStringLiteral("apply")}, KDevelop::OutputJob::Silent)
     *
     */
    KDevelop::VcsJob* gitStash(const QDir& repository, const QStringList& args, KDevelop::OutputJob::OutputJobVerbosity verbosity);

    /**
     * The result (job->fetchResults()) will be a @ref QList of @ref StashItem s
     *
     * @p repository is the repository to work on
     * @p verbosity  determines whether the job output will be shown in the VCS Output ToolView
     */
    KDevelop::VcsJob* stashList(const QDir& repository, KDevelop::OutputJob::OutputJobVerbosity verbosity = KDevelop::OutputJob::Silent);

    bool hasStashes(const QDir& repository);
    bool hasModifications(const QDir& repository);
    bool hasModifications(const QDir& repo, const QUrl& file);

    void registerRepositoryForCurrentBranchChanges(const QUrl& repository) override;

    KDevelop::CheckInRepositoryJob* isInRepository(KTextEditor::Document* document) override;

    KDevelop::DVcsJob* setConfigOption(const QUrl& repository, const QString& key, const QString& value, bool global = false);
    QString readConfigOption(const QUrl& repository, const QString& key);

    // this indicates whether the diff() function will generate a diff (patch) which
    // includes the working copy directory name or not (in which case git diff is called
    // with --no-prefix).
    bool usePrefix() const
    {
        return m_usePrefix;
    }

    void setUsePrefix(bool p)
    {
        m_usePrefix = p;
    }

    std::shared_ptr<KDevelop::VcsEventLogTreeModel> historyModelFor(const KDevelop::IProject* project);
    std::shared_ptr<KDevelop::VcsEventLogTreeModel> historyModelFor(const KDevelop::Path& repository);
    RepoStatusModel* repoStatusModel();
    RebaseController* rebaseController();
    RevisionDiffCtrl* revisionDiffController();
    DiffViewsCtrl* areaDiffViewController();


protected:


    /**
     * Returns the location of the .gitignore file
     */
    QUrl ignoreFile(const QUrl& repository) override;

    QUrl repositoryRoot(const QUrl& path);

    bool isValidDirectory(const QUrl &dirPath) override;

    KDevelop::DVcsJob* lsFiles(const QDir &repository,
                     const QStringList &args,
                     KDevelop::OutputJob::OutputJobVerbosity verbosity = KDevelop::OutputJob::Verbose);
    KDevelop::DVcsJob* gitRevList(const QString &directory,
                        const QStringList &args);
    KDevelop::DVcsJob* gitRevParse(const QString &repository,
                         const QStringList &args,
                         KDevelop::OutputJob::OutputJobVerbosity verbosity = KDevelop::OutputJob::Silent);

private Q_SLOTS:
    void parseGitBlameOutput(KDevelop::DVcsJob *job);
    void parseGitLogOutput(KDevelop::DVcsJob *job);
    void parseGitDiffOutput(KDevelop::DVcsJob* job);
    void parseGitRepoLocationOutput(KDevelop::DVcsJob* job);
    void parseGitStatusOutput(KDevelop::DVcsJob* job);
    void parseGitStatusOutput_old(KDevelop::DVcsJob* job);
    void parseGitVersionOutput(KDevelop::DVcsJob* job);
    void parseGitBranchOutput(KDevelop::DVcsJob* job);
    void parseGitCurrentBranch(KDevelop::DVcsJob* job);
    void parseGitStashList(KDevelop::VcsJob* job);

    void ctxRebase();
    void ctxPushStash();
    void ctxPopStash();
    void ctxStashManager();

    void fileChanged(const QString& file);
    void delayedBranchChanged();

Q_SIGNALS:
    void repositoryBranchChanged(const QUrl& repository);

private:
    bool ensureValidGitIdentity(const QDir& dir);
    void addNotVersionedFiles(const QDir& dir, const QList<QUrl>& files);

    //commit dialog "main" helper
    QStringList getLsFiles(const QDir &directory, const QStringList &args,
        KDevelop::OutputJob::OutputJobVerbosity verbosity);
    KDevelop::DVcsJob* errorsFound(const QString& error, KDevelop::OutputJob::OutputJobVerbosity verbosity);

    void initBranchHash(const QString &repo);

    /**
     * Parses a git status --porcelain line
     *
     * @param statusLine a line as returned by `git status --porcelain`
     * @returns the appropriate extended status
     */
    static ExtendedState parseGitState(const QStringRef& statusLine);

    /**
     * Maps an extended state to a basic state
     *
     * @param state the extended state as provided by git (i.e. describing the combined status in the index & worktree)
     */
    static KDevelop::VcsStatusInfo::State extendedStateToBasic(const ExtendedState state);

    /**
     * This function triggers a reload of
     *
     *   - the history model
     *   - the repo status model (tracking working the tree/staging area state)
     *
     * for project @p path when @p job **successfully** finishes.
     *
     * FIXME: This is a workaround needed for some git actions which, for some
     * reason do not trigger the file watcher.
     */
    void reloadModelsOnSuccess(const KDevelop::Path& repository, KDevelop::VcsJob* job);

    QList<QStringList> branchesShas;
    QList<QUrl> m_urls;

    /** Tells if it's older than 1.7.0 or not */
    bool m_oldVersion = false;

    KDirWatch* m_watcher;
    QList<QUrl> m_branchesChange;
    bool m_usePrefix = true;

    /** A tree model tracking and classifying changes into staged, unstaged and untracked */
    RepoStatusModel* m_repoStatusModel;

    /**
     * History models for opened projects
     *
     * Indexed by the path to the root of the project.
     *
     * @warning: This assumes that a repository is **not** shared across projects!
     */
    std::unordered_map<KDevelop::Path, std::shared_ptr<KDevelop::VcsEventLogTreeModel>> m_historyModels;


    /** A factory for constructing the tool view for preparing commits */
    CommitToolViewFactory* m_commitToolViewFactory;

    RebaseController* m_rebaseController;
    RevisionDiffCtrl* m_revisionDiffViewController;
    DiffViewsCtrl* m_areaDiffViewController; // TODO: eventually merge into RevisionDiffCtrl ?
};

Q_DECLARE_METATYPE(GitPlugin::StashItem)

QVariant runSynchronously(KDevelop::VcsJob* job);

QDebug operator<<(QDebug debug, const GitPlugin::RebasePlan& plan);

Q_DECLARE_METATYPE(GitPlugin::ExtendedRevisionInfo)
Q_DECLARE_OPERATORS_FOR_FLAGS(GitPlugin::RebaseStepOptions)
Q_DECLARE_OPERATORS_FOR_FLAGS(GitPlugin::CommitOptions)
inline uint qHash(GitPlugin::CommitOption key, uint seed) {return ::qHash(static_cast<uint>(key), seed);}
#endif
