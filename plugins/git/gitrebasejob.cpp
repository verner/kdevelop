#include "gitrebasejob.h"

#include "gitplugin.h"
#include "gitjob.h"

#include <interfaces/icore.h>
#include <interfaces/iplugin.h>
#include <interfaces/iproject.h>
#include <interfaces/iprojectfilemanager.h>
#include <vcs/dvcs/dvcsjob.h>
#include <util/pausejob.h>

#include <KJob>
#include <KLocalizedString>
#include <KMessageBox>
#include <KMessageBox_KDevCompat>
#include <KShell>

#include <QDebug>
#include <QDir>
#include <QUuid>

#include <variant>
#include <algorithm>

using namespace KDevelop;

GitRebaseJob::GitRebaseJob(KDevelop::IProject* project, GitPlugin* parent, const KDevelop::Path& repository, const QString& branchName,
                           const GitPlugin::RebasePlan& plan,
                           const QString& backupBranchName)
:
VcsJob(parent, Verbose),
m_project(project),
m_git(parent),
m_repository(repository),
m_branchName(branchName),
m_backupBranchName(backupBranchName),
m_uuid(QUuid::createUuid().toString()),
m_tempBranchName(GIT_SCOPE_PREFIX + QStringLiteral("temp-")+branchName+QStringLiteral("-")+m_uuid),
m_tempStashName(GIT_SCOPE_PREFIX + QStringLiteral("rebase-save-")+m_uuid),
m_lockPath(m_git->gitDirPath(m_repository)),
m_plan(plan),
m_job(new ExecuteCompositeJob(this)),
m_status(JobNotStarted)
{
    m_job->setAbortOnError(true);
    m_lockPath.addPath(LOCK_FILENAME);
    if (m_backupBranchName.isEmpty()) {
        m_backupBranchName = m_tempBranchName;
        m_backupBranchName.replace(QStringLiteral("temp"), QStringLiteral("backup"));
    }
    setCapabilities(Killable);
    connect(this, &GitRebaseJob::rebaseFailed, this, &GitRebaseJob::finishAndEmitResult);
    connect(this, &GitRebaseJob::stepStarted, this, [=](auto* job, int stepIndex) {
        Q_UNUSED(job)
        setProcessedAmount(KJob::Unit::Items, 1+stepIndex*2);
    });
    connect(this, &GitRebaseJob::stepStarted, this, [=](auto* job, int stepIndex) {
        Q_UNUSED(job)
        setProcessedAmount(KJob::Unit::Items, 1+stepIndex*2+1);
    });
    setProgressUnit(KJob::Unit::Items);
}

KDevelop::IPlugin* GitRebaseJob::vcsPlugin() const
{
    return m_git;
}

void GitRebaseJob::finishAndEmitResult()
{
    qDebug() << "rebaseJob: FinishAndEmitResult called";
    if (m_project) m_project->projectFileManager()->resumeChangeWatcher(m_project);
    emitResult();
}

void GitRebaseJob::addStep(int stepIndex)
{
    const auto& step = m_plan.steps[stepIndex];
    KJob *firstStepJob = nullptr;
    KJob *lastStepJob = nullptr;

    Path stepScriptPath(m_git->gitDirPath(m_repository));
    stepScriptPath.addPath(PLAN_DIRNAME);
    stepScriptPath.addPath(QString::number(stepIndex));
    QFile stepScript(stepScriptPath.path());
    stepScript.open(QIODevice::WriteOnly);
    QTextStream out(&stepScript);

    out << QStringLiteral("#!/bin/sh\n");
    out << QStringLiteral("# Step #") << stepIndex << QStringLiteral("\n");

    if (std::holds_alternative<KDevelop::VcsDiff>(step.patch)) {
        auto patch = std::get<KDevelop::VcsDiff>(step.patch);
        auto* patchJob = new PauseOnErrorJob(m_git->apply(patch, GitPlugin::ApplyParams::Both));

        // If the patch job fails, there is probably a conflict
        connect(patchJob, &PauseOnErrorJob::paused, this, [stepIndex, this, step, patchJob, patch]{
            qDebug() << "Failed patch job: " << KShell::joinArgs(dynamic_cast<DVcsJob*>(patchJob->job())->dvcsCommand());
            qDebug() << "Failure code:" << patchJob->job()->error();
            qDebug() << "Failure error string:" << patchJob->job()->errorString();
            qDebug() << "rebaseJob: Conflict at step" << stepIndex << "when applying patch:";
            qDebug().noquote() << patch.diff();
            emit conflictAtStep(this, stepIndex, step);
            if (m_project) m_project->projectFileManager()->resumeChangeWatcher(m_project);
            // If we pause after this step anyway, we don't want to pause twice
            if (step.isPauseSet()) {
                patchJob->finishPause();
            } else {
                m_pause = patchJob;
            }
        });

        *m_job << patchJob;

        out << KShell::joinArgs(dynamic_cast<DVcsJob*>(patchJob->job())->dvcsCommand()) << QStringLiteral("\n");

        lastStepJob = firstStepJob = patchJob;
    } else if (std::holds_alternative<KDevelop::VcsRevision>(step.patch)) {
        auto revision = std::get<KDevelop::VcsRevision>(step.patch);

        // Cherry pick the specified revision
        auto* cherryJob = new PauseOnErrorJob(
            m_git->cherryPick(m_repository, revision, GitPlugin::CherryPickParams::NoCommit)
        );

        // If the cherry pick fails, there is probably a conflict
        connect(cherryJob, &PauseOnErrorJob::paused, this, [stepIndex, this, step, cherryJob, revision]{
            qDebug() << "rebaseJob: Conflict at step" << stepIndex << "when cherrypicking" << revision.prettyValue();
            emit conflictAtStep(this, stepIndex, step);
            if (m_project) m_project->projectFileManager()->resumeChangeWatcher(m_project);
            // If we pause after this step anyway we don't want to pause twice
            if (step.isPauseSet()) {
                cherryJob->finishPause();
            } else {
                m_pause = cherryJob;
            }
        });
        *m_job << cherryJob;
        out << KShell::joinArgs(dynamic_cast<DVcsJob*>(cherryJob->job())->dvcsCommand()) << QStringLiteral("\n");
        lastStepJob = firstStepJob = cherryJob;
    }

    // Add a pause to allow user to edit current state (add commits, ...)
    // will continue when the slot continueRebase is invoked
    if (step.isPauseSet()) {
        auto* pause = new PauseJob(this);
        connect(pause, &PauseJob::paused, this, [stepIndex, this, pause, &step](){
            qDebug() << "rebaseJob: Pausing for user changes";
            m_pause = pause;
            if (m_project) m_project->projectFileManager()->resumeChangeWatcher(m_project);
            emit stepPaused(this, stepIndex, step);
        });
        *m_job << pause;
        lastStepJob = pause;
    }


    // Add a job to commit the current step
    if (step.isCommitSet()) {
        auto* commitJob = new DelayedInitJob(this, [this, &step]() {
            return m_git->commitStaged(step.message, m_repository.toUrl(), step.commitOpts, step.extendedCommitOpts);
        });
        lastStepJob = commitJob;
        *m_job << commitJob;
        out << KShell::joinArgs({
            QStringLiteral("git"), QStringLiteral("commit"), QStringLiteral("--allow-empty"),
            QStringLiteral("-m"), step.message
        });
        lastStepJob = commitJob;
    }

    qDebug() << "rebaseJob: # Start job of " << stepIndex << "is" << firstStepJob;
    qDebug() << "rebaseJob: # End job of " << stepIndex << "is" << lastStepJob;

    // Connect signals for progress reporting
    connect(m_job, &KDevelop::ExecuteCompositeJob::subJobStarted, this,
            [firstStepJob, stepIndex, this, step, stepScriptPath](KJob* job){
                if (job == firstStepJob) {
                    m_currentStepIndex = stepIndex;
                    QFile script(stepScriptPath.path());
                    script.rename(stepScriptPath.path()+QStringLiteral("-running.sh"));
                    qDebug() << "rebaseJob: Starting step" << stepIndex << job;
                    emit stepStarted(this, stepIndex);
                }
            }
    );
    connect(m_job, &KDevelop::ExecuteCompositeJob::subJobFinished, this,
            [lastStepJob, stepIndex, this, stepScriptPath](KJob* job){
                if (job == lastStepJob) {
                    qDebug() << "rebaseJob: Finished step" << stepIndex << job;
                    QFile script(stepScriptPath.path()+QStringLiteral("-running.sh"));
                    script.rename(stepScriptPath.parent().path()+QStringLiteral("/done-")+QString::number(stepIndex));
                    emit stepFinished(this, stepIndex);
                }
            }
    );
}

void GitRebaseJob::continueRebase(const QString& message)
{
    Q_ASSERT(m_pause);
    qDebug() << "rebaseJob: Continuing rebase with message %1" << message;
    if (m_project) m_project->projectFileManager()->pauseChangeWatcher(m_project);
    if (!message.isEmpty()) {
        m_plan.steps[m_currentStepIndex].message = message;
    }
    if (m_pause) {
        m_pause->finishPause();
    }
}

bool GitRebaseJob::doKill()
{
    if (m_job) {
        return m_job->kill();
    }
    return true;
}

void GitRebaseJob::abortRebase()
{
    // FIXME: We should figure out how to kill the currently running job
    // run the cleanup jobs
    if (m_project) m_project->projectFileManager()->pauseChangeWatcher(m_project);
    ExecuteCompositeJob* cleanup = new ExecuteCompositeJob(this);

    auto* resetJob = new GitJob(QDir(m_repository.path()), m_git);
    resetJob->setType(VcsJob::Reset);
    *resetJob << "git" << "reset" << "--hard";
    *cleanup << resetJob;
    *cleanup << m_git->switchBranchNoUI(m_repository.toUrl(), m_branchName, GitPlugin::NoUIOptions::ProceedOnUncleanRepo);
    *cleanup << m_git->deleteBranch(m_repository.toUrl(), m_tempBranchName);
    // TODO: It would be better to find the index of our "saved stash" and pop that;
    // for now we just assume that its the top stash (hopefully the user didn't
    // push a new stash during rebasing)
    *cleanup << m_git->gitStash(QDir(m_repository.path()), {QStringLiteral("pop"),}, OutputJobVerbosity::Verbose);
    connect(cleanup, &ExecuteCompositeJob::finished, this, [this](auto* job) {
        if (job->error()) {
            doFail(ErrorCode::AbortFailed, job->errorText());
        } else {
            if (unlock()) {
                doFail(ErrorCode::RebaseCanceledByUser, {});
            };
        }
    });
    cleanup->setAbortOnError(true);
    cleanup->start();
}

void GitRebaseJob::finishRebase()
{
    qDebug() << "rebaseJob: Finishing rebase";
    ExecuteCompositeJob* completeRebase {new ExecuteCompositeJob(this)};
    *completeRebase << m_git->renameBranch(m_repository.toUrl(), m_branchName, m_backupBranchName);
    *completeRebase << m_git->renameBranch(m_repository.toUrl(), m_tempBranchName, m_branchName);
    *completeRebase << m_git->switchBranchNoUI(m_repository.toUrl(), m_branchName, GitPlugin::NoUIOptions::ProceedOnUncleanRepo);
    // TODO: It would be better to find the index of our "saved stash" and pop that;
    // for now we just assume that its the top stash (hopefully the user didn't
    // push a new stash during rebasing)
    *completeRebase << m_git->gitStash(QDir(m_repository.path()), {QStringLiteral("pop"),}, OutputJobVerbosity::Verbose);
    connect(completeRebase, &ExecuteCompositeJob::finished, this, [this](auto* job) {
        qDebug() << "rebaseJob: Rebase complete";
        if (job->error()) {
            doFail(ErrorCode::CleanupFailed, job->errorString());
        } else {
            if (unlock())
                doSucceed();
        }
    });
    connect(completeRebase, &ExecuteCompositeJob::subJobStarted, this, [](auto* job){
        qDebug() << "rebaseJob: completeRebase started subjob:" << job;
    });
    connect(completeRebase, &ExecuteCompositeJob::subJobFinished, this, [](auto* job){
        qDebug() << "rebaseJob: completeRebase finished subjob:" << job;
    });
    completeRebase->setAbortOnError(true);
    qDebug() << "rebaseJob: Starting completeRebase" << completeRebase;
    completeRebase->start();
    qDebug() << "rebaseJob: Done starting completeRebase";
}

bool GitRebaseJob::lock()
{
    QFile lock(m_lockPath.path());
    if (lock.exists()) {
        doFail(ErrorCode::OtherRebaseInProgress, {});
        return false;
    }
    if (! lock.open(QIODevice::WriteOnly)) {
        doFail(ErrorCode::CannotCreateLockFile, i18n("File error: %1.",lock.error()));
        return false;
    };
    QStringList contents;
    contents << QStringLiteral("#!/bin/sh");
    contents << i18n("# Created by KDevelop's git plugin.");
    contents << i18n("# Run this script (or the following commands by hand) to abort a failed rebase in progress; then delete it.");
    contents << QStringLiteral("git reset --hard");
    contents << QStringLiteral("git switch ") + m_branchName;
    contents << QStringLiteral("git branch -D '")+m_tempBranchName+QStringLiteral("'");
    contents << QStringLiteral("STASH=$(git stash list | grep '")+m_tempStashName+QStringLiteral("'| cut -d: -f1)");
    contents << QStringLiteral("if [ ! -z $STASH ]; then");
    contents << QStringLiteral("    git stash pop \"$STASH\"");
    contents << QStringLiteral("fi;");
    contents << QString{};
    auto buf = contents.join(QLatin1Char('\n')).toUtf8();
    if (lock.write(buf) != buf.size()) {
        doFail(ErrorCode::CannotCreateLockFile, QStringLiteral("Error when writing: ")+QString{lock.error()});
        return false;
    };
    lock.close();
    QDir gitDir(m_git->gitDirPath(m_repository).path());
    gitDir.mkdir(PLAN_DIRNAME);
    return true;
}

bool GitRebaseJob::unlock()
{
    QFile lock(m_lockPath.path());
    if (!lock.exists()) {
        doFail(ErrorCode::CannotRemoveLockFile, i18n("Lock file not found."));
        return false;
    }
    if (! lock.open(QIODevice::ReadOnly)) {
        doFail(ErrorCode::CannotRemoveLockFile, i18n("Lock file not readable."));
        return false;
    };
    if (!QString::fromUtf8(lock.readAll()).contains(m_tempBranchName)) {
        doFail(ErrorCode::InvalidLockFile, i18n("Lockfile does not contain %1.", m_tempBranchName));
        return false;
    }
    if (!lock.remove()) {
        doFail(ErrorCode::CannotRemoveLockFile, i18n("File error: %1.", lock.error()));
        return false;
    };
    // Remove the plan directory
//     Path planDirPath(m_repository);
//     m_repository.addPath(QStringLiteral(".git"));
//     m_repository.addPath(PLAN_DIRNAME);
//     QDir planDir(planDirPath.path());
//     planDir.removeRecursively();
    return true;
}

void GitRebaseJob::start()
{
    ExecuteCompositeJob* startRebase {new ExecuteCompositeJob(this)};

    if (!lock())
        return;

    *startRebase << static_cast<KDevelop::VcsJob*>(
        m_git->gitStash(QDir(m_repository.path()), {
            QStringLiteral("save"),
            QStringLiteral("--include-untracked"),
            QStringLiteral("-m"),
            m_tempStashName,
        }, OutputJobVerbosity::Verbose)
    );
    *startRebase << m_git->branch(m_repository.toUrl(), m_plan.base, m_tempBranchName);
    *startRebase << m_git->switchBranchNoUI(m_repository.toUrl(), m_tempBranchName, GitPlugin::NoUIOptions::ProceedOnUncleanRepo);
    startRebase->setAbortOnError(true);

    for(std::vector<GitPlugin::RebaseStep>::size_type i=0; i < m_plan.steps.size(); i++) {
        addStep(i);
    }
    setTotalAmount(KJob::Unit::Items, m_plan.steps.size()+2);

    // Add a job to check that the resulting branch as the original
    *m_job << confirmRebaseIfChanged(m_repository, m_branchName, m_tempBranchName);

    connect(startRebase, &ExecuteCompositeJob::subJobStarted, this, [](auto* job){
        qDebug() << "rebaseJob: startRebase: started subjob:" << job;
    });
    connect(startRebase, &ExecuteCompositeJob::subJobFinished, this, [](auto* job){
        qDebug() << "rebaseJob: startRebase: finished subjob:" << job;
    });

    connect(startRebase, &ExecuteCompositeJob::finished, this, [this](auto* job){
        setProcessedAmount(KJob::Unit::Items, 1);
        if (job->error()) {
            qWarning() << "rebaseJob: startRebase: failed to prepare rebase with error:" << job->errorString();
            doFail(ErrorCode::FailedToStart, job->errorString());
        } else {
            m_job->start();
        }
    });

    connect(m_job, &ExecuteCompositeJob::finished, this, [this](auto*){
        if (m_job->error()) {
            abortRebase();
        } else {
            finishRebase();
        }
    });
    m_status = JobRunning;
    qDebug() << "rebaseJob: Start rebase job:" << startRebase;
    qDebug() << "rebaseJob: Rebase plan" << &m_job;
    if (m_project) m_project->projectFileManager()->pauseChangeWatcher(m_project);
    startRebase->start();
}

QString GitRebaseJob::errorString() const
{
    switch(m_internalError) {
        case ErrorCode::Ok:
            return {};
        case ErrorCode::RebaseCanceledByUser:
            return i18n("Rebase canceled by user.");
        case ErrorCode::CleanupFailed:
            return i18n("Error cleaning up after rebase: %1", errorText());
        case ErrorCode::AbortFailed:
            return i18n(
                "Git Rebase failed to cleanup after user canceled: %1.\n"
                "Not deleting lockfile %2.",
                errorText(), m_lockPath.path()
            );
        case ErrorCode::OtherRebaseInProgress:
            return i18n(
                "Another rebase already in progress.\n"
                "If this is not the case, please remove the lockfile %1.",
                m_lockPath.path()
            );
        case ErrorCode::CannotCreateLockFile:
            return i18n(
                "Cannot create the lockfile %1 (%2).\n"
                "Not proceeding with rebase.",
                m_lockPath.path(), errorText()
            );
        case ErrorCode::CannotRemoveLockFile:
            return i18n(
                "Failed to remove the lockfile %1 (%2).\n"
                "Future rebasing will fail, until it is removed manually.",
                m_lockPath.path(), errorText()
            );
        case ErrorCode::InvalidLockFile:
            return i18n(
                "Corrupted lockfile found when removing it %1 (%2).\n"
                "Future rebasing will fail, until it is removed manually.",
                m_lockPath.path(), errorText()
            );
        case ErrorCode::FailedToStart:
            return i18n("Cannot start rebase: %1.", errorText());
    }
    return {};
}

void GitRebaseJob::doFail(GitRebaseJob::ErrorCode errorCode, const QString& errorDetail)
{
    if (errorCode == ErrorCode::RebaseCanceledByUser) {
        m_status = JobCanceled;
    } else {
        m_status = JobFailed;
    }
    m_internalError = errorCode;
    setErrorText(errorDetail);
    // Since rebase job errors are already shown in the Git History Tool View,
    // we don't want to show them twice (i.e. via KDevelop::RunController::finished)
    setError(FailedShownError);
    qDebug() << "rebaseJob: failed with error:" << errorDetail;
    emit rebaseFailed(errorString());
}

void GitRebaseJob::doSucceed()
{
    m_status = JobSucceeded;
    setError(0);
    finishAndEmitResult();
    return;
}


KJob* GitRebaseJob::confirmRebaseIfChanged(const KDevelop::Path path, const QString& branchName, const QString& backupBranchName)
{
    auto ret = new PauseJob(this);
    VcsRevision revBranch(branchName, VcsRevision::GlobalNumber)
              , revBackup(backupBranchName, VcsRevision::GlobalNumber);
    auto diff_job = m_git->diff(path, revBranch, revBackup);
    connect(ret, &PauseJob::paused, diff_job, &VcsJob::start);
    connect(diff_job, &VcsJob::finished, this, [=]{
        if (diff_job->error()) {
            auto res = KMessageBox::questionTwoActions(nullptr,
                                                   i18n("Failed to compare the state before and after the rebase:")
                                                        + QLatin1String("<br/><br/>") + diff_job->errorText() + QLatin1String("<br/><br/>") +
                                                   i18n("Do you want to continue with the rebase or cancel it?")
                                                       + QLatin1String("<br/><br/>"),
                                                   {}, KStandardGuiItem::cont(), KStandardGuiItem::cancel());
            if (res != KMessageBox::PrimaryAction) {
                ret->failPause(1);
                return;
            }
            ret->finishPause();
            return;

        }

        auto diff = diff_job->fetchResults().value<VcsDiff>();
        if (! diff.diff().isEmpty()) {
            auto res = KMessageBox::questionTwoActions(nullptr,
                                                i18n("The code after the rebase differs from the code before.") + QLatin1String("<br/><br/>") +
                                                diff.diff().replace(QLatin1Char('\n'), QStringLiteral("<br/>")) + QLatin1String("<br/><br/>") +
                                                i18n("Do you want to continue with the rebase or cancel it?") + QLatin1String("<br/><br/>"),
                                                {}, KStandardGuiItem::cont(), KStandardGuiItem::cancel());
            if (res != KMessageBox::PrimaryAction) {
                ret->failPause(1);
                return;
            }
        }
        ret->finishPause();
    });
    return ret;
}
