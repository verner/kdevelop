/*
    SPDX-FileCopyrightText: 2021 Jonathan L. Verner <jonathan.verner@matfyz.cz>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef KDEVPLATFORM_PLUGIN_GITPLUGIN_GITREBASEJOB_H
#define KDEVPLATFORM_PLUGIN_GITPLUGIN_GITREBASEJOB_H

#include "gitplugin.h"

// kdevplatform headers
#include <vcs/vcsdiff.h>
#include <vcs/vcsrevision.h>
#include <vcs/vcsjob.h>
#include <util/path.h>
#include <util/kdevstringhandler.h>
#include <util/executecompositejob.h>

// Qt Headers
#include <QString>

// System headers
#include <variant>

namespace KDevelop {
    class Path;
    class PauseJob;
    class IProject;
}

/**
 * A job which is used to perform rebase operations.
 *
 * A rebase job consists of three phases:
 *
 *   1. An init phase, where the current state of the working tree
 *   and index is saved and a lock is taken (to prevent multiple
 *   rebase jobs to run in parallel);
 *
 *   2. The actual rebase phase, where the rebase steps are performed.
 *   If a step fails or is paused, the RebaseJob emits corresponding signals
 *   (see @ref GitRebaseJob::stepPaused, @ref GitRebaseJob::conflictAtStep)
 *
 *   3. A cleanup phase, where the original branch being rebased is (moved)
 *   renamed to the backup branch and the temporary rebase branch is renamed (moved)
 *   to the original branch, the original (pre-rebase) state of the worktree
 *   and index is restored (by popping from the stash; this means that any changes
 *   to the worktree/index left from the rebase, e.g. if the user made changes
 *   during rebasing, are <b>preserved</b>) and the lock is released.
 *
 * The rebase job will create a series of bash scripts in `.git/@ref GitRebaseJob::PLAN_DIRNAME`
 * which can be run manually to perform the rebase.
 *
 * <h1>Recovering from a botched rebase</h1>:
 *
 * The rebase is performed in such a way that no changes are made to the branch being rebased until
 * the final step of the rebase. So if, e.g., a crash occurs in the middle, the only
 * thing necessary is to remove the temporary branch starting in the @ref GIT_SCOPE_PREFIX
 * namespace and delete the `.git/@ref GitRebaseJob::PLAN_DIRNAME` directory and the
 * `.git/@ref GitRebaseJob::LOCK_FILENAME` lockfile.
 *
 * If the rebase progressed far enough that the user does not want to lose the associated
 * work, the `.git/@ref GitRebaseJob::PLAN_DIRNAME` should contain bash scripts which can
 * perform the rest of the rebase (scripts whose names start with `done` correspond
 * to the rebase steps which have successfully finished so far). Then the branch which is
 * being rebased can be deleted (or renamed to a backup name) and the current branch
 * (which is a temporary branch in the @ref GIT_SCOPE_PREFIX namespace) can be moved to
 * the rebase branch. Then cleaup of the temporary files needs to be done to allow kdevelop
 * to perform further rebases (in particular the lockfile needs to be deleted).
 *
 */
class GitRebaseJob: public KDevelop::VcsJob {
    Q_OBJECT
public:

    /** The name of the lockfile guarding against running multiple rebase jobs in parallel. */
    static constexpr auto LOCK_FILENAME = ConstLatin1String("ABORT_KDEVELOP_REBASE_IN_PROGRESS.sh");
    /** The name of a directory under .git where bash scripts to manually perform the rebase are created */
    static constexpr auto PLAN_DIRNAME = ConstLatin1String("KDEVELOP_REBASE_PLAN");
    /** Prefix used for all temporary branch names. */
    static constexpr auto GIT_SCOPE_PREFIX = ConstLatin1String("KDEVELOP_REBASE/");

    /**
     * Creates a rebase job in the repository @p repository, which rebases
     * the branch @p branchName (replaces the commits base..branch_head with
     * the steps in @p plan) starting at commit @p base, performing the
     * steps specified in @p plan and, optionally, creating a backup branch
     * @p backupBranchName whose HEAD points to the old (pre-rebase) HEAD
     * of the branch @p branchName.
     *
     * @note The job will abort if it is started when another rebase job is in progress in the repository.
     * @note The rebased branch will be created as a temporary branch and
     * only moved to @p branchName as the last step. This should make recovery
     * easy in case of an unfinished rebase: its enough to just remove the
     * temporary branch.
     */
    GitRebaseJob(
        KDevelop::IProject* project,
        GitPlugin* git,
        const KDevelop::Path& repository,
        const QString& branchName,
        const GitPlugin::RebasePlan& plan,
        const QString& backupBranchName
    );

    /**
     * Methods overriden from the base class.
     *
     * See @ref KDevelop::VcsJob and @ref KJob.
     */
    void start() override;
    KDevelop::VcsJob::JobStatus status() const override {return m_status;};
    KDevelop::IPlugin* vcsPlugin() const override;
    QVariant fetchResults() override {return {};};



    /**
     * An enumeration specifying reasons for a rebase job failure.
     */
    enum class ErrorCode {
        Ok = 0,
        RebaseCanceledByUser = KJob::UserDefinedError,  /**< Job was canceled by user */
        OtherRebaseInProgress, /**< Another rebase is already in progress in the repo */
        CannotCreateLockFile,  /**< Cannot create the lock file */
        CannotRemoveLockFile,  /**< Cannot remove the lock file */
        InvalidLockFile,       /**< Corrupted lockfile found when trying to remove it.*/
        CleanupFailed,         /**< Cleanup after successful rebase failed */
        AbortFailed,           /**< Cleanup after aborted rebase failed */
        FailedToStart,         /**< Rebase preparation failed. */
    };

    /**
     * In case of failure, returns a (translated) string giving reason for
     * the failure, which can be shown in the UI. In case the job did not
     * fail, empty string is returned.
     */
    QString errorString() const override;

    /**
     * @returns The a reference to the rebase step currently being processed.
     * @warning Only call this when you are sure that a step is being processed.
     */
    GitPlugin::RebaseStep& currentStep() {return m_plan.steps[m_currentStepIndex];}


public Q_SLOTS:

    /**
     * Continues a paused rebase. If @p message is nonempty and the current
     * rebase step has @p commit set to true, @p message is used instead
     * for the commit message instead of @ref step.message.
     */
    void continueRebase(const QString& message={});

    /**
     * Aborts a rebase and cleanup (remove the temporary branch, restore the
     * pre-rebase state and release the lockfile)
     */
    void abortRebase();

Q_SIGNALS:

    /**
     * This signal is emitted when a rebase step results in a (merge) conflict, which
     * must be resolved by the user.
     */
    void conflictAtStep(GitRebaseJob* job, uint stepIndex, const GitPlugin::RebaseStep& stepSpec);

    /**
     * Emitted when a rebase step is started.
     */
    void stepStarted(GitRebaseJob* job, uint stepIndex);

    /**
     * Emitted when a rebase step is finished.
     */
    void stepFinished(GitRebaseJob* job, uint stepIndex);

    /**
     * Emitted when a rebase step with @ref pause set to true
     * is successfully processed. The rebase job will continue when the
     * @ref continueRebase slot is called.
     *
     * TODO: Move stepSpec to first arg, since typically its the only used argument
     */
    void stepPaused(GitRebaseJob* job, uint stepIndex, const GitPlugin::RebaseStep& stepSpec);

    /**
     * Emitted when the rebase job fails for any reason.
     *
     * The @p error is a translated string which is suitable for showing to the user
     * in the UI.
     */
    void rebaseFailed(const QString& error);


protected:
    bool doKill() override;

    void finishAndEmitResult();

    /**
     * Sets the error code, text, job status and emits the rebaseFailed signal.
     *
     * @note: The rebaseFailed signal is connected to @ref finishAndEmitResult,
     * so there is no need to call it afterwards.
     */
    void doFail(ErrorCode errorCode, const QString& errorDetail);

    /**
     * Sets the error code, text, job status and calls @ref finishAndEmitResult
     */
    void doSucceed();

private Q_SLOTS:
    void finishRebase();

private:

    void addStep(int stepIndex);

    KJob* confirmRebaseIfChanged(const KDevelop::Path path, const QString& branchName, const QString& backupBranchName);

    /**
     * Tries creating a lock file.
     *
     * @returns true if successful, false otherwise
     *
     * @note If the function fails to create the lockfile, it automatically
     * fails the complete rebaseJob by calliong @ref doFail.

     * @note Typically, the function fails if another rebase is in progress.
     * It can also fail due to filesystem errors (e.g. insufficient permissions
     * to create/write to a file in the .git subdirectory)
     */
    bool lock();

    bool unlock();

    KDevelop::IProject* m_project;
    GitPlugin *m_git;
    KDevelop::Path m_repository;

    /** The branch which is being rebased */
    QString m_branchName;
    /** The name to which the old (pre-rebase) branch will be renamed */
    QString m_backupBranchName;
    /** The revision to start rebasing at (see @ref RebasePlan.base) */
    KDevelop::VcsRevision m_baseRevision;

    /** An UUID for this job used to construct temporary branch & stash names */
    QString m_uuid;
    /** The name of the temporary branch on which the rebase operations are made */
    QString m_tempBranchName;
    /** The name of the stash commit which holds the state of the pre-rebase worktree and indes */
    QString m_tempStashName;

    /** The (absolute) path of the lockfile */
    KDevelop::Path m_lockPath;

    GitPlugin::RebasePlan m_plan;

    /** The rebase job (excluding init & cleanup) */
    KDevelop::ExecuteCompositeJob* m_job;
    /** Currently paused job */
    KDevelop::PauseJob* m_pause;
    /** The index of the step currently being processed */
    uint m_currentStepIndex;
    /** The current status of the complete job (including init & cleanup) */
    JobStatus m_status;
    /** In case of failure, contains a more specific reason why a job failed. */
    ErrorCode m_internalError = ErrorCode::Ok;

};

#endif // KDEVPLATFORM_PLUGIN_GITPLUGIN_GITREBASEJOB_H
