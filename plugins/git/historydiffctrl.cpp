/*
    SPDX-FileCopyrightText: 2021 Jonathan Verner <jonathan@temno.eu>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/
#include "historydiffctrl.h"

// Local headers
#include "revisiondiffview.h"

// kdevplatform headers
#include <interfaces/icore.h>
#include <interfaces/iproject.h>
#include <interfaces/iprojectcontroller.h>

// KDE Frameworks headers
#include <KLocalizedString>

using namespace KDevelop;

RevisionDiffCtrl::RevisionDiffCtrl(QObject* parent)
: QObject(parent)
{
    connect(ICore::self()->projectController(), &IProjectController::projectClosing, this, &RevisionDiffCtrl::projectClosed);
}


void RevisionDiffCtrl::projectClosed(KDevelop::IProject* project)
{
    QList<RevisionDiffView*> toCleanup;
    for(const auto& [key, data]: m_views) {
        Q_UNUSED(key);
        if (data && (!project || data->project() == project)) {
            toCleanup.push_back(data);
        }
    }
    for(auto* data: toCleanup) {
        if (data) {
            data->cleanup();
            delete data;
        }
    }
}


void RevisionDiffCtrl::showRevisionPathDiff(
    KDevelop::IProject* project,
    const KDevelop::VcsRevision rev,
    const QString& relativePath,
    const KTextEditor::Cursor initialPos
)
{

    auto path{project->path()};
    path.addPath(relativePath);

    ViewID key{rev.prettyValue()+QStringLiteral(":")+path.path()};

    // If an appropriate view is already present, activate it
    auto viewDataIt = m_views.find(key);
    if (viewDataIt != m_views.end()) {
        viewDataIt->second->activate();
        return;
    }
    QString title = (relativePath.isEmpty() ? i18n("Commit"):relativePath)+QStringLiteral(" @ ")+rev.prettyValue();
    auto* view = new RevisionDiffView(this, project, rev, relativePath, title, initialPos);
    connect(view, &RevisionDiffView::showRevision, this, &RevisionDiffCtrl::showRevisionPathsDiff);
    connect(view, &RevisionDiffView::splitCommit, this, &RevisionDiffCtrl::splitCommit);
    connect(view, &RevisionDiffView::error, this, &RevisionDiffCtrl::error);
    connect(view, &RevisionDiffView::cleanedUp, this, [=]{
        m_views.erase(key);
    });
    m_views[key] = view;
}


void RevisionDiffCtrl::showRevisionPathsDiff(
    KDevelop::IProject* project,
    const KDevelop::VcsRevision rev,
    const QList<QString> relativePaths,
    const KTextEditor::Cursor initialPos
)
{
    if (relativePaths.isEmpty()) {
        showRevisionPathDiff(project, rev, QString{}, initialPos);
    } else if (relativePaths.size() == 1) {
        showRevisionPathDiff(project, rev, relativePaths[0], initialPos);
    } else {
        ViewID key{
            rev.prettyValue()+
            QStringLiteral(":")+
            project->path().path()+
            QStringLiteral("/")+
            relativePaths.join(QStringLiteral(";"))
        };
        // If an appropriate view is already present, activate it
        auto viewDataIt = m_views.find(key);
        if (viewDataIt != m_views.end()) {
            viewDataIt->second->activate();
            return;
        }
        QString title = i18n("Commit @ %1", rev.prettyValue());
        RevisionDiffView* view = new RevisionDiffView(this, project, rev, relativePaths, title);
        connect(view, &RevisionDiffView::showRevision, this, &RevisionDiffCtrl::showRevisionPathsDiff);
        connect(view, &RevisionDiffView::splitCommit, this, &RevisionDiffCtrl::splitCommit);
        connect(view, &RevisionDiffView::error, this, &RevisionDiffCtrl::error);
        connect(view, &RevisionDiffView::cleanedUp, this, [=]{
            m_views.erase(key);
        });
        m_views[key] = view;
    }
}

void RevisionDiffCtrl::showRevisionRangeDiff(
    KDevelop::IProject* project,
    const KDevelop::VcsRevision revStart,
    const KDevelop::VcsRevision revEnd,
    const QList<QString> relativePaths,
    const KTextEditor::Cursor initialPos
)
{
    if (revStart == revEnd) {
        showRevisionPathsDiff(project, revStart, relativePaths);
        return;
    }
    ViewID key{
        revStart.prettyValue() +
        QStringLiteral("..") +
        revEnd.prettyValue() +
        QStringLiteral(":") +
        project->path().path()+
        QStringLiteral("/")+
        relativePaths.join(QStringLiteral(";"))
    };
    // If an appropriate view is already present, activate it
    auto viewDataIt = m_views.find(key);
    if (viewDataIt != m_views.end()) {
        viewDataIt->second->activate();
        return;
    }
    QString title = i18n("Commit Range %1..%2", revStart.prettyValue(), revEnd.prettyValue());
    RevisionDiffView* view = new RevisionDiffView(this, project, revStart, revEnd, relativePaths, title, initialPos);
    connect(view, &RevisionDiffView::showRevision, this, &RevisionDiffCtrl::showRevisionPathsDiff);
    connect(view, &RevisionDiffView::splitCommit, this, &RevisionDiffCtrl::splitCommit);
    connect(view, &RevisionDiffView::error, this, &RevisionDiffCtrl::error);
    connect(view, &RevisionDiffView::cleanedUp, this, [=]{
        m_views.erase(key);
    });
    m_views[key] = view;
}
