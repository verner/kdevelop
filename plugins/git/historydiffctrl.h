/*
    SPDX-FileCopyrightText: 2021 Jonathan Verner <jonathan@temno.eu>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#ifndef HISTORYDIFFCTRL__H
#define HISTORYDIFFCTRL__H

// kdevplatform headers
#include <vcs/vcsdiff.h>
#include <vcs/vcsrevision.h>
#include <util/path.h>

// KDE Frameworks headers
#include <KTextEditor/Cursor>

// Qt Headers
#include <QList>

// System Headers
#include <memory>

#include <unordered_map>

namespace KDevelop {
    class IProject;
}

class RevisionDiffView;


class RevisionDiffCtrl : public QObject {
    Q_OBJECT
public:
    typedef QString ViewID;

public:
    RevisionDiffCtrl(QObject* parent = nullptr);

Q_SIGNALS:
    void error(const QString& error);
    void splitCommit(KDevelop::IProject* project, const KDevelop::VcsRevision rev, const QList<KDevelop::VcsDiff> patches);

public Q_SLOTS:
    void showRevisionRangeDiff(
        KDevelop::IProject* project,
        const KDevelop::VcsRevision revStart,
        const KDevelop::VcsRevision revEnd,
        const QList<QString> relativePaths,
        const KTextEditor::Cursor pos = {}
    );

    void showRevisionPathDiff(
        KDevelop::IProject* project,
        const KDevelop::VcsRevision rev,
        const QString& relativePath,
        const KTextEditor::Cursor pos = {}
    );
    void showRevisionPathsDiff(
        KDevelop::IProject* project,
        const KDevelop::VcsRevision rev,
        const QList<QString> relativePaths,
        const KTextEditor::Cursor pos = {}
    );

    void projectClosed(KDevelop::IProject* project);

private:
    std::unordered_map<ViewID, RevisionDiffView*> m_views;
};

#endif // HISTORYDIFFCTRL__H
