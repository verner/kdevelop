/*
    SPDX-FileCopyrightText: 2021 Jonathan L. Verner <jonathan.verner@matfyz.cz>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "historyview.h"

#include "historydiffctrl.h"
#include "droptreeview.h"
#include "simplecommitform.h"
#include "gitrebasejob.h"
#include "rebasecontroller.h"

#include <interfaces/icore.h>
#include <interfaces/idocumentcontroller.h>
#include <interfaces/iruncontroller.h>
#include <interfaces/iproject.h>
#include <interfaces/iprojectcontroller.h>
#include <util/path.h>
#include <vcs/vcsevent.h>
#include <vcs/models/vcseventmodel.h>
#include <vcs/widgets/vcseventitemdelegate.h>

#include <KLocalizedString>
#include <KMessageWidget>
#include <KTextEditor/View>

#include <QAction>
#include <QDebug>
#include <QHBoxLayout>
#include <QIcon>
#include <QLabel>
#include <QLineEdit>
#include <QMenu>
#include <QMimeData>
#include <QPushButton>
#include <QRegularExpression>
#include <QSortFilterProxyModel>
#include <QSplitter>
#include <QTreeView>

#include <algorithm>
#include <array>

/*
 * Git-Dch: Ignore
 * GIT_SILENT
 * SVN_SILENT
 */

using namespace KDevelop;

/**
 * A filter to be used on the history model (for, e.g., hiding
 * SVN_SILENT, GIT_SILENT, etc. commits)
 */
class FilterProxyModel : public QSortFilterProxyModel
{
public:
    const QRegularExpression silentPattern {QStringLiteral("(GIT_SILENT)|(SVN_SILENT)")};
    bool ignoreSilent = false;

    using QSortFilterProxyModel::QSortFilterProxyModel;
    bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const override
    {
        const QModelIndex rowIndex = sourceModel()->index(sourceRow, 0, sourceParent);
        auto data = rowIndex.data(VcsEventLogTreeModel::VcsDataRole);

        if (data.canConvert<VcsEvent>()) {
            auto event = rowIndex.data(VcsEventLogTreeModel::VcsDataRole).value<VcsEvent>();
            auto hash = event.customData().canConvert<GitPlugin::ExtendedRevisionInfo>()
                    ? event.customData().value<GitPlugin::ExtendedRevisionInfo>().fullHash
                    : event.revision().revisionValue().toString();

            if (ignoreSilent && silentPattern.match(event.message()).hasMatch())
                return false;
            if (
                !filterRegularExpression().match(event.message()).hasMatch() &&
                !filterRegularExpression().match(event.author()).hasMatch()  &&
                !filterRegularExpression().match(hash).hasMatch() &&
                !matchFiles(event) &&
                !matchTags(event)
            ) {
                return false;
            }
            //TODO: Filter on file names and shas
        }
        return true;
    }
    void setFilterWildcard(const QString& pattern) {
        setFilterRegularExpression(pattern);
    }
    bool matchFiles(const VcsEvent& event) const {
        for(const auto& item: event.items()) {
            if (filterRegularExpression().match(item.repositoryLocation()).hasMatch())
                return true;
        }
        return false;
    }
    bool matchTags(const VcsEvent& event) const {
        if (!event.customData().canConvert<GitPlugin::ExtendedRevisionInfo>()) {
            return false;
        }
        for(const auto& tag: event.customData().value<GitPlugin::ExtendedRevisionInfo>().tags) {
            if (filterRegularExpression().match(tag).hasMatch())
                return true;
        }
        return false;
    }
};



HistoryView::HistoryView(
    QWidget* parent,
    GitPlugin* git
)
    : QWidget(parent)
    , m_git(git)
    , m_historyCtrl(m_git->revisionDiffController())
    , m_rebaseController(m_git->rebaseController())
    , m_proxyModel(new FilterProxyModel(this))
    , m_abortRebaseButton(new QPushButton(
        QIcon::fromTheme(QStringLiteral("media-playback-stop")),
        i18n("Abort rebase")
    ))
    , m_continueRebaseButton(new QPushButton(
        QIcon::fromTheme(QStringLiteral("media-playback-start")),
        i18n("Continue rebase")
    ))
    , m_cancelEditMessageButton(new QPushButton(
        QIcon::fromTheme(QStringLiteral("dialog-cancel")),
        QString{}
    ))
    , m_progressLabel(new QLabel())
    , m_commitMessageForm(new SimpleCommitForm)
    , m_loadMoreCommitsButton(new QPushButton(
        QIcon::fromTheme(QStringLiteral("download")),
        i18n("Load More")
    ))
    , m_inlineError(new KMessageWidget)
    , m_view(new DropTreeView)
    , m_styleDelegate(new VcsEventItemDelegate)
    , m_filter(new QLineEdit)

    , m_editMessageAct(new QAction(QIcon::fromTheme(QStringLiteral("edit")), i18n("Edit commit message"), this))
    , m_removeRevAct(new QAction(QIcon::fromTheme(QStringLiteral("user-trash")), i18n("Remove commit"), this))
    , m_hardResetAct(new QAction(QIcon::fromTheme(QStringLiteral("go-last-view")), i18n("Hard reset to commit"), this))
    , m_softResetAct(new QAction(QIcon::fromTheme(QStringLiteral("go-last-view")), i18n("Soft reset to commit"), this))
    , m_squashUpRevAct(new QAction(QIcon::fromTheme(QStringLiteral("go-top")), i18n("Squash with child commit"), this))
    , m_squashDownRevAct(new QAction(QIcon::fromTheme(QStringLiteral("go-bottom")), i18n("Squash with parent commit"), this))
    , m_squashRevRangeAct(new QAction(QIcon::fromTheme(QStringLiteral("vcs-squash")), i18n("Squash into a single commit"), this))
    , m_moveUpRevAct(new QAction(QIcon::fromTheme(QStringLiteral("go-up-skip")), i18n("Exchange with child commit"), this))
    , m_moveUpFilesAct(new QAction(QIcon::fromTheme(QStringLiteral("go-top")), i18n("Move to child commit"), this))
    , m_moveDownRevAct(new QAction(QIcon::fromTheme(QStringLiteral("go-down-skip")), i18n("Exchange with parent commit"), this))
    , m_moveDownFilesAct(new QAction(QIcon::fromTheme(QStringLiteral("go-bottom")), i18n("Move to parent commit"), this))
    , m_splitUpFilesAct(new QAction(QIcon::fromTheme(QStringLiteral("split")), i18n("Extract into new child commit"), this))
    , m_splitDownFilesAct(new QAction(QIcon::fromTheme(QStringLiteral("split")), i18n("Extract into new parent commit"), this))
    , m_editCommitAct(new QAction(QIcon::fromTheme(QStringLiteral("edit")), i18n("Edit this commit"), this))
    , m_showDiffRevAct(new QAction(QIcon::fromTheme(QStringLiteral("vcs-diff")), i18n("Show diff"), this))
    , m_showDiffFilesAct(new QAction(QIcon::fromTheme(QStringLiteral("vcs-diff")), i18n("Show diff"), this))
    , m_reloadHistoryModelAct(new QAction(QIcon::fromTheme(QStringLiteral("view-refresh")), i18n("Reload"), this))

    , m_actionHandlers({
        {m_editCommitAct, [this](auto evList, auto /* fileList */) {
            m_rebaseController->editCommit(m_project, evList[0].revision());
        }},
        {m_editMessageAct, [this](auto evList, auto /* fileList */) {
            startEditRevisionMessage(evList[0]);
        }},
        {m_squashUpRevAct, [this](auto evList, auto /* fileList */) {
            m_rebaseController->squashWithChild(m_project, evList[0].revision());
        }},
        {m_squashDownRevAct, [this](auto evList, auto /* fileList */) {
            m_rebaseController->squashWithParent(m_project, evList[0].revision());
        }},
        {m_squashRevRangeAct, [this](auto evList, auto /* fileList */) {
            m_rebaseController->squashRevisions(m_project, evList[0].revision(), evList.last().revision());
        }},
        {m_moveUpRevAct, [this](auto evList, auto /* fileList */) {
            m_rebaseController->moveUp(m_project, evList);
        }},
        {m_moveUpFilesAct, [this](auto evList, auto fileList) {
            m_rebaseController->moveUp(m_project, evList[0].revision(), fileList);
        }},
        {m_moveDownRevAct, [this](auto evList, auto /* fileList */) {
            m_rebaseController->moveDown(m_project, evList);
        }},
        {m_moveDownFilesAct, [this](auto evList, auto fileList) {
            m_rebaseController->moveDown(m_project, evList[0].revision(), fileList);
        }},
        {m_splitUpFilesAct, [this](auto evList, auto fileList) {
            m_rebaseController->splitUp(m_project, evList[0].revision(), fileList);
        }},
        {m_splitDownFilesAct, [this](auto evList, auto fileList) {
            m_rebaseController->splitDown(m_project, evList[0].revision(), fileList);
        }},
        {m_removeRevAct, [this](auto evList, auto /* fileList */) {
            m_rebaseController->remove(m_project, evList);
        }},
        {m_hardResetAct, [this](auto evList, auto /* fileList */) {
            Q_ASSERT(evList.count() == 1);
            auto* job = m_git->resetHEADTo(m_projectPath, evList[0].revision(), GitPlugin::ResetType::Hard);
            connect(job, &VcsJob::finished, this, [this, job, evList]() {
                if (job->status() == VcsJob::JobFailed) {
                    showError(i18n("Failed hard reset to '%1': %2",
                                   evList[1].revision().prettyValue(),
                                   job->errorText()
                    ));
                }
            });
            ICore::self()->runController()->registerJob( job );
        }},
        {m_softResetAct, [this](auto evList, auto /* fileList */) {
            Q_ASSERT(evList.count() == 1);
            auto commits = m_historyModel->eventRange(evList[0].revision(), VcsRevision{});
            QString commitMessage;
            if ( commits.size() >= 1 ) {
                QString summary = commits.last().messageSummary();
                QString body = commits.last().messageBody();
                if ( commits.size() >= 2) {
                    for(const auto event: commits.mid(1, commits.size()-2)) {
                        body = event.message() + QLatin1String("\n\n#------------------------------------\n") + body;
                    }
                }
                commitMessage = summary + QLatin1String("\n\n") + body;
            }
            auto* job = m_git->resetHEADTo(m_projectPath, evList[0].revision(), GitPlugin::ResetType::Soft);
            connect(job, &VcsJob::finished, this, [this, job, evList, commitMessage]() {
                if (job->status() == VcsJob::JobFailed) {
                    showError(i18n("Failed soft reset to '%1': %2",
                                   evList[1].revision().prettyValue(),
                                   job->errorText()
                    ));
                } else {
                    emit populateCommitForm(commitMessage);
                }
            });
            ICore::self()->runController()->registerJob( job );
        }},
        {m_showDiffRevAct, [this](auto evList, auto /* fileList */) {
            m_historyCtrl->showRevisionRangeDiff(m_project, evList.last().revision(), evList.first().revision(), {});
        }},
        {m_showDiffFilesAct, [this](auto evList, auto fileList) {
            QList<QString> pathList;
            for(const auto item: fileList) {
                pathList.push_back(item.repositoryLocation());
            }
            m_historyCtrl->showRevisionRangeDiff(m_project, evList.last().revision(), evList.first().revision(), pathList);
        }},
        {m_reloadHistoryModelAct, [this](auto /* evList */, auto /* fileList */) {
            m_historyModel->reloadRequest();
        }}
    })
    , m_revisionMenu(new QMenu)
    , m_fileMenu(new QMenu)
{
    // Add tooltips / whatsthis texts to some actions
    m_hardResetAct->setToolTip(i18n("Moves the HEAD of this branch to this commit."));
    m_hardResetAct->setWhatsThis(
        i18n("Moves the HEAD of this branch to this commit. ")+
        i18n("The command does not modify the current worktree. The subsequent commits "
             "are discarded when git gc is next run.")
    );
    m_softResetAct->setToolTip(
        i18n("Moves the HEAD of this branch to this commit and applies and stages the changes "
             "introduced by subsequent commits in the staging area.")
    );

    // Creates a proxy model so that we can filter the
    // items by the text entered into the filter lineedit
    m_proxyModel->setFilterCaseSensitivity(Qt::CaseInsensitive);
    m_proxyModel->setRecursiveFilteringEnabled(false);

    connect(m_filter, &QLineEdit::textEdited, m_proxyModel, &FilterProxyModel::setFilterWildcard);
    m_filter->setToolTip(i18n("Filter by commit message/author/filename/sha"));
    m_filter->setPlaceholderText(i18n("Filter by commit message/author/filename/sha"));
    m_filter->setClearButtonEnabled(true);

    connect(m_loadMoreCommitsButton, &QPushButton::clicked, this, &HistoryView::loadMoreEvents);
    m_loadMoreCommitsButton->setToolTip(i18n("Load more commit history"));


    // Setup the view
    m_view->setModel(m_proxyModel);
    m_view->setHeaderHidden(true);
    m_view->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_view->setSelectionMode(QAbstractItemView::SelectionMode::ContiguousSelection);
    m_view->setContextMenuPolicy(Qt::CustomContextMenu);
    m_view->setAnimated(true);
    m_view->setItemDelegate(m_styleDelegate);

    /// Setup D'n'D
    m_view->setDragEnabled(true);
    m_view->setAcceptDrops(true);
    m_view->setDropIndicatorShown(true);
    m_view->setDragDropMode(QAbstractItemView::DragDrop);
    m_view->setDefaultDropAction(Qt::CopyAction);
    connect(m_view, &DropTreeView::dropped, this, &HistoryView::handleDrop);


    connect(m_view, &QTreeView::customContextMenuRequested, this, &HistoryView::popupContextMenu);
    connect(m_view, &QTreeView::doubleClicked, this, &HistoryView::dblClicked);
    connect(m_view, &QTreeView::clicked, this, &HistoryView::clicked);


    connect(m_abortRebaseButton, &QPushButton::clicked, m_rebaseController, &RebaseController::abortCurrentRebase);
    connect(m_continueRebaseButton, &QPushButton::clicked, m_rebaseController, &RebaseController::continueCurrentRebase);
    connect(m_rebaseController, &RebaseController::updateProgress, this, &HistoryView::updateProgress);
    connect(m_rebaseController, &RebaseController::showError, this, &HistoryView::showRebaseError);
    connect(m_rebaseController, &RebaseController::showInfo, this, &HistoryView::showInfo);
    connect(m_rebaseController, &RebaseController::clearError, this, &HistoryView::clearRebaseError);
    connect(m_rebaseController, &RebaseController::updateProgress, this, &HistoryView::updateProgress);
    connect(m_rebaseController, &RebaseController::rebaseFinished, this, &HistoryView::rebaseFinished);
    connect(m_rebaseController, &RebaseController::currentRebasePaused, this, &HistoryView::pauseRebase);
    connect(m_rebaseController, &RebaseController::editCurrentRebaseStepCommitMessage, this, &HistoryView::editCurrentRebaseStepMessage);
    connect(m_rebaseController, &RebaseController::editCurrentRebaseStep, this, &HistoryView::populateCommitForm);
    connect(m_rebaseController, &RebaseController::showRebaseInterface, this, &HistoryView::hideFilterShowButtons);


    connect(ICore::self()->projectController(), &IProjectController::projectClosing, this, &HistoryView::projectClosed);


    // Set up Action Handlers


    // Set up Menus
    for(auto* act: {
        m_moveUpFilesAct,
        m_moveDownFilesAct,
        m_splitUpFilesAct,
        m_splitDownFilesAct,
        m_showDiffFilesAct,
    }) {
        m_fileMenu->addAction(act);
    }

    for(auto* act: {
        m_editCommitAct,
        m_editMessageAct,
        m_squashUpRevAct,
        m_squashDownRevAct,
        m_squashRevRangeAct,
        m_moveUpRevAct,
        m_moveDownRevAct,
        m_removeRevAct,
        m_showDiffRevAct,
    }) {
        m_revisionMenu->addAction(act);
    }
    m_revisionMenu->addSeparator();
    for(auto* act: {
        m_softResetAct,
        m_hardResetAct,
        m_removeRevAct,
    }) {
        m_revisionMenu->addAction(act);
    }

    m_revisionMenu->addSeparator();


    // Add common actions
    for(auto* menu: {m_fileMenu, m_revisionMenu}) {
        menu->addSeparator();
        menu->addAction(m_reloadHistoryModelAct);
    }

    // Do the layout
    auto* _splitter = new QSplitter(Qt::Vertical, this);
    auto* _layout = new QHBoxLayout(this);

    auto* _btnLayout = new QHBoxLayout(this);
    auto* _btnWidget = new QWidget;
    _btnLayout->setSpacing(0);
    _btnLayout->addWidget(m_progressLabel);
    _btnLayout->addWidget(m_continueRebaseButton);
    _btnLayout->addWidget(m_abortRebaseButton);
    _btnWidget->setLayout(_btnLayout);

    auto* _filterLayout = new QHBoxLayout(this);
    auto* _filterWidget = new QWidget;
    _filterLayout->setSpacing(0);
    _filterLayout->addWidget(m_filter);
    _filterLayout->addWidget(m_loadMoreCommitsButton);
    _filterLayout->setContentsMargins(0,0,0,0);
    _filterWidget->setLayout(_filterLayout);
    // _filterWidget->setContentsMargins(0,0,0,0);

    _splitter->setContentsMargins(0, 0, 0, 0);
    _splitter->addWidget(m_inlineError);
    _splitter->addWidget(_btnWidget);
    _splitter->addWidget(_filterWidget);
    _splitter->addWidget(m_commitMessageForm);
    _splitter->addWidget(m_view);
    _splitter->setStretchFactor(3, 1);
    _splitter->setStretchFactor(4, 5);
    _layout->addWidget(_splitter);
    _layout->setContentsMargins(0,0,0,0);

    m_inlineError->hide();
    m_inlineError->setMessageType(KMessageWidget::Error);
    m_inlineError->setCloseButtonVisible(true);
    m_inlineError->setWordWrap(true);

    m_commitMessageForm->hide();
    m_commitMessageForm->addCustomButton(m_cancelEditMessageButton);
    connect(m_cancelEditMessageButton, &QPushButton::clicked, this, [=] {
       m_commitMessageForm->hide();
       m_filter->show();
    });

    m_filter->setMaximumHeight(35);

    hideButtonsShowFilter();

    setLayout(_layout);
}

void HistoryView::hideButtonsShowFilter()
{
    for(auto* w: QList<QWidget*>{m_progressLabel, m_abortRebaseButton, m_continueRebaseButton})
        w->hide();
    m_filter->show();
}

void HistoryView::hideFilterShowButtons()
{
    m_filter->hide();
    for(auto* w: QList<QWidget*>{m_progressLabel, m_abortRebaseButton, m_continueRebaseButton}) {
        w->show();
        w->setDisabled(true);
    }
}

void HistoryView::loadMoreEvents()
{
    m_historyModel->loadMoreEvents();
}

void HistoryView::setProject(KDevelop::IProject* newProject)
{
  if (m_project) {
      projectClosed(newProject);
  }
  m_project = newProject;
  m_git = dynamic_cast<GitPlugin*>(m_project->versionControlPlugin());
  m_historyModel = m_git->historyModelFor(newProject);
  m_proxyModel->setSourceModel(m_historyModel.get());
  m_projectPath = m_project->path();
}

void HistoryView::projectClosed(KDevelop::IProject* project)
{
    if (m_project == project) {
        if (m_rebaseInProgress && m_currentRebaseJob) {
            m_currentRebaseJob->abortRebase();
        }
        m_project = nullptr;
        m_historyModel = nullptr;
        m_git = nullptr;
        m_proxyModel->setSourceModel(nullptr);
    }
}

void HistoryView::reloadModel()
{
    if (m_historyModel)
        m_historyModel->reloadRequest();
}

void HistoryView::showError(const QString& error)
{
    m_inlineError->setMessageType(KMessageWidget::Error);
    m_inlineError->setText(error);
    m_inlineError->animatedShow();
}

void HistoryView::clearError()
{
    if (!m_inlineError->isHidden() && !m_inlineError->isHideAnimationRunning()) {
        m_inlineError->animatedHide();
        m_inlineError->setText({});
    }
}

void HistoryView::clearRebaseError([[maybe_unused]] KDevelop::IProject* project)
{
    clearError();
}

void HistoryView::showRebaseError([[maybe_unused]] KDevelop::IProject* project, const QString& error)
{
    showError(error);
}

void HistoryView::showInfo([[maybe_unused]] KDevelop::IProject* project, const QString& info)
{
    m_inlineError->setMessageType(KMessageWidget::Information);
    m_inlineError->setText(info);
    m_inlineError->animatedShow();
}

void HistoryView::updateProgress([[maybe_unused]] KDevelop::IProject* project, const int step, const int totalSteps)
{
    m_progressLabel->setText(i18n("Rebase in progress (%1/%2)", step, totalSteps));
}


void HistoryView::rebaseFinished([[maybe_unused]] KDevelop::IProject* project)
{
    hideButtonsShowFilter();
    m_rebaseInProgress = false;
    m_currentRebaseJob = nullptr;
    reloadModel();
}

void HistoryView::clicked(const QModelIndex& idx)
{
    if (! idx.isValid() || ! idx.data(VcsEventLogTreeModel::ParentVcsDataRole).canConvert<VcsEvent>()) return;
    auto vcsRevisionData = idx.data(VcsEventLogTreeModel::ParentVcsDataRole).value<VcsEvent>();
    auto revision = vcsRevisionData.revision();
    QString relativePath;
    if (idx.data(VcsEventLogTreeModel::VcsDataRole).canConvert<VcsItemEvent>()) {
        relativePath = idx.data(VcsEventLogTreeModel::VcsDataRole).value<VcsItemEvent>().repositoryLocation();
    }
    m_historyCtrl->showRevisionPathDiff(m_project, revision, relativePath);
}

void HistoryView::dblClicked(const QModelIndex& idx)
{
    qDebug() << "Double Clicked" << idx;
}

void HistoryView::popupContextMenu(const QPoint& pos)
{
    const std::array<QAction*,10> rebaseActs {
             m_editMessageAct
            ,m_squashUpRevAct
            ,m_squashDownRevAct
            ,m_squashRevRangeAct
            ,m_moveUpRevAct
            ,m_moveUpFilesAct
            ,m_moveDownRevAct
            ,m_moveDownFilesAct
            ,m_splitUpFilesAct
            ,m_splitDownFilesAct
    };

    if (m_rebaseInProgress || !m_git) {
        for(auto* act: rebaseActs) {
            act->setDisabled(true);
        }
    } else {
        for(auto* act: rebaseActs) {
            act->setEnabled(true);
        }
    }
    QList<QUrl> urls;
    QList<KDevelop::VcsItemEvent> files;
    QList<KDevelop::VcsEvent> events;
    KDevelop::VcsEvent parent;
    const QModelIndexList selectionIdxs = m_view->selectionModel()->selectedIndexes();

    for(const auto idx: selectionIdxs) {
        auto vcsData = idx.data(VcsEventLogTreeModel::VcsDataRole);
        if (vcsData.canConvert<VcsEvent>()) {
            events << vcsData.value<VcsEvent>();
        } else if (vcsData.canConvert<VcsItemEvent>()) {
            if (parent.revision().revisionType() == VcsRevision::Invalid) {
                parent = idx.data(VcsEventLogTreeModel::ParentVcsDataRole).value<VcsEvent>();
            } else if (!(parent.revision() == idx.data(VcsEventLogTreeModel::ParentVcsDataRole).value<VcsEvent>().revision())) {
                qDebug() << "Multiple parents for files";
                return;
            }
            files << vcsData.value<VcsItemEvent>();
        }
    }

    qDebug() << "Revision count" << events.count();
    qDebug() << "Files count" << files.count();

    if (
        (!files.isEmpty() && !events.isEmpty()) ||
        (files.isEmpty() && events.isEmpty())
    ) {
        return;
    }

    QMenu* menu = files.isEmpty() ? m_revisionMenu : m_fileMenu;

    if (! files.isEmpty() ) {
        events = {parent};
    }

    if (! events.isEmpty()) {
        /* Can't squash/move up if we are already at the top */
        if(m_historyModel->isTopEvent(events.first())) {
            for(auto* act: {m_squashUpRevAct, m_moveUpRevAct, m_moveUpFilesAct}) {
                act->setDisabled(true);
            }
        }

        /* Can't squash/move down if we are already at the bottom */
        if(m_historyModel->isBottomEvent(events.last())) {
            for(auto* act: {m_squashDownRevAct, m_moveDownRevAct, m_moveDownFilesAct}) {
                act->setDisabled(true);
            }
        }
    }

    if(events.count() > 1) {
        if(! m_historyModel->contiguous(events)) {
            for(auto* act: rebaseActs) {
                act->setDisabled(true);
            };
        }
        m_hardResetAct->setEnabled(false);
        m_softResetAct->setEnabled(false);
    } else {
        m_squashRevRangeAct->setDisabled(true);
        m_hardResetAct->setEnabled(true);
        m_softResetAct->setEnabled(true);
    }


    QAction* res = menu->exec(m_view->viewport()->mapToGlobal(pos));
    if (auto handler = m_actionHandlers[res]) {
        handler(events, files);
    }
}

void HistoryView::handleDrop(const QMimeData* mimeData, const DropTreeView::Source src, Qt::DropAction, const DropTreeView::DropTarget& tgt)
{
    if (! tgt.isValid() || ! tgt.index.data(VcsEventLogTreeModel::VcsDataRole).canConvert<VcsEvent>()) return;

    QModelIndex targetIndex = tgt.index;
    DropTreeView::Position tgtPos = tgt.pos;
    if (tgt.pos == DropTreeView::Position::Above) {
        targetIndex = tgt.index.model()->index(tgt.index.row()-1, 0);
        tgtPos = DropTreeView::Position::Below;
    }
    VcsRevision targetRev = targetIndex.data(VcsEventLogTreeModel::VcsDataRole).value<VcsEvent>().revision();

    if (src == DropTreeView::Source::Self && mimeData->hasFormat(QLatin1String("application/x-kdevelop-eventlogtreemodel-index-rows"))) {
        auto data = mimeData->data(QLatin1String("application/x-kdevelop-eventlogtreemodel-index-rows"));

        // Check that the indices are homogeneous (i.e. no mixing of files & revisions)
        // and gather the relevant revisions / files
        QDataStream in(data);
        enum idxType {
            Event,
            Item,
            Unknown
        };
        idxType tp = Unknown;
        QList<VcsEvent> events;
        QList<VcsItemEvent> items;
        while (! in.atEnd() ) {
            int row;
            in >> row;
            auto idx = m_historyModel->index(row, 0);
            if (tp == Unknown) {
                if (idx.data(VcsEventLogTreeModel::VcsDataRole).canConvert<VcsEvent>()) {
                    tp = Event;
                } else if (idx.data(VcsEventLogTreeModel::VcsDataRole).canConvert<VcsItemEvent>()) {
                    tp = Item;
                } else {
                    showError(i18n("Missing data for a drag'n'drop item (this is probably a bug)."));
                    return;
                }
            }
            if (tp == Event) {
                if (! idx.data(VcsEventLogTreeModel::VcsDataRole).canConvert<VcsEvent>()) {
                    showError(i18n("Mixing revisions & files in drag'n'drop is not supported."));
                    return;
                }
                events.append(idx.data(VcsEventLogTreeModel::VcsDataRole).value<VcsEvent>());
            } else if (tp == Item ) {
                if (! idx.data(VcsEventLogTreeModel::VcsDataRole).canConvert<VcsItemEvent>()) {
                    showError(i18n("Mixing revisions & files in drag'n'drop is not supported."));
                    return;
                }
                items.append(idx.data(VcsEventLogTreeModel::VcsDataRole).value<VcsItemEvent>());
                auto evt = idx.parent().data(VcsEventLogTreeModel::VcsDataRole).value<VcsEvent>();
                if (events.isEmpty()) {
                    events.append(evt);
                } else if (events[0].revision() != evt.revision()) {
                    // Handling files from multiple revisions is not supported atm
                    showError(i18n("Drag'n'drop for files from multiple revisions is not supported."));
                    return;
                }
            }
        }
        if (items.isEmpty()) {
            if (tgtPos == DropTreeView::Position::Below) {
                m_rebaseController->moveBefore(m_project, targetRev, events);
            } else if (tgtPos == DropTreeView::Position::On) {
                m_rebaseController->squashWith(m_project, targetRev, events);
            } else {
                showError(i18n("Invalid Drag'n'drop position (probably a bug)."));
            }
        } else {
            if (tgtPos == DropTreeView::Position::Below) {
                m_rebaseController->splitBefore(m_project, targetRev, events[0].revision(), items);
            } else if (tgtPos == DropTreeView::Position::On) {
                m_rebaseController->moveTo(m_project, targetRev, events[0].revision(), items);
            } else {
                showError(i18n("Invalid Drag'n'drop position (probably a bug)."));
            }
        }

    } else if (mimeData->hasFormat(QLatin1String("text/plain"))) {
        auto diff = VcsDiff(QString::fromUtf8(mimeData->data(QLatin1String("text/plain"))));
        if (diff.isEmpty()) {
            showError(i18n("Either dropped file is not a diff or it contains no changes."));
            return;
        }
        if (tgt.pos == DropTreeView::Position::Below ) {
            m_rebaseController->cherryPickBefore(m_project, targetRev, diff);
        } else if (tgt.pos == DropTreeView::Position::On) {
            m_rebaseController->cherryPickInto(m_project, targetRev, diff);
        } else {
            showError(i18n("Invalid Drag'n'drop position (probably a bug)."));
        }
    } else {
        showError(i18n("Data not supported for Drag'n'drop"));
    }
}

void HistoryView::pauseRebase()
{
    m_abortRebaseButton->setEnabled(true);
    m_continueRebaseButton->setEnabled(true);
}

void HistoryView::startEditRevisionMessage(const KDevelop::VcsEvent ev)
{
    m_filter->hide();
    m_commitMessageForm->show();
    auto oldMessage = ev.message().split(QLatin1Char('\n'));
    m_commitMessageForm->setSummary(oldMessage[0]);
    m_commitMessageForm->setExtendedDescription(oldMessage.mid(1).join(QLatin1Char('\n')));
    m_commitMessageForm->setCommitButtonTexts(i18n("Save"), i18n("Save commit for %1", ev.revision().prettyValue()), i18n("A commit summary is required"));
    m_commitMessageForm->enable();
    disconnect(m_commitMessageForm);

    // We create a context object, so that we can disconnect the lambda
    // when its done (e.g. disconnect(m_commitMessageForm) does not work
    // for lambdas), since we want this connection to be "single-shot".
    QObject *context = new QObject(this);
    connect(m_commitMessageForm, &SimpleCommitForm::committed, context, [this, ev, context] {
        auto msg = m_commitMessageForm->summary();
        auto extended = m_commitMessageForm->extendedDescription(70);
        if (extended.length() > 0)
            msg += QStringLiteral("\n\n") + extended;
        m_commitMessageForm->hide();
        delete context;
        m_rebaseController->editRevisionMessage(m_project, ev.revision(), msg);
    });
}

void HistoryView::editCurrentRebaseStepMessage(const QString& message)
{
    hideButtonsShowFilter();
    m_filter->hide();
    m_commitMessageForm->show();
    m_commitMessageForm->setSummary(VcsEvent::messageSummary(message));
    m_commitMessageForm->setExtendedDescription(VcsEvent::messageBody(message));
    m_commitMessageForm->setCommitButtonTexts(
        i18n("Save && Continue"),
        i18n("Save commit message for the squashed commit."),
        i18n("A commit summary is required")
    );
    m_commitMessageForm->enable();
    disconnect(m_commitMessageForm);

    // We create a context object, so that we can disconnect the lambda
    // when its done (e.g. disconnect(m_commitMessageForm) does not work
    // for lambdas), since we want this connection to be "single-shot".
    QObject *context = new QObject(this);
    connect(m_commitMessageForm, &SimpleCommitForm::committed, context, [this, context] {
        auto msg = m_commitMessageForm->summary();
        auto extended = m_commitMessageForm->extendedDescription(70);
        if (extended.length() > 0)
            msg += QStringLiteral("\n\n") + extended;
        m_commitMessageForm->hide();
        delete context;
        hideFilterShowButtons();
        m_rebaseController->continueCurrentRebaseStepWithCommitMessage(msg);
    });
    connect(m_cancelEditMessageButton, &QPushButton::clicked, context, [this, context] {
        hideFilterShowButtons();
        m_rebaseController->abortCurrentRebase();
        delete context;
    });
}
