/*
    SPDX-FileCopyrightText: 2021 Jonathan L. Verner <jonathan.verner@matfyz.cz>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef KDEVPLATFORM_PLUGIN_HISTORY_TOOLVIEW_H
#define KDEVPLATFORM_PLUGIN_HISTORY_TOOLVIEW_H

#include "repostatusmodel.h"
#include "diffviewsctrl.h"
#include "droptreeview.h"

// kdevplatform headers
#include <vcs/models/vcseventmodel.h>
#include <util/path.h>

// KDE Frameworks headers
#include <KTextEditor/Cursor>

// Qt Headers
#include <QWidget>

#include <unordered_map>

class FilterProxyModel;
class GitRebaseJob;
class GitPlugin;

namespace KTextEditor {
    class View;
}

namespace KDevelop {
    class VcsEventItemDelegate;
}

class RevisionDiffCtrl;
class RebaseController;
class SimpleCommitForm;

class KMessageWidget;

class QAction;
class QLabel;
class QLineEdit;
class QMimeData;
class QModelIndex;
class QPushButton;


class HistoryView : public QWidget
{
    Q_OBJECT

public:

    HistoryView(QWidget* parent, GitPlugin* git);

    bool rebaseInProgress() const { return m_rebaseInProgress;}

Q_SIGNALS:

    /**
     * Emitted when the view wants to fill in the commit tool view commit form
     * with the supplied message @p message.
     */
    void populateCommitForm(const QString& message);

public Q_SLOTS:

    /**
     * Changes the project for which history is shown, reloads the model.
     */
    void setProject(KDevelop::IProject* newProject);

    /**
     * If the project @p project is the current project, clear up.
     */
    void projectClosed(KDevelop::IProject* newProject);

    /**
     * Shows the history view context menu
     */
    void popupContextMenu(const QPoint& pos);

    /**
     * A handler called when the user double clicks
     * an item in the history view.
     */
    void dblClicked(const QModelIndex& idx);

    /**
     * A handler called when the user clicks an item
     * in the history view.
     */
    void clicked(const QModelIndex& idx);

    /**
     * Shows a form to allow the user to edit the commit message
     * of commit @p ev. When the user is done and clicks the
     * appropriate save button, @ref editRevisionMessage is called
     * to do the actual history rewriting.
     */
    void startEditRevisionMessage(const KDevelop::VcsEvent ev);

public Q_SLOTS:
    /**
     * Shows an error message using the inline KMessageWidget
     */
    void showError(const QString& error);

    /**
     * Hides any inline error messages if shown.
     */
    void clearError();

    /**
     * Reloads the history model for the currently active project.
     */
    void reloadModel();

    /**
     * Enables the cancel/continue rebase buttons.
     *
     * (connected to the rebase controller's @ref RebaseController::currentRebasePaused signal)
     */
    void pauseRebase();

    /**
     * Shows a rebase error message @p error for the project @p project.
     */
    void showRebaseError(KDevelop::IProject* project, const QString& error);

    /**
     * Shows an info message @p info for a rebase of the project @p project.
     */
    void showInfo(KDevelop::IProject* project, const QString& info);

    /**
     * Hides all rebase error messages for project @p project.
     */
    void clearRebaseError(KDevelop::IProject* project);

    /**
     * Update the progress message for a running rebase for project @p project.
     *
     * @param step          the step of the rebase which has started
     * @param totalSteps    the total number of steps in the current rebase
     */
    void updateProgress(KDevelop::IProject* project, const int step, const int totalSteps);

    /**
     * Hides the rebase progress & cancel buttons, reloads the history model for the
     * project @p project and shows the history model filter.
     */
    void rebaseFinished(KDevelop::IProject* project);

    /**
     * Show a form to allow the user to edit the current rebase step's commit message.
     *
     * The commit message form is prefilled with @p message. When the user clicks
     * save button, the current rebase step's message will be set to the form's message
     * and the rebase will continue. When the user clicks the cancel button, the
     * current rebase is aborted.
     */
    void editCurrentRebaseStepMessage(const QString& message);

    void handleDrop(const QMimeData* mimeData, const DropTreeView::Source src, [[maybe_unused]] Qt::DropAction, const DropTreeView::DropTarget& tgt);


private:
    bool m_rebaseInProgress = false;
    GitRebaseJob* m_currentRebaseJob = nullptr;

    KDevelop::IProject* m_project = nullptr;
    KDevelop::Path m_projectPath;

    GitPlugin* m_git = nullptr;


    /**
     * A controller taking care of revision diff views
     */
    RevisionDiffCtrl* m_historyCtrl = nullptr;

    /**
     * A controller taking care of rebasing
     */
    RebaseController* m_rebaseController = nullptr;

    /**
     * The history model
     */
    std::shared_ptr<KDevelop::VcsEventLogTreeModel> m_historyModel = nullptr;

    /**
     * The filtered history model
     */
    FilterProxyModel* m_proxyModel = nullptr;

    /**
     * When a rebase is in progress, holds the revision at which
     * is currently being rebased.
     */
    KDevelop::VcsEvent rebaseCurrentRev;


    /** A button for aborting a rebase in progress */
    QPushButton* m_abortRebaseButton;

    /** A button for continuing a rebase in progress */
    QPushButton* m_continueRebaseButton;

    /** A button for canceling message editing */
    QPushButton* m_cancelEditMessageButton;

    /** A label for showing the progress of a rebase */
    QLabel* m_progressLabel;

    /** A form for editing commit messages */
    SimpleCommitForm* m_commitMessageForm;

    /** A button for loading more commits into the history view */
    QPushButton* m_loadMoreCommitsButton;

private Q_SLOTS:
    void hideFilterShowButtons();
    void hideButtonsShowFilter();
    void loadMoreEvents();

private:

    /**
     * A KMessage inline widget for showing errors
     */
    KMessageWidget* m_inlineError;

    /** The history treeview  */
    DropTreeView* m_view = nullptr;

    /** A style delegate for the tree view */
    KDevelop::VcsEventItemDelegate* m_styleDelegate;

    /** The lineedit for filtering the treeview */
    QLineEdit* m_filter = nullptr;


    /** Actions available in the history view */
    QAction *m_editMessageAct    /**< Edit commit message */
          , *m_removeRevAct      /**< Removes the selected commit */
          , *m_hardResetAct      /**< Does a hard reset to the selected commit */
          , *m_softResetAct      /**< Does a soft reset to the selected commit */
          , *m_squashUpRevAct    /**< Squash commit with child */
          , *m_squashDownRevAct  /**< Squash commit with parent */
          , *m_squashRevRangeAct /**< Squash a revision range into a single commit */
          , *m_moveUpRevAct      /**< Exchange commit with child (might require manual conflict resolution) */
          , *m_moveUpFilesAct    /**< Move changes to file into the child commit */
          , *m_moveDownRevAct    /**< Exchange commit with parent (might require manual conflict resolution) */
          , *m_moveDownFilesAct  /**< Move changes to file into the parent commit  */
          , *m_splitUpFilesAct   /**< Move changes to file into a new child commit */
          , *m_splitDownFilesAct /**< Move changes to file into a new parent commit */
          , *m_editCommitAct        /**< Move changes to file into a new parent commit */
          , *m_showDiffRevAct    /**< Show commit changes */
          , *m_showDiffFilesAct  /**< Show commit changes to file */
          , *m_reloadHistoryModelAct /**< Reload the history model of the current project */
          ;

    std::unordered_map<QAction*, std::function<void (
        const QList<KDevelop::VcsEvent>&,
        const QList<KDevelop::VcsItemEvent>&
    )>> m_actionHandlers;


    /** Context Menus */
    QMenu  *m_revisionMenu /**< Context menu for commits */
        ,  *m_fileMenu;    /**< Menu menu for files in commits */

};

#endif // KDEVPLATFORM_PLUGIN_HISTORY_TOOLVIEW_H
