/*
    SPDX-FileCopyrightText: 2021 Jonathan Verner <jonathan@temno.eu>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "rebasecontroller.h"

#include "gitplugin.h"
#include "gitrebasejob.h"
#include "rebaseplanner.h"

// kdevplatform headers
#include <interfaces/icore.h>
#include <interfaces/iproject.h>
#include <interfaces/iruncontroller.h>
#include <util/path.h>
#include <util/pausejob.h>
#include <vcs/models/vcseventmodel.h>
#include <vcs/vcsrevision.h>
#include <vcs/vcsjob.h>

// KDE Frameworks headers
#include <KLocalizedString>

// Qt Headers
#include <QString>
#include <QUuid>

using namespace KDevelop;

RebaseController::RebaseController(GitPlugin* git, QObject* parent)
: QObject(parent)
, m_git(git)
, m_planner(new RebasePlanner(m_git))
{
    connect(this, &RebaseController::rebaseFinished, this, [this](auto *project) {
        if (auto model = m_git->historyModelFor(project)) {
            model->setIgnoreReloadRequests(false);
            model->reloadRequest();
        }
    });
}

void RebaseController::splitCommit(KDevelop::IProject* project, const KDevelop::VcsRevision rev,
                                   const QList<KDevelop::VcsDiff> patches)
{
    GitPlugin::RebasePlan plan = m_planner->splitCommitPlan(project->path(), rev, patches);
    if (! plan.isValid()) {
        emit showError(project, i18n("Not implemented.", rev.prettyValue()));
        emit rebaseFinished(project);
        return;
    }
    startRebase(project, plan);
}

void RebaseController::splitUp(KDevelop::IProject* project, const KDevelop::VcsRevision rev,
                               QList<KDevelop::VcsItemEvent> files)
{
    auto* diffJob = m_git->diff(project->path().toUrl(), VcsRevision::createSpecialRevision(VcsRevision::Previous), rev);
    connect(diffJob, &VcsJob::resultsReady, this, [this, files, rev, project](VcsJob* job) {
        // Get a list of relative paths of the files to move down
        QList<QString> relativePaths;
        for(const auto& file: files) {
            relativePaths.push_back(file.repositoryLocation());
        }
        // Get the diff of revision rev
        auto revDiff = job->fetchResults().value<VcsDiff>();
        // Remove the changes to the files which will be extracted into parent
        auto filesDiff = revDiff.splitOff(relativePaths);

        auto plan = m_planner->splitCommitPlan(project->path(), rev, {revDiff, filesDiff});
        if (! plan.isValid()) {
            emit showError(project, i18n("Unable to split revision %1. (Splitting from the HEAD commit is not implemented yet.)", rev.prettyValue()));
            emit rebaseFinished(project);
            return;
        }
        startRebase(project, plan);
    });
    connect(diffJob, &VcsJob::finished, diffJob, [this, diffJob, rev, project]() {
        if (diffJob->status() != VcsJob::JobSucceeded) {
            showError(project, i18n("Failed getting diff for commit %1", rev.prettyValue()));
            emit rebaseFinished(project);
        };
    });

    ICore::self()->runController()->registerJob(diffJob);
}

void RebaseController::splitDown(KDevelop::IProject* project, const KDevelop::VcsRevision rev,
                                 QList<KDevelop::VcsItemEvent> files)
{
    auto* diffJob = m_git->diff(project->path().toUrl(), VcsRevision::createSpecialRevision(VcsRevision::Previous), rev);
    connect(diffJob, &VcsJob::resultsReady, this, [this, files, rev, project](VcsJob* job) {
        // Get a list of relative paths of the files to move down
        QList<QString> relativePaths;
        for(const auto& file: files) {
            relativePaths.push_back(file.repositoryLocation());
        }
        // Get the diff of revision rev
        auto revDiff = job->fetchResults().value<VcsDiff>();
        // Remove the changes to the files which will be extracted into parent
        auto filesDiff = revDiff.splitOff(relativePaths);

        auto plan = m_planner->splitCommitPlan(project->path(), rev, {filesDiff, revDiff});
        if (! plan.isValid()) {
            emit showError(project, i18n("Unable to split revision %1. (Splitting from the HEAD commit is not implemented yet.)", rev.prettyValue()));
            emit rebaseFinished(project);
            return;
        }
        startRebase(project, plan);
    });
    connect(diffJob, &VcsJob::finished, diffJob, [this, diffJob, rev, project]() {
        if (diffJob->status() != VcsJob::JobSucceeded) {
            showError(project, i18n("Failed getting diff for commit %1", rev.prettyValue()));
            emit rebaseFinished(project);
        };
    });

    ICore::self()->runController()->registerJob(diffJob);
}

void RebaseController::splitBefore(KDevelop::IProject* project, const KDevelop::VcsRevision beforeRev,
                                   const KDevelop::VcsRevision srcRev, const QList<KDevelop::VcsItemEvent> files)
{
    auto* diffJob = m_git->diff(project->path().toUrl(), VcsRevision::createSpecialRevision(VcsRevision::Previous), srcRev);
    connect(diffJob, &VcsJob::resultsReady, this, [this, beforeRev, srcRev, files, project](VcsJob* job) {
        // Get a list of relative paths of the files to move
        QList<QString> relativePaths;
        for(const auto& file: files) {
            relativePaths.push_back(file.repositoryLocation());
        }

        // Get the diff of revision srcRev
        auto revDiff = job->fetchResults().value<VcsDiff>();

        // Remove the changes to the files which will be moved down
        auto filesDiff = revDiff.splitOff(relativePaths);

        auto plan = m_planner->splitAndMovePlan(project->path(), srcRev, beforeRev, RebasePlanner::RevisionReference::Before, {filesDiff, revDiff}  );
        if (! plan.isValid()) {
            showError(project, i18n("Cannot split files from %1 before %2", srcRev.prettyValue(), beforeRev.prettyValue()));
            emit rebaseFinished(project);
            return;
        }
         startRebase(project, plan);
    });
    connect(diffJob, &VcsJob::finished, diffJob, [this, diffJob, srcRev, project]() {
        if (diffJob->status() != VcsJob::JobSucceeded) {
            showError(project, i18n("Failed getting diff for commit %1", srcRev.prettyValue()));
            emit rebaseFinished(project);
        };
    });

    ICore::self()->runController()->registerJob(diffJob);
}

void RebaseController::cherryPickBefore(KDevelop::IProject* project, const KDevelop::VcsRevision beforeRev,
                                        const KDevelop::VcsDiff& patch)
{
    GitPlugin::RebasePlan plan = m_planner->cherryPickBeforePlan(project->path(), beforeRev, patch);
    if (! plan.isValid()) {
        showError(project, i18n("Cannot cherry pick before %1", beforeRev.prettyValue()));
        emit rebaseFinished(project);
        return;
    }
    startRebase(project, plan);
}

void RebaseController::cherryPickInto(KDevelop::IProject* project, const KDevelop::VcsRevision intoRev,
                                      const KDevelop::VcsDiff& patch)
{
    GitPlugin::RebasePlan plan = m_planner->cherryPickIntoPlan(project->path(), intoRev, patch);
    if (! plan.isValid()) {
        showError(project, i18n("Cannot cherry pick into %1", intoRev.prettyValue()));
        emit rebaseFinished(project);
        return;
    }
    startRebase(project, plan);
}

void RebaseController::editCommit(KDevelop::IProject* project, const KDevelop::VcsRevision rev)
{
    GitPlugin::RebasePlan plan = m_planner->editCommitPlan(project->path(), rev);
    if (! plan.isValid()) {
        emit showError(project, i18n("Not implemented.", rev.prettyValue()));
        emit rebaseFinished(project);
        return;
    }
    startRebase(project, plan);
}

void RebaseController::editRevisionMessage(KDevelop::IProject* project, const KDevelop::VcsRevision rev, const QString& message)
{

    GitPlugin::RebasePlan plan = m_planner->editRevisionMessagePlan(project->path(), rev, message);
    if (! plan.isValid()) {
        emit showError(project, i18n("Cannot find revision %1.", rev.prettyValue()));
        emit rebaseFinished(project);
        return;
    }
    startRebase(project, plan);
}

void RebaseController::moveUp(KDevelop::IProject* project, const KDevelop::VcsRevision rev)
{
    GitPlugin::RebasePlan plan = m_planner->moveUpPlan(project->path(), rev);
    if (! plan.isValid()) {
        showError(project, i18n("Cannot find revision %1 or revision is already at top", rev.prettyValue()));
        emit rebaseFinished(project);
        return;
    }
    startRebase(project, plan);
}

void RebaseController::moveUp(KDevelop::IProject* project, const KDevelop::VcsRevision rev, QList<KDevelop::VcsItemEvent> files)
{
    moveTo(project, m_git->childRev(project->path(), rev), rev, files);
}

void RebaseController::moveUp(KDevelop::IProject* project, const QList<KDevelop::VcsEvent> events)
{
    QList<KDevelop::VcsRevision> revisions;
    for(const auto& ev: events) {
        revisions.push_back(ev.revision());
    }
    GitPlugin::RebasePlan plan = m_planner->moveUpPlan(project->path(), revisions);
    if (! plan.isValid()) {
        showError(project, i18n("Cannot find revisions %1..%2 or already at top", revisions.first().prettyValue(), revisions.last().prettyValue()));
        emit rebaseFinished(project);
        return;
    }
    startRebase(project, plan);
}

void RebaseController::moveDown(KDevelop::IProject* project, const KDevelop::VcsRevision rev)
{
    GitPlugin::RebasePlan plan = m_planner->moveDownPlan(project->path(), rev);
    if (! plan.isValid()) {
        showError(project, i18n("Cannot find revision %1 or already at bottom.", rev.prettyValue()));
        emit rebaseFinished(project);
        return;
    }
    startRebase(project, plan);
}

void RebaseController::moveDown(KDevelop::IProject* project, const KDevelop::VcsRevision rev, QList<KDevelop::VcsItemEvent> files)
{
    moveTo(project, m_git->parentRev(project->path(), rev), rev, files );
}

void RebaseController::moveDown(KDevelop::IProject* project, const QList<KDevelop::VcsEvent> events)
{
    QList<KDevelop::VcsRevision> revisions;
    for(const auto& ev: events) {
        revisions.push_back(ev.revision());
    }
    GitPlugin::RebasePlan plan = m_planner->moveDownPlan(project->path(), revisions);
    if (! plan.isValid()) {
        showError(project, i18n("Cannot find revisions %1..%2 or already at bottom", revisions.first().prettyValue(), revisions.last().prettyValue()));
        emit rebaseFinished(project);
        return;
    }
    startRebase(project, plan);
}

void RebaseController::moveBefore(KDevelop::IProject* project, const KDevelop::VcsRevision beforeRev,
                              const QList<KDevelop::VcsEvent> events)
{
    QList<KDevelop::VcsRevision> revisions;
    for(const auto& ev: events) {
        revisions.push_back(ev.revision());
    }
    GitPlugin::RebasePlan plan = m_planner->moveBeforePlan(project->path(), beforeRev, revisions);
    if (! plan.isValid()) {
        showError(project, i18n("Cannot move selected revisions (%1, ..., %2)", revisions.first().prettyValue(), revisions.last().prettyValue()));
        emit rebaseFinished(project);
        return;
    }
    startRebase(project, plan);
}

void RebaseController::moveTo(KDevelop::IProject* project, const KDevelop::VcsRevision tgtRev,
                              const KDevelop::VcsRevision srcRev, const QList<KDevelop::VcsItemEvent> files)
{
    auto* diffJob = m_git->diff(project->path().toUrl(), VcsRevision::createSpecialRevision(VcsRevision::Previous), srcRev);
    connect(diffJob, &VcsJob::resultsReady, this, [this, srcRev, tgtRev, files, project](VcsJob* job) {
        // Get a list of relative paths of the files to move
        QList<QString> relativePaths;
        for(const auto& file: files) {
            relativePaths.push_back(file.repositoryLocation());
        }

        // Get the diff of revision srcRev
        auto revDiff = job->fetchResults().value<VcsDiff>();

        // Remove the changes to the files which will be moved down
        auto filesDiff = revDiff.splitOff(relativePaths);

        auto plan = m_planner->splitAndMovePlan(project->path(), srcRev, tgtRev, RebasePlanner::RevisionReference::Self, {filesDiff, revDiff}  );
        if (! plan.isValid()) {
            showError(project, i18n("Cannot move files from %1 into %2", srcRev.prettyValue(), tgtRev.prettyValue()));
            emit rebaseFinished(project);
            return;
        }
         startRebase(project, plan);
    });
    connect(diffJob, &VcsJob::finished, diffJob, [this, diffJob, srcRev, project]() {
        if (diffJob->status() != VcsJob::JobSucceeded) {
            showError(project, i18n("Failed getting diff for commit %1", srcRev.prettyValue()));
            emit rebaseFinished(project);
        };
    });

    ICore::self()->runController()->registerJob(diffJob);
}

void RebaseController::squashRevisions(KDevelop::IProject* project, const KDevelop::VcsRevision revStart, const KDevelop::VcsRevision revEnd)
{
    GitPlugin::RebasePlan plan = m_planner->squashRevisionsPlan(
        project->path(), revStart, revEnd, RebasePlanner::SquashOption::PauseToEditMessage
    );
    if (! plan.isValid()) {
        showError(project, i18n("Cannot find revisions %1..%2 or they are a single commit.", revStart.prettyValue(), revEnd.prettyValue()));
        emit rebaseFinished(project);
        return;
    }
    startRebase(project, plan);
}

void RebaseController::squashWithChild(KDevelop::IProject* project, const KDevelop::VcsRevision rev)
{
    GitPlugin::RebasePlan plan = m_planner->squashWithChildPlan(
        project->path(), rev, RebasePlanner::SquashOption::PauseToEditMessage | RebasePlanner::SquashOption::UseLastSummary
    );
    if (! plan.isValid()) {
        showError(project, i18n("Cannot find revision %1 or no child to squash with.", rev.prettyValue()));
        emit rebaseFinished(project);
        return;
    }
    startRebase(project, plan);
}

void RebaseController::squashWithParent(KDevelop::IProject* project, const KDevelop::VcsRevision rev)
{
    GitPlugin::RebasePlan plan = m_planner->squashWithParentPlan(
        project->path(), rev, RebasePlanner::SquashOption::PauseToEditMessage | RebasePlanner::SquashOption::UseFirstSummary
    );
    if (! plan.isValid()) {
        showError(project, i18n("Cannot find revision %1 or no parent to squash with.", rev.prettyValue()));
        emit rebaseFinished(project);
        return;
    }
    startRebase(project, plan);
}

void RebaseController::squashWith(KDevelop::IProject* project, const KDevelop::VcsRevision squashWithRev,
                                  const QList<KDevelop::VcsEvent> events)
{
    QList<KDevelop::VcsRevision> squashList {squashWithRev};
    for(const auto& ev: events) {
        squashList.append(ev.revision());
    }
    m_git->sortRevisions(project->path(), squashList);

    GitPlugin::RebasePlan plan = m_planner->squashWithTargetPlan(
        project->path(), squashWithRev, squashList, RebasePlanner::SquashOption::PauseToEditMessage
    );
    if (! plan.isValid()) {
        showError(project, i18n("Cannot squash with %1.", squashWithRev.prettyValue()));
        emit rebaseFinished(project);
        return;
    }
    startRebase(project, plan);
}

void RebaseController::remove(KDevelop::IProject* project, const QList<KDevelop::VcsEvent> events)
{
    QList<KDevelop::VcsRevision> removeList;
    for(const auto& ev: events) {
        removeList.append(ev.revision());
    }

    GitPlugin::RebasePlan plan = m_planner->removePlan(project->path(), removeList);
    if (! plan.isValid()) {
        showError(project, i18n("Cannot remove commit %1", removeList[0].prettyValue()));
        emit rebaseFinished(project);
        return;
    }
    startRebase(project, plan);
}

void RebaseController::startRebase(KDevelop::IProject* project, const GitPlugin::RebasePlan& plan)
{
    clearError(project);

    if (! plan.isValid()) {
        qDebug() << "Cannot start rebase, got an invalid plan" << plan;
        showError(project, i18n("Cannot start rebase, a rebase plan with no steps or no base commit was constructed."));
        emit rebaseFinished(project);
        return;
    }

    const auto planSize = plan.steps.size();
    qDebug() << "Starting rebase at " << plan.base.prettyValue() << "with" << plan.steps.size() << "steps";

    if (m_rebaseInProgress) {
        showError(project, i18n("Another rebase already in progress."));
        return;
    }
    m_rebaseInProgress = true;

    auto* getBranchJob = m_git->currentBranch(project->path().toUrl());
    qDebug() << "    searching for current branch";
    connect(getBranchJob, &VcsJob::finished, this, [=] {
        if (! m_rebaseInProgress) {
            emit showError(project, i18n("Rebase was canceled before switching branches"));
            return;
        }
        if (getBranchJob->status() == VcsJob::JobSucceeded) {
            auto branch = getBranchJob->fetchResults().toString();
            auto uuid = QUuid::createUuid().toString();
            auto date = QDateTime::currentDateTime().toString(QStringLiteral("yyyy-MM-dd-hh-mm"));
            QString backupBranchName{GitRebaseJob::GIT_SCOPE_PREFIX  + date + QStringLiteral("-") + branch + QStringLiteral("-") + uuid};

            m_currentRebaseJob = m_git->rebase(project->path(), branch, plan, backupBranchName);

            qDebug() << "Branch found:" << branch;
            qDebug() << plan;

            connect(m_currentRebaseJob, &GitRebaseJob::stepStarted, this, [this, planSize, project](auto* /*job*/, int step){
                emit updateProgress(project, step, planSize);
            });
            connect(m_currentRebaseJob, &GitRebaseJob::stepPaused, this, &RebaseController::currentRebasePaused);
            connect(m_currentRebaseJob, &GitRebaseJob::stepPaused, this, [this, plan, project](auto* /* job */, int /* stepIndex */, const auto& step) {
                const QVariant& userData = step.userData;
                if (userData.canConvert<RebasePlanner::SquashOptions>()) {
                    emit editCurrentRebaseStepCommitMessage(step.message);
                } else {
                    if (step.isCommitSet()) {
                        emit showInfo(project, i18n(
                            "Rebase paused for editing. You can modify the current commit by making changes and staging/unstaging them. "
                            "When done, the rebase will continue by committing the staged changes with the original commit message."
                        ));
                    } else {
                        emit showInfo(project, i18n(
                            "Rebase paused for editing. You can make changes and commit them to create a new commit. "
                            "When done, the rebase will continue. Any staged changes will be incorporated into the next commit. "
                            "Unstaged changes will not be committed, but will remain in the work dir."
                        ));
                        emit editCurrentRebaseStep(step.message);
                    }
                }
            });
            connect(m_currentRebaseJob, &GitRebaseJob::conflictAtStep, this, &RebaseController::currentRebasePaused);
            connect(m_currentRebaseJob, &GitRebaseJob::conflictAtStep, this, [this, plan, project](auto* /* job */, int stepIndex, const auto& step) {
                if (std::holds_alternative<KDevelop::VcsRevision>(step.patch)) {
                    auto rev = std::get<KDevelop::VcsRevision>(step.patch);
                    emit showInfo(project, i18n(
                        "Conflict when applying %1: %2. "
                        "Please resolve the conflict (by editing and staging conflicting files) and then continue.",
                        rev.prettyValue(),
                        VcsEvent::messageSummary(step.message)
                    ));
                } else {
                    emit showInfo(project, i18n(
                        "Conflict when applying patch at step %1. "
                        "Please resolve the conflict (by editing and staging conflicting files) and then continue.",
                        stepIndex
                    ));
                }
            });
            connect(this, &RebaseController::continueCurrentRebase, m_currentRebaseJob, [this, project] {
                emit clearError(project);
                m_currentRebaseJob->continueRebase();
            });
            connect(this, &RebaseController::abortCurrentRebase, m_currentRebaseJob, &GitRebaseJob::abortRebase);
            connect(m_currentRebaseJob, &GitRebaseJob::rebaseFailed, this, [this, project](const auto& error) {
                emit showError(project, i18n("Rebasing failed or was aborted: %1", error));
                emit rebaseFinished(project);
            });
            connect(m_currentRebaseJob, &KJob::finished, this, [this, project] {
                m_rebaseInProgress = false;
                m_currentRebaseJob = nullptr;
                emit rebaseFinished(project);
            });
            m_git->historyModelFor(project)->setIgnoreReloadRequests(true);
            emit showRebaseInterface();
            qDebug() << "Scheduling rebase job";
            ICore::self()->runController()->registerJob( m_currentRebaseJob );
        } else {
            emit showError(project, getBranchJob->errorString());
            emit rebaseFinished(project);
            m_rebaseInProgress = false;
        }
    });
    ICore::self()->runController()->registerJob( getBranchJob );
}

void RebaseController::continueCurrentRebaseStepWithCommitMessage(const QString& message)
{
    Q_ASSERT(m_currentRebaseJob);
    if (m_currentRebaseJob) {
        m_currentRebaseJob->currentStep().message = message;
        m_currentRebaseJob->continueRebase();
    }
}
