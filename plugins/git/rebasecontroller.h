/*
    SPDX-FileCopyrightText: 2021 Jonathan Verner <jonathan@temno.eu>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#ifndef REBASECONTROLLER__H
#define REBASECONTROLLER__H

#include "gitplugin.h"

// kdevplatform headers
#include <vcs/vcsdiff.h>
#include <vcs/vcsrevision.h>
#include <vcs/vcsevent.h>

// Qt Headers
#include <QList>
#include <QObject>

namespace KDevelop {
    class IProject;
}

class RebasePlanner;
class GitRebaseJob;

/**
 * This class coordinates git rebases.
 *
 * It takes care of preparing a rebase plan, creating a rebase job
 * and forwarding signals/messages from the rebase job to the UI and
 * relaying signals from the UI back to the rebase job.
 *
 * The rebase UI is currently provided by the Commit Tool View.
 */
class RebaseController : public QObject {
    Q_OBJECT
public:
    RebaseController(GitPlugin* git, QObject* parent = nullptr);

Q_SIGNALS:
    /* Signals which are emitted */

    void showRebaseInterface();

    /**
     * Emitted when the controller wants to show an error message to the user.
     */
    void showError(KDevelop::IProject* project, const QString& error);

    /**
     * Emitted when the controller wants to show an info message to the user.
     */
    void showInfo(KDevelop::IProject* project, const QString& info);

    /**
     * Emitted when the controller wants to clear any pending error message.
     */
    void clearError(KDevelop::IProject* project);

    /**
     * Emitted each time a rebase step starts
     */
    void updateProgress(KDevelop::IProject* project, const int step, const int totalSteps);

    /**
     * Emitted when the currently running rebase finishes.
     */
    void rebaseFinished(KDevelop::IProject* project);

    /**
     * Emitted when the rebase pauses for user intervention.
     *
     * When ready to continue, call @ref continueCurrentRebase or @ref abortCurrentRebase
     * to abort the rebase.
     */
    void currentRebasePaused();

    /**
     * Emitted when the rebase pauses for editing a step's commit message.
     *
     * When the message is ready, call @ref continueRebaseStepWithCommitMessage to
     * continue, or call @ref abortCurrentRebase to abort the rebase.
     */
    void editCurrentRebaseStepCommitMessage(const QString& message);

    /**
     * Emitted when the rebase pauses for editing a commit.
     *
     * @param message is the current step's message (e.g. to prefill the commit form with)
     *
     * When ready, call @ref continueCurrentRebase to continue,
     * or call @ref abortCurrentRebase to abort the rebase.
     */
    void editCurrentRebaseStep(const QString& message);


    /* Signals which are received (and forwarded to the current rebase job) */

    /** Continue the (paused) rebase in progress */
    void continueCurrentRebase();
    /** Abort the (paused) rebase in progress */
    void abortCurrentRebase();

public Q_SLOTS:

    /* Continue the currently paused rebase step setting its message to @p message */
    void continueCurrentRebaseStepWithCommitMessage(const QString& message);

    /**
     * Splits the commit @p rev into the series of commits @p patches
     */
    void splitCommit(KDevelop::IProject* project, const KDevelop::VcsRevision rev, const QList<KDevelop::VcsDiff> patches);

    void splitUp(KDevelop::IProject* project, const KDevelop::VcsRevision rev, QList<KDevelop::VcsItemEvent> files);
    void splitDown(KDevelop::IProject* project, const KDevelop::VcsRevision rev, QList<KDevelop::VcsItemEvent> files);
    void splitBefore(KDevelop::IProject* project, const KDevelop::VcsRevision beforeRev, const KDevelop::VcsRevision srcRev, const QList<KDevelop::VcsItemEvent> files);

    /**
     * Starts rebasing stopping at @p rev and allowing the user to amend the commit.
     */
    void editCommit(KDevelop::IProject* project, const KDevelop::VcsRevision rev);

    /**
     * Amends the message for revision @p rev to @p message.
     */
    void editRevisionMessage(KDevelop::IProject* project, const KDevelop::VcsRevision rev, const QString& message);


    /**
     * Squashes the revision range @p revStart .. @p revEnd into a single commit
     * concatenating their commit messages.
     */
    void squashRevisions(KDevelop::IProject* project, const KDevelop::VcsRevision revStart, const KDevelop::VcsRevision revEnd);

    /**
     * Squashes the revision @p rev with its parent revision,
     * concatenating their commit messages.
     */
    void squashWithParent(KDevelop::IProject* project, const KDevelop::VcsRevision rev);

    /**
     * Squashes the revision @p rev with its child revision,
     * concatenating their commit messages.
     *
     * @note The revision must have a unique child revision
     */
    void squashWithChild(KDevelop::IProject* project, const KDevelop::VcsRevision rev);

    /**
     * Squashes the revisions @p revs with the revision @p targetRev,
     * concatenating their commit messages.
     */
    void squashWith(KDevelop::IProject* project, const KDevelop::VcsRevision squashWithRev, const QList<KDevelop::VcsEvent> events);

    /**
     * Exchanges the revision @p rev with its child. If this cannot be done
     * automatically, starts a rebase session.
     */
    void moveUp(KDevelop::IProject* project, const KDevelop::VcsRevision rev);

    /**
     * Moves the child of the first revision in @p events so that it is the
     * parent of the last revision in @p events. If this cannot be done
     * automatically, starts a rebase session.
     */
    void moveUp(KDevelop::IProject* project, const QList<KDevelop::VcsEvent> events);

    /**
     * Moves the changes to the files @p files from revision @p rev to its
     * child revision.
     */
    void moveUp(KDevelop::IProject* project, const KDevelop::VcsRevision rev, QList<KDevelop::VcsItemEvent> files);

    /**
     * Exchanges the revision @p rev with its parent. If this cannot be done
     * automatically, starts a rebase session.
     */
    void moveDown(KDevelop::IProject* project, const KDevelop::VcsRevision rev);

    /**
     * Moves the changes to the files @p files from revision @p rev to its
     * parent revision.
     */
    void moveDown(KDevelop::IProject* project, const KDevelop::VcsRevision rev, QList<KDevelop::VcsItemEvent> files);

    /**
     * Moves the parent of the last revision in @p events so that it is the
     * child of the first revision in @p events. If this cannot be done
     * automatically, starts a rebase session.
     */
    void moveDown(KDevelop::IProject* project, const QList<KDevelop::VcsEvent> events);

    /**
     * Moves the revisions in @p events just before revision @p beforeRev.
     * If this cannot be done automatically, starts a rebase session.
     */
    void moveBefore(KDevelop::IProject* project, const KDevelop::VcsRevision beforeRev, const QList<KDevelop::VcsEvent> events);


    /**
     * Moves the changes to the files @p files from revision @p srcRev to revision @p tgtRev.
     * If this cannot be done automatically, starts a rebase session.
     */
    void moveTo(KDevelop::IProject* project, const KDevelop::VcsRevision tgtRev, const KDevelop::VcsRevision srcRev, const QList<KDevelop::VcsItemEvent> files);

    /**
     * Creates a new commit just before revision @p beforeRev from the changes in @p patch.
     * If this cannot be done automatically, starts a rebase session.
     */
    void cherryPickBefore(KDevelop::IProject* project, const KDevelop::VcsRevision beforeRev, const KDevelop::VcsDiff& patch);

    /**
     * Merges the changes from @p patch into the commit @p intoRev.
     * If this cannot be done automatically, starts a rebase session.
     */
    void cherryPickInto(KDevelop::IProject* project, const KDevelop::VcsRevision intoRev, const KDevelop::VcsDiff& patch);

    /**
     * Removes the commits @p revs (and the changes introduced by the commit) from history.
     * If this cannot be done automatically, starts a rebase session.
     */
    void remove(KDevelop::IProject* project, const QList<KDevelop::VcsEvent> revs);


private:
    GitPlugin* m_git = nullptr;
    RebasePlanner* m_planner = nullptr;
    bool m_rebaseInProgress = false;
    GitRebaseJob* m_currentRebaseJob = nullptr;

    void startRebase(KDevelop::IProject* project, const GitPlugin::RebasePlan& plan);

};

#endif // REBASECONTROLLER__H
