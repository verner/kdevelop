/*
    SPDX-FileCopyrightText: 2021 Jonathan Verner <jonathan@temno.eu>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "rebaseplanner.h"

// kdevplatform headers
#include <interfaces/iproject.h>
#include <util/path.h>

// KDE Frameworks headers
#include <KLocalizedString>

// Qt Headers
#include <QString>


RebasePlanner::RebasePlanner(GitPlugin* git)
: m_git(git)
{
}

GitPlugin::RebasePlan RebasePlanner::splitCommitPlan(const KDevelop::Path& repo, const KDevelop::VcsRevision rev,
                                                     const QList<KDevelop::VcsDiff> patches) const
{
    GitPlugin::RebasePlan plan;
    const auto range = m_git->rebaseRange(repo, rev, 1);
    if (! range.isValid()) {
        qWarning() << "Cannot split revision" << rev.prettyValue() << "(invalid rebase range)";
        return {};
    }
    plan.base = range.baseRev;
    auto splitEvent = range.eventRange[0];
    int patchNumber = 1;
    for(const auto& p: patches) {
        plan.steps.push_back({
            /* .patch = */ p,
            /* .message = */ splitEvent.messageSummary() + QLatin1String(" (#") + QString::number(patchNumber) + QLatin1String(")\n\n") + splitEvent.messageBody(),
            /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
            /* .commitOpts = */ GitPlugin::CommitOption::None,
            /* .extendedCommitOpts = */ {
                /* .author = */ splitEvent.author(),
                /* .authorDate = */ splitEvent.date()
            },
        });
        patchNumber++;
    }
    for(const auto& event: range.eventRange.mid(1)) {
            plan.steps.push_back({
                /* .patch = */ event.revision(),
                /* .message = */  event.message(),
                /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
                /* .commitOpts = */ GitPlugin::CommitOption::None,
                /* .extendedCommitOpts = */ {
                    /* .author = */ splitEvent.author(),
                    /* .authorDate = */ splitEvent.date()
                },
            });
    }
    return plan;
}

GitPlugin::RebasePlan RebasePlanner::splitAndMovePlan(const KDevelop::Path& repo, const KDevelop::VcsRevision splitRev,
                                                      const KDevelop::VcsRevision targetRev,
                                                      const RevisionReference targetRef,
                                                      const QList<KDevelop::VcsDiff> patches) const
{
    GitPlugin::RebasePlan plan;
    // The rebase range must contain both splitRev and moveAfterRev; since we don't know which comes first,
    // we build rebase ranges for both and take the range which is larger (this assumes splitRev and moveAfterRev
    // are linearly ordered)
    auto rangeSplit = m_git->rebaseRange(repo, splitRev, 1);
    auto rangeAfter = m_git->rebaseRange(repo, targetRev, 1);
    GitPlugin::RebaseRange range;

    if (!rangeSplit.isValid()) {
        range = rangeAfter;
    } else if (!rangeAfter.isValid()) {
        range = rangeSplit;
    } else {
        range = rangeAfter.eventRange.size() > rangeSplit.eventRange.size() ? rangeAfter : rangeSplit;
    }

    if (! range.isValid()) {
        qWarning() << "Cannot move files from revision" << splitRev.prettyValue() << "after revision" << targetRev.prettyValue() <<"(invalid rebase range)";
        return {};
    }
    if (!range.baseRev.isValid()) {
        return {};
    }

    // Find the commit message of the revision which will be split
    QString splitRevSummary, splitRevBody;
    bool targetBeforeSplit = false;
    for(const auto& event: qAsConst(range.eventRange)) {
        if (event.revision() == splitRev) {
            splitRevSummary = event.messageSummary();
            splitRevBody = event.messageBody();
            break;
        }
        if (event.revision() == targetRev) {
            targetBeforeSplit = true;
        }
    }

    plan.base = range.baseRev;
    for(const auto& event: qAsConst(range.eventRange)) {
        if (event.revision() == targetRev) {
            GitPlugin::RebaseStep targetStep {
                /* .patch = */ event.revision(),
                /* .message = */  event.message(),
                /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
                /* .commitOpts = */ GitPlugin::CommitOption::None,
                /* .extendedCommitOpts = */ {
                    /* .author = */ event.author(),
                    /* .authorDate = */ event.date()
                },
            };
            GitPlugin::RebaseStep patchStep {
                /* .patch = */ patches[0],
                /* .message = */  splitRevSummary + QLatin1String(" (#1)\n\n") + splitRevBody,
                /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
                /* .commitOpts = */ GitPlugin::CommitOption::None,
                /* .extendedCommitOpts = */ {
                    /* .author = */ event.author(),
                    /* .authorDate = */ event.date()
                },
            };
            switch(targetRef) {
                case RevisionReference::Before:
                    plan.steps.push_back( patchStep );
                    plan.steps.push_back( targetStep );
                    break;
                case RevisionReference::After:
                    plan.steps.push_back( targetStep );
                    plan.steps.push_back( patchStep );
                    break;
                case RevisionReference::Self:
                    if (targetBeforeSplit) {
                        targetStep.options = GitPlugin::RebaseStepOption::NoCommit | GitPlugin::RebaseStepOption::NoPause;
                        patchStep.message = targetStep.message;
                        plan.steps.push_back( targetStep );
                        plan.steps.push_back( patchStep );
                    } else {
                        patchStep.options = GitPlugin::RebaseStepOption::NoCommit | GitPlugin::RebaseStepOption::NoPause;
                        plan.steps.push_back( patchStep );
                        plan.steps.push_back( targetStep );
                    }
                    break;
            }
        } else if (event.revision() == splitRev) {
            for(const auto& patch: patches.mid(1)) {
                int patchNumber = 2;
                plan.steps.push_back({
                    /* .patch = */ patch,
                    /* .message = */  splitRevSummary + QLatin1String(" (#") + QString::number(patchNumber) + QLatin1String(")\n\n") + splitRevBody,
                    /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
                    /* .commitOpts = */ GitPlugin::CommitOption::None,
                    /* .extendedCommitOpts = */ {
                        /* .author = */ event.author(),
                        /* .authorDate = */ event.date()
                    },
                });
                patchNumber++;
            }
        } else {
            plan.steps.push_back({
                /* .patch = */ event.revision(),
                /* .message = */  event.message(),
                /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
                /* .commitOpts = */ GitPlugin::CommitOption::None,
                /* .extendedCommitOpts = */ {
                    /* .author = */ event.author(),
                    /* .authorDate = */ event.date()
                },
            });
        }
    }
    return plan;
}




GitPlugin::RebasePlan RebasePlanner::editCommitPlan(const KDevelop::Path& repo, const KDevelop::VcsRevision rev) const
{
    GitPlugin::RebasePlan plan;
    const auto range = m_git->rebaseRange(repo, rev, 1);
    if (! range.isValid()) {
        qWarning() << "Cannot edit revision" << rev.prettyValue() << "(invalid rebase range)";
        return {};
    }
    plan.base = range.baseRev;
    plan.steps.push_back({
        /* .patch = */ range.eventRange[0].revision(),
        /* .message = */  range.eventRange[0].message(),
        /* .options = */ GitPlugin::RebaseStepOption::NoCommit | GitPlugin::RebaseStepOption::PauseBeforeNextStep,
        /* .commitOpts = */ GitPlugin::CommitOption::None,
        /* .extendedCommitOpts = */ {
            /* .author = */ range.eventRange[0].author(),
            /* .authorDate = */ range.eventRange[0].date()
        },
    });
    for(const auto& event: range.eventRange.mid(1)) {
            plan.steps.push_back({
                /* .patch = */ event.revision(),
                /* .message = */  event.message(),
                /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
                /* .commitOpts = */ GitPlugin::CommitOption::None,
                /* .extendedCommitOpts = */ {
                    /* .author = */ event.author(),
                    /* .authorDate = */ event.date()
                },
            });
    }
    return plan;
}


GitPlugin::RebasePlan RebasePlanner::editRevisionMessagePlan(const KDevelop::Path& repo, const KDevelop::VcsRevision rev, const QString& message) const
{
    GitPlugin::RebasePlan plan;
    const auto range = m_git->rebaseRange(repo, rev, 1);
    if (! range.isValid()) {
        qWarning() << "Cannot edit revision" << rev.prettyValue() << "message (invalid rebase range)";
        return {};
    }
    plan.steps.push_back({
        /* .patch = */ range.eventRange[0].revision(),
        /* .message = */  message,
        /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
        /* .commitOpts = */ GitPlugin::CommitOption::None,
        /* .extendedCommitOpts = */ {
            /* .author = */ range.eventRange[0].author(),
            /* .authorDate = */ range.eventRange[0].date()
        },
    });
    for(const auto& event: range.eventRange.mid(1)) {
            plan.steps.push_back({
                /* .patch = */ event.revision(),
                /* .message = */  event.message(),
                /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
                /* .commitOpts = */ GitPlugin::CommitOption::None,
                /* .extendedCommitOpts = */ {
                    /* .author = */ event.author(),
                    /* .authorDate = */ event.date()
                },
            });
    }
    plan.base = range.baseRev;
    return plan;
}

GitPlugin::RebasePlan RebasePlanner::cherryPickBeforePlan(const KDevelop::Path& repo,
                                                          const KDevelop::VcsRevision beforeRev,
                                                          const KDevelop::VcsDiff& patch) const
{
    GitPlugin::RebasePlan plan;
    const auto range = m_git->rebaseRange(repo, beforeRev, 1);
    if (! range.isValid()) {
        qWarning() << "Cannot insert a commit before revision" << beforeRev.prettyValue() << "(invalid rebase range)";
        return {};
    }
    plan.steps.push_back({
        /* .patch = */ patch,
        /* .message = */  patch.diffHeader(),
        /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
    });
    for(const auto& event: range.eventRange) {
            plan.steps.push_back({
                /* .patch = */ event.revision(),
                /* .message = */  event.message(),
                /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
                /* .commitOpts = */ GitPlugin::CommitOption::None,
                /* .extendedCommitOpts = */ {
                    /* .author = */ event.author(),
                    /* .authorDate = */ event.date()
                },
            });
    }
    plan.base = range.baseRev;
    return plan;
}

GitPlugin::RebasePlan RebasePlanner::cherryPickIntoPlan(const KDevelop::Path& repo, const KDevelop::VcsRevision intoRev,
                                                        const KDevelop::VcsDiff& patch) const
{
    GitPlugin::RebasePlan plan;
    const auto range = m_git->rebaseRange(repo, intoRev, 1);
    if (! range.isValid()) {
        qWarning() << "Cannot insert a commit before revision" << intoRev.prettyValue() << "(invalid rebase range)";
        return {};
    }
    plan.steps.push_back({
        /* .patch = */ range.eventRange[0].revision(),
        /* .message = */  {},
        /* .options = */ GitPlugin::RebaseStepOption::NoCommit | GitPlugin::RebaseStepOption::NoPause,
        /* .commitOpts = */ GitPlugin::CommitOption::None,
        /* .extendedCommitOpts = */ {
            /* .author = */ range.eventRange[0].author(),
            /* .authorDate = */ range.eventRange[0].date()
        },
    });
    plan.steps.push_back({
        /* .patch = */ patch,
        /* .message = */  range.eventRange[0].message() + QLatin1String("\n") + patch.diffHeader(),
        /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
    });
    for(const auto& event: range.eventRange.mid(1)) {
            plan.steps.push_back({
                /* .patch = */ event.revision(),
                /* .message = */  event.message(),
                /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
                /* .commitOpts = */ GitPlugin::CommitOption::None,
                /* .extendedCommitOpts = */ {
                    /* .author = */ event.author(),
                    /* .authorDate = */ event.date()
                },
            });
    }
    plan.base = range.baseRev;
    return plan;
}

GitPlugin::RebasePlan RebasePlanner::moveUpPlan(const KDevelop::Path& repo, const KDevelop::VcsRevision rev) const
{
    GitPlugin::RebasePlan plan;

    const auto range = m_git->rebaseRange(repo, rev, 1);
    if (! range.isValid()) {
        qWarning() << "Cannot moveUp revision" << rev.prettyValue() << "(invalid rebase range)";
        return {};
    }


    if (range.eventRange.size() < 2) {
        return {};
    }

    plan.steps.push_back({
        /* .patch = */ range.eventRange[1].revision(),
        /* .message = */  range.eventRange[1].message(),
        /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
        /* .commitOpts = */ GitPlugin::CommitOption::None,
        /* .extendedCommitOpts = */ {
            /* .author = */ range.eventRange[1].author(),
            /* .authorDate = */ range.eventRange[1].date()
        },
    });
    plan.steps.push_back({
        /* .patch = */ range.eventRange[0].revision(),
        /* .message = */  range.eventRange[0].message(),
        /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
        /* .commitOpts = */ GitPlugin::CommitOption::None,
        /* .extendedCommitOpts = */ {
            /* .author = */ range.eventRange[0].author(),
            /* .authorDate = */ range.eventRange[0].date()
        },
    });
    for(const auto& event: range.eventRange.mid(2)) {
            plan.steps.push_back({
                /* .patch = */ event.revision(),
                /* .message = */  event.message(),
                /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
                /* .commitOpts = */ GitPlugin::CommitOption::None,
                /* .extendedCommitOpts = */ {
                    /* .author = */ event.author(),
                    /* .authorDate = */ event.date()
                },
            });
    }

    plan.base = range.baseRev;
    return plan;
}

GitPlugin::RebasePlan RebasePlanner::moveUpPlan(const KDevelop::Path& repo, const QList<KDevelop::VcsRevision> revisions) const
{
    GitPlugin::RebasePlan plan;

    const auto range = m_git->rebaseRange(repo, revisions.last(), 1);
    if (! range.isValid()) {
        qWarning() << "Cannot moveUp revisions" << revisions.first().prettyValue() << ".."<< revisions.last().prettyValue() << "(invalid rebase range)";
        return {};
    }
    if (range.eventRange.size() < revisions.count()+1) {
        return {};
    }

    // The child of the last commit in revisions comes first
    plan.steps.push_back({
        /* .patch = */ range.eventRange[revisions.count()].revision(),
        /* .message = */  range.eventRange[revisions.count()].message(),
        /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
        /* .commitOpts = */ GitPlugin::CommitOption::None,
        /* .extendedCommitOpts = */ {
            /* .author = */ range.eventRange[revisions.count()].author(),
            /* .authorDate = */ range.eventRange[revisions.count()].date()
        },
    });

    // Then come the commits in revisions
    for(const auto& event: range.eventRange.mid(0,revisions.count())) {
            plan.steps.push_back({
                /* .patch = */ event.revision(),
                /* .message = */  event.message(),
                /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
                /* .commitOpts = */ GitPlugin::CommitOption::None,
                /* .extendedCommitOpts = */ {
                    /* .author = */ event.author(),
                    /* .authorDate = */ event.date()
                },
            });
    }

    // And finally the remaining commits
    for(const auto& event: range.eventRange.mid(revisions.count()+1)) {
            plan.steps.push_back({
                /* .patch = */ event.revision(),
                /* .message = */  event.message(),
                /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
                /* .commitOpts = */ GitPlugin::CommitOption::None,
                /* .extendedCommitOpts = */ {
                    /* .author = */ event.author(),
                    /* .authorDate = */ event.date()
                },
            });
    }

    plan.base = range.baseRev;
    return plan;
}

GitPlugin::RebasePlan RebasePlanner::moveDownPlan(const KDevelop::Path& repo, const KDevelop::VcsRevision rev) const
{
    const auto range = m_git->rebaseRange(repo, rev, 2);
    if (! range.isValid()) {
        qWarning() << "Cannot moveDown revision" << rev.prettyValue() << "(invalid rebase range)";
        return {};
    }
    if (!range.baseRev.isValid()) {
        return {};
    }

    GitPlugin::RebasePlan plan;

    plan.steps.push_back({
        /* .patch = */ range.eventRange[1].revision(),
        /* .message = */  range.eventRange[1].message(),
        /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
        /* .commitOpts = */ GitPlugin::CommitOption::None,
        /* .extendedCommitOpts = */ {
            /* .author = */ range.eventRange[1].author(),
            /* .authorDate = */ range.eventRange[1].date()
        },
    });
    plan.steps.push_back({
        /* .patch = */ range.eventRange[0].revision(),
        /* .message = */  range.eventRange[0].message(),
        /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
        /* .commitOpts = */ GitPlugin::CommitOption::None,
        /* .extendedCommitOpts = */ {
            /* .author = */ range.eventRange[0].author(),
            /* .authorDate = */ range.eventRange[0].date()
        },
    });
    for(const auto& event: range.eventRange.mid(2)) {
            plan.steps.push_back({
                /* .patch = */ event.revision(),
                /* .message = */  event.message(),
                /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
                /* .commitOpts = */ GitPlugin::CommitOption::None,
                /* .extendedCommitOpts = */ {
                    /* .author = */ event.author(),
                    /* .authorDate = */ event.date()
                },
            });
    }
    plan.base = range.baseRev;
    return plan;
}

GitPlugin::RebasePlan RebasePlanner::moveDownPlan(const KDevelop::Path& repo, const QList<KDevelop::VcsRevision> revisions) const
{
    GitPlugin::RebasePlan plan;

    const auto range = m_git->rebaseRange(repo, revisions.first(), 2);
    if (! range.isValid()) {
        qWarning() << "Cannot moveDown events" << revisions.first().prettyValue() << ".."<< revisions.last().prettyValue() << "(invalid rebase range)";
        return {};
    }
    if (!range.baseRev.isValid()) {
        return {};
    }

    // First come the commits in events
    for(const auto& event: range.eventRange.mid(1, revisions.count())) {
            plan.steps.push_back({
                /* .patch = */ event.revision(),
                /* .message = */  event.message(),
                /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
                /* .commitOpts = */ GitPlugin::CommitOption::None,
                /* .extendedCommitOpts = */ {
                    /* .author = */ event.author(),
                    /* .authorDate = */ event.date()
                },
            });
    }

    // Then the parent of the first event
    plan.steps.push_back({
        /* .patch = */ range.eventRange[0].revision(),
        /* .message = */  range.eventRange[0].message(),
        /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
        /* .commitOpts = */ GitPlugin::CommitOption::None,
        /* .extendedCommitOpts = */ {
            /* .author = */ range.eventRange[0].author(),
            /* .authorDate = */ range.eventRange[0].date()
        },
    });

    // Finally the remaining commits
    for(const auto& event: range.eventRange.mid(revisions.count()+1)) {
            plan.steps.push_back({
                /* .patch = */ event.revision(),
                /* .message = */  event.message(),
                /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
                /* .commitOpts = */ GitPlugin::CommitOption::None,
                /* .extendedCommitOpts = */ {
                    /* .author = */ event.author(),
                    /* .authorDate = */ event.date()
                },
            });
    }

    plan.base = range.baseRev;
    return plan;
}

GitPlugin::RebasePlan RebasePlanner::moveBeforePlan(const KDevelop::Path& repo, const KDevelop::VcsRevision beforeRev,
                                                    const QList<KDevelop::VcsRevision> revisions) const
{
    GitPlugin::RebasePlan plan;

    const auto range = m_git->rebaseRange(repo, beforeRev, 1);
    if (! range.isValid()) {
        qWarning() << "Cannot move before" << beforeRev.prettyValue() << "(invalid rebase range)";
        return {};
    }
    if (range.eventRange.size() < 2) {
        return {};
    }

    std::vector<GitPlugin::RebaseStep> before, after;
    for(const auto& event : range.eventRange) {
        GitPlugin::RebaseStep step {
            /* .patch = */ event.revision(),
            /* .message = */ event.message(),
            /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
            /* .commitOpts = */ GitPlugin::CommitOption::None,
            /* .extendedCommitOpts = */ {
                /* .author = */ event.author(),
                /* .authorDate = */ event.date()
            },
        };
        if (revisions.contains(event.revision())) {
            before.push_back(step);
        } else {
            after.push_back(step);
        }
    }
    plan.steps.insert(plan.steps.end(), before.begin(), before.end());
    plan.steps.insert(plan.steps.end(), after.begin(), after.end());
    plan.base = range.baseRev;
    return plan;
}

GitPlugin::RebasePlan RebasePlanner::removePlan(const KDevelop::Path& repo,
                                                const QList<KDevelop::VcsRevision> revisions) const
{
    GitPlugin::RebasePlan plan;

    const auto range = m_git->rebaseRange(repo, revisions.back(), 1);
    if (! range.isValid()) {
        qWarning() << "Cannot remove revisions (last revision" << revisions.back().prettyValue() << "): invalid rebase range";
        return {};
    }
    if (range.eventRange.size() < 2) {
        return {};
    }

    std::vector<GitPlugin::RebaseStep> steps;
    for(const auto& event : range.eventRange) {
        if (revisions.contains(event.revision())) {
            continue;
        }
        GitPlugin::RebaseStep step {
            /* .patch = */ event.revision(),
            /* .message = */ event.message(),
            /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
            /* .commitOpts = */ GitPlugin::CommitOption::None,
            /* .extendedCommitOpts = */ {
                /* .author = */ event.author(),
                /* .authorDate = */ event.date()
            },
        };
        plan.steps.push_back(step);
    }
    plan.base = range.baseRev;
    return plan;
}

GitPlugin::RebasePlan RebasePlanner::squashRevisionsPlan(const KDevelop::Path& repo, const KDevelop::VcsRevision revStart, const KDevelop::VcsRevision revEnd, SquashOptions options) const
{
    GitPlugin::RebasePlan plan;

    const auto range = m_git->rebaseRange(repo, revStart, 1);
    if (! range.isValid()) {
        qWarning() << "Cannot squash revisions" << revStart.prettyValue() << ".."<< revEnd.prettyValue() << "(invalid rebase range)";
        return {};
    }
    if (range.eventRange.size() < 2) {
        return {};
    }

    bool squashing = true;
    KDevelop::VcsEvent mainCommit = range.eventRange.first();
    QStringList messageBodies;
    for(const auto& event: range.eventRange) {
        auto message = event.message();
        GitPlugin::RebaseStepOptions stepOpts;
        if (squashing) {
            messageBodies << QStringLiteral("# Commit message for ")+event.revision().prettyValue();
            messageBodies << message;
            messageBodies << QString{};
            if (event.revision() == revEnd) {
                if (options & SquashOption::UseLastSummary) {
                    mainCommit = event;
                }
                if (options & SquashOption::DropOtherMessagesBodies) {
                    message = mainCommit.message();
                } else {
                    message = mainCommit.messageSummary() + messageBodies.join(QLatin1Char('\n'));
                }
                squashing = false;
                if (options & SquashOption::PauseToEditMessage) {
                    stepOpts |= GitPlugin::RebaseStepOption::PauseBeforeNextStep;
                }
            }
        }
        stepOpts |= (squashing ? GitPlugin::RebaseStepOption::NoCommit : GitPlugin::RebaseStepOption::Commit);
        plan.steps.push_back({
            /* .patch = */ event.revision(),
            /* .message = */  message,
            /* .options = */  stepOpts,
            /* .commitOpts = */ GitPlugin::CommitOption::None,
            /* .extendedCommitOpts = */ {
                /* .author = */ event.author(),
                /* .authorDate = */ event.date()
            },
            /* .userData */ QVariant::fromValue<SquashOptions>(options)
        });
    }
    plan.base = range.baseRev;
    return plan;
}

GitPlugin::RebasePlan RebasePlanner::squashWithChildPlan(const KDevelop::Path& repo, const KDevelop::VcsRevision rev, const SquashOptions options) const
{
    GitPlugin::RebasePlan plan;

    auto range = m_git->rebaseRange(repo, rev, 1);
    if (! range.isValid()) {
        qWarning() << "Cannot squash" << rev.prettyValue() << "with child (invalid rebase range)";
        return {};
    }
    if (range.eventRange.size() < 2) {
        return {};
    }

    /* Construct the message for the squashed commit */
    QString message;
    if (options & SquashOption::UseLastSummary) {
        message = range.eventRange[1].message().trimmed();
        if (! (options & SquashOption::DropOtherMessagesBodies)) {
            if (! message.isEmpty() ) {
                message += QLatin1Char('\n');
            }
            message += QStringLiteral("\n# Parent commit message\n")+range.eventRange[0].message();
        }
    } else {
        message = range.eventRange[0].message().trimmed();
        if (! (options & SquashOption::DropOtherMessagesBodies)) {
            if (! message.isEmpty() ) {
                message += QLatin1Char('\n');
            }
            message += QStringLiteral("# Child commit message\n")+range.eventRange[1].message();
        }
    }

    GitPlugin::RebaseStepOptions stepOpts;
    if (options & SquashOption::PauseToEditMessage) {
        stepOpts |= GitPlugin::RebaseStepOption::PauseBeforeNextStep;
    }
    plan.steps.push_back({
        /* .patch = */ range.eventRange[0].revision(),
        /* .message = */  {},
        /* .options = */ GitPlugin::RebaseStepOption::NoCommit | GitPlugin::RebaseStepOption::NoPause,
        /* .commitOpts = */ GitPlugin::CommitOption::None,
        /* .extendedCommitOpts = */ {
            /* .author = */ range.eventRange[0].author(),
            /* .authorDate = */ range.eventRange[0].date()
        },
    });
    plan.steps.push_back({
        /* .patch = */ range.eventRange[1].revision(),
        /* .message = */  message,
        /* .options = */ GitPlugin::RebaseStepOption::Commit | ((options & SquashOption::PauseToEditMessage)
                                                                ? GitPlugin::RebaseStepOption::PauseBeforeNextStep
                                                                : GitPlugin::RebaseStepOption::NoPause),
        /* .commitOpts = */ GitPlugin::CommitOption::None,
        /* .extendedCommitOpts = */ {
            /* .author = */ range.eventRange[1].author(),
            /* .authorDate = */ range.eventRange[1].date()
        },
        /* .userData = */ QVariant::fromValue<RebasePlanner::SquashOptions>(options)
    });

    for(const auto& event: range.eventRange.mid(2)) {
        plan.steps.push_back({
            /* .patch = */ event.revision(),
            /* .message = */  event.message(),
            /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
            /* .commitOpts = */ GitPlugin::CommitOption::None,
            /* .extendedCommitOpts = */ {
                /* .author = */ event.author(),
                /* .authorDate = */ event.date()
            },
        });
    }
    plan.base = range.baseRev;
    return plan;
}

GitPlugin::RebasePlan RebasePlanner::squashWithParentPlan(const KDevelop::Path& repo, const KDevelop::VcsRevision rev, const SquashOptions options) const
{
    GitPlugin::RebasePlan plan;

    auto range = m_git->rebaseRange(repo, rev, 2);
    if (! range.isValid()) {
        qWarning() << "Cannot squash" << rev.prettyValue() << "with parent (invalid rebase range)";
        return {};
    }
    if (range.eventRange.size() < 2) {
        return {};
    }

    /* Construct the message for the squashed commit */
    QString message;
    if (options & SquashOption::UseLastSummary) {
        message = range.eventRange[1].message();
        if (! (options & SquashOption::DropOtherMessagesBodies)) {
            message += QStringLiteral("\n# Parent commit message\n")+range.eventRange[0].message();
        }
    } else {
        message = range.eventRange[0].message();
        if (! (options & SquashOption::DropOtherMessagesBodies)) {
            message += QStringLiteral("\n# Child commit message\n")+range.eventRange[1].message();
        }
    }

    plan.steps.push_back({
        /* .patch = */ range.eventRange[0].revision(),
        /* .message = */  {},
        /* .options = */ GitPlugin::RebaseStepOption::NoCommit | GitPlugin::RebaseStepOption::NoPause,
        /* .commitOpts = */ GitPlugin::CommitOption::None,
        /* .extendedCommitOpts = */ {
            /* .author = */ range.eventRange[0].author(),
            /* .authorDate = */ range.eventRange[0].date()
        },
    });
    plan.steps.push_back({
        /* .patch = */ range.eventRange[1].revision(),
        /* .message = */ message,
        /* .options = */ GitPlugin::RebaseStepOption::Commit | ((options & SquashOption::PauseToEditMessage)
                                                                ? GitPlugin::RebaseStepOption::PauseBeforeNextStep
                                                                : GitPlugin::RebaseStepOption::NoPause),
        /* .commitOpts = */ GitPlugin::CommitOption::None,
        /* .extendedCommitOpts = */ {
            /* .author = */ range.eventRange[1].author(),
            /* .authorDate = */ range.eventRange[1].date()
        },
        /* .userData = */ QVariant::fromValue<RebasePlanner::SquashOptions>(options)
    });
    for(const auto& event: range.eventRange.mid(2)) {
        plan.steps.push_back({
            /* .patch = */ event.revision(),
            /* .message = */  event.message(),
            /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
            /* .commitOpts = */ GitPlugin::CommitOption::None,
            /* .extendedCommitOpts = */ {
                /* .author = */ event.author(),
                /* .authorDate = */ event.date()
            },
        });
    }
    plan.base = range.baseRev;
    return plan;
}

GitPlugin::RebasePlan RebasePlanner::squashWithTargetPlan(const KDevelop::Path& repo,
                                                          const KDevelop::VcsRevision revTarget,
                                                          const QList<KDevelop::VcsRevision>& revList,
                                                          const RebasePlanner::SquashOptions options) const
{
    GitPlugin::RebasePlan plan;

    const auto range = m_git->rebaseRange(repo, revList[0], 1);
    if (! range.isValid()) {
        qWarning() << "Cannot squash with" << revTarget.prettyValue() << "(invalid rebase range)";
        return {};
    }
    if (range.eventRange.size() < 2) {
        return {};
    }

    // Construct the rebase steps corresponding to the squashed commits
    std::vector< GitPlugin::RebaseStep > squashSteps;
    for(const auto& rev: revList) {
        squashSteps.push_back({
            /* .patch = */ rev,
            /* .message = */ {},
            /* .options = */ GitPlugin::RebaseStepOption::NoCommit | GitPlugin::RebaseStepOption::NoPause,
        });
    }

    // Construct the rebase plan

    // Note: The following reserve call is not just an optimization;
    // we need it so that the lastSquashStep iterator remains valid
    // when we insert into plan.steps;
    plan.steps.reserve(range.eventRange.size()-revList.size()+1);
    int lastSquashStepIndex = -1;
    QString targetMessageBody;
    QString targetSummary;
    QStringList revListMessages;
    for(const auto& event : range.eventRange) {
        if (event.revision() == revTarget) {
            targetMessageBody = event.messageBody();
            targetSummary = event.messageSummary();
            plan.steps.insert(plan.steps.end(), squashSteps.begin(), squashSteps.end());
            lastSquashStepIndex = plan.steps.size()-1;
        } else if (revList.contains(event.revision())) {
            revListMessages.append(
                i18n("\n# Commit message of %1 (%2)", event.revision().prettyValue(), event.messageSummary())
            );
            revListMessages.append(event.message());
        } else {
            plan.steps.push_back({
                /* .patch = */ event.revision(),
                /* .message = */ event.message(),
                /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause,
                /* .commitOpts = */ GitPlugin::CommitOption::None,
                /* .extendedCommitOpts = */ {
                    /* .author = */ event.author(),
                    /* .authorDate = */ event.date()
                },
            });
        }
    }

    if (lastSquashStepIndex == -1) {
        qWarning() << "Revlist does not contain target !!!";
        return {};
    }

    auto &lastSquashStep = plan.steps[lastSquashStepIndex];
    // Update the final step of the squash steps with the appropriate message
    // and set its options to commit
    lastSquashStep.message = QString{
        targetSummary+QLatin1String("\n\n")+
        targetMessageBody+QLatin1String("\n")+
        revListMessages.join(QLatin1Char('\n'))
    };
    lastSquashStep.options = GitPlugin::RebaseStepOption::Commit | ((options & SquashOption::PauseToEditMessage)
                                                                ? GitPlugin::RebaseStepOption::PauseBeforeNextStep
                                                                : GitPlugin::RebaseStepOption::NoPause);
    lastSquashStep.userData = QVariant::fromValue<RebasePlanner::SquashOptions>(options);

    plan.base = range.baseRev;
    return plan;
}
