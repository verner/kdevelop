/*
    SPDX-FileCopyrightText: 2021 Jonathan Verner <jonathan@temno.eu>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#ifndef REBASEPLANNER_H
#define REBASEPLANNER_H

#include "gitplugin.h"

#include <vcs/vcsdiff.h>
#include <vcs/vcsrevision.h>

#include <QList>

namespace KDevelop {
    class Path;
}

class RebasePlanner {
    Q_GADGET
public:
    RebasePlanner(GitPlugin* git);

    enum class RevisionReference {
        Before,
        After,
        Self
    };

    /**
     * Splits the commit @p rev into the series of commits @p patches
     */
    GitPlugin::RebasePlan splitCommitPlan(const KDevelop::Path& repo, const KDevelop::VcsRevision rev, const QList<KDevelop::VcsDiff> patches) const;

    /**
     * Splits the commit @p splitRev into a series of patches @p patches, moving the first patch before/after/into (based on @p targetRef)
     * @p moveTargetRev and replacing @p splitRev with a series of commits corresponding to the remaining patches.
     *
     * @warning: @p splitRev should be different from @p moveTargetRev. If they are the same, use @ref splitCommitPlan instead.
     */
    GitPlugin::RebasePlan splitAndMovePlan(const KDevelop::Path& repo, const KDevelop::VcsRevision splitRev, const KDevelop::VcsRevision moveTargetRev, const RevisionReference targetRef, const QList<KDevelop::VcsDiff> patches) const;

    /**
     * Lets the user edit the commit @p rev.
     */
    GitPlugin::RebasePlan editCommitPlan(const KDevelop::Path& repo, const KDevelop::VcsRevision rev) const;

    /**
     * Amends the message for revision @p rev to @p message.
     */
    GitPlugin::RebasePlan editRevisionMessagePlan(const KDevelop::Path& repo, const KDevelop::VcsRevision rev, const QString& message) const;


    /**
     * Modifies commit @p intoRev by applying @p patch to it.
     */
    GitPlugin::RebasePlan cherryPickIntoPlan(const KDevelop::Path& repo, const KDevelop::VcsRevision intoRev, const KDevelop::VcsDiff& patch) const;

    /**
     * Cherrypicks the patch @p patch as a new commit before @p beforeRev.
     */
    GitPlugin::RebasePlan cherryPickBeforePlan(const KDevelop::Path& repo, const KDevelop::VcsRevision beforeRev, const KDevelop::VcsDiff& patch) const;

    /**
     * Options for how to create the commit message when squashing
     */
    enum class SquashOption {
        UseFirstSummary =           0x00, /**< Use the first (i.e. oldest) revision summary for the resulting commit summary */
        UseLastSummary =            0x01, /**< Use the last (i.e. newest) revision summary for the resulting commit summary */
        DropOtherMessagesBodies =   0x02, /**< Do not add the other (except for first/last) commit messages to the resulting commit message */
        PauseToEditMessage =        0x04, /**< Pause to give the user the option of modifying the resulting commit message */
    };
    Q_DECLARE_FLAGS(SquashOptions, SquashOption)
    Q_FLAGS(SquashOptions)

    /**
     * Squashes the revision range @p revStart .. @p revEnd into a single commit concatenating their commit messages.
     *
     *
     */
    GitPlugin::RebasePlan squashRevisionsPlan(
        const KDevelop::Path& repo,
        const KDevelop::VcsRevision revStart,
        const KDevelop::VcsRevision revEnd,
        const SquashOptions options
    ) const;

    /**
     * Squashes the revision @p rev with its parent revision,
     * concatenating their commit messages.
     */
    GitPlugin::RebasePlan squashWithParentPlan(const KDevelop::Path& repo, const KDevelop::VcsRevision rev, const SquashOptions options) const;

    /**
     * Squashes the revision @p rev with its child revision,
     * concatenating their commit messages.
     *
     * @note The revision must have a unique child revision
     */
    GitPlugin::RebasePlan squashWithChildPlan(const KDevelop::Path& repo, const KDevelop::VcsRevision rev, const SquashOptions options) const;

    /**
     * Squashes the revisions @p revList with the revision @p revTarget,
     * concatenating their commit messages. Note that @p revTarget **must**
     * be an element of @p revList, which is expected to be ordered with the
     * oldest commit first.
     *
     * @note: The summary message of the final commit will be taken from the @p revTarget commit, regardless of
     * the options flags (i.e. setting UseFirstSummary/UseLastSummary has no effect).
     */
    GitPlugin::RebasePlan squashWithTargetPlan(
        const KDevelop::Path& repo,
        const KDevelop::VcsRevision revTarget,
        const QList<KDevelop::VcsRevision> &revList,
        const SquashOptions options
    ) const;

    /**
     * Exchanges the revision @p rev with its child.
     */
    GitPlugin::RebasePlan moveUpPlan(const KDevelop::Path& repo, const KDevelop::VcsRevision rev) const;

    /**
     * Moves the child of the first revision in @p revisions so that it is the
     * parent of the last revision in @p revisions.
     */
    GitPlugin::RebasePlan moveUpPlan(const KDevelop::Path& repo, const QList<KDevelop::VcsRevision> revisions) const;

    /**
     * Exchanges the revision @p rev with its parent.
     */
    GitPlugin::RebasePlan moveDownPlan(const KDevelop::Path& repo, const KDevelop::VcsRevision rev) const;

    /**
     * Moves the parent of the earliest revision in @p revisions so that it is the
     * child of the latest revision in @p revisions.
     *
     * The following diagram illustrates what happens:
     * ```
     *   commit 6                                     commit 6
     *   commit 5                                     commit 5
     *   -----------                                  commit 1
     *   commit 4   |                                 -----------
     *   commit 3   | - @p revisions  =>              commit 4   |
     *   commit 2   |                                 commit 3   | - @p revisions
     *   -----------                                  commit 2   |
     *   commit 1                                     -----------
     *   commit 0                                     commit 0
     * ```
     *
     * @warning: The revisions list @p revisions must be **increasing** (i.e. oldest revision is @p rev[0])
     * and **contiguous**, otherwise the resulting plan will be nonsensical.
     */
    GitPlugin::RebasePlan moveDownPlan(const KDevelop::Path& repo, const QList<KDevelop::VcsRevision> revisions) const;

    /**
     * Moves the parent of the last revision in @p revisions so that it is the
     * child of the first revision in @p revisions. If this cannot be done
     * automatically, starts a rebase session.
     */
    GitPlugin::RebasePlan moveBeforePlan(const KDevelop::Path& repo, const KDevelop::VcsRevision beforeRev, const QList<KDevelop::VcsRevision> revisions) const;

    /**
     * Removes the revisions @p revisions from history.
     */
    GitPlugin::RebasePlan removePlan(const KDevelop::Path& repo, const QList<KDevelop::VcsRevision> revisions) const;


private:
    GitPlugin* m_git;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(RebasePlanner::SquashOptions)
Q_DECLARE_METATYPE(RebasePlanner::SquashOptions)

#endif // REBASEPLANNER_H
