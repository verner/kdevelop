/*
    SPDX-FileCopyrightText: 2021 Jonathan Verner <jonathan@temno.eu>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "revisiondiffview.h"

#include "gitplugin.h"

#include <interfaces/ibasicversioncontrol.h>
#include <interfaces/icore.h>
#include <interfaces/idocument.h>
#include <interfaces/idocumentcontroller.h>
#include <interfaces/iplugin.h>
#include <interfaces/iproject.h>
#include <interfaces/iruncontroller.h>
#include <interfaces/iuicontroller.h>
#include <sublime/message.h>
#include <util/executecompositejob.h>
#include <vcs/vcsevent.h>
#include <vcs/models/vcseventmodel.h>

#include <KActionCollection>
#include <KLocalizedString>
#include <KTextEditor/Document>
#include <KTextEditor/View>

#include <QAction>
#include <QDateTime>
#include <QIcon>
#include <QMenu>

using namespace KDevelop;

RevisionDiffView::RevisionDiffView(
    QObject* parent, KDevelop::IProject* project,
    const KDevelop::VcsRevision revStart,
    const KDevelop::VcsRevision revEnd,
    const QList<QString> relativePaths, const QString& title,
    const KTextEditor::Cursor initialPos
)
 : DiffView(parent, project, title, initialPos)
 , m_revStart(revStart)
 , m_revEnd(revEnd)
 , m_splitUp(new QAction(QIcon::fromTheme(QStringLiteral("split")), i18n("Extract into new child commit")))
 , m_splitDown(new QAction(QIcon::fromTheme(QStringLiteral("split")), i18n("Extract into new parent commit")))
{
    m_actionCollection += {
        {m_splitUp, {}},
        {m_splitDown, {}},
    };


    connect(m_splitUp, &QAction::triggered, this, &RevisionDiffView::splitUp);
    connect(m_splitDown, &QAction::triggered, this, &RevisionDiffView::splitDown);
    if (revStart != revEnd) {
        m_splitUp->setDisabled(true);
        m_splitDown->setDisabled(true);
    }

    auto vcs = m_project->versionControlPlugin()->extension<KDevelop::IBasicVersionControl>();
    auto git = dynamic_cast<GitPlugin*>(m_project->versionControlPlugin());

    // Crate a job to get the diffs for commits up to head
    auto* getDiffsForLaterCommits{new ExecuteCompositeJob};
    getDiffsForLaterCommits->setAbortOnError(true);
    connect(getDiffsForLaterCommits, &ExecuteCompositeJob::finished, [this](auto* job) {
        if (!job->error()) {
            qDebug() << "Enabling goto source";
            m_gotoSourceAct->setEnabled(true);
        }
    });

    auto historyModel = git->historyModelFor(project);
    auto firstDiffJob = git->diff(m_baseDiffPath, relativePaths, historyModel->parentVcsEvent(revStart).revision(), revEnd);
    *getDiffsForLaterCommits << firstDiffJob;

    auto revsToCurrent = git->rebaseRange(m_project->path(), revEnd, 0);
    firstDiffJob->setProperty("event", QVariant::fromValue<KDevelop::VcsEvent>(historyModel->vcsEvent(revStart)));
    for(const auto& vcsEvent: revsToCurrent.eventRange) {
        auto* diffJob = vcs->diff(
            m_baseDiffPath.toUrl(),
            VcsRevision::createSpecialRevision(VcsRevision::Previous),
            vcsEvent.revision()
        );
        diffJob->setProperty("event", QVariant::fromValue<KDevelop::VcsEvent>(vcsEvent));
        connect(diffJob, &VcsJob::resultsReady, this, [this](VcsJob* job) {
            auto diff = job->fetchResults().value<VcsDiff>();
            diff.setProperty(QStringLiteral("event"), job->property("event"));
            m_diffSequenceToHead.push_back(diff);
        });
        qDebug() << "Adding job to fetch diff for commit" << vcsEvent.revision().prettyValue();
        *getDiffsForLaterCommits << diffJob;
    };

    connect(firstDiffJob, &VcsJob::resultsReady, this, &RevisionDiffView::revisionReady);

    ICore::self()->runController()->registerJob(getDiffsForLaterCommits);
}

RevisionDiffView::RevisionDiffView(
    QObject* parent,
    KDevelop::IProject* project,
    const KDevelop::VcsRevision rev,
    const QString& relativePath,
    const QString& title,
    const KTextEditor::Cursor initialPos
):
    RevisionDiffView(parent, project, rev, rev, relativePath.isEmpty() ? QList<QString>{} : QList<QString>{relativePath}, title, initialPos)
{
}

RevisionDiffView::RevisionDiffView(
    QObject* parent, KDevelop::IProject* project,
    const KDevelop::VcsRevision rev,
    const QList<QString> relativePaths,
    const QString& title,
    const KTextEditor::Cursor initialPos
):
    RevisionDiffView(parent, project, rev, rev, relativePaths, title, initialPos)
{
}

void RevisionDiffView::split(const KDevelop::VcsDiff::SplitOptions opts)
{
    if (auto* view = activeView()) {
        auto selection = view->selectionRange();
        auto firstDiff = m_diffSequenceToHead.first();
        if (selection.isValid()) {
            int start = selection.start().line();
            int stop = selection.end().line();
            emit splitCommit(m_project, m_revStart, m_diffSequenceToHead.first().split(start, stop, opts));
        } else {
            auto line = view->cursorPosition().line();
            emit splitCommit(m_project, m_revStart, m_diffSequenceToHead.first().split(line, opts));
        }
    }
}

void RevisionDiffView::splitDown()
{
    split(KDevelop::VcsDiff::SplitOption::SplitUp);
}

void RevisionDiffView::splitUp()
{
    split(KDevelop::VcsDiff::SplitOption::SplitDown);
}


void RevisionDiffView::gotoSource()
{
    if (auto* view = activeView()) {
        auto pos = view->cursorPosition();


        qDebug() << DiffLine{m_diffSequenceToHead.first(), pos.line()};


        qDebug() << "  -- position in diff:" << pos.line();
        auto loc = m_diffSequenceToHead.first().diffLineToTarget(pos.line());
        if (! loc.isValid()) {
            qDebug() << "Line removed in commit.";
            auto msg = new Sublime::Message(
                i18n("The current line was removed in this commit."),
                Sublime::Message::Error
            );
            msg->setAutoHide(2000);
            ICore::self()->uiController()->postMessage(msg);
            return;
        }
        qDebug() << "  -- position at initial commit:" << loc.line;

        for(const auto& d: m_diffSequenceToHead.mid(1)) {
            auto diffLine = d.sourceLocationToDiffLine(loc);
            loc = d.sourceToTarget(loc);
            if (! loc.isValid()) {
                auto event = d.property(QStringLiteral("event")).value<KDevelop::VcsEvent>();
                qDebug() << "Line removed in commit" << event.revision().prettyValue();
                qDebug() << "  -- position in diff:" << diffLine;
                auto* msg = new Sublime::Message(
                    i18n(
                        "The current line was removed in commit %1 by <i>%2</i> at %3: <br/> %4",
                         event.revision().prettyValue(),
                         event.author(),
                         event.date().toString(),
                         event.message()
                    ),
                    Sublime::Message::Information
                );
                auto* showRevisionAct = new QAction(i18n("Show commit"));
                connect(showRevisionAct, &QAction::triggered, [this, event, diffLine, pos]() {
                    emit showRevision(m_project, event.revision(), {}, {diffLine, pos.column()});
                });

                auto* closeAction = new QAction(QIcon::fromTheme(QStringLiteral("window-close")),
                                                i18nc("@action", "Close"));
                closeAction->setToolTip(i18nc("@info:tooltip", "Close message"));

                msg->addAction(closeAction);
                msg->addAction(showRevisionAct);
                msg->setAutoHide(10000);
                ICore::self()->uiController()->postMessage(msg);
                return;
            }
            qDebug() << "  -- position in diff:" << diffLine;
            qDebug() << "  -- position at commit:" << loc.line;
        }
        KDevelop::Path locPath(m_baseDiffPath, loc.path);
        qDebug() << "Final position:" << loc.line << "at" << locPath.path();
        if ( loc.isValid() ) {
            qDebug() << "Opening" << locPath.path() << "at line" << loc.line;
            if (auto* srcDoc = ICore::self()->documentController()->openDocument(locPath.toUrl())) {
                qDebug() << "Success";
                srcDoc->setCursorPosition(KTextEditor::Cursor(loc.line, pos.column()-1));
                ICore::self()->documentController()->activateDocument(srcDoc);
            }
        }
    }
}

void RevisionDiffView::revisionReady(KDevelop::VcsJob* job)
{
    if (job->status() == VcsJob::JobSucceeded) {
        auto diff = job->fetchResults().value<VcsDiff>();
        diff.setProperty(QStringLiteral("event"), job->property("event"));

        auto event = job->property("event").value<KDevelop::VcsEvent>();
        auto messageLines = event.message().split(QLatin1Char('\n'));
        auto hash = event.revision().prettyValue();
        if (event.customData().canConvert<GitPlugin::ExtendedRevisionInfo>()) {
            hash = event.customData().value<GitPlugin::ExtendedRevisionInfo>().fullHash;
        }
        QList<QString> headerLines {
            i18n("commit %1", hash),
            i18n("Author: %1", event.author()),
            i18n("Date: %1", event.date().toString()),
            {}
        };
        for(const auto& ln: messageLines) {
            headerLines += QStringLiteral("    ")+ln;
        }
        headerLines.push_back({});
        headerLines.push_back(i18n("(Note: "
            "The diff below shows changes between %1 and its parent commit on the current branch. \n"
            "In certain cases this can be different from the changes introduced by %1.)",
            event.revision().prettyValue()
        ));
        diff.setDiffHeader(headerLines.join(QLatin1Char('\n'))+QStringLiteral("\n")+diff.diffHeader());
        m_diffSequenceToHead.push_back(diff);

        diffReady(diff);
    } else {
        emit error(i18n("Error getting diff for revision %1", job->property("event").toString()));
        cleanup();
    }
}

void RevisionDiffView::contextMenuAboutToShow(KTextEditor::View* view, QMenu* menu)
{
    if (!view->selectionRange().isEmpty()) {
        m_splitDown->setText(i18n("Extract selection into new parent commit"));
        m_splitUp->setText(i18n("Extract selection into new child commit"));
    } else {
        m_splitDown->setText(i18n("Extract hunk into new parent commit"));
        m_splitUp->setText(i18n("Extract hunk into new child commit"));
    }
    DiffView::contextMenuAboutToShow(view, menu);
}
