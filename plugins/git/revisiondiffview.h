/*
    SPDX-FileCopyrightText: 2021 Jonathan Verner <jonathan@temno.eu>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#ifndef REVISIONDIFFVIEW__H
#define REVISIONDIFFVIEW__H

#include "diffview.h"

// kdevplatform headers
#include <vcs/vcsrevision.h>

// KDE Frameworks headers
#include <KTextEditor/Cursor>


namespace KDevelop {
    class VcsJob;
}

class RevisionDiffView : public DiffView {
    Q_OBJECT
public:
    RevisionDiffView(
        QObject* parent,
        KDevelop::IProject* project,
        const KDevelop::VcsRevision rev,
        const QString& relativePath,
        const QString& title,
        const KTextEditor::Cursor initialPos = {}
    );
    RevisionDiffView(
        QObject* parent,
        KDevelop::IProject* project,
        const KDevelop::VcsRevision rev,
        const QList<QString> relativePaths,
        const QString& title,
        const KTextEditor::Cursor initialPos = {}
    );
    RevisionDiffView(
        QObject* parent,
        KDevelop::IProject* project,
        const KDevelop::VcsRevision revStart,
        const KDevelop::VcsRevision revEnd,
        const QList<QString> relativePaths,
        const QString& title,
        const KTextEditor::Cursor initialPos = {}
    );

Q_SIGNALS:
    void showRevision(KDevelop::IProject* project, const KDevelop::VcsRevision rev, const QList<QString> relativePaths, const KTextEditor::Cursor pos = {});
    void splitCommit(KDevelop::IProject* project, const KDevelop::VcsRevision rev, const QList<KDevelop::VcsDiff> patches);

protected Q_SLOTS:

    virtual void contextMenuAboutToShow(KTextEditor::View* view, QMenu* menu) override;
    virtual void gotoSource() override;
    void revisionReady(KDevelop::VcsJob* job);
    void split(const KDevelop::VcsDiff::SplitOptions opts);
    void splitUp();
    void splitDown();

protected:

    KDevelop::VcsRevision m_revStart, m_revEnd;
    QAction* m_splitUp;
    QAction* m_splitDown;
    QList<KDevelop::VcsDiff> m_diffSequenceToHead = {};
};

#endif // REVISIONDIFFVIEW__H
