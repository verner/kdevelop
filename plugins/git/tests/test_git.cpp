/*
    This file was partly taken from KDevelop's cvs plugin
    SPDX-FileCopyrightText: 2007 Robert Gruber <rgruber@users.sourceforge.net>

    Adapted for Git
    SPDX-FileCopyrightText: 2008 Evgeniy Ivanov <powerfox@kde.ru>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include "test_git.h"

#include <QTest>
#include <tests/testcore.h>
#include <tests/autotestshell.h>
#include <QUrl>
#include <QDebug>
#include <QSignalSpy>
#include <QUuid>

#include <vcs/dvcs/dvcsjob.h>
#include <vcs/models/vcseventmodel.h>
#include <vcs/vcsannotation.h>
#include <vcs/vcsevent.h>
#include <util/path.h>
#include "../gitplugin.h"
#include "../gitrebasejob.h"
#include "../rebaseplanner.h"

#define VERIFYJOB(j) \
do {auto* ___l = (j); QVERIFY(___l); QVERIFY(___l->exec()); QVERIFY2((___l)->status() == KDevelop::VcsJob::JobSucceeded, (___l)->errorString().toUtf8()); } while(0)

#define VERIFYJOBFAILURE(j) \
do { QVERIFY(j); QVERIFY(((!j->exec()) || ((j)->status() != KDevelop::VcsJob::JobSucceeded))); } while(0)

inline QString tempDir() { return QDir::tempPath(); }
inline QString gitTest_BaseDir() { return tempDir() + "/kdevGit_testdir/"; }
inline QString gitRepo() { return gitTest_BaseDir() + ".git"; }
inline QString gitSrcDir() { return gitTest_BaseDir() + "src/"; }
inline QString gitTest_FileName() { return QStringLiteral("testfile"); }
inline QString gitTest_FileName2() { return QStringLiteral("foo"); }
inline QString gitTest_FileName3() { return QStringLiteral("bar"); }

using namespace KDevelop;

bool writeFile(const QString &path, const QString& content, QIODevice::OpenModeFlag mode = QIODevice::WriteOnly)
{
    QFile f(path);

    if (!f.open(mode)) {
        return false;
    }

    QTextStream input(&f);
    input << content;

    return true;
}

void GitInitTest::initTestCase()
{
    AutoTestShell::init({QStringLiteral("kdevgit")});
    TestCore::initialize();

    m_plugin = new GitPlugin(TestCore::self());

    d = QDir(gitTest_BaseDir());
    dUrl = QUrl::fromLocalFile(gitTest_BaseDir());
    dPath = Path(gitTest_BaseDir());
}

void GitInitTest::cleanupTestCase()
{
    delete m_plugin;

    TestCore::shutdown();
}

void GitInitTest::init()
{
    // Now create the basic directory structure
    QDir tmpdir(tempDir());
    tmpdir.mkdir(gitTest_BaseDir());
    tmpdir.mkdir(gitSrcDir());
}

void GitInitTest::cleanup()
{
    removeTempDirs();
}


void GitInitTest::repoInit()
{
    qDebug() << "Trying to init repo";
    // make job that creates the local repository
    VcsJob* j = m_plugin->init(QUrl::fromLocalFile(gitTest_BaseDir()));
    VERIFYJOB(j);

    // check if the .git directory in the new local repository exists now
    QVERIFY(QFileInfo::exists(gitRepo()));

    // check if isValidDirectory works
    QVERIFY(m_plugin->isValidDirectory(QUrl::fromLocalFile(gitTest_BaseDir())));

    // and for non-git dir, I hope nobody has /tmp under git
    // FIXME: Create an empty directory in tmp and test that
    QVERIFY(!m_plugin->isValidDirectory(QUrl::fromLocalFile(tempDir())));

    // we have nothing, so output should be empty
    DVcsJob * j2 = m_plugin->gitRevParse(gitRepo(), QStringList(QStringLiteral("--branches")));
    QVERIFY(j2);
    QVERIFY(j2->exec());
    qDebug() << j2->output();
    QVERIFY(j2->output().isEmpty());

    // Make sure to set the Git identity so unit tests don't depend on that
    auto j3 = m_plugin->setConfigOption(QUrl::fromLocalFile(gitTest_BaseDir()),
              QStringLiteral("user.email"), QStringLiteral("me@example.com"));
    VERIFYJOB(j3);
    auto j4 = m_plugin->setConfigOption(QUrl::fromLocalFile(gitTest_BaseDir()),
              QStringLiteral("user.name"), QStringLiteral("My Name"));
    VERIFYJOB(j4);
}

void GitInitTest::addFiles()
{
    qDebug() << "Adding files to the repo";

    //we start it after repoInit, so we still have empty git repo
    QVERIFY(writeFile(gitTest_BaseDir() + gitTest_FileName(), QStringLiteral("HELLO WORLD")));
    QVERIFY(writeFile(gitTest_BaseDir() + gitTest_FileName2(), QStringLiteral("No, bar()!")));

    //test git-status exitCode (see DVcsJob::setExitCode).
    VcsJob* j = m_plugin->status(QList<QUrl>() << QUrl::fromLocalFile(gitTest_BaseDir()));
    VERIFYJOB(j);

    // /tmp/kdevGit_testdir/ and testfile
    j = m_plugin->add(QList<QUrl>() << QUrl::fromLocalFile(gitTest_BaseDir() + gitTest_FileName()));
    VERIFYJOB(j);

    QVERIFY(writeFile(gitSrcDir() + gitTest_FileName3(), QStringLiteral("No, foo()! It's bar()!")));

    //test git-status exitCode again
    j = m_plugin->status(QList<QUrl>() << QUrl::fromLocalFile(gitTest_BaseDir()));
    VERIFYJOB(j);

    //repository path without trailing slash and a file in a parent directory
    // /tmp/repo  and /tmp/repo/src/bar
    j = m_plugin->add(QList<QUrl>() << QUrl::fromLocalFile(gitSrcDir() + gitTest_FileName3()));
    VERIFYJOB(j);

    //let's use absolute path, because it's used in ContextMenus
    j = m_plugin->add(QList<QUrl>() << QUrl::fromLocalFile(gitTest_BaseDir() + gitTest_FileName2()));
    VERIFYJOB(j);

    //Now let's create several files and try "git add file1 file2 file3"
    const QStringList files{QStringLiteral("file1"), QStringLiteral("file2"), QStringLiteral("la la")};
    QList<QUrl> multipleFiles;
    for (const QString& file : files) {
        QVERIFY(writeFile(gitTest_BaseDir() + file, file));
        multipleFiles << QUrl::fromLocalFile(gitTest_BaseDir() + file);
    }
    j = m_plugin->add(multipleFiles);
    VERIFYJOB(j);
}

void GitInitTest::commitFiles()
{
    qDebug() << "Committing...";
    //we start it after addFiles, so we just have to commit
    VcsJob* j = m_plugin->commit(QStringLiteral("Test commit"), QList<QUrl>() << QUrl::fromLocalFile(gitTest_BaseDir()));
    VERIFYJOB(j);

    //test git-status exitCode one more time.
    j = m_plugin->status(QList<QUrl>() << QUrl::fromLocalFile(gitTest_BaseDir()));
    VERIFYJOB(j);

    //since we committed the file to the "pure" repository, .git/refs/heads/master should exist
    //TODO: maybe other method should be used
    QString headRefName(gitRepo() + "/refs/heads/master");
    QVERIFY(QFileInfo::exists(headRefName));

    //Test the results of the "git add"
    auto* jobLs = new DVcsJob(gitTest_BaseDir(), m_plugin);
    *jobLs << "git" << "ls-tree" << "--name-only" << "-r" << "HEAD";

    if (jobLs->exec() && jobLs->status() == KDevelop::VcsJob::JobSucceeded) {
        QStringList files = jobLs->output().split('\n');
        QVERIFY(files.contains(gitTest_FileName()));
        QVERIFY(files.contains(gitTest_FileName2()));
        QVERIFY(files.contains("src/" + gitTest_FileName3()));
    }

    QString firstCommit;

    QFile headRef(headRefName);

    if (headRef.open(QIODevice::ReadOnly)) {
        QTextStream output(&headRef);
        output >> firstCommit;
    }
    headRef.close();

    QVERIFY(!firstCommit.isEmpty());

    qDebug() << "Committing one more time";
    //let's try to change the file and test "git commit -a"
    QVERIFY(writeFile(gitTest_BaseDir() + gitTest_FileName(), QStringLiteral("Just another HELLO WORLD\n")));

    //add changes
    j = m_plugin->add(QList<QUrl>() << QUrl::fromLocalFile(gitTest_BaseDir() + gitTest_FileName()));
    VERIFYJOB(j);

    j = m_plugin->commit(QStringLiteral("KDevelop's Test commit2"), QList<QUrl>() << QUrl::fromLocalFile(gitTest_BaseDir()));
    VERIFYJOB(j);

    QString secondCommit;

    if (headRef.open(QIODevice::ReadOnly)) {
        QTextStream output(&headRef);
        output >> secondCommit;
    }
    headRef.close();

    QVERIFY(!secondCommit.isEmpty());
    QVERIFY(firstCommit != secondCommit);

}

void GitInitTest::testInit()
{
    repoInit();
}

static QString runCommand(const QString& cmd, const QStringList& args)
{
    QProcess proc;
    proc.setWorkingDirectory(gitTest_BaseDir());
    proc.start(cmd, args);
    proc.waitForFinished();
    return proc.readAllStandardOutput().trimmed();
}

void GitInitTest::testReadAndSetConfigOption()
{
    repoInit();

    {
        qDebug() << "read non-existing config option";
        QString nameFromPlugin = m_plugin->readConfigOption(QUrl::fromLocalFile(gitTest_BaseDir()),
                                                            QStringLiteral("notexisting.asdads"));
        QVERIFY(nameFromPlugin.isEmpty());
    }

    {
        qDebug() << "write user.name = \"John Tester\"";
        auto job = m_plugin->setConfigOption(QUrl::fromLocalFile(gitTest_BaseDir()),
                                             QStringLiteral("user.name"), QStringLiteral("John Tester"));
        VERIFYJOB(job);
        const auto name = runCommand(QStringLiteral("git"), {"config", "--get", QStringLiteral("user.name")});
        QCOMPARE(name, QStringLiteral("John Tester"));
    }

    {
        qDebug() << "read user.name";
        const QString nameFromPlugin = m_plugin->readConfigOption(QUrl::fromLocalFile(gitTest_BaseDir()),
                                                               QStringLiteral("user.name"));
        QCOMPARE(nameFromPlugin, QStringLiteral("John Tester"));
        const auto name = runCommand(QStringLiteral("git"), {"config", "--get", QStringLiteral("user.name")});
        QCOMPARE(name, QStringLiteral("John Tester"));
    }
}

void GitInitTest::testAdd()
{
    repoInit();
    addFiles();
}

void GitInitTest::testCommit()
{
    repoInit();
    addFiles();
    commitFiles();
}

void GitInitTest::testBranch(const QString& newBranch)
{
    //Already tested, so I assume that it works
    const QUrl baseUrl = QUrl::fromLocalFile(gitTest_BaseDir());
    QString oldBranch = runSynchronously(m_plugin->currentBranch(baseUrl)).toString();

    VcsRevision rev;
    rev.setRevisionValue(oldBranch, KDevelop::VcsRevision::GlobalNumber);
    VcsJob* j = m_plugin->branch(baseUrl, rev, newBranch);
    VERIFYJOB(j);
    QVERIFY(runSynchronously(m_plugin->branches(baseUrl)).toStringList().contains(newBranch));

    // switch branch
    j = m_plugin->switchBranch(baseUrl, newBranch);
    VERIFYJOB(j);
    QCOMPARE(runSynchronously(m_plugin->currentBranch(baseUrl)).toString(), newBranch);

    // get into detached head state
    j = m_plugin->switchBranch(baseUrl, QStringLiteral("HEAD~1"));
    VERIFYJOB(j);
    QCOMPARE(runSynchronously(m_plugin->currentBranch(baseUrl)).toString(), QString());

    // switch back
    j = m_plugin->switchBranch(baseUrl, newBranch);
    VERIFYJOB(j);
    QCOMPARE(runSynchronously(m_plugin->currentBranch(baseUrl)).toString(), newBranch);

    // test rename
    j = m_plugin->renameBranch(baseUrl, oldBranch, QStringLiteral("test-")+oldBranch);
    VERIFYJOB(j);
    QVERIFY(!runSynchronously(m_plugin->branches(baseUrl)).toStringList().contains(oldBranch));

    // Test that we can't delete branch on which we currently are
    j = m_plugin->deleteBranch(baseUrl, newBranch);
    VERIFYJOBFAILURE(j);
    QVERIFY(runSynchronously(m_plugin->branches(baseUrl)).toStringList().contains(newBranch));

    // Test branching off HEAD
    rev = KDevelop::VcsRevision::createSpecialRevision(KDevelop::VcsRevision::Head);
    j = m_plugin->branch(baseUrl, rev, newBranch+QStringLiteral("-new"));
    VERIFYJOB(j);
    QVERIFY(runSynchronously(m_plugin->branches(baseUrl)).toStringList().contains(newBranch+QStringLiteral("-new")));
}

void GitInitTest::testMerge()
{
    const QString branchNames[] = {QStringLiteral("aBranchToBeMergedIntoMaster"), QStringLiteral("AnotherBranch")};

    const QString files[]={QStringLiteral("First File to Appear after merging"),QStringLiteral("Second File to Appear after merging"), QStringLiteral("Another_File.txt")};

    const QString content=QStringLiteral("Testing merge.");

    repoInit();
    addFiles();
    commitFiles();

    const QUrl baseUrl = QUrl::fromLocalFile(gitTest_BaseDir());
    VcsJob* j = m_plugin->branches(baseUrl);
    VERIFYJOB(j);
    QString curBranch = runSynchronously(m_plugin->currentBranch(baseUrl)).toString();
    QCOMPARE(curBranch, QStringLiteral("master"));

    VcsRevision rev;
    rev.setRevisionValue("master", KDevelop::VcsRevision::GlobalNumber);

    j = m_plugin->branch(baseUrl, rev, branchNames[0]);
    VERIFYJOB(j);

    qDebug() << "Adding files to the new branch";

    //we start it after repoInit, so we still have empty git repo
    QVERIFY(writeFile(gitTest_BaseDir() + files[0], content));
    QVERIFY(writeFile(gitTest_BaseDir() + files[1], content));

    QList<QUrl> listOfAddedFiles{QUrl::fromLocalFile(gitTest_BaseDir() + files[0]),
        QUrl::fromLocalFile(gitTest_BaseDir() + files[1])};

    j = m_plugin->add(listOfAddedFiles);
    VERIFYJOB(j);

    j = m_plugin->commit(QStringLiteral("Committing to the new branch"), QList<QUrl>() << QUrl::fromLocalFile(gitTest_BaseDir()));
    VERIFYJOB(j);

    j = m_plugin->switchBranch(baseUrl, QStringLiteral("master"));
    VERIFYJOB(j);

    j = m_plugin->mergeBranch(baseUrl, branchNames[0]);
    VERIFYJOB(j);

    auto jobLs = new DVcsJob(gitTest_BaseDir(), m_plugin);
    *jobLs << "git" << "ls-tree" << "--name-only" << "-r" << "HEAD" ;

    if (jobLs->exec() && jobLs->status() == KDevelop::VcsJob::JobSucceeded) {
        QStringList files = jobLs->output().split('\n');
        qDebug() << "Files in this Branch: " << files;
        QVERIFY(files.contains(files[0]));
        QVERIFY(files.contains(files[1]));
    }

    //Testing one more time.
    j = m_plugin->switchBranch(baseUrl, branchNames[0]);
    VERIFYJOB(j);
    rev.setRevisionValue(branchNames[0], KDevelop::VcsRevision::GlobalNumber);
    j = m_plugin->branch(baseUrl, rev, branchNames[1]);
    VERIFYJOB(j);
    QVERIFY(writeFile(gitTest_BaseDir() + files[2], content));
    j = m_plugin->add(QList<QUrl>() << QUrl::fromLocalFile(gitTest_BaseDir() + files[2]));
    VERIFYJOB(j);
    j = m_plugin->commit(QStringLiteral("Committing to AnotherBranch"), QList<QUrl>() << baseUrl);
    VERIFYJOB(j);
    j = m_plugin->switchBranch(baseUrl, branchNames[0]);
    VERIFYJOB(j);
    j = m_plugin->mergeBranch(baseUrl, branchNames[1]);
    VERIFYJOB(j);
    qDebug() << j->errorString() ;

    jobLs = new DVcsJob(gitTest_BaseDir(), m_plugin);
    *jobLs << "git" << "ls-tree" << "--name-only" << "-r" << "HEAD" ;

    if (jobLs->exec() && jobLs->status() == KDevelop::VcsJob::JobSucceeded) {
        QStringList files = jobLs->output().split('\n');
        QVERIFY(files.contains(files[2]));
        qDebug() << "Files in this Branch: " << files;
    }

    j = m_plugin->switchBranch(baseUrl, QStringLiteral("master"));
    VERIFYJOB(j);
    j = m_plugin->mergeBranch(baseUrl, branchNames[1]);
    VERIFYJOB(j);
    qDebug() << j->errorString() ;

    jobLs = new DVcsJob(gitTest_BaseDir(), m_plugin);
    *jobLs << "git" << "ls-tree" << "--name-only" << "-r" << "HEAD" ;

    if (jobLs->exec() && jobLs->status() == KDevelop::VcsJob::JobSucceeded) {
        QStringList files = jobLs->output().split('\n');
        QVERIFY(files.contains(files[2]));
        qDebug() << "Files in this Branch: " << files;
    }

}

void GitInitTest::testBranching()
{
    repoInit();
    addFiles();
    commitFiles();

    const QUrl baseUrl = QUrl::fromLocalFile(gitTest_BaseDir());
    VcsJob* j = m_plugin->branches(baseUrl);
    VERIFYJOB(j);

    QString curBranch = runSynchronously(m_plugin->currentBranch(baseUrl)).toString();
    QCOMPARE(curBranch, QStringLiteral("master"));

    testBranch(QStringLiteral("new"));
    testBranch(QStringLiteral("averylongbranchnamejusttotestlongnames"));
    testBranch(QStringLiteral("KDE/4.10"));
}

void GitInitTest::revHistory()
{
    repoInit();
    addFiles();
    commitFiles();

    const QVector<KDevelop::DVcsEvent> commits = m_plugin->allCommits(gitTest_BaseDir());
    QVERIFY(!commits.isEmpty());
    QStringList logMessages;

    for (auto& commit : commits)
        logMessages << commit.log();

    QCOMPARE(commits.count(), 2);

    QCOMPARE(logMessages[0], QStringLiteral("KDevelop's Test commit2"));  //0 is later than 1!

    QCOMPARE(logMessages[1], QStringLiteral("Test commit"));

    QVERIFY(commits[1].parents().isEmpty());  //0 is later than 1!

    QVERIFY(!commits[0].parents().isEmpty()); //initial commit is on the top

    QVERIFY(commits[1].commit().contains(QRegExp("^\\w{,40}$")));

    QVERIFY(commits[0].commit().contains(QRegExp("^\\w{,40}$")));

    QVERIFY(commits[0].parents()[0].contains(QRegExp("^\\w{,40}$")));
}

void GitInitTest::testAnnotation()
{
    repoInit();
    addFiles();
    commitFiles();

    // called after commitFiles
    QVERIFY(writeFile(gitTest_BaseDir() + gitTest_FileName(), QStringLiteral("An appended line"), QIODevice::Append));

    VcsJob* j = m_plugin->commit(QStringLiteral("KDevelop's Test commit3"), QList<QUrl>() << QUrl::fromLocalFile(gitTest_BaseDir()));
    VERIFYJOB(j);

    j = m_plugin->annotate(QUrl::fromLocalFile(gitTest_BaseDir() + gitTest_FileName()), VcsRevision::createSpecialRevision(VcsRevision::Head));
    VERIFYJOB(j);

    QList<QVariant> results = j->fetchResults().toList();
    QCOMPARE(results.size(), 2);
    QVERIFY(results.at(0).canConvert<VcsAnnotationLine>());
    VcsAnnotationLine annotation = results.at(0).value<VcsAnnotationLine>();
    QCOMPARE(annotation.lineNumber(), 0);
    QCOMPARE(annotation.commitMessage(), QStringLiteral("KDevelop's Test commit2"));

    QVERIFY(results.at(1).canConvert<VcsAnnotationLine>());
    annotation = results.at(1).value<VcsAnnotationLine>();
    QCOMPARE(annotation.lineNumber(), 1);
    QCOMPARE(annotation.commitMessage(), QStringLiteral("KDevelop's Test commit3"));
}

void GitInitTest::testRemoveEmptyFolder()
{
    repoInit();

    QDir d(gitTest_BaseDir());
    d.mkdir(QStringLiteral("emptydir"));

    VcsJob* j = m_plugin->remove(QList<QUrl>() << QUrl::fromLocalFile(gitTest_BaseDir()+"emptydir/"));
    if (j) VERIFYJOB(j);

    QVERIFY(!d.exists(QStringLiteral("emptydir")));
}

void GitInitTest::testRemoveEmptyFolderInFolder()
{
    repoInit();

    QDir d(gitTest_BaseDir());
    d.mkdir(QStringLiteral("dir"));

    QDir d2(gitTest_BaseDir()+"dir");
    d2.mkdir(QStringLiteral("emptydir"));

    VcsJob* j = m_plugin->remove(QList<QUrl>() << QUrl::fromLocalFile(gitTest_BaseDir()+"dir/"));
    if (j) VERIFYJOB(j);

    QVERIFY(!d.exists(QStringLiteral("dir")));
}

void GitInitTest::testRemoveUnindexedFile()
{
    repoInit();

    QVERIFY(writeFile(gitTest_BaseDir() + gitTest_FileName(), QStringLiteral("An appended line"), QIODevice::Append));

    VcsJob* j = m_plugin->remove(QList<QUrl>() << QUrl::fromLocalFile(gitTest_BaseDir() + gitTest_FileName()));
    if (j) VERIFYJOB(j);

    QVERIFY(!QFile::exists(gitTest_BaseDir() + gitTest_FileName()));
}

void GitInitTest::testRemoveFolderContainingUnversionedFiles()
{
    repoInit();

    QDir d(gitTest_BaseDir());
    d.mkdir(QStringLiteral("dir"));

    QVERIFY(writeFile(gitTest_BaseDir() + "dir/foo", QStringLiteral("An appended line"), QIODevice::Append));
    VcsJob* j = m_plugin->add(QList<QUrl>() << QUrl::fromLocalFile(gitTest_BaseDir()+"dir"));
    VERIFYJOB(j);
    j = m_plugin->commit(QStringLiteral("initial commit"), QList<QUrl>() << QUrl::fromLocalFile(gitTest_BaseDir()));
    VERIFYJOB(j);

    QVERIFY(writeFile(gitTest_BaseDir() + "dir/bar", QStringLiteral("An appended line"), QIODevice::Append));

    j = m_plugin->remove(QList<QUrl>() << QUrl::fromLocalFile(gitTest_BaseDir() + "dir"));
    if (j) VERIFYJOB(j);

    QVERIFY(!QFile::exists(gitTest_BaseDir() + "dir"));

}


void GitInitTest::removeTempDirs()
{
    const auto dirPath = gitTest_BaseDir();
    QDir dir(dirPath);
    if (dir.exists() && !dir.removeRecursively()) {
        qDebug() << "QDir::removeRecursively(" << dirPath << ") returned false";
    }
}

void GitInitTest::testDiff()
{
    repoInit();
    addFiles();
    commitFiles();

    QVERIFY(writeFile(gitTest_BaseDir() + gitTest_FileName(), QStringLiteral("something else")));

    VcsRevision srcrev = VcsRevision::createSpecialRevision(VcsRevision::Base);
    VcsRevision dstrev = VcsRevision::createSpecialRevision(VcsRevision::Working);
    VcsJob* j = m_plugin->diff(QUrl::fromLocalFile(gitTest_BaseDir()), srcrev, dstrev, IBasicVersionControl::Recursive);
    VERIFYJOB(j);

    KDevelop::VcsDiff d = j->fetchResults().value<KDevelop::VcsDiff>();
    QVERIFY(d.baseDiff().isLocalFile());
    QString path = d.baseDiff().toLocalFile();
    QVERIFY(QDir().exists(path+"/.git"));
}

/**
 * Returns all elements of the filesystem (template) hierarchy which
 * lie under @p subRoot (i.e. all paths which start with @p subRoot)
 */
FS subTree(const FS& hierarchy, const QString& subRoot)
{
    FS ret;
    for (const auto& item: hierarchy) {
        if (item.name.startsWith(subRoot)) {
            ret.push_back(item);
        }
    }
    return ret;
}

/**
 * Converts the relative path @p subPath to an absolute path relative to @p root.
 * Moreover, it appends to each element in @p subPath the suffix @p nameSuffix.
 * E.g.  if @p nameSuffix is <tt>"projA"</tt>, @p root is the directory <tt>"/tmp/test"</tt>
 * and @p subPath is <tt>"2/sub/B"</tt> then the result will be
 * <tt>"/tmp/test/2-projA/sub-projA/B-projA"</tt>.
 */
QString absolutePath(QDir root, const QString subPath)
{
    return root.filePath(subPath.chopped(0));
}

/**
 * Creates a FS hierarchy under @p root from the template @p fs. The hierarchy is defined
 * by a list of paths @p fs. A path ending with a '/' is considered to be a directory and
 * each must <b>must not</b> start with '/'. Files <b>must</b> come after their parent
 * directories and will be created so that they contain @p content.
 *
 * Each directory and file in the hierarchy will have an additional '-'+ @p nameSuffix added
 * to it. E.g., if @p nameSuffix is <tt>"projA"</tt>, then <tt>"2/sub/B"</tt> will be converted
 * to <tt>"2-projA/sub-projA/B-projA"</tt> first.
 *
 */
void createHierarchy(QDir root, const FS& fs)
{
//     QByteArray cont = content.toUtf8();
    for (const auto& item: fs) {
        QString absPath = absolutePath(root, item.name);
        if (item.name.endsWith('/')) {
            QVERIFY(root.mkpath(absPath));
        } else {
            QFile file(absPath);
            QVERIFY(file.open(QIODevice::WriteOnly));
            QVERIFY(file.write(item.content.toUtf8()) == item.content.toUtf8().length());
            file.close();
        }
    }
}

/**
 * Checks that @p subPath exists in the filesystem under the directory @p root.
 * Moreover, if @p subPath ends with '/', it checks that it is a directory; otherwise
 * it checks that it is a file @p subPath whose contents is @p content.
 *
 * @note: @p subPath is converted using @ref absolutePath with prefix "-" + @p nameSuffix
 */
bool checkPath(QDir root, const QString& subPath, const QString& content = "")
{
    QString absPath = absolutePath(root, subPath);
    if (subPath.endsWith('/')) {
        QDir dir(absPath);
        if (dir.exists())
            return true;
        return false;
    }
    QFile file(absPath);
    if (!file.exists())
        return false;
    file.open(QIODevice::ReadOnly);
    return (file.readAll() == content);
}

void verifyHierarchy(QDir root, const FS& fs, bool checkAbsent = false) {
    Q_UNUSED(checkAbsent);
    for(const auto& el: fs) {
        if (! checkPath(root, el.name, el.content)) {
            QString msg { QStringLiteral("checkHierarchy: failed at ")+el.name };
            QVERIFY2(checkPath(root, el.name, el.content), msg.toUtf8());
        };
    }
}

/**
 * Checks that @p subPath does <b>not</b> exists in the filesystem under the directory @p root.
 *
 * @note: @p subPath is converted using @ref absolutePath with prefix "-" + @p nameSuffix
 */
bool checkAbsent(const QDir& root, const QString& subPath)
{
    QFileInfo info(absolutePath(root, subPath));
    return !info.exists();
}

FS unionHierarchy(const std::vector<FS>& hierarchies) {
    std::map<QString, QString> state;
    for(const auto& fs: hierarchies) {
        for(const auto& [name, content]: fs) {
            state[name] = content;
        }
    }
    FS ret;
    for(auto [name, content]: state) {
        ret.push_back({name, content});
    }
    return ret;
}

QStringList files(const FS& fs)
{
    QStringList ret;
    for(const auto& [name, content] : fs) {
        if (!name.endsWith(QLatin1Char('/'))) {
            ret << name;
        }
    }
    return ret;
}

void GitInitTest::createCommit(Commit& commit)
{
    QDir d(gitTest_BaseDir());
    QUrl dUrl {QUrl::fromLocalFile(gitTest_BaseDir())};
    Path dPath(gitTest_BaseDir());

    createHierarchy(d, commit.content);

    // Add & commit the FS state
    QList<QUrl> add;
    for(const auto& f: files(commit.content)) {
        add << QUrl::fromLocalFile(absolutePath(d, f));
    }
    VcsJob* j = m_plugin->add(add);
    VERIFYJOB(j);
    j = m_plugin->commit(commit.message, {dUrl});
    VERIFYJOB(j);

    j = m_plugin->log(dUrl, VcsRevision::createSpecialRevision(VcsRevision::Head), 1);
    VERIFYJOB(j);

    const QList<QVariant> l = j->fetchResults().toList();
    auto rev = l.at(0).value<VcsEvent>().revision();
    commit.rev = rev;
}

std::vector<VcsEvent> GitInitTest::getAllAncestorCommits(const KDevelop::VcsRevision& start) {
    QUrl dUrl {QUrl::fromLocalFile(gitTest_BaseDir())};
    auto *j = m_plugin->log(dUrl, start, 0);
    const QList<QVariant> l = runSynchronously(j).toList();
    std::vector<VcsEvent> ret;
    for(const auto& commit: l) {
        ret.insert(ret.begin(), commit.value<VcsEvent>());
    }
    return ret;
}

Commit GitInitTest::getCommit(const KDevelop::VcsRevision& rev) {
    Commit ret;
    QUrl dUrl {QUrl::fromLocalFile(gitTest_BaseDir())};
    auto *j = m_plugin->log(dUrl, rev, 1);
    const QList<QVariant> l = runSynchronously(j).toList();
    const VcsEvent ev = l.at(0).value<VcsEvent>();
    ret.rev = ev.revision();
    ret.message = ev.message();
    return ret;
}

void GitInitTest::buildBranch(Branch& branch)
{
    QDir d(gitTest_BaseDir());
    QUrl dUrl {QUrl::fromLocalFile(gitTest_BaseDir())};
    Path dPath(gitTest_BaseDir());
    auto baseRev = branch.baseRev();

    qDebug() << "Creating branch" << branch.name << "at" << branch.baseRev().prettyValue();
    VcsJob* j = m_plugin->branch(dUrl, baseRev, branch.name);
    VERIFYJOB(j);
    j = m_plugin->switchBranch(dUrl, branch.name);
    VERIFYJOB(j);

    int cnt=0;
    for(auto& commit: branch.commits) {
        createCommit(commit);
        qDebug() << "-- Created commit #" << cnt << branch.commits[cnt].rev.prettyValue() << "(on " << branch.commits[cnt].message << ")";
        ++cnt;
    }
    j = m_plugin->checkOut(dPath, baseRev);
    VERIFYJOB(j);
}

void GitInitTest::buildRepo(Repo& repo)
{
    for(auto& b: repo) {
        buildBranch(b);
    }
    VERIFYJOB(m_plugin->switchBranch(dUrl, "master"));
}

KDevelop::VcsRevision Branch::baseRev() const
{
    return base ? base->rev : VcsRevision {};
}

FS branchContent(const Branch& b) {
    std::vector<FS> commitContents;
    for(const auto &c: b.commits) {
        commitContents.push_back(c.content);
    }
    return unionHierarchy(commitContents);
}

Branch master {
    /*.name =*/ QStringLiteral("madness"),
    /*.base =*/ nullptr,
    /*.commits =*/ {
        {
            /*.message =*/ QStringLiteral("initial commit"),
            /*.content =*/ {
                {QStringLiteral("dir/"), {}},
                {QStringLiteral("dir/test"), QStringLiteral("dir/test-initial\n")},
                {QStringLiteral("test"), QStringLiteral("test-initial\n")},
            },
        },
        {
            /*.message =*/ QStringLiteral("master: second commit"),
            /*.content =*/ {
                {QStringLiteral("dir/test"), QStringLiteral("dir/test-second\n")},
                {QStringLiteral("dir/testII"), QStringLiteral("dir/test-secondII\n")}
            },
        },
        {
            /*.message =*/ QStringLiteral("master: third commit"),
            /*.content =*/ {
                {QStringLiteral("dir/testII"), QStringLiteral("dir/test-thirdIII\n")}
            },
        },
    }
};

Branch wip {
    /*.name =*/ QStringLiteral("wip"),
    /*.base =*/ &master.commits[0],
    /*.commits =*/ {
        {
            /*.message =*/ QStringLiteral("wip: first commit"),
            /*.content =*/ {
                {QStringLiteral("dir/wip"), QStringLiteral("dir/wip-first\n")},
            },
        },
    },
};

Branch multimove {
    /*.name =*/ QStringLiteral("multimove"),
    /*.base =*/ &master.commits[0],
    /*.commits =*/ {
        {
            /*.message =*/ QStringLiteral("wip: 1. commit"),
            /*.content =*/ {
                {QStringLiteral("dir/1-wip"), QStringLiteral("1-content: 1\n")},
            },
        },
        {
            /*.message =*/ QStringLiteral("wip: 2. commit"),
            /*.content =*/ {
                {QStringLiteral("dir/2-independent-wip"), QStringLiteral("2-content: 1\n")},
            },
        },
        {
            /*.message =*/ QStringLiteral("wip: 3. commit (depends on commit 1)"),
            /*.content =*/ {
                {QStringLiteral("dir/1-wip"), QStringLiteral("1-content: 2\n")},
                {QStringLiteral("dir/3-wip"), QStringLiteral("3-content: 1\n")},
            },
        },
        {
            /*.message =*/ QStringLiteral("wip: 4. commit (depends on commit 3.)"),
            /*.content =*/ {
                {QStringLiteral("dir/3-wip"), QStringLiteral("3-content: 2\n")},
                {QStringLiteral("dir/4-wip"), QStringLiteral("4-content: 1\n")},
            },
        },
        {
            /*.message =*/ QStringLiteral("wip: 5. commit (depends commits 1., 3., 4.)"),
            /*.content =*/ {
                {QStringLiteral("dir/1-wip"), QStringLiteral("1-content: 3\n")},
                {QStringLiteral("dir/3-wip"), QStringLiteral("3-content: 3\n")},
                {QStringLiteral("dir/4-wip"), QStringLiteral("4-content: 4\n")},
            },
        },
    },
};

const int MULTIMOVE_FIRST_COMMIT_INDEX=2;

Repo testRepo {master, wip, multimove};

FS untracked {
    {QStringLiteral("untracked/"), {}},
    {QStringLiteral("untracked/test"), QStringLiteral("test-untracked")},
};


void GitInitTest::saveToStash()
{
    auto tempStash = QUuid::createUuid().toString();
    QVERIFY(m_plugin->gitStash(d, {
            QStringLiteral("save"),
            QStringLiteral("--include-untracked"),
            QStringLiteral("-m"),
            tempStash,
    }, KDevelop::DVcsJob::OutputJobVerbosity::Verbose));
}

void GitInitTest::restoreFromStash()
{
    QVERIFY(m_plugin->gitStash(d, {QStringLiteral("pop")}, KDevelop::DVcsJob::OutputJobVerbosity::Verbose));
}

void GitInitTest::verifyBranch(const Branch& b) {

    qDebug() << "Verifying branch" << b.name;

    auto preBranch = runSynchronously(m_plugin->currentBranch(dUrl)).toString();

    qDebug() << " -- stashing";
    saveToStash();

    qDebug() << " -- switching to" << b.name;
    VERIFYJOB(m_plugin->switchBranch(dUrl, b.name));

    qDebug() << " -- verifying commits";
    const QList<QVariant> l = runSynchronously(m_plugin->log(dUrl, VcsRevision::createSpecialRevision(KDevelop::VcsRevision::Head), b.commits.size())).toList();
    int log_pos = l.size()-1;
    for(const auto& c: b.commits) {
        auto ev = l[log_pos--].value<VcsEvent>();
        QVERIFY(c.message == ev.message());
        QVERIFY(c.rev == ev.revision());
    }

    qDebug() << " -- switching back to" << preBranch;
    VERIFYJOB(m_plugin->switchBranch(dUrl, preBranch));

    qDebug() << " -- restoring from stash";
    restoreFromStash();
}


void GitInitTest::initialCommit()
{
    QFile empty(gitTest_BaseDir()+QStringLiteral("/empty"));
    empty.open(QIODevice::WriteOnly);
    empty.write(QByteArrayLiteral("empty\n"));
    empty.close();
    VcsJob* j = m_plugin->add({QUrl::fromLocalFile(gitTest_BaseDir()+QStringLiteral("/empty"))});
    VERIFYJOB(j);
    j = m_plugin->commit("empty commit", {QUrl::fromLocalFile(gitTest_BaseDir())});
    VERIFYJOB(j);
}

void GitInitTest::testRebase()
{
    repoInit();
    initialCommit();
    createHierarchy(QDir(gitTest_BaseDir()), {
        {QStringLiteral("untracked/"), {}},
        {QStringLiteral("untracked/test"), QStringLiteral("test-untracked")},
    });
    auto root = getCommit(VcsRevision::createSpecialRevision(KDevelop::VcsRevision::Head));
    master.base = &root;

    buildRepo(testRepo);

    VcsJob* job = nullptr;

    // Test rewriting messages
    QStringList revisedMessages {
        QStringLiteral("Revised message 3"),
        QStringLiteral("Revised message 2"),
        QStringLiteral("Revised message 1"),
    };
    GitPlugin::RebasePlan plan {
        {
            { master.commits[0].rev, revisedMessages[2] },
            { master.commits[1].rev, revisedMessages[1] },
            { master.commits[2].rev, revisedMessages[0] },
        },
        master.baseRev()
    };
    QString backupName {master.name+QStringLiteral("-old")};
    auto* rebaseJob = m_plugin->rebase(dPath, master.name, plan, backupName);
    QSignalSpy spyStepFinished(rebaseJob, &GitRebaseJob::stepFinished);
    QSignalSpy spyStepStarted(rebaseJob, &GitRebaseJob::stepStarted);
    VERIFYJOB(rebaseJob);

    // Verify that step finished & started signals were emitted
    QVERIFY(spyStepStarted.count() == 3);
    QVERIFY(spyStepFinished.count() == 3);

    // -- Test that the messages were rewritten
    job = m_plugin->log(dUrl, VcsRevision::createSpecialRevision(VcsRevision::Head), 3);
    const QList<QVariant> log1 = runSynchronously(job).toList();

    for(auto i=0; i < revisedMessages.length(); ++i) {
        QString failMsg {"Message "+QString::number(i)+" does not match: expected '" +revisedMessages[i]+ "' got '"+log1[i].value<VcsEvent>().message()+"'"};
        QVERIFY2(revisedMessages[i]== log1[i].value<VcsEvent>().message(), failMsg.toUtf8());
    }

    // -- Test that untracked stuff remains
    verifyHierarchy(d, untracked);

    // -- Test state as of last commit
    verifyHierarchy(d, branchContent(master));

    // -- Test that the backup branch contains old messages & commits
    auto mname = master.name;
    master.name = backupName;
    verifyBranch(master);
    master.name = mname;

    // -- Test that the wip branch is unchanged
    verifyBranch(wip);

    // -- Delete the backup branch
    VERIFYJOB(m_plugin->deleteBranch(dUrl, backupName));

    // Test squashing commits
    GitPlugin::RebasePlan plan2 {
        {
            { /*.patch = */ master.commits[0].rev, /*.message = */ {}, /* .options = */ GitPlugin::RebaseStepOption::NoCommit | GitPlugin::RebaseStepOption::NoPause },
            { /*.patch = */ master.commits[1].rev, /*.message = */ QStringLiteral("Squashed"), /* .options = GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause */ },
            { /*.patch = */ master.commits[2].rev, /*.message = */ revisedMessages[0], /* .options = GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause */ },
        },
        master.baseRev()
    };
    rebaseJob = m_plugin->rebase(dPath, master.name, plan2, backupName);
    QSignalSpy spyStepFinishedII(rebaseJob, &GitRebaseJob::stepFinished);
    QSignalSpy spyStepStartedII(rebaseJob, &GitRebaseJob::stepStarted);
    VERIFYJOB(rebaseJob);

    // Verify that step finished & started signals were emitted
    QVERIFY(spyStepStarted.count() == 3);
    QVERIFY(spyStepFinished.count() == 3);

    // -- Test that the messages were rewritten
    job = m_plugin->log(dUrl, VcsRevision::createSpecialRevision(VcsRevision::Head), 2);
    const QList<QVariant> log2 = runSynchronously(job).toList();

    QVERIFY(log2[0].value<VcsEvent>().message() == revisedMessages[0]);
    QVERIFY(log2[1].value<VcsEvent>().message() == QStringLiteral("Squashed"));


    // -- Test that untracked stuff remains
    verifyHierarchy(d, untracked);

    // -- Test state as of last commit
    verifyHierarchy(d, branchContent(master));

    // -- Delete the backup branch
    VERIFYJOB(m_plugin->deleteBranch(dUrl, backupName));
}

void GitInitTest::testRebaseAbort()
{
    repoInit();
    initialCommit();
    createHierarchy(QDir(gitTest_BaseDir()), {
        {QStringLiteral("untracked/"), {}},
        {QStringLiteral("untracked/test"), QStringLiteral("test-untracked")},
    });
    auto root = getCommit(VcsRevision::createSpecialRevision(KDevelop::VcsRevision::Head));
    master.base = &root;

    buildRepo(testRepo);
    QStringList initialBranchList;
    for(const auto& b: runSynchronously(m_plugin->branches(dUrl)).toList()) {
        initialBranchList << b.toString();
    }

    // Test aborting
    QStringList revisedMessages {
        QStringLiteral("Revised message 3"),
        QStringLiteral("Revised message 2"),
        QStringLiteral("Revised message 1"),
    };
    GitPlugin::RebasePlan plan {
        {
            { master.commits[0].rev, revisedMessages[2] },
            { master.commits[1].rev, revisedMessages[1], /* .options = */ GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::PauseBeforeNextStep },
            { master.commits[2].rev, revisedMessages[0] },
        },
        master.baseRev(),
    };
    QString backupName {master.name+QStringLiteral("-old")};

    auto* rebaseJob = m_plugin->rebase(dPath, master.name, plan, backupName);
    QSignalSpy spyStepFinished(rebaseJob, &GitRebaseJob::stepFinished);
    QSignalSpy spyStepStarted(rebaseJob, &GitRebaseJob::stepStarted);
    QSignalSpy spyPaused(rebaseJob, &GitRebaseJob::stepPaused);
    connect(rebaseJob, &GitRebaseJob::stepPaused, this, [rebaseJob]{
        rebaseJob->abortRebase();
    });
    rebaseJob->exec();
    QVERIFY(spyPaused.count() == 1);
    QVERIFY(spyStepFinished.count() == 1);

    // -- Test that untracked stuff remains
    verifyHierarchy(d, untracked);

    // -- Test that master branch did not change
    verifyBranch(master);

    // -- Test that content did not change
    verifyHierarchy(d, branchContent(master));

    // -- Test cleanup (no backup branch, no stash, everything as before)
    QStringList branchList;
    for(const auto& b: runSynchronously(m_plugin->branches(dUrl)).toList()) {
        branchList << b.toString();
    }
    QString failMsg {
        "\n   expected: " + initialBranchList.join(QStringLiteral(", ")) +
        "\n   got: " + branchList.join(QStringLiteral(", ")) + QLatin1Char('\n')
    };
    QVERIFY2(initialBranchList == branchList, failMsg.toUtf8());
    QVERIFY(!branchList.contains(backupName));
    QVERIFY(branchList.contains(master.name));
    QVERIFY(branchList.contains(wip.name));
    QVERIFY(!m_plugin->hasStashes(d));
}

void GitInitTest::testRebaseMoveMultiCommits() {
    repoInit();
    initialCommit();

    createHierarchy(QDir(gitTest_BaseDir()), {
        {QStringLiteral("untracked/"), {}},
        {QStringLiteral("untracked/test"), QStringLiteral("test-untracked")},
    });
    auto root = getCommit(VcsRevision::createSpecialRevision(KDevelop::VcsRevision::Head));

    master.base = &root;

    buildRepo(testRepo);

    RebasePlanner planner(m_plugin);

    qDebug() << " -- switching to" << multimove.name;
    VERIFYJOB(m_plugin->switchBranch(dUrl, multimove.name));

    qDebug() << " -- reloading the history model";
    auto hm = m_plugin->historyModelFor(KDevelop::Path{dUrl});
    QSignalSpy spyReloaded(hm.get(), &VcsEventLogTreeModel::finishedLoading);
    hm->reloadRequest();
    hm->fetchMore(QModelIndex{});
    QVERIFY(spyReloaded.wait());


    // Test moving commits: 1, 2, 3, 4, 5 => 1, 3, 4, 5, 2
    auto plan = planner.moveDownPlan(dPath, {
        multimove.commits[2].rev,
        multimove.commits[3].rev,
        multimove.commits[4].rev
    });
    QString backupName {multimove.name+QStringLiteral("-old")};
    auto* moveJob = m_plugin->rebase(dPath, multimove.name, plan, backupName);
    QSignalSpy spyStepFinished(moveJob, &GitRebaseJob::stepFinished);
    QSignalSpy spyStepStarted(moveJob, &GitRebaseJob::stepStarted);
    VERIFYJOB(moveJob);

    // -- Test that untracked stuff remains
    verifyHierarchy(d, untracked);

    // -- Test state as of last commit
    verifyHierarchy(d, branchContent(multimove));

    // -- Delete the backup branch
    VERIFYJOB(m_plugin->deleteBranch(dUrl, backupName));
}

void GitInitTest::testRebaseSquashMultiCommits() {
    repoInit();
    initialCommit();

    createHierarchy(QDir(gitTest_BaseDir()), {
        {QStringLiteral("untracked/"), {}},
        {QStringLiteral("untracked/test"), QStringLiteral("test-untracked")},
    });
    auto root = getCommit(VcsRevision::createSpecialRevision(KDevelop::VcsRevision::Head));

    master.base = &root;

    buildRepo(testRepo);

    RebasePlanner planner(m_plugin);

    qDebug() << " -- switching to" << multimove.name;
    VERIFYJOB(m_plugin->switchBranch(dUrl, multimove.name));

    qDebug() << " -- reloading the history model";
    auto hm = m_plugin->historyModelFor(KDevelop::Path{dUrl});
    QSignalSpy spyReloaded(hm.get(), &VcsEventLogTreeModel::finishedLoading);
    hm->reloadRequest();
    hm->fetchMore(QModelIndex{});
    QVERIFY(spyReloaded.wait());


    // Test squashing commits: 3,4,5 with 1: 1, 2, 3, 4, 5 => 1', 2
    auto plan = planner.squashWithTargetPlan(dPath, multimove.commits[1].rev, {
        multimove.commits[1].rev,
        multimove.commits[2].rev,
        multimove.commits[3].rev,
        multimove.commits[4].rev
    }, RebasePlanner::SquashOption::UseFirstSummary);
    QString backupName {multimove.name+QStringLiteral("-old")};
    auto* moveJob = m_plugin->rebase(dPath, multimove.name, plan, backupName);
    QSignalSpy spyStepFinished(moveJob, &GitRebaseJob::stepFinished);
    QSignalSpy spyStepStarted(moveJob, &GitRebaseJob::stepStarted);
    VERIFYJOB(moveJob);

    // -- Test that untracked stuff remains
    verifyHierarchy(d, untracked);

    // -- Test state as of last commit
    verifyHierarchy(d, branchContent(multimove));

    // -- Delete the backup branch
    VERIFYJOB(m_plugin->deleteBranch(dUrl, backupName));
}

void GitInitTest::testRebaseSplitCommit() {
    repoInit();
    initialCommit();

    createHierarchy(QDir(gitTest_BaseDir()), {
        {QStringLiteral("untracked/"), {}},
        {QStringLiteral("untracked/test"), QStringLiteral("test-untracked")},
    });
    auto root = getCommit(VcsRevision::createSpecialRevision(KDevelop::VcsRevision::Head));

    master.base = &root;

    buildRepo(testRepo);

    RebasePlanner planner(m_plugin);

    qDebug() << " -- switching to" << multimove.name;
    VERIFYJOB(m_plugin->switchBranch(dUrl, multimove.name));

    qDebug() << " -- reloading the history model";
    auto hm = m_plugin->historyModelFor(KDevelop::Path{dUrl});
    QSignalSpy spyReloaded(hm.get(), &VcsEventLogTreeModel::finishedLoading);
    hm->reloadRequest();
    hm->fetchMore(QModelIndex{});
    QVERIFY(spyReloaded.wait());

    // Test splitting commit: 1, 2, 3, 4, 5 => 1, 2, 3, 4.1, 4.2, 5
    qDebug() << " -- computing diff for" << multimove.commits[3].rev.prettyValue();
    auto* diffJob = m_plugin->diff(dPath.toUrl(), VcsRevision::createSpecialRevision(VcsRevision::Previous), multimove.commits[3].rev);
    VERIFYJOB(diffJob);
    auto rev41Diff = diffJob->fetchResults().value<VcsDiff>();
    auto rev42Diff = rev41Diff.splitOff({QStringLiteral("dir/4-wip")});

    auto plan = planner.splitCommitPlan(dPath, multimove.commits[3].rev, {rev41Diff, rev42Diff});
    QString backupName {multimove.name+QStringLiteral("-old")};
    auto* splitJob = m_plugin->rebase(dPath, multimove.name, plan, backupName);
    QSignalSpy spyStepFinished(splitJob, &GitRebaseJob::stepFinished);
    QSignalSpy spyStepStarted(splitJob, &GitRebaseJob::stepStarted);
    VERIFYJOB(splitJob);

    // -- Test that untracked stuff remains
    verifyHierarchy(d, untracked);

    // -- Test state as of last commit
    verifyHierarchy(d, branchContent(multimove));

    // -- Delete the backup branch
    VERIFYJOB(m_plugin->deleteBranch(dUrl, backupName));

    // -- Verify that the split occured as expected
    auto commits = getAllAncestorCommits(VcsRevision::createSpecialRevision(VcsRevision::Head));
    auto rev41Ev = commits[MULTIMOVE_FIRST_COMMIT_INDEX + 3];
    auto rev42Ev = commits[MULTIMOVE_FIRST_COMMIT_INDEX + 4];

    QVERIFY(rev41Ev.items().count() == 1);
    QVERIFY(rev41Ev.items().at(0).repositoryLocation() == QStringLiteral("dir/3-wip"));
    QVERIFY(rev42Ev.items().count() == 1);
    QVERIFY(rev42Ev.items().at(0).repositoryLocation() == QStringLiteral("dir/4-wip"));
}

void GitInitTest::testRebaseSplitAndMoveIntoParent() {
    repoInit();
    initialCommit();

    createHierarchy(QDir(gitTest_BaseDir()), {
        {QStringLiteral("untracked/"), {}},
        {QStringLiteral("untracked/test"), QStringLiteral("test-untracked")},
    });
    auto root = getCommit(VcsRevision::createSpecialRevision(KDevelop::VcsRevision::Head));

    master.base = &root;

    buildRepo(testRepo);

    RebasePlanner planner(m_plugin);

    qDebug() << " -- switching to" << multimove.name;
    VERIFYJOB(m_plugin->switchBranch(dUrl, multimove.name));

    qDebug() << " -- reloading the history model";
    auto hm = m_plugin->historyModelFor(KDevelop::Path{dUrl});
    QSignalSpy spyReloaded(hm.get(), &VcsEventLogTreeModel::finishedLoading);
    hm->reloadRequest();
    hm->fetchMore(QModelIndex{});
    QVERIFY(spyReloaded.wait());

    // Test splitting commit: 4: 1, 2, 3, 4, 5 => 1, 2, 3+4.1, 4.2, 5
    qDebug() << " -- computing diff for" << multimove.commits[3].rev.prettyValue();
    auto* diffJob = m_plugin->diff(dPath.toUrl(), VcsRevision::createSpecialRevision(VcsRevision::Previous), multimove.commits[3].rev);
    VERIFYJOB(diffJob);
    auto rev41Diff = diffJob->fetchResults().value<VcsDiff>();
    auto rev42Diff = rev41Diff.splitOff({QStringLiteral("dir/4-wip")});

    auto plan = planner.splitAndMovePlan(dPath, multimove.commits[3].rev, multimove.commits[2].rev, RebasePlanner::RevisionReference::Self, {rev41Diff, rev42Diff});
    QString backupName {multimove.name+QStringLiteral("-old")};
    auto* splitJob = m_plugin->rebase(dPath, multimove.name, plan, backupName);
    QSignalSpy spyStepFinished(splitJob, &GitRebaseJob::stepFinished);
    QSignalSpy spyStepStarted(splitJob, &GitRebaseJob::stepStarted);
    VERIFYJOB(splitJob);

    // -- Test that untracked stuff remains
    verifyHierarchy(d, untracked);

    // -- Test state as of last commit
    verifyHierarchy(d, branchContent(multimove));

    // -- Delete the backup branch
    VERIFYJOB(m_plugin->deleteBranch(dUrl, backupName));

    // -- Verify that the split occured as expected
    auto commits = getAllAncestorCommits(VcsRevision::createSpecialRevision(VcsRevision::Head));
    auto rev341Ev = commits[MULTIMOVE_FIRST_COMMIT_INDEX + 2];
    auto rev42Ev = commits[MULTIMOVE_FIRST_COMMIT_INDEX + 4];



    QVERIFY(rev341Ev.items().count() == 2);
    QList<QString> rev341Locations = { rev341Ev.items().at(0).repositoryLocation(), rev341Ev.items().at(1).repositoryLocation() };
    QVERIFY(rev341Locations.contains(QStringLiteral("dir/1-wip")));
    QVERIFY(rev341Locations.contains(QStringLiteral("dir/3-wip")));
    QVERIFY(rev42Ev.items().count() == 1);
    QVERIFY(rev42Ev.items().at(0).repositoryLocation() == QStringLiteral("dir/4-wip"));
}

void GitInitTest::testRebaseSplitAndMoveBeforeParent() {
    repoInit();
    initialCommit();

    createHierarchy(QDir(gitTest_BaseDir()), {
        {QStringLiteral("untracked/"), {}},
        {QStringLiteral("untracked/test"), QStringLiteral("test-untracked")},
    });
    auto root = getCommit(VcsRevision::createSpecialRevision(KDevelop::VcsRevision::Head));

    master.base = &root;

    buildRepo(testRepo);

    RebasePlanner planner(m_plugin);

    qDebug() << " -- switching to" << multimove.name;
    VERIFYJOB(m_plugin->switchBranch(dUrl, multimove.name));

    qDebug() << " -- reloading the history model";
    auto hm = m_plugin->historyModelFor(KDevelop::Path{dUrl});
    QSignalSpy spyReloaded(hm.get(), &VcsEventLogTreeModel::finishedLoading);
    hm->reloadRequest();
    hm->fetchMore(QModelIndex{});
    QVERIFY(spyReloaded.wait());

    // Test splitting commit: 4: 1, 2, 3, 4, 5 => 1, 2, 4.2, 3, 4.1, 5
    qDebug() << " -- computing diff for" << multimove.commits[3].rev.prettyValue();
    auto* diffJob = m_plugin->diff(dPath.toUrl(), VcsRevision::createSpecialRevision(VcsRevision::Previous), multimove.commits[3].rev);
    VERIFYJOB(diffJob);
    auto rev41Diff = diffJob->fetchResults().value<VcsDiff>();
    auto rev42Diff = rev41Diff.splitOff({QStringLiteral("dir/4-wip")});

    // Note, rev41Diff can't come before commit 3, since it depends on it
    auto plan = planner.splitAndMovePlan(dPath, multimove.commits[3].rev, multimove.commits[2].rev, RebasePlanner::RevisionReference::Before, {rev42Diff, rev41Diff});
    QString backupName {multimove.name+QStringLiteral("-old")};
    auto* splitJob = m_plugin->rebase(dPath, multimove.name, plan, backupName);
    QSignalSpy spyStepFinished(splitJob, &GitRebaseJob::stepFinished);
    QSignalSpy spyStepStarted(splitJob, &GitRebaseJob::stepStarted);
    VERIFYJOB(splitJob);

    // -- Test that untracked stuff remains
    verifyHierarchy(d, untracked);

    // -- Test state as of last commit
    verifyHierarchy(d, branchContent(multimove));

    // -- Delete the backup branch
    VERIFYJOB(m_plugin->deleteBranch(dUrl, backupName));

    // -- Verify that the split occured as expected
    auto commits = getAllAncestorCommits(VcsRevision::createSpecialRevision(VcsRevision::Head));
    auto rev41Ev = commits[MULTIMOVE_FIRST_COMMIT_INDEX + 2];
    auto rev42Ev = commits[MULTIMOVE_FIRST_COMMIT_INDEX + 4];

    QVERIFY(rev41Ev.items().count() == 1);
    QVERIFY(rev41Ev.items().at(0).repositoryLocation() == QStringLiteral("dir/4-wip"));
    QVERIFY(rev42Ev.items().count() == 1);
    QVERIFY(rev42Ev.items().at(0).repositoryLocation() == QStringLiteral("dir/3-wip"));
}

void GitInitTest::testRebaseSplitAndMoveAfterChild() {
    repoInit();
    initialCommit();

    createHierarchy(QDir(gitTest_BaseDir()), {
        {QStringLiteral("untracked/"), {}},
        {QStringLiteral("untracked/test"), QStringLiteral("test-untracked")},
    });
    auto root = getCommit(VcsRevision::createSpecialRevision(KDevelop::VcsRevision::Head));

    master.base = &root;

    buildRepo(testRepo);

    RebasePlanner planner(m_plugin);

    qDebug() << " -- switching to" << multimove.name;
    VERIFYJOB(m_plugin->switchBranch(dUrl, multimove.name));

    qDebug() << " -- reloading the history model";
    auto hm = m_plugin->historyModelFor(KDevelop::Path{dUrl});
    QSignalSpy spyReloaded(hm.get(), &VcsEventLogTreeModel::finishedLoading);
    hm->reloadRequest();
    hm->fetchMore(QModelIndex{});
    QVERIFY(spyReloaded.wait());

    // Test splitting commit: 4: 1, 2, 3, 4, 5 => 1, 2, 3.2, 4., 3.1, 5
    qDebug() << " -- computing diff for" << multimove.commits[2].rev.prettyValue();
    auto* diffJob = m_plugin->diff(dPath.toUrl(), VcsRevision::createSpecialRevision(VcsRevision::Previous), multimove.commits[2].rev);
    VERIFYJOB(diffJob);
    auto rev31 = diffJob->fetchResults().value<VcsDiff>();
    auto rev32 = rev31.splitOff({QStringLiteral("dir/3-wip")});

    auto plan = planner.splitAndMovePlan(dPath, multimove.commits[2].rev, multimove.commits[3].rev, RebasePlanner::RevisionReference::After, { rev31, rev32 });
    QString backupName {multimove.name+QStringLiteral("-old")};
    auto* splitJob = m_plugin->rebase(dPath, multimove.name, plan, backupName);
    QSignalSpy spyStepFinished(splitJob, &GitRebaseJob::stepFinished);
    QSignalSpy spyStepStarted(splitJob, &GitRebaseJob::stepStarted);
    VERIFYJOB(splitJob);

    // -- Test that untracked stuff remains
    verifyHierarchy(d, untracked);

    // -- Test state as of last commit
    verifyHierarchy(d, branchContent(multimove));

    // -- Delete the backup branch
    VERIFYJOB(m_plugin->deleteBranch(dUrl, backupName));

    // -- Verify that the split occured as expected
    auto commits = getAllAncestorCommits(VcsRevision::createSpecialRevision(VcsRevision::Head));
    auto rev32Ev = commits[MULTIMOVE_FIRST_COMMIT_INDEX + 2];
    auto rev31Ev = commits[MULTIMOVE_FIRST_COMMIT_INDEX + 4];

    QVERIFY(rev31Ev.items().count() == 1);
    QVERIFY(rev31Ev.items().at(0).repositoryLocation() == QStringLiteral("dir/1-wip"));
    QVERIFY(rev32Ev.items().count() == 1);
    QVERIFY(rev32Ev.items().at(0).repositoryLocation() == QStringLiteral("dir/3-wip"));
}



void GitInitTest::testStash()
{
    repoInit();
    initialCommit();
    createHierarchy(QDir(gitTest_BaseDir()), {
        {QStringLiteral("untracked/"), {}},
        {QStringLiteral("untracked/test"), QStringLiteral("test-untracked")},
    });
    auto root = getCommit(VcsRevision::createSpecialRevision(KDevelop::VcsRevision::Head));
    master.base = &root;

    buildRepo(testRepo);

    VcsJob* job = nullptr;

    // Test rewriting messages
    QStringList revisedMessages {
        QStringLiteral("Revised message 3"),
        QStringLiteral("Revised message 2"),
        QStringLiteral("Revised message 1"),
    };
    GitPlugin::RebasePlan plan {
        {
            { master.commits[0].rev, revisedMessages[2] },
            { master.commits[1].rev, revisedMessages[1] },
            { master.commits[2].rev, revisedMessages[0] },
        },
        master.baseRev()
    };
    QString backupName {master.name+QStringLiteral("-old")};
    auto* rebaseJob = m_plugin->rebase(dPath, master.name, plan, backupName);
    QSignalSpy spyStepFinished(rebaseJob, &GitRebaseJob::stepFinished);
    QSignalSpy spyStepStarted(rebaseJob, &GitRebaseJob::stepStarted);
    VERIFYJOB(rebaseJob);

    // Verify that step finished & started signals were emitted
    QVERIFY(spyStepStarted.count() == 3);
    QVERIFY(spyStepFinished.count() == 3);

    // -- Test that the messages were rewritten
    job = m_plugin->log(dUrl, VcsRevision::createSpecialRevision(VcsRevision::Head), 3);
    const QList<QVariant> log1 = runSynchronously(job).toList();

    for(auto i=0; i < revisedMessages.length(); ++i) {
        QString failMsg {"Message "+QString::number(i)+" does not match: expected '" +revisedMessages[i]+ "' got '"+log1[i].value<VcsEvent>().message()+"'"};
        QVERIFY2(revisedMessages[i]== log1[i].value<VcsEvent>().message(), failMsg.toUtf8());
    }

    // -- Test that untracked stuff remains
    verifyHierarchy(d, untracked);

    // -- Test state as of last commit
    verifyHierarchy(d, branchContent(master));

    // -- Test that the backup branch contains old messages & commits
    auto mname = master.name;
    master.name = backupName;
    verifyBranch(master);
    master.name = mname;

    // -- Test that the wip branch is unchanged
    verifyBranch(wip);

    // -- Delete the backup branch
    VERIFYJOB(m_plugin->deleteBranch(dUrl, backupName));

    // Test squashing commits
    GitPlugin::RebasePlan plan2 {
        {
            { /*.patch = */ master.commits[0].rev, /*.message = */ {}, /* .options = */ GitPlugin::RebaseStepOption::NoCommit | GitPlugin::RebaseStepOption::NoPause },
            { /*.patch = */ master.commits[1].rev, /*.message = */ QStringLiteral("Squashed"), /* .options = GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause */ },
            { /*.patch = */ master.commits[2].rev, /*.message = */ revisedMessages[0], /* .options = GitPlugin::RebaseStepOption::Commit | GitPlugin::RebaseStepOption::NoPause */ },
        },
        master.baseRev()
    };
    rebaseJob = m_plugin->rebase(dPath, master.name, plan2, backupName);
    QSignalSpy spyStepFinishedII(rebaseJob, &GitRebaseJob::stepFinished);
    QSignalSpy spyStepStartedII(rebaseJob, &GitRebaseJob::stepStarted);
    VERIFYJOB(rebaseJob);

    // Verify that step finished & started signals were emitted
    QVERIFY(spyStepStarted.count() == 3);
    QVERIFY(spyStepFinished.count() == 3);

    // -- Test that the messages were rewritten
    job = m_plugin->log(dUrl, VcsRevision::createSpecialRevision(VcsRevision::Head), 2);
    const QList<QVariant> log2 = runSynchronously(job).toList();

    QVERIFY(log2[0].value<VcsEvent>().message() == revisedMessages[0]);
    QVERIFY(log2[1].value<VcsEvent>().message() == QStringLiteral("Squashed"));


    // -- Test that untracked stuff remains
    verifyHierarchy(d, untracked);

    // -- Test state as of last commit
    verifyHierarchy(d, branchContent(master));

    // -- Delete the backup branch
    VERIFYJOB(m_plugin->deleteBranch(dUrl, backupName));
}

QTEST_MAIN(GitInitTest)

// #include "gittest.moc"
#include "moc_test_git.cpp"
