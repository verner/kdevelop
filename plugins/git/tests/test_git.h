/*
    This file was partly taken from KDevelop's cvs plugin
    SPDX-FileCopyrightText: 2007 Robert Gruber <rgruber@users.sourceforge.net>

    Adapted for Git
    SPDX-FileCopyrightText: 2008 Evgeniy Ivanov <powerfox@kde.ru>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#ifndef KDEVPLATFORM_PLUGIN_GIT_INIT_H
#define KDEVPLATFORM_PLUGIN_GIT_INIT_H

#include <QObject>
#include <QDir>
#include <QUrl>

#include <vcs/vcsrevision.h>
#include <util/path.h>

class GitPlugin;

namespace KDevelop
{
    class TestCore;
    class VcsRevision;
    class VcsEvent;
}

struct FSItem {
    QString name;
    QString content;
};

typedef std::vector<FSItem> FS;

QStringList files(const FS& fs);

struct Commit {
    QString message;
    FS content;
    std::vector<Commit> children;
    QString branchName;
    KDevelop::VcsRevision rev;
};

struct Branch {
    QString name;
    Commit* base = nullptr;
    std::vector<Commit> commits;
    KDevelop::VcsRevision baseRev() const;
};

typedef std::vector<std::reference_wrapper<Branch>> Repo;

class GitInitTest: public QObject
{
    Q_OBJECT

private:
    /**
     * Creates and initializes a new empty git repository in the
     * temporary folder (see gitTest_BaseDir() for the exact location)
     */
    void repoInit();

    /**
     * Creates an initial commit with 'empty commit' as a commit message
     * and an empty file 'empty' in the root of the testing repository.
     */
    void initialCommit();

    /**
     * Creates the filesystem hierarchy @p commit.content and then commits
     * all the files with the message @p commit.message. It also updates
     * the revision @p commit.rev to point to the new commit.
     */
    void createCommit(Commit& commit);

    /**
     * Returns a @ref Commit structure filled with information
     * about revision @p rev.
     */
    Commit getCommit(const KDevelop::VcsRevision& rev);

    /**
     * Returns a vector of @ref KDevelop::VcsEvent instances filled with information
     * about revision @p rev and its ancestors ordered from oldest (first)
     * to newest (i.e. @p rev, last).
     */
    std::vector<KDevelop::VcsEvent> getAllAncestorCommits(const KDevelop::VcsRevision& start);

    void buildBranch(Branch& branch);
    void buildRepo(Repo& repo);
    void addFiles();
    void commitFiles();
    void verifyBranch(const Branch& branch);

    void saveToStash();
    void restoreFromStash();

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void init();
    void cleanup();

    void testInit();
    void testReadAndSetConfigOption();
    void testAdd();
    void testCommit();
    void testBranching();
    void testBranch(const QString &branchName);
    void testMerge();
    void revHistory();
    void testAnnotation();
    void testRemoveEmptyFolder();
    void testRemoveEmptyFolderInFolder();
    void testRemoveUnindexedFile();
    void testRemoveFolderContainingUnversionedFiles();
    void testDiff();

    void testRebase();
    void testRebaseMoveMultiCommits();
    void testRebaseSquashMultiCommits();
    void testRebaseSplitCommit();
    void testRebaseSplitAndMoveIntoParent();
    void testRebaseSplitAndMoveBeforeParent();
    void testRebaseSplitAndMoveAfterChild();
    void testRebaseAbort();

    // TODO: We should test worktrees (in particular, rebasing in a worktree).
    void testStash();

private:
    QDir d;
    QUrl dUrl;
    KDevelop::Path dPath;

    GitPlugin* m_plugin;
    void removeTempDirs();
};

#endif
