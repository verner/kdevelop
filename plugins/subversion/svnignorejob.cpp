/*
    SPDX-FileCopyrightText: 2021 Jonathan L. Verner <jonathan.verner@matfyz.cz>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#include "svnignorejob.h"

#include <QMutexLocker>

#include <ThreadWeaver/ThreadWeaver>
#include <ThreadWeaver/Weaver>
#include <QStandardItemModel>
#include <KLocalizedString>

#include "kdevsvncpp/client.hpp"
#include "kdevsvncpp/path.hpp"
#include "kdevsvncpp/targets.hpp"
#include "kdevsvncpp/property.hpp"

#include <iostream>

#include "svninternaljobbase.h"
#include "kdevsvnplugin.h"


#include <QDir>
#include <QUrl>





SvnInternalIgnoreJob::SvnInternalIgnoreJob( SvnJobBase* parent )
    : SvnInternalJobBase( parent )
{
}


void SvnInternalIgnoreJob::setPatterns( const QStringList& patterns )
{
    QMutexLocker l( &m_mutex );
    m_patterns = patterns;
}

void SvnInternalIgnoreJob::setRepository(const QUrl& repository)
{
    QMutexLocker l( &m_mutex );
    m_repository = repository;
}


QStringList SvnInternalIgnoreJob::patterns() const
{
    QMutexLocker l( &m_mutex );
    return m_patterns;
}

QUrl SvnInternalIgnoreJob::repository() const
{
    QMutexLocker l( &m_mutex );
    return m_repository;
}


void SvnInternalIgnoreJob::run(ThreadWeaver::JobPointer /*self*/, ThreadWeaver::Thread* /*thread*/)
{
    initBeforeRun();
    svn::Property prop(m_ctxt);
    QDir repo(repository().toLocalFile());
    try
    {
        for(const auto& pattern: patterns()) {
            QByteArray path = repo.filePath(pattern).toUtf8();
            prop.set("svn:ignore", path.data());
        }
    }catch( const svn::ClientException& ce )
    {
        qCDebug(PLUGIN_SVN) << "Couldn't commit:" << QString::fromUtf8( ce.message() );
        setErrorMessage( QString::fromUtf8( ce.message() ) );
        m_success = false;
    }
}

SvnIgnoreJob::SvnIgnoreJob( KDevSvnPlugin* parent )
    : SvnJobBaseImpl(parent, KDevelop::OutputJob::Verbose )
{
    setType( KDevelop::VcsJob::Commit );
    setObjectName(i18n("Subversion Ignore"));
}

QVariant SvnIgnoreJob::fetchResults()
{
    return QVariant();
}

void SvnIgnoreJob::start()
{
    setTitle(QStringLiteral("ignore"));
    setBehaviours( KDevelop::IOutputView::AllowUserClose | KDevelop::IOutputView::AutoScroll );
    startOutput();

    auto *m = qobject_cast<QStandardItemModel*>(model());
    m->setColumnCount(1);
    m->appendRow(new QStandardItem(i18n("Ignoring...")));

    if( m_job->patterns().isEmpty() ) {
        internalJobFailed();
        setErrorText( i18n( "No patterns to ignore" ) );
        m->appendRow(new QStandardItem(errorText()));
    } else {
        startInternalJob();
    }
}

void SvnIgnoreJob::setPatterns(const QStringList& patterns)
{
    if( status() == KDevelop::VcsJob::JobNotStarted )
        m_job->setPatterns( patterns );
}


void SvnIgnoreJob::setRepository( const QUrl& repository )
{
    if( status() == KDevelop::VcsJob::JobNotStarted )
        m_job->setRepository( repository );
}
