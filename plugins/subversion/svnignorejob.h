/*
    SPDX-FileCopyrightText: 2021 Jonathan L. Verner <jonathan.verner@matfyz.cz>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef KDEVPLATFORM_PLUGIN_SVNIGNOREJOB_H
#define KDEVPLATFORM_PLUGIN_SVNIGNOREJOB_H

#include "svnjobbase.h"
#include "svninternaljobbase.h"

#include <QUrl>
#include <ThreadWeaver/Job>


class SvnInternalIgnoreJob : public SvnInternalJobBase
{
    Q_OBJECT
public:
    explicit SvnInternalIgnoreJob( SvnJobBase* parent = nullptr );
    void setPatterns( const QStringList& );
    void setRepository( const QUrl& repository);

    QStringList patterns() const;
    QUrl repository() const;

protected:
    void run(ThreadWeaver::JobPointer job, ThreadWeaver::Thread* thread) override;
private:
    QStringList m_patterns;
    QUrl m_repository;
};



class SvnIgnoreJob : public SvnJobBaseImpl<SvnInternalIgnoreJob>
{
    Q_OBJECT
public:
    explicit SvnIgnoreJob( KDevSvnPlugin* parent );
    QVariant fetchResults() override;
    void start() override;
    void setPatterns( const QStringList& patterns );
    void setRepository(const QUrl& repository);
};



#endif


